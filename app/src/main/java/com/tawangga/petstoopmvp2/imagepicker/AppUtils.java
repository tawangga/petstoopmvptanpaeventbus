package com.tawangga.petstoopmvp2.imagepicker;

import android.app.Activity;
import android.net.Uri;
import android.os.Environment;

import java.io.File;
import java.io.IOException;

public class AppUtils {

    static File getWorkingDirectory(Activity activity) {
        File directory = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return directory;
    }

    static FileUri createImageFile(Activity activity, String prefix) {
        FileUri fileUri = new FileUri();

        File image = null;
        try {
            image = File.createTempFile(prefix + System.currentTimeMillis(), ".jpg",
                    getWorkingDirectory(activity));
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (image != null) {
            fileUri.setFile(image);
            fileUri.setImageUrl(Uri.parse("file:" + image.getAbsolutePath()));
        }
        return fileUri;
    }
}
