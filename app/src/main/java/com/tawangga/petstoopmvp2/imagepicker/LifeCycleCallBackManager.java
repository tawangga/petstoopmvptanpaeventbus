package com.tawangga.petstoopmvp2.imagepicker;

import android.content.Intent;

import androidx.annotation.NonNull;

public interface LifeCycleCallBackManager {

    void onActivityResult(int requestCode, int resultCode, Intent data);

    void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                    @NonNull int[] grantResults);

    void onDestroy();

    void onStartActivity();

}