package com.tawangga.petstoopmvp2.Modul.Shop;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Model.ProductModel;
import com.tawangga.petstoopmvp2.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    private List<ProductModel.DATABean.ItemsBean> data = new ArrayList<>();
    private Context context;
    private AdapterView.OnItemClickListener onItemClickListener;
    Fragment fragment;

    public ProductAdapter(Context context, Fragment fragment) {

        this.context = context;
        this.fragment = fragment;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_produk, parent, false);

        return new ViewHolder(itemView);
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.position = position;
        final long identity = System.currentTimeMillis();
        holder.identity = identity;

        ProductModel.DATABean.ItemsBean selectedData = data.get(position);

        holder.txtProdukCode.setText("Code: " + selectedData.getProductCode());
        holder.txtProdukName.setText(selectedData.getProductName());
        holder.txtProdukPrice.setText(Utils.toRupiah(selectedData.getProductPrice() + ""));
        Picasso.get()
                .load(selectedData.getProductImage())
                .error(context.getResources().getDrawable(R.drawable.royal))
                .into(holder.ivProduk);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setDatas(List<ProductModel.DATABean.ItemsBean> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public List<ProductModel.DATABean.ItemsBean> getData() {
        return data;
    }

    public void clearData() {
        this.data.clear();
        notifyDataSetChanged();
    }

    public void addData(List<ProductModel.DATABean.ItemsBean> items) {
        data.addAll(items);
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        int position;
        long identity;
        @BindView(R.id.iv_produk)
        ImageView ivProduk;
        @BindView(R.id.txt_produk_name)
        TextView txtProdukName;
        @BindView(R.id.txt_produk_code)
        TextView txtProdukCode;
        @BindView(R.id.txt_produk_price)
        TextView txtProdukPrice;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(null, v, position, 0);
            }
        }
    }
}
