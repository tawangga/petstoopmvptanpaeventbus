package com.tawangga.petstoopmvp2.Modul.Invoice;

import com.tawangga.petstoopmvp2.EventBus.OnApiResultSuccess;
import com.tawangga.petstoopmvp2.EventBus.RefreshOrder;
import com.tawangga.petstoopmvp2.Model.DataInvoice;
import com.tawangga.petstoopmvp2.Model.ProcessInvoice;
import com.tawangga.petstoopmvp2.Modul.BaseView;

import java.util.List;

public interface InvoiceFragmentView extends BaseView {
    void onGetOrder(RefreshOrder data);

    void onCancelInvoice(OnApiResultSuccess data);

    void onAddOrderToInvoice(DataInvoice data);

    void onRemoveOrder(DataInvoice data);

    void onRemoveOrderDetail(DataInvoice data);

    void onProcessInvoice(ProcessInvoice.DATABean data);

    void onGetInvoiceDP(List<DataInvoice> data);
}
