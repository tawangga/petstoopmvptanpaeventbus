package com.tawangga.petstoopmvp2.Modul.SplashScreen;

import com.tawangga.petstoopmvp2.ModelNew.GetCurrentVersion;
import com.tawangga.petstoopmvp2.Modul.BaseView;

public interface SplashScreenView extends BaseView {
    void onSuccessGetCurrentVersion(GetCurrentVersion.DATABean dataBean);
}
