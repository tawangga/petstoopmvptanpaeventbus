package com.tawangga.petstoopmvp2.Modul.TreatmentFragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Model.DataInvoice;
import com.tawangga.petstoopmvp2.ModelNew.GetTreatment;
import com.tawangga.petstoopmvp2.Modul.Main.MainActivity;
import com.tawangga.petstoopmvp2.R;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class TreatmentOrderAdapter extends RecyclerView.Adapter<TreatmentOrderAdapter.ViewHolder> {

    private List<GetTreatment.DATABean.ItemsBeanX> data;
    private Context context;
    public ViewHolder holder;

    public TreatmentOrderAdapter(Context context) {
        this.context = context;
        data = new ArrayList<>();
    }

    @NonNull
    @Override
    public TreatmentOrderAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_treatment, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final TreatmentOrderAdapter.ViewHolder holder, final int position) {
        GetTreatment.DATABean.ItemsBeanX selectedData = data.get(position);
        this.holder = holder;
        if (position % 2 == 1) {
            holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.brown_light_2));
        } else {
            holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.white));
        }


        holder.txtReservationNumber.setTextColor(context.getResources().getColor(R.color.brown_light));
        holder.llBackground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedData.isShowingDetail()) {
                    selectedData.setShowingDetail(false);
                    setShowingDetail(holder, selectedData);

                } else {
                    selectedData.setCount(selectedData.getCount() + 1);
                    selectedData.setShowingDetail(true);
                    setShowingDetail(holder, selectedData);

                }
            }
        });

        setShowingDetail(holder, selectedData);

//        if (selectedData.getBoardingId() != 0) {
//            GetReservation.DATABean.ItemsBean item = new GetReservation.DATABean.ItemsBean();
//            item.setServiceType("3");
//            item.setCartDetailServiceId(0);
//            item.setServiceName("Boarding");
//            selectedData.getItems().add(item);
//        }

        holder.txtReservationNumber.setText(selectedData.getOrderCode());
        holder.txtOwnerPetName.setText(selectedData.getPet().getCustomerName() + " - " + selectedData.getPet().getPetName());
        holder.txtServices.setText(selectedData.getItems().size() + " SERVICES");

        TreatmentDetailAdapter adapter = new TreatmentDetailAdapter(context);
        adapter.setFromOrder(true);
        adapter.setDatas(selectedData.getItems(), selectedData);
        holder.rvDetail.setLayoutManager(new LinearLayoutManager(context));
        holder.rvDetail.setAdapter(adapter);

        holder.btnAddtorecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedData.getHasAnamnesis() == 0) {
                    addToRecipe(selectedData);
                } else {
                    if (selectedData.getTakeAction() == 0) {
                        Toast.makeText(context, "Cannot convert to Invoice due to Doctor hasn't taken action to this order", Toast.LENGTH_SHORT).show();
                    } else {
                        addToRecipe(selectedData);
                    }
                }
            }
        });

    }

    private void addToRecipe(GetTreatment.DATABean.ItemsBeanX selectedData) {
//        ((MainActivity) context).setIsLoading(true);
        ProgressDialog dialog = Utils.showProgressDialog(context, false, "Loading...");
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("cart_service_id", selectedData.getCartServiceId() + "");
        RequestBody formBody = formBuilder.build();

        Connector.newInstance(context).addToInvoice(formBody, new Connector.ApiCallback<DataInvoice>() {
            @Override
            public String getKey() {
                return null;
            }

            @Override
            public void success(DataInvoice s, String messages) {
//                ((MainActivity) context).setIsLoading(false);
                dialog.dismiss();

                ((MainActivity) Objects.requireNonNull(context)).bundle.putParcelable("dataInvoice", s);
                ((MainActivity) Objects.requireNonNull(context)).position = 3;
                ((MainActivity) Objects.requireNonNull(context)).changePosition();


            }

            @Override
            public void onFailure(Throwable t) {
//                ((MainActivity) context).setIsLoading(false);
                dialog.dismiss();
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();

            }
        });
    }

    public void setShowingDetail(ViewHolder holder, GetTreatment.DATABean.ItemsBeanX data) {
        boolean isShowingDetail = data.isShowingDetail();

        if (!isShowingDetail) {
            holder.rlButton.setVisibility(View.GONE);
            holder.btnAddtorecipe.setVisibility(View.GONE);
            holder.btnCompleteorder.setVisibility(View.GONE);
            holder.btnMakeOrder.setVisibility(View.GONE);
            holder.rvDetail.setVisibility(View.GONE);
        } else {

            holder.rlButton.setVisibility(View.VISIBLE);
            holder.btnCompleteorder.setVisibility(View.GONE);
            holder.btnAddtorecipe.setVisibility(View.GONE);
            holder.rvDetail.setVisibility(View.VISIBLE);
            holder.btnAddtorecipe.setVisibility(data.isInvoice() ? View.GONE : View.VISIBLE);
        }

        holder.llBackground.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                holder.llBackground.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                holder.rowBG.getLayoutParams().height = holder.llBackground.getHeight();
                holder.rowBG.requestLayout();

            }
        });

        holder.delete_task.measure(0, 0);
        holder.edit_task.getLayoutParams().width = holder.delete_task.getMeasuredWidth();
        holder.edit_task.requestLayout();


    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setDatas(List<GetTreatment.DATABean.ItemsBeanX> data) {
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    public List<GetTreatment.DATABean.ItemsBeanX> getData() {
        return data;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_reservation_number)
        TextView txtReservationNumber;
        @BindView(R.id.txt_owner_pet_name)
        TextView txtOwnerPetName;
        @BindView(R.id.txt_services)
        TextView txtServices;
        @BindView(R.id.rv_detail)
        RecyclerView rvDetail;
        @BindView(R.id.btn_completeorder)
        Button btnCompleteorder;
        @BindView(R.id.btn_addtorecipe)
        LinearLayout btnAddtorecipe;
        @BindView(R.id.rl_button)
        RelativeLayout rlButton;
        @BindView(R.id.ll_background)
        LinearLayout llBackground;
        @BindView(R.id.btn_make_order)
        LinearLayout btnMakeOrder;
        @BindView(R.id.rowBG)
        LinearLayout rowBG;
        @BindView(R.id.edit_task)
        RelativeLayout edit_task;
        @BindView(R.id.delete_task)
        RelativeLayout delete_task;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
