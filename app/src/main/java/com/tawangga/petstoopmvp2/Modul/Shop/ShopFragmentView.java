package com.tawangga.petstoopmvp2.Modul.Shop;

import com.tawangga.petstoopmvp2.EventBus.CleaningCart;
import com.tawangga.petstoopmvp2.EventBus.OnCreateCart;
import com.tawangga.petstoopmvp2.Model.DataInvoice;
import com.tawangga.petstoopmvp2.Model.ProductCategories;
import com.tawangga.petstoopmvp2.Model.ProductModel;
import com.tawangga.petstoopmvp2.Model.PromoModel;
import com.tawangga.petstoopmvp2.ModelNew.GetDoctorSchedule;
import com.tawangga.petstoopmvp2.Modul.BaseView;

public interface ShopFragmentView extends BaseView {
    void onGetProduct(ProductModel.DATABean dataBeans);

    void onCreateCart(OnCreateCart dataBeans);

    void onGetProductCategories(ProductCategories.DATABean dataBean);

    void onGetPromo(PromoModel.DATABean dataBean);

    void onGetAllDoctor(GetDoctorSchedule.DATABean dataBeans);

    void onCleanCart(CleaningCart dataBeans);

    void onCreateInvoiceProduct(DataInvoice dataInvoice);
}
