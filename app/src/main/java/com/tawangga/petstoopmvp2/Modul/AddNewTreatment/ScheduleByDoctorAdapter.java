package com.tawangga.petstoopmvp2.Modul.AddNewTreatment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.Helper.GlobalVariable;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.ModelNew.GetDoctorSchedule;
import com.tawangga.petstoopmvp2.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ScheduleByDoctorAdapter extends RecyclerView.Adapter<ScheduleByDoctorAdapter.ViewHolder> {

    private List<GetDoctorSchedule.DATABean.ItemsBean> data;
    private Context context;

    public ScheduleByDoctorAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_bydoctor, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (position % 2 == 1) {
            holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.gray_light));
        } else {
            holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

        GetDoctorSchedule.DATABean.ItemsBean selectedData = data.get(position);
        holder.txt_doctor_name.setText(selectedData.getName());

        if (selectedData.getDoctor().getWorkingdays().toLowerCase().contains(Utils.getInstance().getDateName()[0].toLowerCase())) {
            holder.txtSunday.setText(Utils.changeDateFormat(selectedData.getDoctor().getSunTimeOperationalStart(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H) + "-" + Utils.changeDateFormat(selectedData.getDoctor().getSunTimeOperationalEnd(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));
        }
        if (selectedData.getDoctor().getWorkingdays().toLowerCase().contains(Utils.getInstance().getDateName()[1].toLowerCase())) {
            holder.txtMonday.setText(Utils.changeDateFormat(selectedData.getDoctor().getMonTimeOperationalStart(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H) + "-" + Utils.changeDateFormat(selectedData.getDoctor().getMonTimeOperationalEnd(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));
        }
        if (selectedData.getDoctor().getWorkingdays().toLowerCase().contains(Utils.getInstance().getDateName()[2].toLowerCase())) {
            holder.txtTuesday.setText(Utils.changeDateFormat(selectedData.getDoctor().getTueTimeOperationalStart(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H) + "-" + Utils.changeDateFormat(selectedData.getDoctor().getTueTimeOperationalEnd(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));
        }
        if (selectedData.getDoctor().getWorkingdays().toLowerCase().contains(Utils.getInstance().getDateName()[3].toLowerCase())) {
            holder.txtWednesday.setText(Utils.changeDateFormat(selectedData.getDoctor().getWedTimeOperationalStart(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H) + "-" + Utils.changeDateFormat(selectedData.getDoctor().getWedTimeOperationalEnd(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));
        }
        if (selectedData.getDoctor().getWorkingdays().toLowerCase().contains(Utils.getInstance().getDateName()[4].toLowerCase())) {
            holder.txtThursday.setText(Utils.changeDateFormat(selectedData.getDoctor().getThuTimeOperationalStart(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H) + "-" + Utils.changeDateFormat(selectedData.getDoctor().getThuTimeOperationalEnd(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));
        }
        if (selectedData.getDoctor().getWorkingdays().toLowerCase().contains(Utils.getInstance().getDateName()[5].toLowerCase())) {
            holder.txtFriday.setText(Utils.changeDateFormat(selectedData.getDoctor().getFriTimeOperationalStart(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H) + "-" + Utils.changeDateFormat(selectedData.getDoctor().getFriTimeOperationalEnd(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));
        }
        if (selectedData.getDoctor().getWorkingdays().toLowerCase().contains(Utils.getInstance().getDateName()[6].toLowerCase())) {
            holder.txtSaturday.setText(Utils.changeDateFormat(selectedData.getDoctor().getSatTimeOperationalStart(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H) + "-" + Utils.changeDateFormat(selectedData.getDoctor().getSatTimeOperationalEnd(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));
        }


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setDatas(List<GetDoctorSchedule.DATABean.ItemsBean> data) {
        this.data = data;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_doctor_name)
        TextView txt_doctor_name;
        @BindView(R.id.txt_sunday)
        TextView txtSunday;
        @BindView(R.id.txt_monday)
        TextView txtMonday;
        @BindView(R.id.txt_tuesday)
        TextView txtTuesday;
        @BindView(R.id.txt_wednesday)
        TextView txtWednesday;
        @BindView(R.id.txt_thursday)
        TextView txtThursday;
        @BindView(R.id.txt_friday)
        TextView txtFriday;
        @BindView(R.id.txt_saturday)
        TextView txtSaturday;
        @BindView(R.id.ll_background)
        LinearLayout llBackground;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
