package com.tawangga.petstoopmvp2.Modul.TreatmentFragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.Helper.GlobalVariable;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.ModelNew.GetTreatment;
import com.tawangga.petstoopmvp2.R;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class TreatmentDetailAdapter extends RecyclerView.Adapter<TreatmentDetailAdapter.ViewHolder> {
    private Context context;
    private boolean isFromOrder = false;
    private List<GetTreatment.DATABean.ItemsBeanX.ItemsBean> reservationItems;
    private GetTreatment.DATABean.ItemsBeanX reservationData;

    public TreatmentDetailAdapter(Context context) {
        this.context = context;
        reservationItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_treatment_reservation, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GetTreatment.DATABean.ItemsBeanX.ItemsBean reservationItem = reservationItems.get(position);


        holder.txtPayment.setText(reservationItem.isInvoice() ? "Paid" : "Unpaid");
        if (reservationItem.getServiceType().equals("1")) {
            holder.txtTime.setText(Utils.changeDateFormat(reservationData.getDoctorSchedule().getScheduleTime(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));
            holder.txtService.setText(reservationItem.getServiceName() + " - " + reservationData.getDoctorSchedule().getName());
        }
        if (reservationItem.getServiceType().equals("2")) {
            holder.txtTime.setText(Utils.changeDateFormat(reservationData.getGroomingLocationSchedule().getScheduleTime(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));
            holder.txtService.setText(reservationItem.getServiceName() + " - " + reservationData.getGroomingLocationSchedule().getLocationName());
        }
        if (reservationItem.getServiceType().equals("3")) {
            holder.txtTime.setTextColor(context.getResources().getColor(R.color.transparent));
            holder.txtService.setText("Boarding - " + reservationItem.getServiceName());
        }

        if (isFromOrder) {
            holder.rlProgress.setVisibility(View.VISIBLE);
            if (reservationItem.getOrderStatus() != null) {
                holder.txtProgress.setText(Utils.getOrderStatus()[Integer.parseInt(reservationItem.getOrderStatus())]);
            }

            holder.txtProgress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    if (reservationItem.getca() != 0) {
                    showDialogChangeOrderStatus(holder, reservationItem);
//                    }
                }
            });
        }
    }

    private void showDialogChangeOrderStatus(ViewHolder holder, GetTreatment.DATABean.ItemsBeanX.ItemsBean reservationItem) {
        if (reservationItem.getServiceType().equals("1")) {
            Toast.makeText(context, "Only doctor can change this status", Toast.LENGTH_SHORT).show();
        }
        if (reservationItem.getServiceType().equals("1") || reservationItem.getOrderStatus().equals("3")) {
            return;
        }
        AlertDialog dialog = new AlertDialog.Builder(context).create();
        View v = View.inflate(context, R.layout.dialog_change_order_status, null);
        LinearLayout btn_onqueue = v.findViewById(R.id.btn_onqueue);
        LinearLayout btn_onprogress = v.findViewById(R.id.btn_onprogress);
        LinearLayout btn_done = v.findViewById(R.id.btn_done);
        btn_onqueue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeOrderStatus("1", reservationItem, dialog);

            }
        });

        btn_onprogress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeOrderStatus("2", reservationItem, dialog);
            }
        });
        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeOrderStatus("3", reservationItem, dialog);
            }
        });
        dialog.setView(v);
        dialog.show();
    }

    private void changeOrderStatus(String orderStatus, GetTreatment.DATABean.ItemsBeanX.ItemsBean reservationItem, AlertDialog dialog) {
//        ((MainActivity) context).setIsLoading(true);
        ProgressDialog loadingDialog = Utils.showProgressDialog(context, false, "Loading...");
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("cart_service_detail_id", reservationItem.getCartServiceDetailId() + "");
        formBuilder.add("order_status", orderStatus);
        RequestBody formBody = formBuilder.build();

        Connector.newInstance(context).changeOrderStatus(formBody, new Connector.ApiCallback<String>() {
            @Override
            public String getKey() {
                return null;
            }

            @Override
            public void success(String s, String messages) {
                reservationItem.setOrderStatus(orderStatus);
                notifyDataSetChanged();
//                ((MainActivity) context).setIsLoading(false);
                loadingDialog.dismiss();
                dialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
//                ((MainActivity) context).setIsLoading(false);
                loadingDialog.dismiss();
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();

            }
        });

    }

    @Override
    public int getItemCount() {
        return reservationItems.size();
    }

    public void setFromOrder(boolean isFromOrder) {
        this.isFromOrder = isFromOrder;
    }

    public void setDatas(List<GetTreatment.DATABean.ItemsBeanX.ItemsBean> reservationItems, GetTreatment.DATABean.ItemsBeanX reservationData) {

        this.reservationItems = reservationItems;
        this.reservationData = reservationData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_time)
        TextView txtTime;
        @BindView(R.id.txt_service)
        TextView txtService;
        @BindView(R.id.txt_payment)
        TextView txtPayment;
        @BindView(R.id.txt_progress)
        TextView txtProgress;
        @BindView(R.id.rl_progress)
        LinearLayout rlProgress;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
