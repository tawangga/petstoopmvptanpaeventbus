package com.tawangga.petstoopmvp2.Modul.About;

import com.tawangga.petstoopmvp2.ModelNew.GetAppVersion;
import com.tawangga.petstoopmvp2.Modul.BaseView;

public interface AboutAppView extends BaseView {
    void onSuccessAboutApp(GetAppVersion.DATABean data);
}
