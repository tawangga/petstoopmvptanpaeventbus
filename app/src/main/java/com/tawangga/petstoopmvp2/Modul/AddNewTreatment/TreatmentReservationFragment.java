package com.tawangga.petstoopmvp2.Modul.AddNewTreatment;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.tawangga.petstoopmvp2.Dialog.DialogDoctorSchedule.DialogDoctorSchedule;
import com.tawangga.petstoopmvp2.Dialog.DialogSearchService.DialogSearchService;
import com.tawangga.petstoopmvp2.EventBus.OnCancelTreatmentService;
import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.EventBus.OnSuccessReservTreatment;
import com.tawangga.petstoopmvp2.Helper.GlobalVariable;
import com.tawangga.petstoopmvp2.Helper.TreatmentHelper;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.ModelNew.CreateCart;
import com.tawangga.petstoopmvp2.ModelNew.GetBoarding;
import com.tawangga.petstoopmvp2.ModelNew.GetDoctor;
import com.tawangga.petstoopmvp2.ModelNew.GetDoctorSchedule;
import com.tawangga.petstoopmvp2.ModelNew.GetGroomingLocation;
import com.tawangga.petstoopmvp2.Modul.Main.MainActivity;
import com.tawangga.petstoopmvp2.R;
import com.tawangga.petstoopmvp2.ServerSide.Connector;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class TreatmentReservationFragment extends Fragment implements AddNewTreatmentView {
    private static final String TAG = TreatmentReservationFragment.class.getSimpleName();
    @BindView(R.id.iv_pic_pet)
    CircleImageView ivPicPet;
    @BindView(R.id.pb_loading_iv_pet)
    ProgressBar pbLoadingIvPet;
    @BindView(R.id.txt_owner_name)
    TextView txtOwnerName;
    @BindView(R.id.txt_pet_name)
    TextView txtPetName;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.btn_dec_date)
    FrameLayout btnDecDate;
    @BindView(R.id.txt_search_date)
    TextView txtSearchDate;
    @BindView(R.id.btn_inc_date)
    FrameLayout btnIncDate;
    @BindView(R.id.btn_view_doctor_schedule)
    TextView btnViewDoctorSchedule;
    @BindView(R.id.iv_left_btnschedule)
    ImageView ivLeftBtnschedule;
    @BindView(R.id.btn_view_schedule)
    LinearLayout btnViewSchedule;
    @BindView(R.id.rv_list)
    RecyclerView rvList;
    @BindView(R.id.pb_loading)
    AVLoadingIndicatorView pbLoading;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.ll_datetime)
    LinearLayout llDatetime;
    @BindView(R.id.tv_txt_customer)
    TextView tvTxtCustomer;
    @BindView(R.id.tv_customer_name)
    TextView tvCustomerName;
    @BindView(R.id.tv_search_service)
    TextView tvSearchService;
    @BindView(R.id.btn_search_service)
    RelativeLayout btnSearchService;
    @BindView(R.id.rv_services)
    RecyclerView rvServices;
    @BindView(R.id.txt_estimated_prie)
    TextView txtEstimatedPrie;
    @BindView(R.id.ll_services)
    LinearLayout llServices;
    @BindView(R.id.tv_from)
    TextView tvFrom;
    @BindView(R.id.tv_until)
    TextView tvUntil;
    @BindView(R.id.ll_from_until)
    LinearLayout llFromUntil;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    @BindView(R.id.btn_order)
    Button btnOrder;
    private int cart_service_id;
    private String tanggal;
    private ProgressDialog dialog;
    private String[] tabItems;
    private TreatmentOrderPresenter presenter;
    private SelectTreatmentNewAdapter adapter;
    private LinearLayoutManager layoutManager;
    int selectedTabPosition = 0;
    private CreateCart.DATABean cartData;
    private SelectedServiceAdapter adapterServices;
    private int page = 0, lastPage;

    public static Fragment newInstance(int cart_service_id, String tanggal) {
        TreatmentReservationFragment fragment = new TreatmentReservationFragment();
        fragment.cart_service_id = cart_service_id;
        fragment.tanggal = tanggal;

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_treatment_reservation, container, false);
        ButterKnife.bind(this, v);

        tabItems = getResources().getStringArray(R.array.arrayService);
        presenter = new TreatmentOrderPresenter(getActivity(), TAG, this);
        if (tanggal.equals("")) {
            txtSearchDate.setText(Utils.changeDateFormat(Utils.getTodayDate(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
        } else {
            txtSearchDate.setText(Utils.changeDateFormat(tanggal, GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
        }
        setAdapter();
        createCart();

        return v;
    }

    private void createCart() {
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("pet_id", TreatmentHelper.getInstance().getSelectedPet().getPetId() + "");
        formBuilder.add("cart_service_id", cart_service_id + "");
        RequestBody formBody = formBuilder.build();
        dialog = Utils.showProgressDialog(getActivity(), false, "Loading...");
        presenter.createCartTreatment(formBody);

    }


    private void setData() {
        String date = "";
        if (tanggal.equals("")) {
            date = Utils.getTodayDate();
        } else {
            date = tanggal;
        }
        if (cartData.getDoctorSchedule().getStaffId() != 0) {
            date = cartData.getDoctorSchedule().getScheduleDate();
        } else if (cartData.getGroomingLocationSchedule().getGroomingLocationId() != 0) {
            date = cartData.getGroomingLocationSchedule().getScheduleDate();
        } else if (cartData.getBoardingSchedule().getCageId() != 0) {
            date = cartData.getBoardingSchedule().getScheduleDateFrom();
        }
        txtSearchDate.setText(Utils.changeDateFormat(date, GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));


        txtOwnerName.setText(cartData.getPet().getCustomerName());
        txtPetName.setText(cartData.getPet().getPetName());
        ivPicPet.setVisibility(View.GONE);
        pbLoadingIvPet.setVisibility(View.VISIBLE);
        Picasso.get().load(cartData.getPet().getPetPhoto())
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .tag(TAG)
                .into(ivPicPet, new Callback() {
                    @Override
                    public void onSuccess() {
                        pbLoadingIvPet.setVisibility(View.GONE);
                        ivPicPet.setVisibility(View.VISIBLE);

                    }

                    @Override
                    public void onError(Exception e) {
                        ivPicPet.setImageDrawable(getResources().getDrawable(R.drawable.no_image));
                        pbLoadingIvPet.setVisibility(View.GONE);
                        ivPicPet.setVisibility(View.VISIBLE);
                        e.printStackTrace();
                    }
                });

    }

    @Override
    public void onFailGettingData(OnFailGettingData data) {
        Toast.makeText(getActivity(), data.getThrowable().getMessage(), Toast.LENGTH_SHORT).show();
        Utils.dismissProgressDialog(dialog);
        if (data.getApi() == OnFailGettingData.GET_DOCTOR
                || data.getApi() == OnFailGettingData.GET_BOARDING
                || data.getApi() == OnFailGettingData.GET_GROOMING_LOCATION) {
            setVisibleLoading(false);
        }
        if (data.getApi() == OnFailGettingData.CREATE_CART_TREATMENT) {
            try {
                getActivity().onBackPressed();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        if (data.getApi() == OnFailGettingData.RESERV_TREATMENT) {
            ((MainActivity) getActivity()).toSelectCustomer(false, "treatment", "");
            dialog.dismiss();

        }
        data.getThrowable().printStackTrace();

    }


    private void setVisibleLoading(boolean isVisible) {
        if (isVisible) {
            pbLoading.setVisibility(View.VISIBLE);
        } else {
            pbLoading.setVisibility(View.GONE);
        }
    }

    private void setAdapter() {
        layoutManager = new LinearLayoutManager(getActivity());
        rvList.setLayoutManager(layoutManager);
        adapter = new SelectTreatmentNewAdapter(getActivity());
        rvList.setAdapter(adapter);
        rvList.addOnScrollListener(recyclerViewOnScrollListener);
        adapter.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (selectedTabPosition == 0) {
                    GetDoctor.DATABean.ItemsBean itemsBean = adapter.getDataDoctor().get(position);
                    if (!itemsBean.isIsAvailable()) {
                        return;
                    }
                    if ((itemsBean.getStaffId() == cartData.getDoctorSchedule().getStaffId()) &&
                            (itemsBean.getTime().equals(cartData.getDoctorSchedule().getScheduleTime()))) {
                        cartData.getDoctorSchedule().setStaffId(0);
                        cartData.getDoctorSchedule().setScheduleTime("");
                        cartData.getDoctorSchedule().setScheduleDate("");
                        cartData.getDoctorSchedule().setName("");
                        adapter.notifyDataSetChanged();

                    } else {
                        cartData.getDoctorSchedule().setStaffId(itemsBean.getStaffId());
                        cartData.getDoctorSchedule().setScheduleTime(itemsBean.getTime());
                        cartData.getDoctorSchedule().setScheduleDate(Utils.changeDateFormat(txtSearchDate.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME, GlobalVariable.WEB_DATE_FORMAT));
                        cartData.getDoctorSchedule().setName(itemsBean.getName());
                        adapter.notifyDataSetChanged();
                    }

                    tvDate.setText(Utils.changeDateFormat(cartData.getDoctorSchedule().getScheduleDate(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
                    tvTime.setText(Utils.changeDateFormat(cartData.getDoctorSchedule().getScheduleTime(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));
                    tvCustomerName.setText(cartData.getDoctorSchedule().getName());

                } else if (selectedTabPosition == 1) {
                    GetGroomingLocation.DATABean.ItemsBean itemsBean = adapter.getDataGroomingLocation().get(position);
                    if (!itemsBean.isIsAvailable()) {
                        return;
                    }
                    if ((itemsBean.getGroomingLocationId() == cartData.getGroomingLocationSchedule().getGroomingLocationId()) &&
                            (itemsBean.getTime().equals(cartData.getGroomingLocationSchedule().getScheduleTime()))) {
                        cartData.getGroomingLocationSchedule().setGroomingLocationId(0);
                        cartData.getGroomingLocationSchedule().setScheduleTime("");
                        cartData.getGroomingLocationSchedule().setScheduleDate("");
                        cartData.getGroomingLocationSchedule().setLocationName("");
                        adapter.notifyDataSetChanged();

                    } else {
                        cartData.getGroomingLocationSchedule().setGroomingLocationId(itemsBean.getGroomingLocationId());
                        cartData.getGroomingLocationSchedule().setScheduleTime(itemsBean.getTime());
                        cartData.getGroomingLocationSchedule().setScheduleDate(Utils.changeDateFormat(txtSearchDate.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME, GlobalVariable.WEB_DATE_FORMAT));
                        cartData.getGroomingLocationSchedule().setLocationName(itemsBean.getName());
                        adapter.notifyDataSetChanged();
                    }
                    tvDate.setText(Utils.changeDateFormat(cartData.getGroomingLocationSchedule().getScheduleDate(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
                    tvTime.setText(Utils.changeDateFormat(cartData.getGroomingLocationSchedule().getScheduleTime(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));
                    tvCustomerName.setText(cartData.getGroomingLocationSchedule().getLocationName());

                } else {
                    GetBoarding.DATABean.ItemsBean itemsBean = adapter.getDataBoarding().get(position);
                    if (!itemsBean.isIsAvailable()) {
                        return;
                    }
                    if (itemsBean.getCageId() == cartData.getBoardingSchedule().getCageId()) {
                        cartData.getBoardingSchedule().setCageId(0);
                        cartData.getBoardingSchedule().setScheduleDateFrom("");
                        cartData.getBoardingSchedule().setCageName("");
                        cartData.getBoardingSchedule().setScheduleDateUntil("");
                        cartData.getBoardingSchedule().setCagePrice("");
                        adapter.notifyDataSetChanged();

                    } else {
                        cartData.getBoardingSchedule().setCageId(itemsBean.getCageId());
                        cartData.getBoardingSchedule().setScheduleDateFrom(Utils.changeDateFormat(txtSearchDate.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME, GlobalVariable.WEB_DATE_FORMAT));
                        cartData.getBoardingSchedule().setCageName(itemsBean.getCageName());
                        cartData.getBoardingSchedule().setScheduleDateUntil(Utils.changeDateFormat(tvUntil.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME, GlobalVariable.WEB_DATE_FORMAT));
                        cartData.getBoardingSchedule().setCagePrice(itemsBean.getCagePrice());
                        adapter.notifyDataSetChanged();
                    }

                    tvCustomerName.setText(cartData.getBoardingSchedule().getCageName());
                    if (Utils.changeDateFormat(cartData.getBoardingSchedule().getScheduleDateFrom(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME).equals("")) {
                        tvFrom.setText(txtSearchDate.getText().toString().trim());
                    } else {
                        tvFrom.setText(Utils.changeDateFormat(cartData.getBoardingSchedule().getScheduleDateFrom(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
                    }
                    tvUntil.setText(Utils.changeDateFormat(cartData.getBoardingSchedule().getScheduleDateUntil(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));

                }
            }
        });

        for (int i = 0; i < tabItems.length; i++) {
            tabLayout.addTab(tabLayout.newTab().setText(tabItems[i]));
        }
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
//                if (selectedTabPosition == -1) {
//                    return;
//                }
//                adapter.setSelectedPosition(-1);
                selectedTabPosition = tab.getPosition();
                updatePagination(0, 1);
                selectTab();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        rvServices.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapterServices = new SelectedServiceAdapter(getActivity(), txtEstimatedPrie);
        rvServices.setAdapter(adapterServices);

    }


    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            if (page != lastPage) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= adapter.getItemCount()) {
                    if (selectedTabPosition == 0) {
                        getDoctor();
                    } else if (selectedTabPosition == 1) {
                        getGroomingLocation();
                    } else if (selectedTabPosition == 2) {
                        getBoarding();
                    }
                }
            }
        }
    };

    private void updatePagination(int page, int lastPage) {
        this.page = page;
        this.lastPage = lastPage;
    }

    private void selectTab() {
        if (selectedTabPosition == 0) {
            tvTxtCustomer.setText("DOCTOR");
            adapter.clearAllData();
            adapterServices.setDatas(cartData.getServiceMedical());
            tvDate.setText(Utils.changeDateFormat(cartData.getDoctorSchedule().getScheduleDate(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
            tvTime.setText(Utils.changeDateFormat(cartData.getDoctorSchedule().getScheduleTime(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));
            tvCustomerName.setText(cartData.getDoctorSchedule().getName());

            getDoctor();

            llServices.setVisibility(View.VISIBLE);
            llFromUntil.setVisibility(View.GONE);
            llDatetime.setVisibility(View.VISIBLE);
            btnViewSchedule.setVisibility(View.VISIBLE);

        } else if (selectedTabPosition == 1) {
            tvTxtCustomer.setText("LOCATION");
            adapter.clearAllData();
            adapterServices.setDatas(cartData.getServiceGrooming());
            tvDate.setText(Utils.changeDateFormat(cartData.getGroomingLocationSchedule().getScheduleDate(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
            tvTime.setText(Utils.changeDateFormat(cartData.getGroomingLocationSchedule().getScheduleTime(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));
            tvCustomerName.setText(cartData.getGroomingLocationSchedule().getLocationName());
            getGroomingLocation();

            llServices.setVisibility(View.VISIBLE);
            btnViewSchedule.setVisibility(View.GONE);
            llFromUntil.setVisibility(View.GONE);
            llDatetime.setVisibility(View.VISIBLE);

        } else if (selectedTabPosition == 2) {
            tvTxtCustomer.setText("HOUSE");
            llServices.setVisibility(View.GONE);
            llFromUntil.setVisibility(View.VISIBLE);
            llDatetime.setVisibility(View.GONE);
            btnViewSchedule.setVisibility(View.GONE);

            adapter.clearAllData();
            tvCustomerName.setText(cartData.getBoardingSchedule().getCageName());
            if (Utils.changeDateFormat(cartData.getBoardingSchedule().getScheduleDateFrom(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME).equals("")) {
                tvFrom.setText(txtSearchDate.getText().toString().trim());
            } else {
                tvFrom.setText(Utils.changeDateFormat(cartData.getBoardingSchedule().getScheduleDateFrom(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
            }


            tvUntil.setText(Utils.changeDateFormat(cartData.getBoardingSchedule().getScheduleDateUntil(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));

            getBoarding();

        }
    }

    @OnClick(R.id.btn_search_service)
    public void onClickSearchService() {
        DialogSearchService dialog = DialogSearchService.newInstance(adapterServices, tabLayout.getSelectedTabPosition() == 0, cartData);
        dialog.show(getChildFragmentManager(), "");

    }


    @OnClick(R.id.txt_search_date)
    public void onSearchDate() {
        Calendar calendar = Calendar.getInstance();

        Date date = Utils.convertStringToDate(txtSearchDate.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME);
        Calendar selectedDate = Calendar.getInstance();
        selectedDate.setTime(date);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.datepicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String dd = year + "-" + (month + 1) + "-" + dayOfMonth;
                if (Utils.convertStringToDate(Utils.getTodayDate()).after(Utils.convertStringToDate(dd))) {
                    Toast.makeText(getActivity(), "Invalid date", Toast.LENGTH_SHORT).show();
                    return;
                }
                txtSearchDate.setText(Utils.changeDateFormat(dd, GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));

                resetCartData();

                adapter.clearAllData();
                updatePagination(0, 1);

                if (selectedTabPosition == 0) {
                    getDoctor();
                    tvDate.setText(Utils.changeDateFormat(cartData.getDoctorSchedule().getScheduleDate(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
                    tvTime.setText(Utils.changeDateFormat(cartData.getDoctorSchedule().getScheduleTime(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));
                    tvCustomerName.setText(cartData.getDoctorSchedule().getName());
                } else if (selectedTabPosition == 1) {
                    getGroomingLocation();
                    tvDate.setText(Utils.changeDateFormat(cartData.getGroomingLocationSchedule().getScheduleDate(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
                    tvTime.setText(Utils.changeDateFormat(cartData.getGroomingLocationSchedule().getScheduleTime(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));
                    tvCustomerName.setText(cartData.getGroomingLocationSchedule().getLocationName());
                } else if (selectedTabPosition == 2) {
                    getBoarding();
                    tvCustomerName.setText(cartData.getBoardingSchedule().getCageName());
                    if (Utils.changeDateFormat(cartData.getBoardingSchedule().getScheduleDateFrom(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME).equals("")) {
                        tvFrom.setText(txtSearchDate.getText().toString().trim());
                    } else {
                        tvFrom.setText(Utils.changeDateFormat(cartData.getBoardingSchedule().getScheduleDateFrom(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
                    }
                    tvUntil.setText(Utils.changeDateFormat(cartData.getBoardingSchedule().getScheduleDateUntil(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
                }
            }
        }, selectedDate.get(Calendar.YEAR), selectedDate.get(Calendar.MONTH), selectedDate.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        datePickerDialog.show();
    }

    private void resetCartData() {
        cartData.getDoctorSchedule().setStaffId(0);
        cartData.getDoctorSchedule().setScheduleTime("");
        cartData.getDoctorSchedule().setScheduleDate("");
        cartData.getDoctorSchedule().setName("");
        cartData.getGroomingLocationSchedule().setGroomingLocationId(0);
        cartData.getGroomingLocationSchedule().setScheduleTime("");
        cartData.getGroomingLocationSchedule().setScheduleDate("");
        cartData.getGroomingLocationSchedule().setLocationName("");
        cartData.getBoardingSchedule().setCageId(0);
        cartData.getBoardingSchedule().setScheduleDateFrom("");
        cartData.getBoardingSchedule().setCageName("");
        cartData.getBoardingSchedule().setScheduleDateUntil("");
        cartData.getBoardingSchedule().setCagePrice("");
    }

    @OnClick(R.id.tv_until)
    public void onUntilClicked() {
        Date minDate = Utils.convertStringToDate(tvFrom.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(minDate);

        Date date = Utils.convertStringToDate(tvFrom.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME);
        if (!TextUtils.isEmpty(tvUntil.getText().toString().trim())) {
            date = Utils.convertStringToDate(tvUntil.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME);
        }
        Calendar selectedDate = Calendar.getInstance();
        selectedDate.setTime(date);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.datepicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String dd = year + "-" + (month + 1) + "-" + dayOfMonth;
//                selectedUntil = dd;
                tvUntil.setText(Utils.changeDateFormat(dd, GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
                cartData.getBoardingSchedule().setScheduleDateUntil(dd);
                updatePagination(0, 1);
                adapter.clearAllData();

                getBoarding();
            }
        }, selectedDate.get(Calendar.YEAR), selectedDate.get(Calendar.MONTH), selectedDate.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        datePickerDialog.show();

    }

    @OnClick(R.id.btn_inc_date)
    public void plusDate() {
        Date newDate = Utils.addDayToDate(Utils.convertStringToDate(txtSearchDate.getText().toString(), GlobalVariable.DATE_FORMAT_MONTH_NAME), 1);

        txtSearchDate.setText(Utils.changeDateFormat(Utils.convertDateToString(newDate, GlobalVariable.WEB_DATE_FORMAT), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));

        resetCartData();

        adapter.clearAllData();
        updatePagination(0, 1);

        if (selectedTabPosition == 0) {
            tvDate.setText(Utils.changeDateFormat(cartData.getDoctorSchedule().getScheduleDate(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
            tvTime.setText(Utils.changeDateFormat(cartData.getDoctorSchedule().getScheduleTime(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));
            tvCustomerName.setText(cartData.getDoctorSchedule().getName());
            getDoctor();
        } else if (selectedTabPosition == 1) {
            tvDate.setText(Utils.changeDateFormat(cartData.getGroomingLocationSchedule().getScheduleDate(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
            tvTime.setText(Utils.changeDateFormat(cartData.getGroomingLocationSchedule().getScheduleTime(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));
            tvCustomerName.setText(cartData.getGroomingLocationSchedule().getLocationName());
            getGroomingLocation();
        } else if (selectedTabPosition == 2) {
            tvCustomerName.setText(cartData.getBoardingSchedule().getCageName());
            if (Utils.changeDateFormat(cartData.getBoardingSchedule().getScheduleDateFrom(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME).equals("")) {
                tvFrom.setText(txtSearchDate.getText().toString().trim());
            } else {
                tvFrom.setText(Utils.changeDateFormat(cartData.getBoardingSchedule().getScheduleDateFrom(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
            }
            tvUntil.setText(Utils.changeDateFormat(cartData.getBoardingSchedule().getScheduleDateUntil(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
            getBoarding();
        }
    }

    @OnClick(R.id.btn_dec_date)
    public void onMinDateOrder() {
        Date newDate = Utils.addDayToDate(Utils.convertStringToDate(txtSearchDate.getText().toString(), GlobalVariable.DATE_FORMAT_MONTH_NAME), -1);
        if (Utils.convertStringToDate(Utils.getTodayDate()).after(newDate)) {
            Toast.makeText(getActivity(), "Invalid date", Toast.LENGTH_SHORT).show();
            return;
        }
        txtSearchDate.setText(Utils.changeDateFormat(Utils.convertDateToString(newDate, GlobalVariable.WEB_DATE_FORMAT), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));

        resetCartData();

        adapter.clearAllData();
        updatePagination(0, 1);

        if (selectedTabPosition == 0) {
            tvDate.setText(Utils.changeDateFormat(cartData.getDoctorSchedule().getScheduleDate(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
            tvTime.setText(Utils.changeDateFormat(cartData.getDoctorSchedule().getScheduleTime(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));
            tvCustomerName.setText(cartData.getDoctorSchedule().getName());
            getDoctor();
        } else if (selectedTabPosition == 1) {
            tvDate.setText(Utils.changeDateFormat(cartData.getGroomingLocationSchedule().getScheduleDate(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
            tvTime.setText(Utils.changeDateFormat(cartData.getGroomingLocationSchedule().getScheduleTime(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));
            tvCustomerName.setText(cartData.getGroomingLocationSchedule().getLocationName());
            getGroomingLocation();
        } else if (selectedTabPosition == 2) {
            tvCustomerName.setText(cartData.getBoardingSchedule().getCageName());
            if (Utils.changeDateFormat(cartData.getBoardingSchedule().getScheduleDateFrom(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME).equals("")) {
                tvFrom.setText(txtSearchDate.getText().toString().trim());
            } else {
                tvFrom.setText(Utils.changeDateFormat(cartData.getBoardingSchedule().getScheduleDateFrom(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
            }
            tvUntil.setText(Utils.changeDateFormat(cartData.getBoardingSchedule().getScheduleDateUntil(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
            getBoarding();
        }
    }


    @OnClick(R.id.btn_cancel)
    void openDialogCancel() {
        final AlertDialog dialog = new AlertDialog.Builder(getActivity()).create();
        dialog.setTitle("Cancel Reservation");
        dialog.setMessage("Are you sure want to cancel this reservation?");
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteService(cartData.getCartServiceId(), dialog);
            }
        });
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    void deleteService(int cartServiceId, DialogInterface dialogMain) {

        dialog = Utils.showProgressDialog(getActivity(), false, "Loading...");
        presenter.deleteService(cartServiceId);

    }


    private void getDoctor() {
        if (page == lastPage)
            return;

        if (adapter.getDataDoctor().size() == 0) {
            setLoadingCenter();
        } else {
            setLoadingBottom();
        }
        setVisibleLoading(true);
        Map<String, String> params = new HashMap<>();
        params.put("limit", String.valueOf(GlobalVariable.LIMIT_ITEM_DOCTOR));
        params.put("page", String.valueOf(page + 1));
        params.put("search", "");
        params.put("date", getServiceDate());

        presenter.getDoctor(params);
    }


    private void getBoarding() {
        if (page == lastPage)
            return;

        if (adapter.getDataBoarding().size() == 0) {
            setLoadingCenter();
        } else {
            setLoadingBottom();
        }
        setVisibleLoading(true);
        Map<String, String> params = new HashMap<>();
        params.put("limit", String.valueOf(GlobalVariable.LIMIT_ITEM));
        params.put("page", String.valueOf(page + 1));
        params.put("search", "");
        params.put("date", Utils.changeDateFormat(tvFrom.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME, GlobalVariable.WEB_DATE_FORMAT));
        params.put("end_date", Utils.changeDateFormat(tvUntil.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME, GlobalVariable.WEB_DATE_FORMAT));
        presenter.getBoarding(params);
    }

    private void getGroomingLocation() {
        if (page == lastPage)
            return;

        if (adapter.getDataGroomingLocation().size() == 0) {
            setLoadingCenter();
        } else {
            setLoadingBottom();
        }

        setVisibleLoading(true);
        Map<String, String> params = new HashMap<>();
        params.put("limit", String.valueOf(GlobalVariable.LIMIT_ITEM_DOCTOR));
        params.put("page", String.valueOf(page + 1));
        params.put("search", "");
        params.put("date", getServiceDate());
        presenter.getGroomingLocation(params);

    }

    private String getServiceDate() {
        return Utils.changeDateFormat(txtSearchDate.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME, GlobalVariable.WEB_DATE_FORMAT);
    }

    private void setLoadingCenter() {
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.CENTER_IN_PARENT);
        pbLoading.setLayoutParams(lp);

    }

    private void setLoadingBottom() {
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
        pbLoading.setLayoutParams(lp);

    }

    @OnClick(R.id.btn_order)
    public void reservTreatment() {

        if (cartData.getServiceMedical().size() == 0 && cartData.getServiceGrooming().size() == 0 && cartData.getBoardingSchedule().getCageId() == 0) {
            Toast.makeText(getActivity(), "Tidak ada service yang dipilih", Toast.LENGTH_SHORT).show();
            return;
        }

        if (cartData.getDoctorSchedule().getStaffId() == 0 && cartData.getServiceMedical().size() > 0) {
            Toast.makeText(getActivity(), "Pilih dokter untuk medical service", Toast.LENGTH_SHORT).show();
            return;
        }
        if (cartData.getDoctorSchedule().getStaffId() != 0) {
            boolean hasKonsul = false;
            for (CreateCart.DATABean.ServiceBean med : cartData.getServiceMedical()) {
                if (med.getServiceId() == 1) {
                    hasKonsul = true;
                }
            }
            if (!hasKonsul) {
                Toast.makeText(getActivity(), "Tambahkan konsultasi dokter pada medical service", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        if (cartData.getGroomingLocationSchedule().getGroomingLocationId() == 0 && cartData.getServiceGrooming().size() > 0) {
            Toast.makeText(getActivity(), "Pilih grooming location untuk grooming service", Toast.LENGTH_SHORT).show();
            return;
        }

        if (cartData.getGroomingLocationSchedule().getGroomingLocationId() != 0 && cartData.getServiceGrooming().size() == 0) {
            Toast.makeText(getActivity(), "Tambahkan service pada grooming", Toast.LENGTH_SHORT).show();
            return;
        }

        if (cartData.getBoardingSchedule().getCageId() != 0 && cartData.getBoardingSchedule().getScheduleDateUntil().equals("")) {
            Toast.makeText(getActivity(), "Tentukan tanggal pengambilan hewan", Toast.LENGTH_SHORT).show();
            return;

        }


        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("cart_service_id", cartData.getCartServiceId() + "");
        formBuilder.add("pet_id", cartData.getPetId() + "");
        formBuilder.add("doctor_id", cartData.getDoctorSchedule().getStaffId() + "");
        formBuilder.add("doctor_schedule_date", cartData.getDoctorSchedule().getScheduleDate() + "");
        formBuilder.add("doctor_schedule_time", cartData.getDoctorSchedule().getScheduleTime() + "");

        formBuilder.add("grooming_location_id", cartData.getGroomingLocationSchedule().getGroomingLocationId() + "");
        formBuilder.add("grooming_location_date", cartData.getGroomingLocationSchedule().getScheduleDate() + "");
        formBuilder.add("grooming_location_time", cartData.getGroomingLocationSchedule().getScheduleTime() + "");

        formBuilder.add("cage_id", cartData.getBoardingSchedule().getCageId() + "");
        formBuilder.add("cage_schedule_date_from", cartData.getBoardingSchedule().getScheduleDateFrom() + "");
        formBuilder.add("cage_schedule_date_until", cartData.getBoardingSchedule().getScheduleDateUntil() + "");
        formBuilder.add("cage_price", cartData.getBoardingSchedule().getCagePrice() + "");
        formBuilder.add("cage_name", cartData.getBoardingSchedule().getCageName() + "");

        formBuilder.add("order_date", Utils.changeDateFormat(txtSearchDate.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME, GlobalVariable.WEB_DATE_FORMAT));
        formBuilder.add("cart_service_type", "1");
        RequestBody formBody = formBuilder.build();
        dialog = Utils.showProgressDialog(getActivity(), false, "Loading...");

        presenter.reservTreatment(formBody);
        Connector.newInstance(getActivity()).reservTreatment(formBody, new Connector.ApiCallback<CreateCart.DATABean>() {
            @Override
            public String getKey() {
                return TAG;
            }

            @Override
            public void success(CreateCart.DATABean dataBean, String messages) {
                ((MainActivity) getActivity()).setClearBackStack(true);
                ((MainActivity) getActivity()).toTreatment();
                dialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                t.printStackTrace();

            }
        });

    }


    @OnClick(R.id.btn_view_schedule)
    void onClickDoctorSchedule() {
        dialog = Utils.showProgressDialog(getActivity(), false, "Loading...");

        Map<String, String> params = new HashMap<>();
        params.put("limit", "999");
        params.put("page", "0");
        params.put("search", "");
        presenter.getAllDoctor(params);

    }


    private void showDoctorSchedule(List<GetDoctorSchedule.DATABean.ItemsBean> dataBeans) {
        DialogDoctorSchedule dialogDoctorSchedule = DialogDoctorSchedule.newInstance(dataBeans);
        dialogDoctorSchedule.show(getChildFragmentManager(), "");

    }


    @Override
    public void onGetDoctor(GetDoctor.DATABean data) {
        setVisibleLoading(false);
        updatePagination(data.getPage(), data.getLastPage());
        adapter.setDataDoctor(data.getItems(), cartData);
    }

    @Override
    public void onGetGroomingLocation(GetGroomingLocation.DATABean data) {
        setVisibleLoading(false);
        updatePagination(data.getPage(), data.getLastPage());
        adapter.setDataGroomingLocation(data.getItems(), cartData);

    }

    @Override
    public void onGetBoarding(GetBoarding.DATABean data) {
        setVisibleLoading(false);
        updatePagination(data.getPage(), data.getLastPage());
        adapter.setDataBoarding(data.getItems(), cartData, tvCustomerName);
    }

    @Override
    public void onRollbackService(OnCancelTreatmentService dataBean) {

    }

    @Override
    public void onDeleteService(OnCancelTreatmentService dataBean) {
        Utils.dismissProgressDialog(dialog);
        ((MainActivity) getActivity()).setClearBackStack(true);
        ((MainActivity) getActivity()).toTreatment();

    }

    @Override
    public void onCreateCartTreatment(CreateCart.DATABean dataBean) {
        dialog.dismiss();
        cartData = dataBean;
        setData();
        selectTab();
    }

    @Override
    public void onReservTreatment(OnSuccessReservTreatment dataBean) {
        ((MainActivity) getActivity()).setClearBackStack(true);
        ((MainActivity) getActivity()).toTreatment();
        dialog.dismiss();
    }

    @Override
    public void onGetAllDoctor(GetDoctorSchedule.DATABean dataBean) {
        dialog.dismiss();
        showDoctorSchedule(dataBean.getItems());
    }
}
