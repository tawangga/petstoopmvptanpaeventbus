package com.tawangga.petstoopmvp2.Modul.SplashScreen;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.ModelNew.GetCurrentVersion;
import com.tawangga.petstoopmvp2.Modul.LoginCustomer.SignInCustomerActivity;
import com.tawangga.petstoopmvp2.R;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

public class SplashScreenActivity extends AppCompatActivity implements SplashScreenView {


    private static final String TAG = SplashScreenActivity.class.getSimpleName();
    SplashScreenPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen_activity);

        presenter = new SplashScreenPresenter(this, TAG, this);
        presenter.getCurrentVersion();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Connector.newInstance(this).close(TAG);
    }

    @Override
    public void onSuccessGetCurrentVersion(GetCurrentVersion.DATABean dataBean) {
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            Log.e("Version Name", version);
            if (!version.equals(dataBean.getVersionName())) {
                showDialog();
            } else {
                startActivity(new Intent(this, SignInCustomerActivity.class));
                finish();
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    private void showDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Version Check");
        dialog.setMessage("You need to update application!");
        dialog.setCancelable(false);
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
                dialogInterface.dismiss();
            }
        });
        dialog.show();
    }


    @Override
    public void onFailGettingData(OnFailGettingData data) {
        data.getThrowable().printStackTrace();
    }
}
