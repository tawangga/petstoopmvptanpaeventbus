package com.tawangga.petstoopmvp2.Modul.About;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.Helper.GlobalVariable;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.ModelNew.GetAppVersion;
import com.tawangga.petstoopmvp2.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AboutAppFragment extends Fragment implements AboutAppView {

    private static final String TAG = AboutAppFragment.class.getSimpleName();

    @BindView(R.id.rv_app_version)
    RecyclerView rvAppVersion;
    Unbinder unbinder;
    AboutAppAdapter adapter;
    private AboutAppPresenter presenter;


    public AboutAppFragment() {
        // Required empty public constructor
    }

    public static AboutAppFragment newInstance() {
        AboutAppFragment fragment = new AboutAppFragment();
        return fragment;
    }

    ProgressDialog dialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_about_app, container, false);
        unbinder = ButterKnife.bind(this, v);
        presenter = new AboutAppPresenter(getActivity(), TAG, this);
        initView();
        return v;
    }

    private void initView() {
        rvAppVersion.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new AboutAppAdapter(getActivity());
        adapter.setDatas(new ArrayList<>());
        rvAppVersion.setAdapter(adapter);
        getAppVersion();
    }

    void getAppVersion() {
        dialog = Utils.showProgressDialog(getActivity(), false, "Loading...");
        Map<String, String> params = new HashMap<>();
        params.put("limit", String.valueOf(GlobalVariable.LIMIT_ITEM_MAX));
        params.put("page", "1");

        presenter.getAppVersion(params);
    }

    @Override
    public void onSuccessAboutApp(GetAppVersion.DATABean data) {
        adapter.setDatas(data.getItems());
        dialog.dismiss();

    }

    @Override
    public void onFailGettingData(OnFailGettingData data) {
        if (data.getApi() == OnFailGettingData.GET_APP_VERSION) {
            dialog.dismiss();
        }
        Toast.makeText(getActivity(), data.getThrowable().getMessage(), Toast.LENGTH_SHORT).show();
        data.getThrowable().printStackTrace();

    }
}