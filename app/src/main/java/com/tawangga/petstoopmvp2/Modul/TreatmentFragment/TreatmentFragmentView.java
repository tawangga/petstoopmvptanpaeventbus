package com.tawangga.petstoopmvp2.Modul.TreatmentFragment;

import com.tawangga.petstoopmvp2.EventBus.UpdateTreatmentData;
import com.tawangga.petstoopmvp2.Modul.BaseView;

public interface TreatmentFragmentView extends BaseView {
    void getReservasionOrder(UpdateTreatmentData data);
}
