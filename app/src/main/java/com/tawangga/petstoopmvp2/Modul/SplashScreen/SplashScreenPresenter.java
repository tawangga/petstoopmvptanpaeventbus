package com.tawangga.petstoopmvp2.Modul.SplashScreen;

import android.content.Context;

import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.ModelNew.GetCurrentVersion;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

public class SplashScreenPresenter {

    private String TAG;
    private SplashScreenView view;
    private Context context;

    public SplashScreenPresenter(Context context, String TAG, SplashScreenView view) {
        this.context = context;
        this.TAG = TAG;
        this.view = view;
    }

    public void getCurrentVersion() {
        Connector.newInstance(context).getVersion(new Connector.ApiCallback<GetCurrentVersion.DATABean>() {
            @Override
            public String getKey() {
                return TAG;
            }

            @Override
            public void success(GetCurrentVersion.DATABean dataBean, String messages) {
                view.onSuccessGetCurrentVersion(dataBean);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.GET_APP_VERSION, t));
            }
        });
    }


}
