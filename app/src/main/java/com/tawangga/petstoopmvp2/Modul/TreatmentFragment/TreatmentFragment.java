package com.tawangga.petstoopmvp2.Modul.TreatmentFragment;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.EventBus.RefreshTreatment;
import com.tawangga.petstoopmvp2.EventBus.UpdateTreatmentData;
import com.tawangga.petstoopmvp2.Helper.GlobalVariable;
import com.tawangga.petstoopmvp2.Helper.SharedPreference;
import com.tawangga.petstoopmvp2.Helper.TreatmentHelper;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Model.DetailCustomerModel;
import com.tawangga.petstoopmvp2.ModelNew.GetTreatment;
import com.tawangga.petstoopmvp2.Modul.Main.MainActivity;
import com.tawangga.petstoopmvp2.R;
import com.tawangga.petstoopmvp2.RecyclerSwipe.RecyclerTouchListener;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class TreatmentFragment extends Fragment implements TreatmentFragmentView {


    private static final String TAG = TreatmentFragment.class.getSimpleName();
    TreatmentPresenter presenter;
    @BindView(R.id.minDateReserv)
    FrameLayout minDateReserv;
    @BindView(R.id.txt_reservation_date)
    TextView txtReservationDate;
    @BindView(R.id.plusDateReserv)
    FrameLayout plusDateReserv;
    @BindView(R.id.btn_add_reservation)
    TextView btnAddReservation;
    @BindView(R.id.rv_reservation)
    RecyclerView rvReservation;
    @BindView(R.id.pb_loading_reservation)
    ProgressBar pbLoadingReservation;
    @BindView(R.id.txt_nothing_reservation)
    TextView txtNothingReservation;
    @BindView(R.id.minDateOrder)
    FrameLayout minDateOrder;
    @BindView(R.id.txt_order_date)
    TextView txtOrderDate;
    @BindView(R.id.plusDateOrder)
    FrameLayout plusDateOrder;
    @BindView(R.id.btn_add_order)
    TextView btnAddOrder;
    @BindView(R.id.txt_nothing_order)
    TextView txtNothingOrder;
    @BindView(R.id.pb_loading_order)
    ProgressBar pbLoadingOrder;
    @BindView(R.id.rv_order)
    RecyclerView rvOrder;
    @BindView(R.id.pageOrder)
    LinearLayout pageOrder;

    RecyclerTouchListener touchListener, touchListenerOrder;
    private TreatmentReservationAdapter adapterReservation;
    private TreatmentOrderAdapter adapterOrder;

    public TreatmentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_treatment, container, false);
        ButterKnife.bind(this, v);
        presenter = new TreatmentPresenter(getActivity(), TAG, this);

        initView();
        return v;
    }

    private void initView() {
        txtReservationDate.setText(Utils.changeDateFormat(Utils.getTodayDate(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
        txtOrderDate.setText(Utils.changeDateFormat(Utils.getTodayDate(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));

        adapterReservation = new TreatmentReservationAdapter(getActivity(), this);
        rvReservation.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvReservation.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        rvReservation.setAdapter(adapterReservation);

        touchListener = new RecyclerTouchListener(getActivity(), rvReservation);


        touchListener.setSwipeOptionViews(R.id.edit_task, R.id.delete_task)
                .setSwipeable(R.id.ll_background, R.id.rowBG, new RecyclerTouchListener.OnSwipeOptionsClickListener() {
                    @Override
                    public void onSwipeOptionClicked(int viewID, int position) {
                        switch (viewID) {
                            case R.id.delete_task:
                                final AlertDialog dialog = new AlertDialog.Builder(getActivity()).create();
                                dialog.setTitle("Delete Reservation");
                                dialog.setMessage("Are you sure want to delete reservation number " + adapterReservation.getData().get(position).getOrderCode());
                                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        presenter.deleteService(adapterReservation.getData().get(position).getCartServiceId(), true, dialog);
                                    }
                                });
                                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                dialog.show();
                                break;
                            case R.id.edit_task:
                                GetTreatment.DATABean.ItemsBeanX selectedData = adapterReservation.getData().get(position);
                                DetailCustomerModel.DATABean.PetsBean pet = new DetailCustomerModel.DATABean.PetsBean();
                                pet.setPetId(selectedData.getPet().getPetId());
                                pet.setPetName(selectedData.getPet().getPetName());
                                pet.setPetPhoto(selectedData.getPet().getPetPhoto());
                                TreatmentHelper.getInstance().setselectedPet(pet);

                                DetailCustomerModel.DATABean customer = new DetailCustomerModel.DATABean();
                                customer.setCustomerId(selectedData.getPet().getPetId());
                                customer.setCustomerName(selectedData.getPet().getCustomerName());
                                TreatmentHelper.getInstance().setSelectedCustomer(customer);

                                ((MainActivity) getActivity()).toTreatmentReserv(adapterReservation.getData().get(position).getCartServiceId(), "");
                                break;
                        }
                    }
                });


        adapterOrder = new TreatmentOrderAdapter(getActivity());
        rvOrder.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvOrder.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        rvOrder.setAdapter(adapterOrder);

        touchListenerOrder = new RecyclerTouchListener(getActivity(), rvOrder);

        touchListenerOrder.setSwipeOptionViews(R.id.edit_task, R.id.delete_task).setSwipeable(R.id.ll_background, R.id.rowBG, new RecyclerTouchListener.OnSwipeOptionsClickListener() {
            @Override
            public void onSwipeOptionClicked(int viewID, int position) {

                boolean is_invoice = false;
                for (GetTreatment.DATABean.ItemsBeanX.ItemsBean item : adapterOrder.getData().get(position).getItems()) {
                    if (item.isInvoice())
                        is_invoice = true;
                }
                if (is_invoice) {
                    Toast.makeText(getActivity(), "You can't edit or delete this treatment, because it was entered to payment.", Toast.LENGTH_SHORT).show();
                    return;
                }
                switch (viewID) {
                    case R.id.delete_task:
                        if (adapterOrder.getData().get(position).isInvoice()) {
                            Toast.makeText(getActivity(), "Tidak dapat menghapus order, sudah masuk invoice.", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        final AlertDialog dialog = new AlertDialog.Builder(getActivity()).create();
                        dialog.setTitle("Delete Order");
                        dialog.setMessage("Are you sure want to delete order number " + adapterOrder.getData().get(position).getOrderCode());
                        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                presenter.deleteService(adapterOrder.getData().get(position).getCartServiceId(), false, dialog);
                            }
                        });
                        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                        break;
                    case R.id.edit_task:
                        if (adapterOrder.getData().get(position).isInvoice()) {
                            Toast.makeText(getActivity(), "Tidak dapat mengedit order, sudah masuk invoice.", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        GetTreatment.DATABean.ItemsBeanX selectedData = adapterOrder.getData().get(position);
                        DetailCustomerModel.DATABean.PetsBean pet = new DetailCustomerModel.DATABean.PetsBean();
                        pet.setPetId(selectedData.getPet().getPetId());
                        pet.setPetName(selectedData.getPet().getPetName());
                        pet.setPetPhoto(selectedData.getPet().getPetPhoto());
                        TreatmentHelper.getInstance().setselectedPet(pet);

                        DetailCustomerModel.DATABean customer = new DetailCustomerModel.DATABean();
                        customer.setCustomerId(selectedData.getPet().getPetId());
                        customer.setCustomerName(selectedData.getPet().getCustomerName());
                        TreatmentHelper.getInstance().setSelectedCustomer(customer);

                        ((MainActivity) getActivity()).toTreatmentOrder(selectedData.getCartServiceId());
                        break;
                }
            }
        });


        if (new SharedPreference(getActivity()).isCustomer()){
            pageOrder.setVisibility(View.GONE);
        }

        getReservation("");
        getOrder("");

    }

    @Subscribe
    public void onRefreshTreatment(RefreshTreatment refresh) {
        getReservation("");
        getOrder("");
    }

    void getReservation(String date) {
        if (date.equals("")) {
            date = Utils.changeDateFormat(txtReservationDate.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME, GlobalVariable.WEB_DATE_FORMAT);
        }

        pbLoadingReservation.setVisibility(View.VISIBLE);
        txtNothingReservation.setVisibility(View.GONE);

        adapterReservation.setDatas(new ArrayList<>());

        presenter.getReservation(date);
    }

    void getOrder(String date) {
        if (date.equals("")) {
            date = Utils.changeDateFormat(txtOrderDate.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME, GlobalVariable.WEB_DATE_FORMAT);
        }

        pbLoadingOrder.setVisibility(View.VISIBLE);
        txtNothingOrder.setVisibility(View.GONE);

        adapterOrder.setDatas(new ArrayList<>());

        presenter.getOrder(date);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Connector.newInstance(getActivity()).close(TAG);
    }

    @Override
    public void onResume() {
        super.onResume();
        rvReservation.addOnItemTouchListener(touchListener);
        rvOrder.addOnItemTouchListener(touchListenerOrder);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @OnClick({R.id.btn_add_reservation, R.id.btn_add_order})
    public void onBtnAddClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_add_reservation:
                TreatmentHelper.getInstance().setOrder(false);
                ((MainActivity) getActivity()).toSelectCustomer(false, "treatment", Utils.changeDateFormat(txtReservationDate.getText().toString(), GlobalVariable.DATE_FORMAT_MONTH_NAME, GlobalVariable.WEB_DATE_FORMAT));
                break;
            case R.id.btn_add_order:
                TreatmentHelper.getInstance().setOrder(true);
                ((MainActivity) getActivity()).toSelectCustomer(false, "treatment", "");
                break;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @OnClick(R.id.txt_reservation_date)
    public void onReservDateClicked(View view) {

        Date date = Utils.convertStringToDate(txtReservationDate.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME);
        Calendar selectedDate = Calendar.getInstance();
        selectedDate.setTime(date);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.datepicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String dd = year + "-" + (month + 1) + "-" + dayOfMonth;
                txtReservationDate.setText(Utils.changeDateFormat(dd, GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
                getReservation(Utils.changeDateFormat(txtReservationDate.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME, GlobalVariable.WEB_DATE_FORMAT));
            }
        }, selectedDate.get(Calendar.YEAR), selectedDate.get(Calendar.MONTH), selectedDate.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.show();


    }

    @OnClick(R.id.txt_order_date)
    public void onOrderDateClick() {

        Date date = Utils.convertStringToDate(txtOrderDate.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME);
        Calendar selectedDate = Calendar.getInstance();
        selectedDate.setTime(date);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.datepicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String dd = year + "-" + (month + 1) + "-" + dayOfMonth;
                txtOrderDate.setText(Utils.changeDateFormat(dd, GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
                getOrder(Utils.changeDateFormat(txtOrderDate.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME, GlobalVariable.WEB_DATE_FORMAT));
            }
        }, selectedDate.get(Calendar.YEAR), selectedDate.get(Calendar.MONTH), selectedDate.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();

    }

    @OnClick(R.id.plusDateReserv)
    public void onPlusDateReserv() {
        Date newDate = Utils.addDayToDate(Utils.convertStringToDate(txtReservationDate.getText().toString(), GlobalVariable.DATE_FORMAT_MONTH_NAME), 1);
        txtReservationDate.setText(Utils.convertDateToString(newDate, GlobalVariable.DATE_FORMAT_MONTH_NAME));
        getReservation("");
    }

    @OnClick(R.id.minDateReserv)
    public void onMinDateReserv() {
        Date newDate = Utils.addDayToDate(Utils.convertStringToDate(txtReservationDate.getText().toString(), GlobalVariable.DATE_FORMAT_MONTH_NAME), -1);
        txtReservationDate.setText(Utils.convertDateToString(newDate, GlobalVariable.DATE_FORMAT_MONTH_NAME));
        getReservation("");
    }

    @OnClick(R.id.plusDateOrder)
    public void onPlusDateOrder() {
        Date newDate = Utils.addDayToDate(Utils.convertStringToDate(txtOrderDate.getText().toString(), GlobalVariable.DATE_FORMAT_MONTH_NAME), 1);
        txtOrderDate.setText(Utils.convertDateToString(newDate, GlobalVariable.DATE_FORMAT_MONTH_NAME));
        getOrder("");
    }

    @OnClick(R.id.minDateOrder)
    public void onMinDateOrder() {
        Date newDate = Utils.addDayToDate(Utils.convertStringToDate(txtOrderDate.getText().toString(), GlobalVariable.DATE_FORMAT_MONTH_NAME), -1);
        txtOrderDate.setText(Utils.convertDateToString(newDate, GlobalVariable.DATE_FORMAT_MONTH_NAME));
        getOrder("");
    }

    @Override
    public void getReservasionOrder(UpdateTreatmentData data) {
        if (data.getTAG().equals("GetReservation")) {
            pbLoadingReservation.setVisibility(View.GONE);
            adapterReservation.setDatas(data.getData().getItems());
            if (adapterReservation.getData().size() == 0) {
                txtNothingReservation.setVisibility(View.VISIBLE);
            } else {
                txtNothingReservation.setVisibility(View.GONE);
            }
        } else {
            pbLoadingOrder.setVisibility(View.GONE);
            adapterOrder.setDatas(data.getData().getItems());
            if (adapterOrder.getData().size() == 0) {
                txtNothingOrder.setVisibility(View.VISIBLE);
            } else {
                txtNothingOrder.setVisibility(View.GONE);
            }

        }

    }

    @Override
    public void onFailGettingData(OnFailGettingData data) {
        if (data.getApi() == OnFailGettingData.GET_RESERVATION) {
            pbLoadingReservation.setVisibility(View.GONE);
        } else {
            pbLoadingOrder.setVisibility(View.GONE);
        }
        Toast.makeText(getContext(), data.getThrowable().getMessage(), Toast.LENGTH_SHORT).show();
        data.getThrowable().printStackTrace();

    }
}
