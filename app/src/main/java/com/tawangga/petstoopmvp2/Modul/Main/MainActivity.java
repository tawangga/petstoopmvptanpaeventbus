package com.tawangga.petstoopmvp2.Modul.Main;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.tawangga.petstoopmvp2.Dialog.ProfileDialog.ProfileDialog;
import com.tawangga.petstoopmvp2.EventBus.NavigateToShop;
import com.tawangga.petstoopmvp2.EventBus.NetworkStateCheck;
import com.tawangga.petstoopmvp2.Helper.NetworkStateReceiver;
import com.tawangga.petstoopmvp2.Helper.SharedPreference;
import com.tawangga.petstoopmvp2.Helper.TreatmentHelper;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Modul.About.AboutAppFragment;
import com.tawangga.petstoopmvp2.Modul.AddNewTreatment.TreatmentOrderFragment;
import com.tawangga.petstoopmvp2.Modul.AddNewTreatment.TreatmentReservationFragment;
import com.tawangga.petstoopmvp2.Modul.Anamnesis.KonsultasiDokterFragment;
import com.tawangga.petstoopmvp2.Modul.DoctorTakeAction.DoctorTakeActionFragment;
import com.tawangga.petstoopmvp2.Modul.Invoice.InvoiceFragment;
import com.tawangga.petstoopmvp2.Modul.InvoiceDetail.InvoiceDetailPaymentFragment;
import com.tawangga.petstoopmvp2.Modul.SelectCustomer.SelectCustomerFragment;
import com.tawangga.petstoopmvp2.Modul.Setting.SettingFragment;
import com.tawangga.petstoopmvp2.Modul.Shop.ShopFragment;
import com.tawangga.petstoopmvp2.Modul.TreatmentDoctor.TreatmentDoctorFragment;
import com.tawangga.petstoopmvp2.Modul.TreatmentFragment.TreatmentFragment;
import com.tawangga.petstoopmvp2.R;
import com.tawangga.petstoopmvp2.imagepicker.AdjustBitmap;
import com.tawangga.petstoopmvp2.imagepicker.CompressImage;
import com.tawangga.petstoopmvp2.imagepicker.FileManagement;
import com.tawangga.petstoopmvp2.imagepicker.FilePickUtils;
import com.tawangga.petstoopmvp2.imagepicker.LifeCycleCallBackManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.tawangga.petstoopmvp2.imagepicker.FilePickUtils.CAMERA_PERMISSION;
import static com.tawangga.petstoopmvp2.imagepicker.FilePickUtils.STORAGE_PERMISSION_IMAGE;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    @BindView(R.id.iv_pic)
    CircleImageView ivPic;
    @BindView(R.id.pb_pic_main)
    ProgressBar pbPicMain;
    @BindView(R.id.tv_username)
    TextView tvUsername;
    @BindView(R.id.ll_account)
    LinearLayout llAccount;
    @BindView(R.id.iv_treatment)
    ImageView ivTreatment;
    @BindView(R.id.tv_treatment)
    TextView tvTreatment;
    @BindView(R.id.lin_treatment)
    LinearLayout linTreatment;
    @BindView(R.id.iv_shop)
    ImageView ivShop;
    @BindView(R.id.tv_shop)
    TextView tvShop;
    @BindView(R.id.lin_shop)
    LinearLayout linShop;
    @BindView(R.id.iv_payment)
    ImageView ivPayment;
    @BindView(R.id.tv_payment)
    TextView tvPayment;
    @BindView(R.id.lin_payment)
    LinearLayout linPayment;
    @BindView(R.id.iv_doctor_action)
    ImageView ivDoctorAction;
    @BindView(R.id.tv_doctor_action)
    TextView tvDoctorAction;
    @BindView(R.id.lin_doctor_action)
    LinearLayout linDoctorAction;
    @BindView(R.id.iv_customer)
    ImageView ivCustomer;
    @BindView(R.id.tv_customer)
    TextView tvCustomer;
    @BindView(R.id.lin_customer)
    LinearLayout linCustomer;
    @BindView(R.id.iv_setting)
    ImageView ivSetting;
    @BindView(R.id.tv_setting)
    TextView tvSetting;
    @BindView(R.id.lin_setting)
    LinearLayout linSetting;
    @BindView(R.id.connChecker)
    ImageView connChecker;
    @BindView(R.id.container)
    FrameLayout container;
    private NetworkStateReceiver networkStateReceiver;
    private static final int POSITION_TREATMENT = 1;
    private static final int POSITION_SHOP = 2;
    private static final int POSITION_PAYMENT = 3;
    private static final int POSITION_DOCTOR_ACTION = 4;
    private static final int POSITION_CUSTOMER = 5;
    private static final int POSITION_SETTING = 6;
    public int position = 0;
    public Bundle bundle = new Bundle();

    boolean isClearBackStack = false;
    private boolean isDoctor;
    private LifeCycleCallBackManager lifeCycleCallBackManager;
    private FilePickUtils filePickUtils;

    public static final int REQ_CAMERA = 1001;
    public static final int REQ_GALLERY = 1002;
    private static final int STORAGE_PERMISSION_CAMERA = 112;
    private static final int CAMERA_BUT_STORAGE_PERMISSION = 116;
    Bitmap bitmap;
    Uri uri;
    ByteArrayOutputStream byteStream;
    private String selectedImagePath;
    ProfileDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        networkStateReceiver = new NetworkStateReceiver();
        registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
        Utils.setLocale(Locale.US, this);
        filePickUtils = new FilePickUtils(this, onFileChoose);

        isDoctor = new SharedPreference(this).isDoctor();



        if (new SharedPreference(this).isCustomer()){
            position = POSITION_TREATMENT;
            linPayment.setVisibility(View.GONE);
            linDoctorAction.setVisibility(View.GONE);
            tvUsername.setText(new SharedPreference(this).getCustomerName());
            Picasso.get().load(new SharedPreference(this).getCustomerPhoto())
                    .tag(TAG)
                    .into(ivPic, new Callback() {
                        @Override
                        public void onSuccess() {
                            pbPicMain.setVisibility(View.GONE);
                            ivPic.setVisibility(View.VISIBLE);

                        }

                        @Override
                        public void onError(Exception e) {
                            ivPic.setImageDrawable(getResources().getDrawable(R.drawable.pic));
                            pbPicMain.setVisibility(View.GONE);
                            ivPic.setVisibility(View.VISIBLE);
                            e.printStackTrace();
                            Log.e("Staff Photo", new SharedPreference(getApplicationContext()).getStaffPhoto());
                        }
                    });
        } else {
            tvUsername.setText(new SharedPreference(this).getStaffUsername());
            Picasso.get().load(new SharedPreference(this).getStaffPhoto())
                    .tag(TAG)
                    .into(ivPic, new Callback() {
                        @Override
                        public void onSuccess() {
                            pbPicMain.setVisibility(View.GONE);
                            ivPic.setVisibility(View.VISIBLE);

                        }

                        @Override
                        public void onError(Exception e) {
                            ivPic.setImageDrawable(getResources().getDrawable(R.drawable.pic));
                            pbPicMain.setVisibility(View.GONE);
                            ivPic.setVisibility(View.VISIBLE);
                            e.printStackTrace();
                            Log.e("Staff Photo", new SharedPreference(getApplicationContext()).getStaffPhoto());
                        }
                    });      if (isDoctor) {
                position = POSITION_DOCTOR_ACTION;
                linTreatment.setVisibility(View.GONE);
                linShop.setVisibility(View.GONE);
                linPayment.setVisibility(View.GONE);
                linCustomer.setVisibility(View.GONE);
            } else {
                position = POSITION_TREATMENT;
                linDoctorAction.setVisibility(View.GONE);
            }
        }
        changePosition();

    }


    public void setClearBackStack(boolean clearBackStack) {
        isClearBackStack = clearBackStack;
    }

    public void toTreatment() {
        Utils.cancelInvoice("", this, TAG);
        if (isClearBackStack) {
            clearBackStack();
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, new TreatmentFragment());
        ft.addToBackStack(null);
        ft.commit();

    }

    public void toKonsultasiDokter() {
        Utils.cancelInvoice("", this, TAG);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, KonsultasiDokterFragment.newInstance());
        ft.addToBackStack(null);
        ft.commit();
    }

    public void toDoctorTakeAction() {
//        Utils.cancelInvoice("", this, TAG);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, DoctorTakeActionFragment.newInstance());
        ft.addToBackStack(null);
        ft.commit();
    }

    public void changePosition() {
        setUnselectMenu();
        switch (position) {
            case POSITION_TREATMENT:
                bundle.clear();
                TreatmentHelper.getInstance().setShop(false);

                setSelectedMenu(linTreatment, tvTreatment);

                isClearBackStack = true;
                toTreatment();
                break;

            case POSITION_SHOP:
                bundle.clear();
                TreatmentHelper.getInstance().setShop(true);

                setSelectedMenu(linShop, tvShop);
                isClearBackStack = true;
                toSelectCustomer(false, "shop", "");
                break;

            case POSITION_DOCTOR_ACTION:
                bundle.clear();
                setSelectedMenu(linDoctorAction, tvDoctorAction);

                isClearBackStack = true;
                toDoctorAction();

                break;

            case POSITION_PAYMENT:
                TreatmentHelper.getInstance().setShop(false);
                setSelectedMenu(linPayment, tvPayment);

                isClearBackStack = true;
                toInvoice();
                break;

            case POSITION_CUSTOMER:
                bundle.clear();
                TreatmentHelper.getInstance().setShop(false);
                setSelectedMenu(linCustomer, tvCustomer);

                isClearBackStack = true;
                toSelectCustomer(true, "customer", "");
                break;

            case POSITION_SETTING:
                bundle.clear();
                TreatmentHelper.getInstance().setShop(false);
                setSelectedMenu(linSetting, tvSetting);
                isClearBackStack = true;
                toSetting();
                break;
        }

        InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);

    }

    @Subscribe
    public void toShop(NavigateToShop navigate) {
        if (!isDoctor)
            Utils.cancelInvoice("", this, TAG);
        ShopFragment fragment = new ShopFragment();
        Bundle bundle = new Bundle();
        bundle.putString("cart_service_id", navigate.getCartServiceId());
        fragment.setArguments(bundle);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.addToBackStack(null);
        ft.commit();

    }

    public void toSetting() {
        if (!isDoctor)
            Utils.cancelInvoice("", this, TAG);
        if (isClearBackStack) {
            clearBackStack();
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, SettingFragment.newInstance());
        ft.addToBackStack(null);
        ft.commit();

    }


    public void toAboutApplication() {
        if (!isDoctor)
            Utils.cancelInvoice("", this, TAG);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, AboutAppFragment.newInstance());
        ft.addToBackStack(null);
        ft.commit();

    }

    private void clearBackStack() {
        try {
            FragmentManager fm = getSupportFragmentManager();
            for (Fragment fragment : fm.getFragments()) {
                fm.beginTransaction().remove(fragment).commit();
            }
            isClearBackStack = false;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setSelectedMenu(LinearLayout lin, TextView tv) {
        lin.setBackgroundColor(getResources().getColor(R.color.white));
        tv.setTextColor(getResources().getColor(R.color.brown_2));

    }

    private void setUnselectMenu() {
        linTreatment.setBackgroundColor(getResources().getColor(R.color.brown_2));
        tvTreatment.setTextColor(getResources().getColor(R.color.white));

        linShop.setBackgroundColor(getResources().getColor(R.color.brown_2));
        tvShop.setTextColor(getResources().getColor(R.color.white));

        linPayment.setBackgroundColor(getResources().getColor(R.color.brown_2));
        tvPayment.setTextColor(getResources().getColor(R.color.white));

        linDoctorAction.setBackgroundColor(getResources().getColor(R.color.brown_2));
        tvDoctorAction.setTextColor(getResources().getColor(R.color.white));

        linCustomer.setBackgroundColor(getResources().getColor(R.color.brown_2));
        tvCustomer.setTextColor(getResources().getColor(R.color.white));

        linSetting.setBackgroundColor(getResources().getColor(R.color.brown_2));
        tvSetting.setTextColor(getResources().getColor(R.color.white));

    }


    @OnClick({R.id.lin_treatment, R.id.lin_shop, R.id.lin_payment, R.id.lin_customer, R.id.lin_setting, R.id.lin_doctor_action})
    public void onClickSideMenu(View view) {

        switch (view.getId()) {
            case R.id.lin_treatment:
                position = POSITION_TREATMENT;
                break;
            case R.id.lin_shop:
                position = POSITION_SHOP;
                break;
            case R.id.lin_payment:
                position = POSITION_PAYMENT;
                break;
            case R.id.lin_doctor_action:
                position = POSITION_DOCTOR_ACTION;
                break;
            case R.id.lin_customer:
                position = POSITION_CUSTOMER;
                break;
            case R.id.lin_setting:
                position = POSITION_SETTING;
                break;
        }
        changePosition();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(networkStateReceiver);

    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void networkCheck(NetworkStateCheck network) {
        connChecker.setBackgroundResource(network.isConnected() ? R.drawable.on : R.drawable.off);
    }

    @OnClick(R.id.ll_account)
    public void showAccount() {
        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.40);
        dialog = new ProfileDialog(this, width);
        dialog.setLocationByAttachedView(llAccount);
        dialog.show();
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        lifeCycleCallBackManager = filePickUtils.getCallBackManager();

        MenuItem camera = menu.add("Camera").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                boolean hasCameraPermission = checkPermission(Manifest.permission.CAMERA);
                boolean hasStoragePermission = checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                boolean hasStoragePermission2 = checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE);

                if (hasCameraPermission && hasStoragePermission && hasStoragePermission2) {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    Uri imageUri = FileProvider.getUriForFile(MainActivity.this,
                            getApplicationContext().getPackageName() + ".fileprovider",
                            new File(Environment.getExternalStorageDirectory(), "fname_" +
                                    System.currentTimeMillis() + ".jpg"));
                    uri = imageUri;
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                    startActivityForResult(intent, REQ_CAMERA);


                } else if (!hasCameraPermission && !hasStoragePermission) {
                    requestPermissionForCameraStorage();
                } else if (!hasCameraPermission) {
                    requestPermissionForCamera();
                } else {
                    requestPermissionForCameraButStorage();
                }

                return true;
            }
        });

        MenuItem galleri = menu.add("Gallery").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                boolean hasStoragePermission =
                        checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) && checkPermission(
                                Manifest.permission.READ_EXTERNAL_STORAGE);

                if (hasStoragePermission) {
                    Intent cameraIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    cameraIntent.setType("image/*");
                    if (cameraIntent.resolveActivity(getApplicationContext().getPackageManager()) != null) {
                        bitmap = null;
                        startActivityForResult(cameraIntent, REQ_GALLERY);
                    }
                } else {
                    requestPermissionForExternalStorage();
                }
                return true;
            }
        });

    }

    public void toTreatmentOrder(int cart_service_id) {
        Utils.cancelInvoice("", this, TAG);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, TreatmentOrderFragment.newInstance(cart_service_id));
        ft.addToBackStack(null);
        ft.commit();

    }

    public void toTreatmentReserv(int cart_service_id, String tanggal) {
        Utils.cancelInvoice("", this, TAG);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//        ft.replace(R.id.container, new TreatmentReservationFragment());
        ft.replace(R.id.container, TreatmentReservationFragment.newInstance(cart_service_id, tanggal));
        ft.addToBackStack(null);
        ft.commit();

    }

    public void toSelectCustomer(boolean fromCustomer, String state, String tanggal) {
        Utils.cancelInvoice("", this, TAG);
        if (isClearBackStack) {
            clearBackStack();
        }
        Bundle args = new Bundle();
        args.putString("state", state);
        args.putString("tanggal", tanggal);
        args.putBoolean("fromCustomer", fromCustomer);
        SelectCustomerFragment fragment = new SelectCustomerFragment();
        fragment.setArguments(args);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.addToBackStack(null);
        ft.commit();

    }

    private boolean checkPermission(String permission) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M
                || ActivityCompat.checkSelfPermission(this, permission)
                == PackageManager.PERMISSION_GRANTED;
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestPermissionForExternalStorage() {

        final String[] permissions = new String[]{Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE};
        /*
        final String[] permissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};
*/
        requestPermissionWithRationale(permissions, STORAGE_PERMISSION_IMAGE, "Storage");
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestPermissionForCameraStorage() {
/*        final String[] permissions = new String[]{Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE};*/

        final String[] permissions = new String[]{Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE};
        requestPermissionWithRationale(permissions, STORAGE_PERMISSION_CAMERA, "Camera & Storage");
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestPermissionForCamera() {
        final String[] permissions = new String[]{Manifest.permission.CAMERA};
        requestPermissionWithRationale(permissions, CAMERA_PERMISSION, "Camera");
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestPermissionForCameraButStorage() {

        final String[] permissions = new String[]{Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE};
        /*
        final String[] permissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};
*/
        requestPermissionWithRationale(permissions, CAMERA_BUT_STORAGE_PERMISSION, "Storage");
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestPermissionWithRationale(final String[] permissions, final int requestCode,
                                                String rationaleDialogText) {
        boolean showRationale = false;
        for (String permission : permissions) {
            if (this.shouldShowRequestPermissionRationale(permission)) {
                showRationale = true;
            }
        }

        if (showRationale) {
            AlertDialog.Builder builder =
                    new AlertDialog.Builder(this).setPositiveButton("AGREE",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    requestPermissions(permissions, requestCode);
                                }
                            })
                            .setNegativeButton("DENY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .setMessage("Allow "
                                    + this.getString(R.string.app_name)
                                    + " to access "
                                    + rationaleDialogText
                                    + "?");
            builder.create().show();
        } else {
            requestPermissions(permissions, requestCode);
        }
    }

    private FilePickUtils.OnFileChoose onFileChoose = new FilePickUtils.OnFileChoose() {
        @Override
        public void onFileChoose(String fileUri, int requestCode) {
            Log.e(TAG, "onFileChoose: " + fileUri);
            File image = new File(fileUri);

            Bitmap bitmap = BitmapFactory.decodeFile(image.getPath());
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);

            selectedImagePath = image.getPath();

            dialog.setImage(selectedImagePath, bitmap);
            Toast.makeText(MainActivity.this, "data : " + selectedImagePath, Toast.LENGTH_SHORT).show();
            lifeCycleCallBackManager = null;
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri uri;
        Bitmap bitmap;

        if (requestCode == REQ_GALLERY && resultCode == RESULT_OK) {
            uri = data.getData();
            bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            compressImage(bitmap);
        }

        if (requestCode == REQ_CAMERA && resultCode == RESULT_OK) {
            uri = this.uri;
            bitmap = new AdjustBitmap(getApplicationContext(), uri).resizeBitmap().adjustBitmap().getBitmap();
            compressImage(bitmap);
        }
    }


    private void compressImage(Bitmap bitmap) {
        new CompressImage(bitmap).setOnImageCompressed(new CompressImage.OnCompressCallback() {
            @Override
            public void onImageCompressed(ByteArrayOutputStream _byteStream, Bitmap bitmap) {
                byteStream = _byteStream;
                File photo = new FileManagement(MainActivity.this, byteStream).getTempFileImage();
                selectedImagePath = photo.toString();

                Toast.makeText(MainActivity.this, "data : " + selectedImagePath, Toast.LENGTH_SHORT).show();
                dialog.setImage(selectedImagePath, photo);
            }
        }).execute();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (lifeCycleCallBackManager != null) {
            lifeCycleCallBackManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    public void toInvoiceDetailPayment(boolean isDownPayment, Bundle bundle) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, InvoiceDetailPaymentFragment.newInstance(isDownPayment, bundle));
        ft.commit();

    }

    public void toInvoice() {
        if (isClearBackStack) {
            clearBackStack();
        }
        InvoiceFragment fragment = new InvoiceFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.commit();

    }


    @Override
    public void onBackPressed() {

        Fragment myFragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (myFragment instanceof InvoiceDetailPaymentFragment) {
            bundle.putParcelable("dataInvoice", null);
            isClearBackStack = true;
            toInvoice();
            return;
        }
        if (myFragment == null || getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        }

        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    public void toDoctorAction() {
        if (isClearBackStack)
            clearBackStack();
        TreatmentHelper.getInstance().setSavedAnamnesis(null);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, TreatmentDoctorFragment.newInstance());
        ft.addToBackStack(null);
        ft.commit();
    }

}
