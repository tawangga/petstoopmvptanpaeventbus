package com.tawangga.petstoopmvp2.Modul.TreatmentDoctor;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.EventBus.UpdateTreatmentData;
import com.tawangga.petstoopmvp2.Helper.GlobalVariable;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Modul.TreatmentFragment.TreatmentFragmentView;
import com.tawangga.petstoopmvp2.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TreatmentDoctorFragment extends Fragment implements TreatmentFragmentView {
    private static final String TAG = TreatmentDoctorFragment.class.getSimpleName();
    @BindView(R.id.minDateReserv)
    FrameLayout minDateReserv;
    @BindView(R.id.txt_reservation_date)
    TextView txtReservationDate;
    @BindView(R.id.plusDateReserv)
    FrameLayout plusDateReserv;
    @BindView(R.id.rv_reservation)
    RecyclerView rvReservation;
    @BindView(R.id.pb_loading_reservation)
    ProgressBar pbLoadingReservation;
    @BindView(R.id.txt_nothing_reservation)
    TextView txtNothingReservation;
    @BindView(R.id.minDateOrder)
    FrameLayout minDateOrder;
    @BindView(R.id.txt_order_date)
    TextView txtOrderDate;
    @BindView(R.id.plusDateOrder)
    FrameLayout plusDateOrder;
    @BindView(R.id.txt_nothing_order)
    TextView txtNothingOrder;
    @BindView(R.id.pb_loading_order)
    ProgressBar pbLoadingOrder;
    @BindView(R.id.rv_order)
    RecyclerView rvOrder;
    private TreatmentDoctorPresenter presenter;
    private DoctorActionOrderAdapter adapterOrder;
    private DoctorActionReservationAdapter adapterReservation;

    public static TreatmentDoctorFragment newInstance() {
        TreatmentDoctorFragment fragment = new TreatmentDoctorFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_doctor_action, container, false);
        ButterKnife.bind(this, v);
        presenter = new TreatmentDoctorPresenter(getActivity(), TAG, this);
        txtReservationDate.setText(Utils.changeDateFormat(Utils.getTodayDate(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
        txtOrderDate.setText(Utils.changeDateFormat(Utils.getTodayDate(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));

        setAdapter();
        getReservation(Utils.getTodayDate());
        getOrder(Utils.getTodayDate());

        return v;
    }

    private void setAdapter() {

        adapterOrder = new DoctorActionOrderAdapter(getActivity());
        adapterOrder.setDatas(new ArrayList<>());
        rvOrder.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvOrder.setAdapter(adapterOrder);

        adapterReservation = new DoctorActionReservationAdapter(getActivity());
        adapterReservation.setDatas(new ArrayList<>());
        rvReservation.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvReservation.setAdapter(adapterReservation);
    }


    @OnClick(R.id.txt_reservation_date)
    public void onDateClicked(View view) {
        Calendar calendar = Calendar.getInstance();

        Date date = Utils.convertStringToDate(txtReservationDate.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME);
        Calendar selectedDate = Calendar.getInstance();
        selectedDate.setTime(date);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.datepicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String dd = year + "-" + (month + 1) + "-" + dayOfMonth;
                txtReservationDate.setText(Utils.changeDateFormat(dd, GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
                getReservation(Utils.changeDateFormat(txtReservationDate.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME, GlobalVariable.WEB_DATE_FORMAT));
            }
        }, selectedDate.get(Calendar.YEAR), selectedDate.get(Calendar.MONTH), selectedDate.get(Calendar.DAY_OF_MONTH));
//                datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        datePickerDialog.show();

    }

    @OnClick(R.id.txt_order_date)
    public void onOrderDateClick() {
        Calendar calendar = Calendar.getInstance();

        Date date = Utils.convertStringToDate(txtOrderDate.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME);
        Calendar selectedDate = Calendar.getInstance();
        selectedDate.setTime(date);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.datepicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String dd = year + "-" + (month + 1) + "-" + dayOfMonth;
                txtOrderDate.setText(Utils.changeDateFormat(dd, GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
                getOrder(Utils.changeDateFormat(txtOrderDate.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME, GlobalVariable.WEB_DATE_FORMAT));
            }
        }, selectedDate.get(Calendar.YEAR), selectedDate.get(Calendar.MONTH), selectedDate.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();

    }

    @OnClick(R.id.plusDateReserv)
    public void onPlusDateReserv() {
        Date newDate = Utils.addDayToDate(Utils.convertStringToDate(txtReservationDate.getText().toString(), GlobalVariable.DATE_FORMAT_MONTH_NAME), 1);
        txtReservationDate.setText(Utils.convertDateToString(newDate, GlobalVariable.DATE_FORMAT_MONTH_NAME));
        getReservation("");
    }

    @OnClick(R.id.minDateReserv)
    public void onMinDateReserv() {
        Date newDate = Utils.addDayToDate(Utils.convertStringToDate(txtReservationDate.getText().toString(), GlobalVariable.DATE_FORMAT_MONTH_NAME), -1);
        txtReservationDate.setText(Utils.convertDateToString(newDate, GlobalVariable.DATE_FORMAT_MONTH_NAME));
        getReservation("");
    }

    @OnClick(R.id.plusDateOrder)
    public void onPlusDateOrder() {
        Date newDate = Utils.addDayToDate(Utils.convertStringToDate(txtOrderDate.getText().toString(), GlobalVariable.DATE_FORMAT_MONTH_NAME), 1);
        txtOrderDate.setText(Utils.convertDateToString(newDate, GlobalVariable.DATE_FORMAT_MONTH_NAME));
        getOrder("");
    }

    @OnClick(R.id.minDateOrder)
    public void onMinDateOrder() {
        Date newDate = Utils.addDayToDate(Utils.convertStringToDate(txtOrderDate.getText().toString(), GlobalVariable.DATE_FORMAT_MONTH_NAME), -1);
        txtOrderDate.setText(Utils.convertDateToString(newDate, GlobalVariable.DATE_FORMAT_MONTH_NAME));
        getOrder("");
    }

    private void getOrder(String date) {
        if (date.equals("")) {
            date = Utils.changeDateFormat(txtOrderDate.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME, GlobalVariable.WEB_DATE_FORMAT);
        }
        pbLoadingOrder.setVisibility(View.VISIBLE);
        txtNothingOrder.setVisibility(View.GONE);

        adapterOrder.getData().clear();
        Map<String, String> param = new HashMap<>();
        param.put("date", date);
        param.put("cart_service_type", "2");
        param.put("page", "1");
        param.put("limit", "999");
        presenter.getOrder(param);

    }

    private void getReservation(String date) {
        if (date.equals("")) {
            date = Utils.changeDateFormat(txtReservationDate.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME, GlobalVariable.WEB_DATE_FORMAT);
        }
        pbLoadingReservation.setVisibility(View.VISIBLE);
        txtNothingReservation.setVisibility(View.GONE);
        adapterReservation.getData().clear();
        Map<String, String> param = new HashMap<>();
        param.put("date", date);
        param.put("cart_service_type", "1");
        param.put("page", "1");
        param.put("limit", "999");
        presenter.getReservation(param);

    }


    @Override
    public void getReservasionOrder(UpdateTreatmentData data) {
        if (data.getTAG().equals("GET_ORDER")) {
            pbLoadingOrder.setVisibility(View.GONE);
            adapterOrder.setDatas(data.getData().getItems());
            if (adapterOrder.getData().size() == 0) {
                txtNothingOrder.setVisibility(View.VISIBLE);
            } else {
                txtNothingOrder.setVisibility(View.GONE);
            }
        } else {
            pbLoadingReservation.setVisibility(View.GONE);
            adapterReservation.setDatas(data.getData().getItems());
            if (adapterReservation.getData().size() == 0) {
                txtNothingReservation.setVisibility(View.VISIBLE);
            } else {
                txtNothingReservation.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onFailGettingData(OnFailGettingData data) {
        Toast.makeText(getActivity(), data.getThrowable().getMessage(), Toast.LENGTH_SHORT).show();
        if (data.getApi() == OnFailGettingData.GET_ORDER) {
            pbLoadingOrder.setVisibility(View.GONE);
        } else if (data.getApi() == OnFailGettingData.GET_RESERVATION) {
            pbLoadingReservation.setVisibility(View.GONE);
        }
        data.getThrowable().printStackTrace();

    }


}
