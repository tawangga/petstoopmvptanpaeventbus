package com.tawangga.petstoopmvp2.Modul.InvoiceDetail;

import android.content.Context;

import com.tawangga.petstoopmvp2.EventBus.OnApiResultSuccess;
import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import okhttp3.RequestBody;

public class InvoiceDetailPresenter {
    private final Context context;
    private final String TAG;
    private InvoiceDetailPaymentFragmentView view;

    public InvoiceDetailPresenter(Context context, String TAG, InvoiceDetailPaymentFragmentView view) {

        this.context = context;
        this.TAG = TAG;
        this.view = view;
    }

    public void processInvoice(RequestBody formBody) {
        Connector.newInstance(context).processInvoice2(formBody, new Connector.ApiCallback<String>() {
            @Override
            public String getKey() {
                return TAG;
            }

            @Override
            public void success(String s, String messages) {
                view.onProcessInvoice(new OnApiResultSuccess(OnApiResultSuccess.PROCESS_INVOICE));
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.PROCESS_INVOICE2, t));
            }
        });

    }
}
