package com.tawangga.petstoopmvp2.Modul.SelectCustomer;

import android.content.Context;

import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.Helper.SaveOwner;
import com.tawangga.petstoopmvp2.Model.CustomerModel;
import com.tawangga.petstoopmvp2.Model.DetailCustomerModel;
import com.tawangga.petstoopmvp2.Model.PetListModel;
import com.tawangga.petstoopmvp2.ModelNew.GetTreatmentRecord;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SelectCustomerPresenter {
    private String TAG;
    private SelectCustomerView view;
    private Context context;

    public SelectCustomerPresenter(Context context, String TAG, SelectCustomerView view) {
        this.context = context;
        this.TAG = TAG;
        this.view = view;
    }

    public void getPetTreatmentRecord(int petId) {
        Connector.newInstance(context).getPetTreatmentRecord(String.valueOf(petId), new Connector.ApiCallback<List<GetTreatmentRecord.DATABean>>() {
            @Override
            public String getKey() {
                return TAG;
            }

            @Override
            public void success(List<GetTreatmentRecord.DATABean> dataBeans, String messages) {
                view.onGetPetTreatmentRecord(dataBeans);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.GET_TREATMENT_RECORD, t));
            }
        });

    }

    public void getCustomerByPet(String limit, String search, int laman) {

        Connector.newInstance(context).getCustomerListByPet(limit, laman + "", search, new Connector.ApiCallback<CustomerModel.DATABean>() {
            @Override
            public String getKey() {
                return TAG;
            }

            @Override
            public void success(CustomerModel.DATABean dataBeans, String messages) {
                view.onGetCustomerByPet(dataBeans);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.GET_CUSTOMER_BY_PET, t));
            }
        });
    }

    public void getPetList(String limit, String search, int laman) {

        Connector.newInstance(context).getPetList(limit, laman + "", search, new Connector.ApiCallback<PetListModel.DATABean>() {
            @Override
            public String getKey() {
                return TAG;
            }

            @Override
            public void success(PetListModel.DATABean dataBeans, String messages) {
                view.onGetPetList(dataBeans);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.GET_PET_LIST, t));
            }
        });
    }

    public void getCustomerPage(String limit, String search, int laman) {

        Connector.newInstance(context).getCustomerPage(limit, laman + "", search, new Connector.ApiCallback<PetListModel.DATABean>() {
            @Override
            public String getKey() {
                return TAG;
            }

            @Override
            public void success(PetListModel.DATABean dataBeans, String messages) {
                view.onGetCustomerPage(dataBeans);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.GET_PET_LIST, t));
            }
        });
    }

    void getCustomer(String limit, String search, int laman) {
        Connector.newInstance(context).getCustomerList(limit, laman + "", search, new Connector.ApiCallback<CustomerModel.DATABean>() {
            @Override
            public String getKey() {
                return TAG;
            }

            @Override
            public void success(CustomerModel.DATABean dataBeans, String messages) {
                view.onGetCustomer(dataBeans);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.GET_CUSTOMER, t));
            }
        });
    }

    void getDetailCustomer(String customerId) {
        Connector.newInstance(context).detailCustomer(customerId, new Connector.ApiCallback<DetailCustomerModel.DATABean>() {
            @Override
            public String getKey() {
                return TAG;
            }

            @Override
            public void success(DetailCustomerModel.DATABean dataBean, String messages) {
                view.onGetDetailCustomer(dataBean);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.GET_DETAIL_CUSTOMER, t));
            }
        });

    }


    public void saveOwner(HashMap<String, RequestBody> data, MultipartBody.Part customer_photo, String edit, boolean isEdit) {
        Connector.newInstance(context).addCustomer(data, customer_photo, edit, new Connector.ApiCallback<DetailCustomerModel.DATABean>() {
            @Override
            public String getKey() {
                return TAG;
            }

            @Override
            public void success(DetailCustomerModel.DATABean dataBean, String messages) {
                view.onSaveOwner(new SaveOwner(dataBean, isEdit));
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.SAVE_OWNER, t));
            }
        });

    }

}
