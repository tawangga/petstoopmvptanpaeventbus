package com.tawangga.petstoopmvp2.Modul.Login;

import android.content.Context;

import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.Model.SignInModel;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import okhttp3.FormBody;
import okhttp3.RequestBody;

public class SignInPresenter {
    private final Context context;
    private final String TAG;
    private SignInView view;

    public SignInPresenter(Context context, String TAG, SignInView view) {
        this.context = context;
        this.TAG = TAG;
        this.view = view;
    }

    public void doLogin(String username, String password, int siteId) {
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("username", username);
        formBuilder.add("password", password);
        formBuilder.add("site_id", siteId + "");
        RequestBody formBody = formBuilder.build();
        Connector.newInstance(context).loginPost(formBody, new Connector.ApiCallback<SignInModel.DATABean>() {
            @Override
            public String getKey() {
                return TAG;
            }

            @Override
            public void success(SignInModel.DATABean dataBean, String messages) {
                view.onSuccessSignIn(dataBean);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.GET_APP_VERSION, t));
            }
        });

    }

}
