package com.tawangga.petstoopmvp2.Modul.InvoiceDetail;

import com.tawangga.petstoopmvp2.EventBus.OnApiResultSuccess;
import com.tawangga.petstoopmvp2.Modul.BaseView;

public interface InvoiceDetailPaymentFragmentView extends BaseView {
    void onProcessInvoice(OnApiResultSuccess data);
}
