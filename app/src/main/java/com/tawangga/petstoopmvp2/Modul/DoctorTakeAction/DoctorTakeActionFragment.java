package com.tawangga.petstoopmvp2.Modul.DoctorTakeAction;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.tawangga.petstoopmvp2.Dialog.DialogAddCCU.DialogAddNewCCU;
import com.tawangga.petstoopmvp2.Dialog.DialogSearchServiceDoctor.DialogSearchServiceDoctor;
import com.tawangga.petstoopmvp2.Dialog.DialogSeeAllTreatment.AdapterMedicalItems;
import com.tawangga.petstoopmvp2.Dialog.DialogSeeAllTreatment.DialogSeeAllTreatment;
import com.tawangga.petstoopmvp2.Dialog.DialogSeeAllTreatment.TreatmentKonsultasiDokterAdapter;
import com.tawangga.petstoopmvp2.EventBus.NavigateToShop;
import com.tawangga.petstoopmvp2.EventBus.OnApiResultSuccess;
import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.Helper.GlobalVariable;
import com.tawangga.petstoopmvp2.Helper.TreatmentHelper;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Model.GetOrderConfirmAnamnesis;
import com.tawangga.petstoopmvp2.Modul.Anamnesis.AdapterClinicalCheckup;
import com.tawangga.petstoopmvp2.Modul.Main.MainActivity;
import com.tawangga.petstoopmvp2.Param.SaveConfirmOrderAnamnesis;
import com.tawangga.petstoopmvp2.R;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class DoctorTakeActionFragment extends Fragment implements DoctorTakeActionView {
    private static final String TAG = DoctorTakeActionFragment.class.getSimpleName();
    @BindView(R.id.iv_pic_pet)
    CircleImageView ivPicPet;
    @BindView(R.id.txt_owner_pet_name)
    TextView txtOwnerPetName;
    @BindView(R.id.txt_reservcode)
    TextView txtReservcode;
    @BindView(R.id.btn_medical_records)
    Button btnMedicalRecords;
    @BindView(R.id.txt_anamnesis)
    EditText txtAnamnesis;
    @BindView(R.id.ll_anamnesis)
    LinearLayout llAnamnesis;
    @BindView(R.id.btn_add_new_ccu)
    TextView btnAddNewCcu;
    @BindView(R.id.rv_clinical_checkup)
    RecyclerView rvClinicalCheckup;
    @BindView(R.id.ll_clinical_checkup)
    LinearLayout llClinicalCheckup;
    @BindView(R.id.view_left)
    NestedScrollView viewLeft;
    @BindView(R.id.txt_diagnosis)
    EditText txtDiagnosis;
    @BindView(R.id.ll_diagnosis)
    LinearLayout llDiagnosis;
    @BindView(R.id.btn_add_new_treatment)
    TextView btnAddNewTreatment;
    @BindView(R.id.rv_treatment)
    RecyclerView rvTreatment;
    @BindView(R.id.ll_treatment)
    LinearLayout llTreatment;
    @BindView(R.id.tv_addNewMedicalItem)
    TextView tvAddNewMedicalItem;
    @BindView(R.id.rv_medical_item)
    RecyclerView rvMedicalItem;
    @BindView(R.id.ll_medical_items)
    LinearLayout llMedicalItems;
    @BindView(R.id.txt_return_desc)
    EditText txtReturnDesc;
    @BindView(R.id.txt_return_date)
    TextView txtReturnDate;
    @BindView(R.id.view_right)
    NestedScrollView viewRight;
    @BindView(R.id.ll_other)
    LinearLayout llOther;
    @BindView(R.id.btn_submit)
    Button btnSubmit;

    private GetOrderConfirmAnamnesis.DATABean savedAnamnesis;
    private AdapterClinicalCheckup adapterClinicalCheckup;
    private AdapterMedicalItems adapterMedicalItems;
    private List<GetOrderConfirmAnamnesis.DATABean.TreatmentsBean> treatments;
    private GetOrderConfirmAnamnesis.DATABean dataAnamnesis = null;
    private DoctorTakeActionPresenter presenter;
    private ProgressDialog dialog;
    private TreatmentKonsultasiDokterAdapter konsultasiDokterAdapter;

    public static DoctorTakeActionFragment newInstance() {
        DoctorTakeActionFragment fragment = new DoctorTakeActionFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_doctor_take_action, container, false);
        ButterKnife.bind(this, v);

        presenter = new DoctorTakeActionPresenter(getActivity(), TAG, this);
        savedAnamnesis = TreatmentHelper.getInstance().getSavedAnamnesis();

        initCustomer();
        setAdapter();
        getOrderConfirmAnamnesis();
        return v;
    }

    private void setAdapter() {
        rvClinicalCheckup.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapterClinicalCheckup = new AdapterClinicalCheckup(getActivity());
        adapterClinicalCheckup.setDatas(new ArrayList<>(), true);
        rvClinicalCheckup.setAdapter(adapterClinicalCheckup);


        rvMedicalItem.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapterMedicalItems = new AdapterMedicalItems(getActivity(), false);
        adapterMedicalItems.setDatas(new ArrayList<>());
        rvMedicalItem.setAdapter(adapterMedicalItems);

        llAnamnesis.measure(0, 0);
        llTreatment.getLayoutParams().height = llAnamnesis.getMeasuredHeight();
        llTreatment.requestLayout();
        llMedicalItems.getLayoutParams().height = llAnamnesis.getMeasuredHeight();
        llMedicalItems.requestLayout();


        viewRight.measure(0, 0);
        viewLeft.getLayoutParams().height = viewRight.getMeasuredHeight();
        viewLeft.requestLayout();

        treatments = new ArrayList<>();

    }

    private void initCustomer() {
        txtOwnerPetName.setText(TreatmentHelper.getInstance().getSelectedCustomer().getCustomerName() + " - " + TreatmentHelper.getInstance().getSelectedPet().getPetName());

        txtReservcode.setText(TreatmentHelper.getInstance().getDataTreatment().getOrderCode());
        Picasso.get().load(TreatmentHelper.getInstance().getSelectedPet().getPetPhoto())
                .tag(TAG)
                .into(ivPicPet, new Callback() {
                    @Override
                    public void onSuccess() {
                        ivPicPet.setVisibility(View.VISIBLE);

                    }

                    @Override
                    public void onError(Exception e) {
                        ivPicPet.setImageDrawable(getResources().getDrawable(R.drawable.no_image));
                        ivPicPet.setVisibility(View.VISIBLE);
                        e.printStackTrace();
                    }
                });
    }

    @OnClick(R.id.tv_addNewMedicalItem)
    public void tv_addNewMedicalItem() {
        if (dataAnamnesis != null) {
            String cartServiceId = String.valueOf(dataAnamnesis.getCartServiceId());
            dataAnamnesis.setAnamnesis(txtAnamnesis.getText().toString().trim());
            dataAnamnesis.setDiagnosis(txtDiagnosis.getText().toString().trim());
            dataAnamnesis.setReturnScheduleDesc(txtReturnDesc.getText().toString().trim());
            if (!txtReturnDate.getText().toString().trim().equals("Select Date")) {
                dataAnamnesis.setReturnScheduleDate(Utils.changeDateFormat(txtReturnDate.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME, GlobalVariable.WEB_DATE_FORMAT));
            }
            TreatmentHelper.getInstance().setSavedAnamnesis(dataAnamnesis);
            EventBus.getDefault().post(new NavigateToShop(cartServiceId));
        }
    }

    @OnClick(R.id.txt_return_date)
    public void onOrderDateClick() {
        Calendar selectedDate = Calendar.getInstance();
        Calendar calendar = Calendar.getInstance();

        if (!txtReturnDate.getText().toString().trim().equals("Select Date")) {
            Date date = Utils.convertStringToDate(txtReturnDate.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME);
            selectedDate.setTime(date);

        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.datepicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String dd = year + "-" + (month + 1) + "-" + dayOfMonth;
                txtReturnDate.setText(Utils.changeDateFormat(dd, GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
            }
        }, selectedDate.get(Calendar.YEAR), selectedDate.get(Calendar.MONTH), selectedDate.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        datePickerDialog.show();

    }


    private void getOrderConfirmAnamnesis() {
        dialog = Utils.showProgressDialog(getActivity(), false, "Loading...");

        String cartServiceId = TreatmentHelper.getInstance().getDataTreatment().getCartServiceId() + "";
        presenter.getOrderConfirmAnamnesisDoctor(cartServiceId);
    }


    @Override
    public void onFailGettingData(OnFailGettingData data) {
        Toast.makeText(getActivity(), data.getThrowable().getMessage(), Toast.LENGTH_SHORT).show();
        if (data.getApi() == OnFailGettingData.GET_CONFIRM_ORDER_ANAMNESIS) {
            dialog.dismiss();
        }
        data.getThrowable().printStackTrace();
    }

    @OnClick(R.id.btn_submit)
    public void onSubmit() {
        SaveConfirmOrderAnamnesis param = new SaveConfirmOrderAnamnesis();
        param.setReturnScheduleDesc(txtReturnDesc.getText().toString().trim());

        if (!txtReturnDate.getText().toString().trim().equals("Select Date")) {
            param.setReturnScheduleDate(Utils.changeDateFormat(txtReturnDate.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME, GlobalVariable.WEB_DATE_FORMAT));

        }
        param.setAnamnesis(txtAnamnesis.getText().toString().trim());
        param.setDiagnosis(txtDiagnosis.getText().toString().trim());
        List<SaveConfirmOrderAnamnesis.ClinicalCheckupBean> listClinicalCheckUp = new ArrayList<>();
        for (int i = 0; i < adapterClinicalCheckup.getData().size(); i++) {
            GetOrderConfirmAnamnesis.DATABean.ClinicalCheckupsBean clinicalCheckupsBean = adapterClinicalCheckup.getData().get(i);
            if (clinicalCheckupsBean.getClinicalCheckupIsPriority() == 1) {
                SaveConfirmOrderAnamnesis.ClinicalCheckupBean clinicalCheckUpItem = new SaveConfirmOrderAnamnesis.ClinicalCheckupBean();
                clinicalCheckUpItem.setClinicalCheckupId(clinicalCheckupsBean.getClinicalCheckupId());
                clinicalCheckUpItem.setClinicalCheckupName(clinicalCheckupsBean.getClinicalCheckupName());
                clinicalCheckUpItem.setValue(clinicalCheckupsBean.getValue() + "");
                clinicalCheckUpItem.setClinicalCheckupUnit(clinicalCheckupsBean.getClinicalCheckupUnit());
                if (clinicalCheckupsBean.isExam()) {
                    clinicalCheckUpItem.setIsExam(1);
                } else {
                    clinicalCheckUpItem.setIsExam(0);
                }
                listClinicalCheckUp.add(clinicalCheckUpItem);
            }
        }
        param.setIsTakeAction(true);
        param.setClinicalCheckup(listClinicalCheckUp);

        dialog = Utils.showProgressDialog(getActivity(), false, "Loading...");
        presenter.saveOrderConfirmAnamnesisDokter(TreatmentHelper.getInstance().getDataTreatment().getCartServiceId(), param);
    }


    @OnClick(R.id.btn_medical_records)
    public void onViewMedicalRecords() {
        DialogSeeAllTreatment dialog = DialogSeeAllTreatment.newInstance(TreatmentHelper.getInstance().getSelectedCustomer(), TreatmentHelper.getInstance().getSelectedPet());
        dialog.show(getChildFragmentManager(), "");
    }

    @OnClick(R.id.btn_add_new_treatment)
    public void onClickAddNewTreatment() {
        DialogSearchServiceDoctor dialogSearchServiceDoctor = DialogSearchServiceDoctor.newInstance(treatments, dataAnamnesis, konsultasiDokterAdapter);
        dialogSearchServiceDoctor.show(getChildFragmentManager(), "");
    }

    @OnClick(R.id.btn_add_new_ccu)
    public void onClickAddNewCCU() {
        DialogAddNewCCU dialogAddNewCCU = DialogAddNewCCU.newInstance(dataAnamnesis, adapterClinicalCheckup);
        dialogAddNewCCU.show(getChildFragmentManager(), "");
    }

    @Override
    public void onGetOrderConfirmAnamnesis(GetOrderConfirmAnamnesis.DATABean dataBean) {
        dialog.dismiss();
        dataAnamnesis = dataBean;
        if (savedAnamnesis != null) {
            dataBean.setReturnScheduleDesc(savedAnamnesis.getReturnScheduleDesc());
            dataBean.setReturnScheduleDate(savedAnamnesis.getReturnScheduleDate());
            dataBean.setAnamnesis(savedAnamnesis.getAnamnesis());
            dataBean.setDiagnosis(savedAnamnesis.getDiagnosis());
            dataBean.setClinicalCheckups(savedAnamnesis.getClinicalCheckups());
        }
        adapterClinicalCheckup.setDatas(dataBean.getClinicalCheckups(), true);
        for (GetOrderConfirmAnamnesis.DATABean.TreatmentsBean allTreatment : dataBean.getTreatments()) {
            if (allTreatment.getServiceType().equals("1")) {
                treatments.add(allTreatment);
            }
        }
        adapterMedicalItems.setDatas(dataBean.getMedicalItems());

        rvTreatment.setLayoutManager(new LinearLayoutManager(getActivity()));
        konsultasiDokterAdapter = new TreatmentKonsultasiDokterAdapter(getActivity(), dataAnamnesis);
        rvTreatment.setAdapter(konsultasiDokterAdapter);

        konsultasiDokterAdapter.setDatas(treatments);
        txtAnamnesis.setText(dataBean.getAnamnesis());
        txtDiagnosis.setText(dataBean.getDiagnosis());
        txtReturnDesc.setText(dataBean.getReturnScheduleDesc());
        if (dataBean.getReturnScheduleDate() != null) {
            txtReturnDate.setText(Utils.changeDateFormat(dataBean.getReturnScheduleDate(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
        }

    }

    @Override
    public void onSaveOrderConfirmAnamnesis(OnApiResultSuccess data) {
        if (data.getApi() == OnApiResultSuccess.SAVE_ANAMNESIS) {
            dialog.dismiss();
            ((MainActivity) getActivity()).setClearBackStack(true);
            ((MainActivity) getActivity()).toDoctorAction();
        }
    }
}
