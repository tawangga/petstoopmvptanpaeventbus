package com.tawangga.petstoopmvp2.Modul.AddNewTreatment;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.tawangga.petstoopmvp2.Dialog.DialogCreateOrder.DialogCreateOrder;
import com.tawangga.petstoopmvp2.Dialog.DialogSearchService.DialogSearchService;
import com.tawangga.petstoopmvp2.EventBus.OnCancelTreatmentService;
import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.EventBus.OnSuccessReservTreatment;
import com.tawangga.petstoopmvp2.Helper.GlobalVariable;
import com.tawangga.petstoopmvp2.Helper.TreatmentHelper;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.ModelNew.CreateCart;
import com.tawangga.petstoopmvp2.ModelNew.GetBoarding;
import com.tawangga.petstoopmvp2.ModelNew.GetDoctor;
import com.tawangga.petstoopmvp2.ModelNew.GetDoctorSchedule;
import com.tawangga.petstoopmvp2.ModelNew.GetGroomingLocation;
import com.tawangga.petstoopmvp2.Modul.Main.MainActivity;
import com.tawangga.petstoopmvp2.Param.CancelServiceParam;
import com.tawangga.petstoopmvp2.R;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class TreatmentOrderFragment extends Fragment implements AddNewTreatmentView {
    private static final String TAG = TreatmentOrderFragment.class.getSimpleName();
    @BindView(R.id.iv_pic_pet)
    CircleImageView ivPicPet;
    @BindView(R.id.pb_loading_iv_pet)
    ProgressBar pbLoadingIvPet;
    @BindView(R.id.txt_owner_name)
    TextView txtOwnerName;
    @BindView(R.id.txt_pet_name)
    TextView txtPetName;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.rv_list)
    RecyclerView rvList;
    @BindView(R.id.pb_loading)
    AVLoadingIndicatorView pbLoading;
    @BindView(R.id.tv_customer_name)
    TextView tvCustomerName;
    @BindView(R.id.tv_search_service)
    TextView tvSearchService;
    @BindView(R.id.btn_search_service)
    RelativeLayout btnSearchService;
    @BindView(R.id.rv_services)
    RecyclerView rvServices;
    @BindView(R.id.txt_estimated_prie)
    TextView txtEstimatedPrie;
    @BindView(R.id.ll_services)
    LinearLayout llServices;
    @BindView(R.id.tv_from)
    TextView tvFrom;
    @BindView(R.id.tv_until)
    TextView tvUntil;
    @BindView(R.id.ll_from_until)
    LinearLayout llFromUntil;
    @BindView(R.id.btn_delete)
    Button btnDelete;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    @BindView(R.id.btn_order)
    Button btnOrder;

    private int cart_service_id = 0;
    private int page = 0, lastPage;
    private SelectTreatmentNewAdapter adapter;
    private LinearLayoutManager layoutManager;
    int selectedTabPosition = 0;
    private CreateCart.DATABean cartData;
    private SelectedServiceAdapter adapterServices;
    private CancelServiceParam savedService;
    private TreatmentOrderPresenter presenter;
    private String[] tabItems;
    private ProgressDialog dialog;


    public TreatmentOrderFragment() {
    }


    public static Fragment newInstance(int cart_service_id) {
        TreatmentOrderFragment fragment = new TreatmentOrderFragment();
        fragment.cart_service_id = cart_service_id;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_treatment_order, container, false);
        ButterKnife.bind(this, v);
        tabItems = getResources().getStringArray(R.array.arrayService);
        presenter = new TreatmentOrderPresenter(getActivity(), TAG, this);

        if (cart_service_id == 0) {
            btnDelete.setVisibility(View.GONE);
        } else {
            btnDelete.setVisibility(View.VISIBLE);
        }

        setAdapter();
        createCart();

        return v;

    }


    private void setAdapter() {
        layoutManager = new LinearLayoutManager(getActivity());
        rvList.setLayoutManager(layoutManager);
        adapter = new SelectTreatmentNewAdapter(getActivity());
        rvList.setAdapter(adapter);
        rvList.addOnScrollListener(recyclerViewOnScrollListener);
        adapter.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (selectedTabPosition == 0) {
                    GetDoctor.DATABean.ItemsBean itemsBean = adapter.getDataDoctor().get(position);
                    if (!itemsBean.isIsAvailable()) {
                        return;
                    }
                    if ((itemsBean.getStaffId() == cartData.getDoctorSchedule().getStaffId()) &&
                            (itemsBean.getTime().equals(cartData.getDoctorSchedule().getScheduleTime()))) {
                        cartData.getDoctorSchedule().setStaffId(0);
                        cartData.getDoctorSchedule().setScheduleTime("");
                        cartData.getDoctorSchedule().setScheduleDate("");
                        cartData.getDoctorSchedule().setName("");
                        adapter.notifyDataSetChanged();

                    } else {
                        cartData.getDoctorSchedule().setStaffId(itemsBean.getStaffId());
                        cartData.getDoctorSchedule().setScheduleTime(itemsBean.getTime());
                        cartData.getDoctorSchedule().setScheduleDate(Utils.getTodayDate());
                        cartData.getDoctorSchedule().setName(itemsBean.getName());
                        adapter.notifyDataSetChanged();
                    }

                } else if (selectedTabPosition == 1) {
                    GetGroomingLocation.DATABean.ItemsBean itemsBean = adapter.getDataGroomingLocation().get(position);
                    if (!itemsBean.isIsAvailable()) {
                        return;
                    }
                    if ((itemsBean.getGroomingLocationId() == cartData.getGroomingLocationSchedule().getGroomingLocationId()) &&
                            (itemsBean.getTime().equals(cartData.getGroomingLocationSchedule().getScheduleTime()))) {
                        cartData.getGroomingLocationSchedule().setGroomingLocationId(0);
                        cartData.getGroomingLocationSchedule().setScheduleTime("");
                        cartData.getGroomingLocationSchedule().setScheduleDate("");
                        cartData.getGroomingLocationSchedule().setLocationName("");
                        adapter.notifyDataSetChanged();

                    } else {
                        cartData.getGroomingLocationSchedule().setGroomingLocationId(itemsBean.getGroomingLocationId());
                        cartData.getGroomingLocationSchedule().setScheduleTime(itemsBean.getTime());
                        cartData.getGroomingLocationSchedule().setScheduleDate(Utils.getTodayDate());
                        cartData.getGroomingLocationSchedule().setLocationName(itemsBean.getName());
                        adapter.notifyDataSetChanged();
                    }

                } else {
                    GetBoarding.DATABean.ItemsBean itemsBean = adapter.getDataBoarding().get(position);
                    if (!itemsBean.isIsAvailable()) {
                        return;
                    }
                    if (itemsBean.getCageId() == cartData.getBoardingSchedule().getCageId()) {
                        cartData.getBoardingSchedule().setCageId(0);
                        cartData.getBoardingSchedule().setScheduleDateFrom("");
                        cartData.getBoardingSchedule().setCageName("");
                        cartData.getBoardingSchedule().setScheduleDateUntil("");
                        cartData.getBoardingSchedule().setCagePrice("");
                        adapter.notifyDataSetChanged();

                    } else {
                        cartData.getBoardingSchedule().setCageId(itemsBean.getCageId());
                        cartData.getBoardingSchedule().setScheduleDateFrom(Utils.getTodayDate());
                        cartData.getBoardingSchedule().setCageName(itemsBean.getCageName());
                        cartData.getBoardingSchedule().setScheduleDateUntil(Utils.changeDateFormat(tvUntil.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME, GlobalVariable.WEB_DATE_FORMAT));
                        cartData.getBoardingSchedule().setCagePrice(itemsBean.getCagePrice());
                        adapter.notifyDataSetChanged();
                    }

                    tvFrom.setText(Utils.changeDateFormat(getServiceDate(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));

                    tvUntil.setText(Utils.changeDateFormat(cartData.getBoardingSchedule().getScheduleDateUntil(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));

                }
            }
        });

        for (int i = 0; i < tabItems.length; i++) {
            tabLayout.addTab(tabLayout.newTab().setText(tabItems[i]));
        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selectedTabPosition = tab.getPosition();
                updatePagination(0, 1);
                selectTab();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        tabLayout.selectTab(tabLayout.getTabAt(0));


        rvServices.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapterServices = new SelectedServiceAdapter(getActivity(), txtEstimatedPrie);
        rvServices.setAdapter(adapterServices);


    }


    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            if (page != lastPage) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= adapter.getItemCount()) {
                    if (selectedTabPosition == 0) {
                        getDoctor();
                    } else if (selectedTabPosition == 1) {
                        getGroomingLocation();
                    } else if (selectedTabPosition == 2) {
                        getBoarding();
                    }
                }
            }
        }
    };

    private void setVisibleLoading(boolean isVisible) {
        if (isVisible) {
            pbLoading.setVisibility(View.VISIBLE);
        } else {
            pbLoading.setVisibility(View.GONE);
        }
    }

    private void selectTab() {
        if (selectedTabPosition == 0) {
            adapter.clearAllData();
            adapterServices.setDatas(cartData.getServiceMedical());
            getDoctor();
            llServices.setVisibility(View.VISIBLE);
            llFromUntil.setVisibility(View.GONE);

        } else if (selectedTabPosition == 1) {
            adapter.clearAllData();
            adapterServices.setDatas(cartData.getServiceGrooming());
            getGroomingLocation();
            llServices.setVisibility(View.VISIBLE);
            llFromUntil.setVisibility(View.GONE);

        } else if (selectedTabPosition == 2) {
            llServices.setVisibility(View.GONE);
            llFromUntil.setVisibility(View.VISIBLE);
            adapter.clearAllData();
            tvFrom.setText(Utils.changeDateFormat(getServiceDate(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
            tvUntil.setText(Utils.changeDateFormat(cartData.getBoardingSchedule().getScheduleDateUntil(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
            getBoarding();

        }
    }

    private String getServiceDate() {
        String date = Utils.getTodayDate();
        if (cartData.getDoctorSchedule().getStaffId() != 0) {
            date = cartData.getDoctorSchedule().getScheduleDate();
        } else if (cartData.getGroomingLocationSchedule().getGroomingLocationId() != 0) {
            date = cartData.getGroomingLocationSchedule().getScheduleDate();
        } else if (cartData.getBoardingSchedule().getCageId() != 0) {
            date = cartData.getBoardingSchedule().getScheduleDateFrom();
        }
        return date;
    }

    private void setLoadingCenter() {
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.CENTER_IN_PARENT);
        pbLoading.setLayoutParams(lp);

    }

    private void setLoadingBottom() {
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
        pbLoading.setLayoutParams(lp);

    }

    @Override
    public void onFailGettingData(OnFailGettingData data) {
        Toast.makeText(getActivity(), data.getThrowable().getMessage(), Toast.LENGTH_SHORT).show();
        Utils.dismissProgressDialog(dialog);
        if (data.getApi() == OnFailGettingData.GET_DOCTOR
                || data.getApi() == OnFailGettingData.GET_BOARDING
                || data.getApi() == OnFailGettingData.GET_GROOMING_LOCATION) {
            setVisibleLoading(false);
        }
        if (data.getApi() == OnFailGettingData.CREATE_CART_TREATMENT) {
            try {
                getActivity().onBackPressed();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        data.getThrowable().printStackTrace();

    }

    private void getDoctor() {
        if (page == lastPage)
            return;

        if (adapter.getDataDoctor().size() == 0) {
            setLoadingCenter();
        } else {
            setLoadingBottom();
        }
        setVisibleLoading(true);
        Map<String, String> params = new HashMap<>();
        params.put("limit", String.valueOf(GlobalVariable.LIMIT_ITEM_DOCTOR));
        params.put("page", String.valueOf(page + 1));
        params.put("search", "");
        params.put("date", getServiceDate());

        presenter.getDoctor(params);
    }

    private void getBoarding() {
        if (page == lastPage)
            return;

        if (adapter.getDataBoarding().size() == 0) {
            setLoadingCenter();
        } else {
            setLoadingBottom();
        }
        setVisibleLoading(true);
        Map<String, String> params = new HashMap<>();
        params.put("limit", String.valueOf(GlobalVariable.LIMIT_ITEM));
        params.put("page", String.valueOf(page + 1));
        params.put("search", "");
        params.put("date", Utils.changeDateFormat(tvFrom.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME, GlobalVariable.WEB_DATE_FORMAT));
        params.put("end_date", Utils.changeDateFormat(tvUntil.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME, GlobalVariable.WEB_DATE_FORMAT));
        presenter.getBoarding(params);
    }

    private void getGroomingLocation() {
        if (page == lastPage)
            return;

        if (adapter.getDataGroomingLocation().size() == 0) {
            setLoadingCenter();
        } else {
            setLoadingBottom();
        }

        setVisibleLoading(true);
        Map<String, String> params = new HashMap<>();
        params.put("limit", String.valueOf(GlobalVariable.LIMIT_ITEM_DOCTOR));
        params.put("page", String.valueOf(page + 1));
        params.put("search", "");
        params.put("date", getServiceDate());
        presenter.getGroomingLocation(params);

    }

    private void updatePagination(int page, int lastPage) {
        this.page = page;
        this.lastPage = lastPage;
    }

    private void createCart() {
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("pet_id", TreatmentHelper.getInstance().getSelectedPet().getPetId() + "");
        formBuilder.add("cart_service_id", cart_service_id + "");
        RequestBody param = formBuilder.build();
        dialog = Utils.showProgressDialog(getActivity(), false, "Loading...");

        presenter.createCartTreatment(param);
    }


    private List<CreateCart.DATABean.ServiceBean> saveService(List<CreateCart.DATABean.ServiceBean> services) {
        List<CreateCart.DATABean.ServiceBean> savedService = new ArrayList<>();
        for (CreateCart.DATABean.ServiceBean datas : services) {
            CreateCart.DATABean.ServiceBean data = new CreateCart.DATABean.ServiceBean();
            data.setCartServiceDetailId(datas.getCartServiceDetailId());
            data.setCartServiceId(datas.getCartServiceId());
            data.setServiceType(datas.getServiceType());
            data.setServiceId(datas.getServiceId());
            data.setServiceName(datas.getServiceName());
            data.setServicePrice(datas.getServicePrice());
            data.setOrderStatus(datas.getOrderStatus());
            data.setIsInvoice(datas.getIsInvoice());
            data.setCreatedBy(datas.getCreatedBy());
            data.setCreatedAt(datas.getCreatedAt());
            data.setUpdatedAt(datas.getUpdatedAt());
            data.setUpdatedBy(datas.getUpdatedBy());
            data.setDeletedAt(datas.getDeletedAt());
            savedService.add(data);
        }

        return savedService;
    }

    private void setData() {
        txtOwnerName.setText(cartData.getPet().getCustomerName());
        txtPetName.setText(cartData.getPet().getPetName());
        ivPicPet.setVisibility(View.GONE);
        pbLoadingIvPet.setVisibility(View.VISIBLE);
        tvCustomerName.setText(cartData.getPet().getCustomerName());

        Picasso.get().load(cartData.getPet().getPetPhoto())
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .tag(TAG)
                .into(ivPicPet, new Callback() {
                    @Override
                    public void onSuccess() {
                        pbLoadingIvPet.setVisibility(View.GONE);
                        ivPicPet.setVisibility(View.VISIBLE);

                    }

                    @Override
                    public void onError(Exception e) {
                        ivPicPet.setImageDrawable(getResources().getDrawable(R.drawable.no_image));
                        pbLoadingIvPet.setVisibility(View.GONE);
                        ivPicPet.setVisibility(View.VISIBLE);
                        e.printStackTrace();
                    }
                });
    }

    @OnClick(R.id.tv_until)
    public void onUntilClicked() {
        Date minDate = Utils.convertStringToDate(tvFrom.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(minDate);

        Date date = Utils.convertStringToDate(tvFrom.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME);
        if (!TextUtils.isEmpty(tvUntil.getText().toString().trim())) {
            date = Utils.convertStringToDate(tvUntil.getText().toString().trim(), GlobalVariable.DATE_FORMAT_MONTH_NAME);
        }
        Calendar selectedDate = Calendar.getInstance();
        selectedDate.setTime(date);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.datepicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String dd = year + "-" + (month + 1) + "-" + dayOfMonth;
//                selectedUntil = dd;
                tvUntil.setText(Utils.changeDateFormat(dd, GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));
                cartData.getBoardingSchedule().setScheduleDateUntil(dd);
                updatePagination(0, 1);
                adapter.clearAllData();

                getBoarding();
            }
        }, selectedDate.get(Calendar.YEAR), selectedDate.get(Calendar.MONTH), selectedDate.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        datePickerDialog.show();

    }

    @OnClick(R.id.btn_delete)
    void openDialogDelete() {
        final AlertDialog dialog = new AlertDialog.Builder(getActivity()).create();
        dialog.setTitle("Delete Order");
        dialog.setMessage("Are you sure want to delete this order?");
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteService(cartData.getCartServiceId());
            }
        });
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @OnClick(R.id.btn_cancel)
    void openDialogCancel() {
        final AlertDialog dialog = new AlertDialog.Builder(getActivity()).create();
        dialog.setTitle("Cancel Order");
        dialog.setMessage("Are you sure want to cancel editing this order ?");
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                cancelService();
            }
        });
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void cancelService() {
        dialog = Utils.showProgressDialog(getActivity(), false, "Loading...");
        presenter.rollbackService(savedService);

    }

    void deleteService(int cartServiceId) {
        dialog = Utils.showProgressDialog(getActivity(), false, "Loading...");
        presenter.deleteService(cartServiceId);
    }

    @OnClick(R.id.btn_search_service)
    public void onViewClicked() {
        DialogSearchService dialog = DialogSearchService.newInstance(adapterServices, tabLayout.getSelectedTabPosition() == 0, cartData);
        dialog.show(getChildFragmentManager(), "");
    }


    @OnClick(R.id.btn_order)
    public void onBtnOrder() {
        DialogCreateOrder dialog = DialogCreateOrder.newInstance(cartData, adapter, adapterServices);
        dialog.show(getChildFragmentManager(), "");
    }

    @Override
    public void onGetDoctor(GetDoctor.DATABean data) {
        setVisibleLoading(false);
        updatePagination(data.getPage(), data.getLastPage());
        adapter.setDataDoctor(data.getItems(), cartData);

    }

    @Override
    public void onGetGroomingLocation(GetGroomingLocation.DATABean data) {
        setVisibleLoading(false);
        updatePagination(data.getPage(), data.getLastPage());
        adapter.setDataGroomingLocation(data.getItems(), cartData);
    }

    @Override
    public void onGetBoarding(GetBoarding.DATABean data) {
        setVisibleLoading(false);
        updatePagination(data.getPage(), data.getLastPage());
        adapter.setDataBoarding(data.getItems(), cartData, tvCustomerName);

    }

    @Override
    public void onRollbackService(OnCancelTreatmentService dataBean) {
        Utils.dismissProgressDialog(dialog);
        ((MainActivity) getActivity()).setClearBackStack(true);
        ((MainActivity) getActivity()).toTreatment();
    }

    @Override
    public void onDeleteService(OnCancelTreatmentService dataBean) {
        Utils.dismissProgressDialog(dialog);
        ((MainActivity) getActivity()).setClearBackStack(true);
        ((MainActivity) getActivity()).toTreatment();
    }

    @Override
    public void onCreateCartTreatment(CreateCart.DATABean dataBean) {
        dialog.dismiss();
        savedService = new CancelServiceParam();
        savedService.setCartServiceId(dataBean.getCartServiceId());
        savedService.setMedicalServiceOld(saveService(dataBean.getServiceMedical()));
        savedService.setGroomingServiceOld(saveService(dataBean.getServiceGrooming()));
        cartData = dataBean;
        setData();
        selectTab();

    }

    @Override
    public void onReservTreatment(OnSuccessReservTreatment dataBean) {

    }

    @Override
    public void onGetAllDoctor(GetDoctorSchedule.DATABean dataBean) {

    }
}
