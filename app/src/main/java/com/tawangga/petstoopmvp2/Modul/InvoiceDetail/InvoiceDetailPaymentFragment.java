package com.tawangga.petstoopmvp2.Modul.InvoiceDetail;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.mazenrashed.printooth.Printooth;
import com.mazenrashed.printooth.ui.ScanningActivity;
import com.tawangga.petstoopmvp2.EventBus.OnApiResultSuccess;
import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.Helper.GlobalVariable;
import com.tawangga.petstoopmvp2.Helper.PrintHelper;
import com.tawangga.petstoopmvp2.Helper.SharedPreference;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Model.DataInvoice;
import com.tawangga.petstoopmvp2.Model.SignInModel;
import com.tawangga.petstoopmvp2.Modul.Invoice.SelectedInvoiceAdapter;
import com.tawangga.petstoopmvp2.Modul.Main.MainActivity;
import com.tawangga.petstoopmvp2.R;

import java.util.Arrays;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import naturalizer.separator.Separator;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class InvoiceDetailPaymentFragment extends Fragment implements InvoiceDetailPaymentFragmentView {
    private static final String TAG = InvoiceDetailPaymentFragment.class.getSimpleName();

    @BindView(R.id.tv_invoiceCode)
    TextView tvInvoiceCode;
    @BindView(R.id.btn_make_order)
    LinearLayout btnMakeOrder;
    @BindView(R.id.rl_button)
    RelativeLayout rlButton;
    @BindView(R.id.txt_site_name)
    TextView txtSiteName;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.txt_site_address)
    TextView txtSiteAddress;
    @BindView(R.id.llPetshopdesc)
    LinearLayout llPetshopdesc;
    @BindView(R.id.rv_selected_invoice)
    RecyclerView rvSelectedInvoice;
    @BindView(R.id.txt_total)
    TextView txtTotal;
    @BindView(R.id.rl_total)
    RelativeLayout rlTotal;
    @BindView(R.id.txt_down_payment)
    TextView txtDownPayment;
    @BindView(R.id.rl_down_payment)
    RelativeLayout rlDownPayment;
    @BindView(R.id.txt_grand_total)
    TextView txtGrandTotal;
    @BindView(R.id.et_dp)
    EditText etDp;
    @BindView(R.id.llDownPayment)
    LinearLayout llDownPayment;
    @BindView(R.id.switch_cash)
    Switch switchCash;
    @BindView(R.id.tv_swCash)
    TextView tvSwCash;
    @BindView(R.id.et_cash)
    EditText etCash;
    @BindView(R.id.switch_debit)
    Switch switchDebit;
    @BindView(R.id.tv_swDebit)
    TextView tvSwDebit;
    @BindView(R.id.et_debit)
    EditText etDebit;
    @BindView(R.id.switch_cc)
    Switch switchCc;
    @BindView(R.id.tv_swCC)
    TextView tvSwCC;
    @BindView(R.id.et_cc)
    EditText etCc;
    @BindView(R.id.txt_grand_total2)
    TextView txtGrandTotal2;
    @BindView(R.id.btn_proceed_payment)
    Button btnProceedPayment;
    private boolean isDownPayment;
    private Bundle bundle = new Bundle();
    String customerID = "";
    DataInvoice invoice;
    private InvoiceDetailPresenter presenter;
    private ProgressDialog dialog;

    public static InvoiceDetailPaymentFragment newInstance(boolean isDownPayment, Bundle bundle) {
        InvoiceDetailPaymentFragment fragment = new InvoiceDetailPaymentFragment();
        fragment.isDownPayment = isDownPayment;
        fragment.bundle = bundle;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_invoice_detail_payment, container, false);
        ButterKnife.bind(this, v);
        presenter = new InvoiceDetailPresenter(getActivity(), TAG, this);
        showClientSite();
        getDataBundle();
        initView();
        return v;
    }

    private void getDataBundle() {
        customerID = bundle.getString("customer_id");
        Log.d("custID", customerID);
        invoice = bundle.getParcelable("invoice");
    }

    private void showClientSite() {
        String json = new SharedPreference(getActivity()).getSite();
        SignInModel.DATABean.SiteBean site = new Gson().fromJson(json, SignInModel.DATABean.SiteBean.class);
        txtSiteName.setText(site.getSiteName());
        txtSiteAddress.setText(site.getSiteAddress());

    }

    private void initView() {

        SelectedInvoiceAdapter selectedInvoiceAdapter = new SelectedInvoiceAdapter(getActivity(), null);
        selectedInvoiceAdapter.setDatas(invoice.getOrders(), true);
        rvSelectedInvoice.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvSelectedInvoice.setAdapter(selectedInvoiceAdapter);

        txtTotal.setText(Utils.toRupiah(invoice.getTotalTransaction()));
        txtDownPayment.setText(Utils.toRupiah(invoice.getTransactionDp()));
        tvInvoiceCode.setText(invoice.getTransactionInvoiceCode());

        txtDate.setText(Utils.changeDateFormat(Utils.getTodayDate(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME_FULL));

        etCash.addTextChangedListener(getTextWatcher(etCash));

        etDebit.addTextChangedListener(getTextWatcher(etDebit));

        etCc.addTextChangedListener(getTextWatcher(etCc));

        initValuePembayaran();


        float total = Float.parseFloat(invoice.getTotalTransaction());
        float dp = Float.parseFloat(invoice.getTransactionDp());
        float gt = total - dp;
        txtGrandTotal.setText(String.format("%s", Utils.toRupiah(String.valueOf(gt))));
        txtGrandTotal2.setText(String.format("%s", Utils.toRupiah("0")));

        if (isDownPayment) {
            rlDownPayment.setVisibility(View.GONE);
            rlTotal.setVisibility(View.GONE);
            btnProceedPayment.setText("Proceed Down Payment");
            btnProceedPayment.setBackground(getResources().getDrawable(R.drawable.bg_border_brown_2));
        } else {
            rlDownPayment.setVisibility(View.VISIBLE);
            rlTotal.setVisibility(View.VISIBLE);
            btnProceedPayment.setText("Proceed Payment");
            btnProceedPayment.setBackground(getResources().getDrawable(R.drawable.bg_border_yellow));
        }

        switchCash.setOnCheckedChangeListener(new Utils.setswitch(getActivity(), tvSwCash, etCash));
        switchDebit.setOnCheckedChangeListener(new Utils.setswitch(getActivity(), tvSwDebit, etDebit));
        switchCc.setOnCheckedChangeListener(new Utils.setswitch(getActivity(), tvSwCC, etCc));

    }

    private void initValuePembayaran() {
        etCash.setText("0");
        etDebit.setText("0");
        etCc.setText("0");
    }

    private TextWatcher getTextWatcher(EditText editText) {
        return new TextWatcher() {
            String current = "";

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equals(current)) {
                    editText.removeTextChangedListener(this);
                    String currentNum;
                    if (editable.length() < 5) {
                        currentNum = editable.toString();
                    } else {
                        String y = editable.toString().replaceAll("[^0-9]", "");
                        currentNum = y;
                    }
                    Log.d("base_price", currentNum);
                    current = Separator.getInstance().doSeparate(currentNum, Locale.GERMANY);
                    if (!current.equals("")) {
                        editText.setText(String.format("%s IDR", current));
                    } else {
                        editText.setText(String.format("%s IDR", "0"));
                    }
                    editText.setSelection(current.length());
                    editText.addTextChangedListener(this);
                    double tot = Utils.getNumFromCurrency(etCash.getText().toString()) + Utils.getNumFromCurrency(etDebit.getText().toString()) + Utils.getNumFromCurrency(etCc.getText().toString());
                    txtGrandTotal2.setText(String.format("%s", Utils.toRupiah(String.valueOf(tot))));
                }

            }
        };
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Utils.cancelInvoice("", getContext(), TAG);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @OnClick(R.id.btn_proceed_payment)
    public void processPayment() {
        validasi();
    }

    private void validasi() {
        double tot = Utils.getNumFromCurrency(etCash.getText().toString()) + Utils.getNumFromCurrency(etDebit.getText().toString()) + Utils.getNumFromCurrency(etCc.getText().toString());
        if (tot == 0) {
            Toast.makeText(getContext(), "Invalid payment value", Toast.LENGTH_SHORT).show();
            return;
        }

        if (isDownPayment) {
            if (tot >= Utils.getNumFromCurrency(txtTotal.getText().toString())) {
                Toast.makeText(getContext(), "Down payment must less than total transaction", Toast.LENGTH_SHORT).show();
                return;

            }
            processInvoice2();

        } else {
            if (tot != Utils.getNumFromCurrency(txtGrandTotal.getText().toString())) {
                Toast.makeText(getContext(), "Total payment not same with payment entered", Toast.LENGTH_SHORT).show();
            } else {
                processInvoice2();
            }
        }
    }


    private void processInvoice2() {
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("transaction_id", String.valueOf(invoice.getTransactionId()));
        if (isDownPayment)
            formBuilder.add("payment_type", "1");
        else
            formBuilder.add("payment_type", "2");
        formBuilder.add("cash", "" + Utils.getNumFromCurrency(etCash.getText().toString()));
        formBuilder.add("debit", "" + Utils.getNumFromCurrency(etDebit.getText().toString()));
        formBuilder.add("cc", "" + Utils.getNumFromCurrency(etCc.getText().toString()));
        formBuilder.add("payment_by", customerID);
        RequestBody formBody = formBuilder.build();
        dialog = Utils.showProgressDialog(getContext(), false, "Loading...");

        presenter.processInvoice(formBody);

    }


    private void print() {
        String json = new SharedPreference(getActivity()).getSite();
        SignInModel.DATABean.SiteBean site = new Gson().fromJson(json, SignInModel.DATABean.SiteBean.class);
        PrintHelper print = new PrintHelper();

        /**
         * Init header
         */
        print.setHeader(site.getSiteName())
                .setDesc(site.getSiteAddress())
                .line()
                .setBody(printInit(
                        "PAYMENT\t= " + invoice.getTransactionInvoiceCode(),
                        "DATE\t= " + invoice.getCreatedAt(),
                        "CUSTOMER\t= " + invoice.getCustomerName(),
                        "CASHIER\t= " + new SharedPreference(getActivity()).getStaffName() + "\n"
                ), PrintHelper.ALIGN_LEFT)
                .line()
                .setBody(printInit(
                        PrintHelper.padRight("ITEM", 15) +
                                PrintHelper.padRight("QTY", 5) +
                                PrintHelper.padRight("PRICE", 14) +
                                PrintHelper.padRight("TOTAL", 14) + "\n"
                ), PrintHelper.ALIGN_LEFT)
                .line();
        /**
         * Init main data
         */
        for (DataInvoice.Ordersget_invoice data : invoice.getOrders()) {
            // owner name
            print.setBody(printInit(
                    data.getCustomerName() + "\n"
            ), PrintHelper.ALIGN_LEFT, true);
            // items
            for (DataInvoice.Ordersget_invoice.Itemsget_invoice item : data.getItems()) {
                String item_name = item.getItemName().length() > 15 ? item.getItemName().substring(0, 11) + "..." : item.getItemName();
                print.setBody(
                        printInit(
                                PrintHelper.padRight(item_name, 15) +
                                        PrintHelper.padRight(item.getItemQty(), 5) +
                                        PrintHelper.padRight("Rp." + Utils.toMoney(item.getItemPrice()), 14) +
                                        PrintHelper.padRight("Rp." + Utils.toMoney(String.valueOf(Integer.parseInt(item.getItemQty()) * Double.parseDouble(item.getItemPrice()))), 14) + "\n"
                        ), PrintHelper.ALIGN_LEFT
                );
            }

            print.setBody("\n", PrintHelper.ALIGN_LEFT);
        }

        print.line()
                .setBody(printInit(
                        PrintHelper.padRight("GRAND TOTAL :", 20) + "Rp." + Utils.toMoney(invoice.getTotalTransaction()) + "\n"
                ), PrintHelper.ALIGN_LEFT);

        if (invoice.getPaymentStatus().equals("1") && isDownPayment) { //0=cancel,1=unpaid,2=dp,3=complete
            print.setBody(PrintHelper.padRight("DOWN PAYMENT :", 20) +
                            "Rp." + Utils.toMoney((Double.parseDouble(invoice.getTotalTransaction()) - Double.parseDouble(invoice.getTransactionDp())) + "") + "\n"
                    , PrintHelper.ALIGN_LEFT);
        }

        if (invoice.getPaymentStatus().equals("2") && invoice.getTransactionDp() != null) {
            print.setBody(PrintHelper.padRight("REMAINING PAYMENT:", 20) +
                            "Rp." + Utils.toMoney((Double.parseDouble(invoice.getTotalTransaction()) - Double.parseDouble(invoice.getTransactionDp())) + "") + "\n"
                    , PrintHelper.ALIGN_LEFT);
        }

        print.setBody(printInit("\nPAYMENT METHOD:"), PrintHelper.ALIGN_LEFT);

        if (Utils.getNumFromCurrency(etCash.getText().toString()) > 0) {
            print.setBody(printInit("\n" + PrintHelper.padRight("CASH", 8) + "Rp." + Utils.toMoney(Utils.getNumFromCurrency(etCash.getText().toString()) + "")), PrintHelper.ALIGN_LEFT);
        }
        if (Utils.getNumFromCurrency(etCc.getText().toString()) > 0) {
            print.setBody(printInit("\n" + PrintHelper.padRight("CC", 8) + "Rp." + Utils.toMoney(Utils.getNumFromCurrency(etCc.getText().toString()) + "")), PrintHelper.ALIGN_LEFT);
        }
        if (Utils.getNumFromCurrency(etDebit.getText().toString()) > 0) {
            print.setBody(printInit("\n" + PrintHelper.padRight("DEBIT", 8) + "Rp." + Utils.toMoney(Utils.getNumFromCurrency(etDebit.getText().toString()) + "")), PrintHelper.ALIGN_LEFT);
        }

        print.setBody(printInit("")
                , PrintHelper.ALIGN_LEFT)
                .space(3);
        Printooth.INSTANCE.printer().print(print.build());
    }


    private String printInit(String... args) {
        return TextUtils.join("\n", Arrays.asList(args));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ScanningActivity.SCANNING_FOR_PRINTER && resultCode == Activity.RESULT_OK) {
            print();
        }
    }

    @Override
    public void onProcessInvoice(OnApiResultSuccess result) {
        if (result.getApi() == OnApiResultSuccess.PROCESS_INVOICE) {
            Utils.dismissProgressDialog(dialog);
            if (!isDownPayment) {
                if (!Printooth.INSTANCE.hasPairedPrinter()) {
                    startActivityForResult(new Intent(getContext(), ScanningActivity.class), ScanningActivity.SCANNING_FOR_PRINTER);
                } else {
                    print();
                }
            } else {
                if (!Printooth.INSTANCE.hasPairedPrinter()) {
                    startActivityForResult(new Intent(getContext(), ScanningActivity.class), ScanningActivity.SCANNING_FOR_PRINTER);
                } else {
                    print();
                }
            }
            Toast.makeText(getContext(), "Payment Success", Toast.LENGTH_SHORT).show();
            ((MainActivity) Objects.requireNonNull(getContext())).bundle.putParcelable("dataInvoice", null);
            ((MainActivity) Objects.requireNonNull(getContext())).position = 3;
            ((MainActivity) Objects.requireNonNull(getContext())).changePosition();
        }


    }

    @Override
    public void onFailGettingData(OnFailGettingData data) {
        if (data.getApi() == OnFailGettingData.PROCESS_INVOICE2) {
            Utils.dismissProgressDialog(dialog);
            data.getThrowable().printStackTrace();
        }

    }
}
