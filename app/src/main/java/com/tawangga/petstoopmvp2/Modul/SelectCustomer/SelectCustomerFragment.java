package com.tawangga.petstoopmvp2.Modul.SelectCustomer;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.tawangga.petstoopmvp2.Dialog.DialogSeeAllTreatment.DialogSeeAllTreatment;
import com.tawangga.petstoopmvp2.EventBus.NavigateToShop;
import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.Helper.GlobalVariable;
import com.tawangga.petstoopmvp2.Helper.SaveOwner;
import com.tawangga.petstoopmvp2.Helper.SharedPreference;
import com.tawangga.petstoopmvp2.Helper.TreatmentHelper;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Model.CustomerModel;
import com.tawangga.petstoopmvp2.Model.DetailCustomerModel;
import com.tawangga.petstoopmvp2.Model.PetListModel;
import com.tawangga.petstoopmvp2.ModelNew.GetTreatmentRecord;
import com.tawangga.petstoopmvp2.Modul.Main.MainActivity;
import com.tawangga.petstoopmvp2.R;
import com.tawangga.petstoopmvp2.ServerSide.Connector;
import com.tawangga.petstoopmvp2.imagepicker.LifeCycleCallBackManager;
import com.wang.avi.AVLoadingIndicatorView;

import org.greenrobot.eventbus.EventBus;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SelectCustomerFragment extends Fragment implements SelectCustomerView {
    private static final String TAG = SelectCustomerFragment.class.getSimpleName();

    @BindView(R.id.txt_search_pet)
    EditText txtSearchPet;
    @BindView(R.id.btn_add_new)
    RelativeLayout btnAddNew;
    @BindView(R.id.rv_petlist)
    RecyclerView rvPetlist;
    @BindView(R.id.tabLayout)
    TabLayout petTab;
    Unbinder unbinder;
    @BindView(R.id.txt_total_pets)
    TextView txtTotalPets;
    @BindView(R.id.iv_pic_owner_2)
    CircleImageView ivPicOwner;
    @BindView(R.id.txt_owner_name)
    TextView txtOwnerName;
    @BindView(R.id.txt_email)
    TextView txtEmail;
    @BindView(R.id.txt_phone)
    TextView txtPhone;
    @BindView(R.id.txt_address)
    TextView txtAddress;
    @BindView(R.id.ll_owner_profile)
    LinearLayout llOwnerProfile;
    @BindView(R.id.ed_owner_name)
    EditText edOwnerName;
    @BindView(R.id.ed_email)
    EditText edEmail;
    @BindView(R.id.ed_phone)
    EditText edPhone;
    @BindView(R.id.ed_address)
    EditText edAddress;
    @BindView(R.id.ll_owner_profile_edit)
    LinearLayout llOwnerProfileEdit;
    @BindView(R.id.iv_pic_pet)
    CircleImageView ivPicPet;
    @BindView(R.id.txt_pet_name)
    TextView txtPetName;
    @BindView(R.id.txt_race)
    TextView txtRace;
    @BindView(R.id.txt_gender)
    TextView txtGender;
    @BindView(R.id.txt_dob)
    TextView txtDob;
    @BindView(R.id.txt_color)
    TextView txtColor;
    @BindView(R.id.txt_special_notes)
    TextView txtSpecialNotes;
    @BindView(R.id.txt_microchip)
    TextView txtMicrochip;
    @BindView(R.id.txt_breed)
    TextView txtBreed;
    @BindView(R.id.ll_pet_profile)
    LinearLayout llPetProfile;
    @BindView(R.id.txt_pet_name_edit)
    EditText txtPetNameEdit;
    @BindView(R.id.txt_race_edit)
    EditText txtRaceEdit;
    @BindView(R.id.txt_gender_edit)
    EditText txtGenderEdit;
    @BindView(R.id.txt_dob_edit)
    EditText txtDobEdit;

    @BindView(R.id.btn_edit_pet)
    TextView btnEditPet;
    @BindView(R.id.btn_edit_owner)
    TextView btnEditOwner;
    @BindView(R.id.txt_breed_edit)
    EditText txtBreedEdit;
    @BindView(R.id.txt_color_edit)
    EditText txtColorEdit;
    @BindView(R.id.txt_microchip_edit)
    EditText txtMicrochipEdit;
    @BindView(R.id.txt_special_notes_edit)
    EditText txtSpecialNotesEdit;

    @BindView(R.id.ll_pet_profile_edit)
    LinearLayout llPetProfileEdit;
    @BindView(R.id.rv_last_medic)
    RecyclerView rvLastMedic;
    @BindView(R.id.ll_last_medic)
    LinearLayout llLastMedic;
    @BindView(R.id.btn_select_customer)
    Button btnSelectCustomer;
    @BindView(R.id.btn_cancel_owner)
    Button btnCancelOwner;
    @BindView(R.id.btn_add_new_customer)
    TextView btnAddNewCustomer;
    @BindView(R.id.btn_add_new_pet)
    TextView btnAddNewPet;
    @BindView(R.id.btn_save_pet)
    Button btnSavePet;
    @BindView(R.id.btn_save_owner)
    Button btnSaveOwner;
    @BindView(R.id.ll_tab)
    LinearLayout llTab;
    @BindView(R.id.ll_pet)
    LinearLayout llPet;
    @BindView(R.id.rl_dont_have_pet)
    RelativeLayout rlDontHavePet;
    @BindView(R.id.ll_petlayout)
    LinearLayout llPetlayout;
    @BindView(R.id.ll_savepet)
    LinearLayout llSavePet;
    @BindView(R.id.btn_cancel_pet)
    Button btnCancelPet;
    @BindView(R.id.txt_select_customer)
    TextView txtSelectCustomer;
    @BindView(R.id.btn_see_all_treatment)
    TextView btnSeeAllTreatment;
    @BindView(R.id.refreshPet)
    SwipeRefreshLayout refreshPet;
    @BindView(R.id.ll_include_layout)
    LinearLayout ll_include_layout;
    @BindView(R.id.iv_nopet)
    ImageView ivNopet;
    @BindView(R.id.pb_loading_iv_customer)
    ProgressBar pbLoadingIvCustomer;
    @BindView(R.id.pb_loading_iv_pet)
    ProgressBar pbLoadingIvPet;

    @BindView(R.id.progressbar)
    AVLoadingIndicatorView progressbar;
    @BindView(R.id.loadingTreatmentRecord)
    ProgressBar loadingTreatmentRecord;
    @BindView(R.id.nothingTreatmentRecord)
    TextView nothingTreatmentRecord;
    @BindView(R.id.list_customer)
    LinearLayout list_customer;

    private boolean fromCustomer;
    private boolean isSearchCustomer = false;
    PetAdapter adapterPet;
    private String selectedImagePath = "";

    private LifeCycleCallBackManager lifeCycleCallBackManager;
    //private FilePickUtils filePickUtils;
    private DetailCustomerModel.DATABean selectedDetailCustomer;
    private boolean isAddCustomer = false;
    private boolean isAddPet = false;

    int selectedPetGender = 0;
    private int selectedPetId;
    private DetailCustomerModel.DATABean.PetsBean selectedPet;
    LinearLayoutManager layoutManager;
    String state;

    //for Pagination
    int page = 0;
    int p = 0;
    int lastpage = 1;

    protected Context ctx;
    LastMedicAdapter adapterLastMedic;
    String[] tabItems = new String[]{"MEDICAL", "GROOMING", "BOARDING"};
    private int pageMedicalRecord = 0, lastPageMedicalRecord = 1;
    private int pageGroomingRecord = 0, lastPageGroomingRecord = 1;
    private int pageBoardingRecord = 0, lastPageBoardingRecord = 1;
    private ProgressDialog dialog;

    @BindView(R.id.iv_search)
    ImageView ivSearch;
    private String tanggal;


    public static final int REQ_CAMERA = 1001;
    public static final int REQ_GALLERY = 1002;
    private static final int STORAGE_PERMISSION_CAMERA = 112;
    private static final int CAMERA_BUT_STORAGE_PERMISSION = 116;

    Bitmap bitmap;
    Uri uri;
    ByteArrayOutputStream byteStream;
    private SelectCustomerPresenter presenter;

    private boolean isClearCustomer, isClearPet;

    public static SelectCustomerFragment newInstance(boolean fromCustomer) {
        SelectCustomerFragment fragment = new SelectCustomerFragment();
        fragment.fromCustomer = fromCustomer;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.ctx = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_select_customer, container, false);
        presenter = new SelectCustomerPresenter(getActivity(), TAG, this);
        state = getArguments().getString("state");
        tanggal = getArguments().getString("tanggal");
        fromCustomer = getArguments().getBoolean("fromCustomer");
        unbinder = ButterKnife.bind(this, v);
        //filePickUtils = new FilePickUtils(this, onFileChoose);
        //lifeCycleCallBackManager = filePickUtils.getCallBackManager();

        if (new SharedPreference(getActivity()).isCustomer()){
            list_customer.setVisibility(View.GONE);
            getDetailCustomer(Integer.valueOf(new SharedPreference(getActivity()).getCustomerId()));
        }
        setAdapter();
        ll_include_layout.setVisibility(View.GONE);
        progressbar.setVisibility(View.GONE);
        if (TreatmentHelper.getInstance().isShop()) {
            llPetlayout.setVisibility(View.GONE);
            getCustomer(GlobalVariable.LIMIT_ITEM + "", "");
            txtSearchPet.setHint(R.string.searchOwner);
        } else {
            if (state.equals("customer")) {
                isSearchCustomer = false;
                getCustomerPage(GlobalVariable.LIMIT_ITEM + "", "");
                txtSearchPet.setHint(R.string.searchPet);
            } else {
                getPetList(GlobalVariable.LIMIT_ITEM + "", "");
                txtSearchPet.setHint(R.string.searchPet);
            }
        }


        if (fromCustomer) {
            txtSelectCustomer.setText("Customer");
            btnSelectCustomer.setVisibility(View.GONE);
            btnSeeAllTreatment.setVisibility(View.VISIBLE);

        } else {
            txtSelectCustomer.setText("Select Customer");
        }


        refreshPet.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshPet.setRefreshing(false);
                resetSearchPage();
                if (fromCustomer) {
                    isSearchCustomer = false;
                    adapterPet.clearData();
                    getCustomerPage(GlobalVariable.LIMIT_ITEM + "", "");
                    txtSearchPet.setHint(R.string.searchPet);
                } else if (TreatmentHelper.getInstance().isShop()) {
                    llPetlayout.setVisibility(View.GONE);
                    adapterPet.clearData();
                    getCustomer(GlobalVariable.LIMIT_ITEM + "", "");
                    txtSearchPet.setHint(R.string.searchOwner);
                } else {
                    adapterPet.clearData();
                    getPetList(GlobalVariable.LIMIT_ITEM + "", "");
                    txtSearchPet.setHint(R.string.searchPet);

                }
            }
        });


        ivPicOwner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAddCustomer || btnSaveOwner.getTag().equals("edit")) {
                    registerForContextMenu(ivPicOwner);
                    getActivity().openContextMenu(ivPicOwner);
                    unregisterForContextMenu(ivPicOwner);
                    isAddCustomer = true;
                }
            }
        });
        ivPicPet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAddPet || btnSavePet.getTag().equals("edit")) {
                    registerForContextMenu(ivPicPet);
                    getActivity().openContextMenu(ivPicPet);
                    unregisterForContextMenu(ivPicPet);
                    isAddCustomer = false;
                }
            }
        });

        txtSearchPet.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionID, KeyEvent keyEvent) {
                if (actionID == EditorInfo.IME_ACTION_SEARCH) {

                    resetSearchPage();
                    if (fromCustomer) {
                        adapterPet.clearData();
                        if (!isSearchCustomer) {
                            isSearchCustomer = true;
                            getCustomerPage(GlobalVariable.LIMIT_ITEM + "", txtSearchPet.getText().toString());
                        } else {
                            getCustomerPage(GlobalVariable.LIMIT_ITEM + "", txtSearchPet.getText().toString());
                        }
                    } else if (TreatmentHelper.getInstance().isShop()) {
                        adapterPet.clearData();
                        getCustomer(GlobalVariable.LIMIT_ITEM + "", txtSearchPet.getText().toString());
                    } else {
                        adapterPet.clearData();
                        getPetList(GlobalVariable.LIMIT_ITEM + "", txtSearchPet.getText().toString());
                    }
                    Utils.hideSoftKeyboardUsingView(getActivity(), txtSearchPet);
                    return false;
                }
                return false;
            }
        });

        txtSearchPet.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.toString().length() == 0) {
                    Utils.hideSoftKeyboardUsingView(getActivity(), txtSearchPet);
                    resetSearchPage();
                    if (fromCustomer) {
                        adapterPet.clearData();
                        getCustomerPage(GlobalVariable.LIMIT_ITEM + "", "");
                    } else if (TreatmentHelper.getInstance().isShop()) {
                        adapterPet.clearData();
                        getCustomer(GlobalVariable.LIMIT_ITEM + "", "");
                    } else {
                        adapterPet.clearData();
                        getPetList(GlobalVariable.LIMIT_ITEM + "", "");
                    }
                }
            }
        });

        return v;
    }

    private void resetSearchPage() {
        page = 0;
        p = 0;
        lastpage = 1;
    }


    private void setAdapter() {
        adapterPet = new PetAdapter(ctx);
        adapterPet.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                llOwnerProfile.setVisibility(View.VISIBLE);
                llOwnerProfileEdit.setVisibility(View.GONE);
                btnSelectCustomer.setVisibility(View.VISIBLE);
                llPet.setVisibility(View.VISIBLE);
                llTab.setVisibility(View.VISIBLE);
                btnSaveOwner.setVisibility(View.GONE);
                rlDontHavePet.setVisibility(View.GONE);
                btnEditOwner.setVisibility(View.VISIBLE);
                nothingTreatmentRecord.setVisibility(View.GONE);
                if (fromCustomer) {
                    btnSelectCustomer.setVisibility(View.GONE);
                    btnSeeAllTreatment.setVisibility(View.VISIBLE);
                }

                petTab.removeAllTabs();
                if (TreatmentHelper.getInstance().isShop()) {
                    llPetlayout.setVisibility(View.GONE);
                }

                isClearCustomer = false;
                isClearPet = false;

                if (adapterPet.getDatas() != null && adapterPet.getDatas().size() != 0) {
                    isAddCustomer = false;
                    selectedPetId = adapterPet.getDatas().get(position).getPetId();
                    getDetailCustomer(adapterPet.getDatas().get(position).getCustomerId());
                } else if (adapterPet.getDatasCustomer() != null && adapterPet.getDatasCustomer().size() != 0) {
                    isAddCustomer = false;
                    getDetailCustomer(Integer.parseInt(adapterPet.getDatasCustomer().get(position).getCustomerId()));
                }

            }
        });
        layoutManager = new LinearLayoutManager(ctx);
        rvPetlist.setLayoutManager(layoutManager);
        rvPetlist.setAdapter(adapterPet);
        rvPetlist.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
                if (page != lastpage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= adapterPet.getItemCount()) {

                        if (TreatmentHelper.getInstance().isShop()) {
                            getCustomer(GlobalVariable.LIMIT_ITEM + "", "");
                        } else {
                            if (state.equals("customer")) {
                                getCustomerPage(GlobalVariable.LIMIT_ITEM + "", "");
                            } else {
                                getPetList(GlobalVariable.LIMIT_ITEM + "", "");
                            }
                        }


                    }
                }
            }
        });


        petTab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                DetailCustomerModel.DATABean.PetsBean pet = selectedDetailCustomer.getPets().get((Integer) tab.getTag());
                if (!pet.isFinish()) {
                    showCreateNewPet();
                    return;
                } else {
                    showSelectedPet(pet);
                    if (fromCustomer) {
                        btnSelectCustomer.setVisibility(View.GONE);
                        btnSeeAllTreatment.setVisibility(View.VISIBLE);
                    }

                    if (!isAddPet) {
                        btnEditPet.setVisibility(View.VISIBLE);
                    } else {
                        btnEditPet.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        adapterLastMedic = new LastMedicAdapter(getActivity());
        rvLastMedic.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvLastMedic.setAdapter(adapterLastMedic);

    }

    private void showSelectedPet(DetailCustomerModel.DATABean.PetsBean pet) {
        isAddPet = false;
        selectedPet = pet;

        llLastMedic.setVisibility(View.VISIBLE);
        llPetProfileEdit.setVisibility(View.GONE);
        llSavePet.setVisibility(View.GONE);

        llPetProfile.setVisibility(View.VISIBLE);
        btnSelectCustomer.setVisibility(View.VISIBLE);


        txtPetName.setText(pet.getPetName());
        txtRace.setText(pet.getPetRace());
        txtGender.setText(Utils.getInstance().getPetGender()[Integer.parseInt(pet.getPetGender())]);
        txtBreed.setText(pet.getPetBreed());
        txtColor.setText(pet.getPetColor());
        txtMicrochip.setText(pet.getPetMicrochip());
        txtSpecialNotes.setText(pet.getPetSpecialNotes());

        pbLoadingIvPet.setVisibility(View.VISIBLE);
        ivPicPet.setVisibility(View.GONE);
        Picasso.get().load(pet.getPetPhoto())
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .tag(TAG)
                .into(ivPicPet, new Callback() {
                    @Override
                    public void onSuccess() {
                        pbLoadingIvPet.setVisibility(View.GONE);
                        ivPicPet.setVisibility(View.VISIBLE);

                    }

                    @Override
                    public void onError(Exception e) {
                        ivPicPet.setImageDrawable(getResources().getDrawable(R.drawable.pic_pet));
                        pbLoadingIvPet.setVisibility(View.GONE);
                        ivPicPet.setVisibility(View.VISIBLE);
                        e.printStackTrace();
                    }
                });

        txtDob.setText(Utils.changeDateFormat(pet.getPetBirthday(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME_FULL));
        nothingTreatmentRecord.setVisibility(View.GONE);
        getPetTreatmentRecord(pet.getPetId());
    }

    private void showCreateNewPet() {
        llPetProfileEdit.setVisibility(View.VISIBLE);
        llSavePet.setVisibility(View.VISIBLE);
        llPetProfile.setVisibility(View.GONE);
        btnSelectCustomer.setVisibility(View.GONE);
        llLastMedic.setVisibility(View.GONE);

        txtPetNameEdit.setText("");
        txtRaceEdit.setText("");
        txtGenderEdit.setText("");
        txtDobEdit.setText("");
        txtBreedEdit.setText("");
        txtColorEdit.setText("");
        txtSpecialNotesEdit.setText("");
        txtMicrochipEdit.setText("");
        ivPicPet.setImageDrawable(getResources().getDrawable(R.drawable.no_image));
        selectedImagePath = "";
        isAddPet = true;
    }

    private void getPetTreatmentRecord(int petId) {
        nothingTreatmentRecord.setVisibility(View.GONE);
        loadingTreatmentRecord.setVisibility(View.VISIBLE);
        if (!fromCustomer) {
            btnSelectCustomer.setVisibility(View.GONE);
        }
        adapterLastMedic.getDatas().clear();
        adapterLastMedic.notifyDataSetChanged();
        presenter.getPetTreatmentRecord(petId);
    }

    @Override
    public void onStop() {
        resetSearchPage();
        pageMedicalRecord = 0;
        lastPageMedicalRecord = 1;
        pageGroomingRecord = 0;
        lastPageGroomingRecord = 1;
        pageBoardingRecord = 0;
        lastPageBoardingRecord = 1;

        super.onStop();
        Log.e(TAG, "onstop");

    }

    @OnClick(R.id.btn_cancel_owner)
    public void cancelOwner() {
        llLastMedic.setVisibility(View.VISIBLE);
        llPetProfile.setVisibility(View.VISIBLE);
        btnSelectCustomer.setVisibility(View.VISIBLE);

        llOwnerProfile.setVisibility(View.VISIBLE);
        llOwnerProfileEdit.setVisibility(View.GONE);
        llPet.setVisibility(petTab.getTabCount() > 0 ? View.VISIBLE : View.GONE);
        llTab.setVisibility(View.VISIBLE);
        btnEditOwner.setVisibility(View.VISIBLE);
        btnSaveOwner.setVisibility(View.GONE);
        btnCancelOwner.setVisibility(View.GONE);
        rlDontHavePet.setVisibility(petTab.getTabCount() == 0 ? View.VISIBLE : View.GONE);
        btnSelectCustomer.setVisibility(state.equals("customer") ? View.GONE : View.VISIBLE);
    }

    @OnClick(R.id.btn_edit_owner)
    public void modeEditCustomer() {
        llPetProfileEdit.setVisibility(View.GONE);
        llSavePet.setVisibility(View.GONE);
        llLastMedic.setVisibility(View.GONE);
        llPetProfile.setVisibility(View.GONE);
        btnSelectCustomer.setVisibility(View.GONE);
        btnEditPet.setVisibility(View.GONE);

        llOwnerProfile.setVisibility(View.GONE);
        llOwnerProfileEdit.setVisibility(View.VISIBLE);
        llPet.setVisibility(View.GONE);
        llTab.setVisibility(View.GONE);
        btnEditOwner.setVisibility(View.GONE);
        btnSaveOwner.setVisibility(View.VISIBLE);
        btnCancelOwner.setVisibility(View.VISIBLE);
        rlDontHavePet.setVisibility(View.GONE);
        btnSaveOwner.setTag("edit");
        selectedImagePath = "";
        edPhone.setText(selectedDetailCustomer.getCustomerPhone());
        edAddress.setText(selectedDetailCustomer.getCustomerAddress());
        edEmail.setText(selectedDetailCustomer.getCustomerEmail());
        edOwnerName.setText(selectedDetailCustomer.getCustomerName());
    }

    @OnClick(R.id.btn_add_new_customer)
    public void showEditCustomer() {


        isAddCustomer = true;
        selectedImagePath = "";
        edPhone.setText("");
        edAddress.setText("");
        edEmail.setText("");
        edOwnerName.setText("");
        ivPicOwner.setImageDrawable(getResources().getDrawable(R.drawable.no_image));
        txtTotalPets.setText("0 Pet");
        ll_include_layout.setVisibility(View.VISIBLE);

        llOwnerProfile.setVisibility(View.GONE);
        llOwnerProfileEdit.setVisibility(View.VISIBLE);
        llPet.setVisibility(View.GONE);
        btnSelectCustomer.setVisibility(View.GONE);
        llTab.setVisibility(View.GONE);
        btnSaveOwner.setVisibility(View.VISIBLE);
        btnEditOwner.setVisibility(View.GONE);
        petTab.removeAllTabs();
        rlDontHavePet.setVisibility(View.GONE);
        if (fromCustomer) {
            btnSelectCustomer.setVisibility(View.GONE);
            btnSeeAllTreatment.setVisibility(View.VISIBLE);
        }

        if (TreatmentHelper.getInstance().isShop()) {
            llPetlayout.setVisibility(View.GONE);
        }

    }


    @OnClick(R.id.txt_search_pet)
    public void showProfileCustomer() {
    }


    private boolean validasiNewCustomer() {

        boolean valid = true;
        if (TextUtils.isEmpty(edOwnerName.getText().toString().trim())) {
            edOwnerName.setError("Please insert customer name");
            valid = false;
        }
        if (TextUtils.isEmpty(edEmail.getText().toString().trim())) {
            edEmail.setError("Please insert customer email");
            valid = false;
        }
        if (TextUtils.isEmpty(edPhone.getText().toString().trim())) {
            edPhone.setError("Please insert customer phone");
            valid = false;
        }
        if (TextUtils.isEmpty(edAddress.getText().toString().trim())) {
            edAddress.setError("Please insert customer address");
            valid = false;
        }
        if (TextUtils.isEmpty(selectedImagePath) && !btnSaveOwner.getTag().equals("edit")) {
            Toast.makeText(getActivity(), "Please select customer image", Toast.LENGTH_SHORT).show();
            valid = false;
        }
        return valid;
    }

    @OnClick(R.id.btn_add_new_pet)
    public void showEditPet() {
        selectedImagePath = "";

        btnEditPet.setVisibility(View.GONE);
        btnSelectCustomer.setVisibility(View.GONE);
        llLastMedic.setVisibility(View.GONE);
        llSavePet.setVisibility(View.VISIBLE);

        rlDontHavePet.setVisibility(View.GONE);
        llPet.setVisibility(View.VISIBLE);
        llPetProfile.setVisibility(View.GONE);
        llPetProfileEdit.setVisibility(View.VISIBLE);

        if (fromCustomer) {
            btnSelectCustomer.setVisibility(View.GONE);
            btnSeeAllTreatment.setVisibility(View.VISIBLE);
        }


        for (int i = 0; i < selectedDetailCustomer.getPets().size(); i++) {
            DetailCustomerModel.DATABean.PetsBean ownerPet = selectedDetailCustomer.getPets().get(i);
            if (!ownerPet.isFinish()) {
                petTab.getTabAt(i).select();
                return;
            }
        }

        DetailCustomerModel.DATABean.PetsBean newPet = new DetailCustomerModel.DATABean.PetsBean();
        newPet.setPetName("New Pet");
        newPet.setFinish(false);
        selectedDetailCustomer.getPets().add(newPet);
        petTab.addTab(petTab.newTab().setTag(selectedDetailCustomer.getPets().size() - 1).setText("New Pet"));


        for (int i = 0; i < selectedDetailCustomer.getPets().size(); i++) {
            DetailCustomerModel.DATABean.PetsBean ownerPet = selectedDetailCustomer.getPets().get(i);
            if (!ownerPet.isFinish()) {
                petTab.getTabAt(i).select();
            }
        }

    }

    @OnClick(R.id.btn_edit_pet)
    public void modeEditPet() {
        llPetProfileEdit.setVisibility(View.VISIBLE);
        llSavePet.setVisibility(View.VISIBLE);
        llLastMedic.setVisibility(View.GONE);
        llPetProfile.setVisibility(View.GONE);
        btnSelectCustomer.setVisibility(View.GONE);
        btnEditPet.setVisibility(View.GONE);
        btnCancelPet.setTag("edit");
        btnSavePet.setTag("edit");

        txtPetNameEdit.setText(selectedPet.getPetName());
        txtRaceEdit.setText(selectedPet.getPetRace());
        txtBreedEdit.setText(selectedPet.getPetBreed());
        txtSpecialNotesEdit.setText(selectedPet.getPetSpecialNotes());
        txtMicrochipEdit.setText(selectedPet.getPetMicrochip());
        txtColorEdit.setText(selectedPet.getPetColor());
        selectedPetGender = Integer.parseInt(selectedPet.getPetGender());
        txtDobEdit.setText((Utils.changeDateFormat(selectedPet.getPetBirthday(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME_FULL)));
        txtGenderEdit.setText(Utils.getInstance().getPetGender()[Integer.parseInt(selectedPet.getPetGender())]);
    }

    @OnClick(R.id.btn_cancel_pet)
    public void onPetCancel(View view) {
        if (view.getTag().equals("edit")) {
            view.setTag("");
            btnSavePet.setTag("");
            llPetProfileEdit.setVisibility(View.GONE);
            llSavePet.setVisibility(View.GONE);
            llLastMedic.setVisibility(View.VISIBLE);
            llPetProfile.setVisibility(View.VISIBLE);
            btnSelectCustomer.setVisibility(View.VISIBLE);
            btnEditPet.setVisibility(View.VISIBLE);
            return;
        }
        for (int i = 0; i < selectedDetailCustomer.getPets().size(); i++) {
            DetailCustomerModel.DATABean.PetsBean ownerPet = selectedDetailCustomer.getPets().get(i);
            if (!ownerPet.isFinish()) {
                selectedDetailCustomer.getPets().remove(i);
                petTab.removeTabAt(i);
                if (selectedDetailCustomer.getPets().size() == 0) {
                    rlDontHavePet.setVisibility(View.VISIBLE);
                    llPet.setVisibility(View.GONE);
                    llSavePet.setVisibility(View.GONE);
                }
            }
        }


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Connector.newInstance(getActivity()).close(TAG);
        Picasso.get().cancelTag(TAG);
        unbinder.unbind();
    }


    @OnClick(R.id.iv_search)
    public void search() {


        resetSearchPage();

        if (fromCustomer) {
            adapterPet.clearData();
            if (!isSearchCustomer) {
                isSearchCustomer = true;
                getCustomerPage(GlobalVariable.LIMIT_ITEM + "", txtSearchPet.getText().toString());
            } else {
                getCustomerPage(GlobalVariable.LIMIT_ITEM + "", txtSearchPet.getText().toString());
            }
        } else if (TreatmentHelper.getInstance().isShop()) {
            adapterPet.clearData();
            getCustomer(GlobalVariable.LIMIT_ITEM + "", txtSearchPet.getText().toString());
        } else {
            adapterPet.clearData();
            getPetList(GlobalVariable.LIMIT_ITEM + "", txtSearchPet.getText().toString());
        }
        Utils.hideSoftKeyboardUsingView(getActivity(), ivSearch);

    }


    private void getCustomerByPet(String limit, String search) {

        if (adapterPet.getDatasCustomer().size() == 0) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
            progressbar.setLayoutParams(layoutParams);
        } else {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
            progressbar.setLayoutParams(layoutParams);
        }
        setLoading(true);
        rvPetlist.setVisibility(View.VISIBLE);

        int laman = getLaman(search);


        presenter.getCustomerByPet(limit, search, laman);

    }

    public void setLoading(Boolean visible) {
        try {
            if (visible) {
                progressbar.setVisibility(View.VISIBLE);
            } else {
                progressbar.setVisibility(View.GONE);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    void getPetList(String limit, String search) {
        if (page == lastpage) {
            return;
        }
        if (adapterPet.getDatas().size() == 0) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
            progressbar.setLayoutParams(layoutParams);
            adapterPet.clearData();
        } else {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
            progressbar.setLayoutParams(layoutParams);
        }
        setLoading(true);
        rvPetlist.setVisibility(View.VISIBLE);
        int laman = getLaman(search);

        presenter.getPetList(limit, search, laman);

    }

    @Override
    public void onGetPetTreatmentRecord(List<GetTreatmentRecord.DATABean> dataBeans) {
        adapterLastMedic.setDatas(dataBeans);
        if (dataBeans.size() == 0) {
            nothingTreatmentRecord.setVisibility(View.VISIBLE);
        } else {
            nothingTreatmentRecord.setVisibility(View.GONE);

        }
        loadingTreatmentRecord.setVisibility(View.GONE);
        if (!fromCustomer) {
            btnSelectCustomer.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onGetCustomerByPet(CustomerModel.DATABean dataBeans) {
        page = Integer.parseInt(dataBeans.getPage());
        lastpage = Integer.parseInt(dataBeans.getLastPage());
        adapterPet.setDatas(null, dataBeans.getItems());
        rvPetlist.setVisibility(View.VISIBLE);
        isSearchCustomer = false;
        setLoading(false);
    }

    @Override
    public void onGetPetList(PetListModel.DATABean dataBeans) {
        page = dataBeans.getPage();
        lastpage = dataBeans.getLastPage();
        adapterPet.setDatas(dataBeans.getItems(), null);
        rvPetlist.setVisibility(View.VISIBLE);
        setLoading(false);

    }

    @Override
    public void onGetCustomerPage(PetListModel.DATABean dataBeans) {
        page = dataBeans.getPage();
        lastpage = dataBeans.getLastPage();
        adapterPet.setDatas(dataBeans.getItems(), null);
        rvPetlist.setVisibility(View.VISIBLE);
        setLoading(false);
    }

    void getCustomerPage(String limit, String search) {
        if (page == lastpage) {
            return;
        }
        if (adapterPet.getDatas().size() == 0) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
            progressbar.setLayoutParams(layoutParams);
            adapterPet.clearData();
        } else {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
            progressbar.setLayoutParams(layoutParams);
        }
        setLoading(true);
        rvPetlist.setVisibility(View.VISIBLE);
        int laman = getLaman(search);


        presenter.getCustomerPage(limit, search, laman);

    }

    void getCustomer(String limit, String search) {
        if (page == lastpage) {
            return;
        }
        if (adapterPet.getDatasCustomer().size() == 0) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
            progressbar.setLayoutParams(layoutParams);
        } else {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
            progressbar.setLayoutParams(layoutParams);
        }
        setLoading(true);
        rvPetlist.setVisibility(View.VISIBLE);


        int laman = getLaman(search);

        presenter.getCustomer(limit, search, laman);
    }

    private int getLaman(String search) {
        if (search.length() != 0) {
            return 0;
        } else {
            return page + 1;
        }
    }

    @Override
    public void onGetCustomer(CustomerModel.DATABean dataBeans) {
        page = Integer.parseInt(dataBeans.getPage());
        lastpage = Integer.parseInt(dataBeans.getLastPage());
        adapterPet.setDatas(null, dataBeans.getItems());
        rvPetlist.setVisibility(View.VISIBLE);

        setLoading(false);

    }


    void getDetailCustomer(int custId) {
        dialog = Utils.showProgressDialog(getActivity(), false, "Loading...");
        ll_include_layout.setVisibility(View.GONE);

        presenter.getDetailCustomer(custId + "");

    }

    @Override
    public void onGetDetailCustomer(DetailCustomerModel.DATABean dataBean) {
        selectedDetailCustomer = dataBean;
        Utils.dismissProgressDialog(dialog);
        ll_include_layout.setVisibility(View.VISIBLE);

        txtOwnerName.setText(dataBean.getCustomerName());
        txtEmail.setText(dataBean.getCustomerEmail());
        txtPhone.setText(dataBean.getCustomerPhone());
        txtAddress.setText(dataBean.getCustomerAddress());

        ivPicOwner.setVisibility(View.GONE);
        pbLoadingIvCustomer.setVisibility(View.VISIBLE);
        Picasso.get().load(dataBean.getCustomerPhoto())
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .tag(TAG)
                .into(ivPicOwner, new Callback() {
                    @Override
                    public void onSuccess() {
                        pbLoadingIvCustomer.setVisibility(View.GONE);
                        ivPicOwner.setVisibility(View.VISIBLE);

                    }

                    @Override
                    public void onError(Exception e) {
                        ivPicOwner.setImageDrawable(getResources().getDrawable(R.drawable.pic));
                        pbLoadingIvCustomer.setVisibility(View.GONE);
                        ivPicOwner.setVisibility(View.VISIBLE);
                        e.printStackTrace();
                    }
                });
        if (dataBean.getPets().size() > 1) {
            txtTotalPets.setText(dataBean.getPets().size() + " Pets");
        } else {
            txtTotalPets.setText(dataBean.getPets().size() + " Pet");
        }

        if (dataBean.getPets().size() == 0) {
            rlDontHavePet.setVisibility(View.VISIBLE);
            llPet.setVisibility(View.GONE);
            llTab.setVisibility(View.VISIBLE);
        } else {
            rlDontHavePet.setVisibility(View.GONE);
            llPet.setVisibility(View.VISIBLE);
            llTab.setVisibility(View.VISIBLE);
            llLastMedic.setVisibility(View.VISIBLE);
        }


        TabLayout.Tab selectedTab = null;
        for (int i = 0; i < dataBean.getPets().size(); i++) {
            DetailCustomerModel.DATABean.PetsBean petBean = dataBean.getPets().get(i);
            petTab.addTab(petTab.newTab().setTag(i).setText(petBean.getPetName()));
            if (petBean.getPetId() == selectedPetId) {
                selectedTab = petTab.getTabAt(i);
            }
        }

        if (selectedTab != null) {
            selectedTab.select();
        }

        if (isClearCustomer) {
            if (fromCustomer) {
                getCustomerPage(GlobalVariable.LIMIT_ITEM + "", "");

            } else {
                getCustomer(GlobalVariable.LIMIT_ITEM + "", "");
            }
        }
        if (isClearPet) {
            getPetList(GlobalVariable.LIMIT_ITEM + "", "");
        }

    }

    @OnClick(R.id.btn_save_owner)
    public void saveOwner(View view) {
        if (!validasiNewCustomer()) {
            return;
        }

        ll_include_layout.setVisibility(View.GONE);
        dialog = Utils.showProgressDialog(getActivity(), false, "Loading...");

        HashMap<String, RequestBody> data = new HashMap<>();
        data.put("customer_name", RequestBody.create(MultipartBody.FORM, edOwnerName.getText().toString().trim()));
        data.put("customer_email", RequestBody.create(MultipartBody.FORM, edEmail.getText().toString().trim()));
        data.put("customer_address", RequestBody.create(MultipartBody.FORM, edAddress.getText().toString().trim()));
        data.put("customer_phone", RequestBody.create(MultipartBody.FORM, edPhone.getText().toString().trim()));


        String edit = "";
        boolean isEdit = false;
        if (view.getTag().equals("edit")) {
            data.put("customer_photo_old", RequestBody.create(MultipartBody.FORM, selectedDetailCustomer.getCustomerPhotoOld()));
            edit = "/" + selectedDetailCustomer.getCustomerId();
            btnSaveOwner.setTag("");
            btnCancelOwner.setTag("");
            isEdit = true;
        }
        MultipartBody.Part CUSTOMER_PHOTO = null;

        if (!selectedImagePath.isEmpty()) {
            File file = new File(selectedImagePath);
            RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
            CUSTOMER_PHOTO =
                    MultipartBody.Part.createFormData("customer_photo", file.getName(), requestFile);
        } else {
            RequestBody requestFile = RequestBody.create(MultipartBody.FORM, "");

            CUSTOMER_PHOTO = MultipartBody.Part.createFormData("customer_photo", "", requestFile);
        }
        presenter.saveOwner(data, CUSTOMER_PHOTO, edit, isEdit);
    }


    @Override
    public void onSaveOwner(SaveOwner data) {
        ll_include_layout.setVisibility(View.VISIBLE);
        dialog.dismiss();

        if (data.isEdit()) {
            adapterPet.clearData();
            resetSearchPage();
            if (fromCustomer) {
                getCustomerPage(GlobalVariable.LIMIT_ITEM + "", "");

            } else {
                getCustomer(GlobalVariable.LIMIT_ITEM + "", "");

            }
            cancelOwner();
        }

        llOwnerProfile.setVisibility(View.VISIBLE);
        llOwnerProfileEdit.setVisibility(View.GONE);
        btnSaveOwner.setVisibility(View.GONE);
        btnSelectCustomer.setVisibility(View.GONE);
        if (fromCustomer) {
            btnSelectCustomer.setVisibility(View.GONE);
            btnSeeAllTreatment.setVisibility(View.VISIBLE);
        }
        if (TreatmentHelper.getInstance().isShop()) {
            llPetlayout.setVisibility(View.GONE);
            btnSelectCustomer.setVisibility(View.VISIBLE);
        }
        petTab.removeAllTabs();
        isAddCustomer = false;
        if (TreatmentHelper.getInstance().isShop() || fromCustomer) {
            resetSearchPage();
            adapterPet.clearData();
            isClearCustomer = true;
            isClearPet = false;
            getDetailCustomer(data.getData().getCustomerId());
        } else {
            isClearCustomer = false;
            isClearPet = true;
            getDetailCustomer(data.getData().getCustomerId());
        }

    }

    @OnClick(R.id.btn_see_all_treatment)
    public void onViewMedicalRecords() {
        DialogSeeAllTreatment dialog = DialogSeeAllTreatment.newInstance(selectedDetailCustomer, selectedPet);
        dialog.show(getChildFragmentManager(), "");
    }

    @OnClick(R.id.btn_select_customer)
    public void onSelectCustomer() {
        TreatmentHelper.getInstance().setSelectedCustomer(selectedDetailCustomer);
        if (TreatmentHelper.getInstance().isShop()) {
            EventBus.getDefault().post(new NavigateToShop(""));
            TreatmentHelper.getInstance().setselectedPet(null);
        } else {
            if (selectedPet == null) return;
            TreatmentHelper.getInstance().setselectedPet(selectedPet);
            if (TreatmentHelper.getInstance().isOrder()) {
                ((MainActivity) getActivity()).toTreatmentOrder(0);
            } else {
                ((MainActivity) getActivity()).toTreatmentReserv(0, tanggal);
            }
        }
    }


    @Override
    public void onFailGettingData(OnFailGettingData data) {
        if (data.getApi() == OnFailGettingData.GET_TREATMENT_RECORD) {
            loadingTreatmentRecord.setVisibility(View.GONE);
            if (!fromCustomer) {
                btnSelectCustomer.setVisibility(View.VISIBLE);
            }
            Toast.makeText(getActivity(), data.getThrowable().getMessage(), Toast.LENGTH_SHORT).show();
            data.getThrowable().printStackTrace();
        }
        if (data.getApi() == OnFailGettingData.GET_CUSTOMER_BY_PET) {
            rvPetlist.setVisibility(View.VISIBLE);
            Utils.showToast(getActivity(), data.getThrowable().getMessage());
            data.getThrowable().printStackTrace();
            setLoading(false);
            isSearchCustomer = false;
        }
        if (data.getApi() == OnFailGettingData.GET_PET_LIST) {
            try {
                rvPetlist.setVisibility(View.VISIBLE);
                Utils.showToast(getActivity(), data.getThrowable().getMessage());
                setLoading(false);
                data.getThrowable().printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        if (data.getApi() == OnFailGettingData.GET_CUSTOMER) {
            setLoading(false);

            try {
                rvPetlist.setVisibility(View.VISIBLE);
                Utils.showToast(getActivity(), data.getThrowable().getMessage());
                data.getThrowable().printStackTrace();
                setLoading(false);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        if (data.getApi() == OnFailGettingData.GET_DETAIL_CUSTOMER) {

            Utils.showToast(getActivity(), data.getThrowable().getMessage());
            Utils.dismissProgressDialog(dialog);
            ll_include_layout.setVisibility(View.GONE);
            data.getThrowable().printStackTrace();
        }

        if (data.getApi() == OnFailGettingData.SAVE_OWNER) {

            ll_include_layout.setVisibility(View.VISIBLE);
            dialog.dismiss();

            Toast.makeText(getActivity(), data.getThrowable().getMessage(), Toast.LENGTH_SHORT).show();
            data.getThrowable().printStackTrace();
        }

    }
}