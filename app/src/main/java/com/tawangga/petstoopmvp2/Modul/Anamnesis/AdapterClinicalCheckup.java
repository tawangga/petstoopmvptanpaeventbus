package com.tawangga.petstoopmvp2.Modul.Anamnesis;

import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Model.GetOrderConfirmAnamnesis;
import com.tawangga.petstoopmvp2.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterClinicalCheckup extends RecyclerView.Adapter<AdapterClinicalCheckup.ViewHolder> {

    private List<GetOrderConfirmAnamnesis.DATABean.ClinicalCheckupsBean> data;
    private boolean isDoctor;
    private Context context;
    private AdapterView.OnItemClickListener onItemClickListener;

    public AdapterClinicalCheckup(Context context) {

        this.context = context;
        data = new ArrayList<>();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_clinical_checkup, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.position = position;
        final long identity = System.currentTimeMillis();
        holder.identity = identity;


        GetOrderConfirmAnamnesis.DATABean.ClinicalCheckupsBean selectedData = data.get(position);
        if (selectedData.isExam()) {
            holder.btnCancel.setVisibility(View.VISIBLE);
        } else {
            holder.btnCancel.setVisibility(View.GONE);
        }

        holder.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                data.remove(position);
                notifyDataSetChanged();
            }
        });


        holder.tvClinicalCheckup.setText(position + 1 + ". " + selectedData.getClinicalCheckupName());
        holder.tvUnit.setText(selectedData.getClinicalCheckupUnit());
        holder.edValue.setText(selectedData.getValue() + "");
        if (isDoctor) {
            selectedData.setClinicalCheckupIsPriority(1);
        }
        if (selectedData.getClinicalCheckupIsPriority() == 0) {
            holder.edValue.setEnabled(false);
            holder.tvUnit.setTextColor(context.getResources().getColor(R.color.disabled_color));
            holder.tvClinicalCheckup.setTextColor(context.getResources().getColor(R.color.disabled_color));
        } else {
            holder.edValue.setEnabled(true);
            holder.tvUnit.setTextColor(context.getResources().getColor(R.color.black));
            holder.tvClinicalCheckup.setTextColor(context.getResources().getColor(R.color.black));
        }

        if (selectedData.getClinicalCheckupType().equals("1")) {
            holder.edValue.setInputType(InputType.TYPE_CLASS_TEXT);
            if (selectedData.getValue().equals("0")) {
                holder.edValue.setText("");
                selectedData.setValue("");
            }
        } else
            holder.edValue.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_CLASS_NUMBER); //for decimal numbers


        holder.edValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() != 0) {
                    selectedData.setValue(s.toString().trim());
                } else {
                    selectedData.setValue("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        holder.edValue.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    if (data.size() == position + 1) {
                        Utils.hideSoftKeyboardUsingView(context, holder.edValue);
                        return true;
                    }
                    if (data.get(position + 1).getClinicalCheckupIsPriority() == 0) {
                        Utils.hideSoftKeyboardUsingView(context, holder.edValue);
                        return true;

                    }
                }
                return false;
            }
        });


    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setDatas(List<GetOrderConfirmAnamnesis.DATABean.ClinicalCheckupsBean> data, boolean isDoctor) {
        this.data = data;
        this.isDoctor = isDoctor;
        notifyDataSetChanged();
    }

    public List<GetOrderConfirmAnamnesis.DATABean.ClinicalCheckupsBean> getData() {
        return data;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        int position;
        long identity;
        @BindView(R.id.tv_clinical_checkup)
        TextView tvClinicalCheckup;
        @BindView(R.id.tv_unit)
        TextView tvUnit;
        @BindView(R.id.ed_value)
        TextView edValue;
        @BindView(R.id.btn_cancel)
        ImageView btnCancel;
        @BindView(R.id.ll_background)
        LinearLayout llBackground;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(null, v, position, 0);
            }

        }
    }
}