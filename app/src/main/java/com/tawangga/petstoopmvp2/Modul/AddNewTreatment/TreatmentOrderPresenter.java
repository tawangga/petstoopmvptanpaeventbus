package com.tawangga.petstoopmvp2.Modul.AddNewTreatment;

import android.content.Context;

import com.tawangga.petstoopmvp2.EventBus.OnCancelTreatmentService;
import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.EventBus.OnSuccessReservTreatment;
import com.tawangga.petstoopmvp2.ModelNew.CreateCart;
import com.tawangga.petstoopmvp2.ModelNew.GetBoarding;
import com.tawangga.petstoopmvp2.ModelNew.GetDoctor;
import com.tawangga.petstoopmvp2.ModelNew.GetDoctorSchedule;
import com.tawangga.petstoopmvp2.ModelNew.GetGroomingLocation;
import com.tawangga.petstoopmvp2.Param.CancelServiceParam;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import java.util.Map;

import okhttp3.RequestBody;

public class TreatmentOrderPresenter {
    private final Context context;
    private final String tag;
    private final AddNewTreatmentView view;

    public TreatmentOrderPresenter(Context context, String TAG, AddNewTreatmentView view) {
        this.context = context;
        tag = TAG;
        this.view = view;
    }

    public void getDoctor(Map<String, String> param) {
        Connector.newInstance(context).getDoctor(param, new Connector.ApiCallback<GetDoctor.DATABean>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(GetDoctor.DATABean dataBean, String messages) {
                view.onGetDoctor(dataBean);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.GET_DOCTOR, t));
            }
        });

    }

    public void getGroomingLocation(Map<String, String> param) {
        Connector.newInstance(context).getGroomingLocation(param, new Connector.ApiCallback<GetGroomingLocation.DATABean>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(GetGroomingLocation.DATABean dataBean, String messages) {
                view.onGetGroomingLocation(dataBean);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.GET_GROOMING_LOCATION, t));
            }
        });

    }

    public void getBoarding(Map<String, String> param) {
        Connector.newInstance(context).getBoarding(param, new Connector.ApiCallback<GetBoarding.DATABean>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(GetBoarding.DATABean dataBean, String messages) {
                view.onGetBoarding(dataBean);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.GET_BOARDING, t));
            }
        });

    }


    public void rollbackService(CancelServiceParam savedService) {
        Connector.newInstance(context).rollbackService(savedService, new Connector.ApiCallback<String>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(String s, String messages) {
                view.onRollbackService(new OnCancelTreatmentService());
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.TREATMENT_CANCEL, t));
            }
        });
    }

    public void deleteService(int cartServiceId) {
        Connector.newInstance(context).deleteService(cartServiceId, new Connector.ApiCallback<String>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(String s, String messages) {
                view.onDeleteService(new OnCancelTreatmentService());
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.TREATMENT_CANCEL, t));

            }
        });

    }

    public void createCartTreatment(RequestBody param) {
        Connector.newInstance(context).createCartTreatment(param, new Connector.ApiCallback<CreateCart.DATABean>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(CreateCart.DATABean dataBean, String messages) {
                view.onCreateCartTreatment(dataBean);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.CREATE_CART_TREATMENT, t));
            }
        });

    }

    public void reservTreatment(RequestBody formBody) {
        Connector.newInstance(context).reservTreatment(formBody, new Connector.ApiCallback<CreateCart.DATABean>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(CreateCart.DATABean dataBean, String messages) {
                view.onReservTreatment(new OnSuccessReservTreatment());

            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.RESERV_TREATMENT, t));
            }
        });

    }

    public void getAllDoctor(Map<String, String> params) {
        Connector.newInstance(context).getAllDoctor(params, new Connector.ApiCallback<GetDoctorSchedule.DATABean>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(GetDoctorSchedule.DATABean dataBeans, String messages) {
                view.onGetAllDoctor(dataBeans);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.DOCTOR_SCHEDULE, t));

            }
        });

    }
}
