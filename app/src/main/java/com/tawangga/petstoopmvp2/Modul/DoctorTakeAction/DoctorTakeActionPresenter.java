package com.tawangga.petstoopmvp2.Modul.DoctorTakeAction;

import android.content.Context;

import com.tawangga.petstoopmvp2.EventBus.OnApiResultSuccess;
import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.Model.GetOrderConfirmAnamnesis;
import com.tawangga.petstoopmvp2.Param.SaveConfirmOrderAnamnesis;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

public class DoctorTakeActionPresenter {
    private final Context context;
    private final String tag;
    private DoctorTakeActionView view;

    public DoctorTakeActionPresenter(Context context, String TAG, DoctorTakeActionView view) {
        this.context = context;
        tag = TAG;
        this.view = view;
    }

    public void getOrderConfirmAnamnesisDoctor(String cartServiceId) {
        Connector.newInstance(context).getOrderConfirmAnamnesisDoctor(cartServiceId, 0, new Connector.ApiCallback<GetOrderConfirmAnamnesis.DATABean>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(GetOrderConfirmAnamnesis.DATABean dataBean, String messages) {
                view.onGetOrderConfirmAnamnesis(dataBean);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.GET_CONFIRM_ORDER_ANAMNESIS, t));
            }
        });

    }

    public void saveOrderConfirmAnamnesisDokter(int cartServiceId, SaveConfirmOrderAnamnesis param) {
        Connector.newInstance(context).saveOrderConfirmAnamnesisDokter(cartServiceId, param, new Connector.ApiCallback<String>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(String s, String messages) {
                view.onSaveOrderConfirmAnamnesis(new OnApiResultSuccess(OnApiResultSuccess.SAVE_ANAMNESIS));
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.GET_CONFIRM_ORDER_ANAMNESIS, t));
            }
        });

    }

}
