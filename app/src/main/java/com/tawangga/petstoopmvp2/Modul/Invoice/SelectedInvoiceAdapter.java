package com.tawangga.petstoopmvp2.Modul.Invoice;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Model.DataInvoice;
import com.tawangga.petstoopmvp2.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectedInvoiceAdapter extends RecyclerView.Adapter<SelectedInvoiceAdapter.ViewHolder> {


    @BindView(R.id.ll_background)
    LinearLayout llBackground;
    List<DataInvoice.Ordersget_invoice> data = new ArrayList<>();
    private Context context;
    private InvoiceFragment invoiceFragment;
    private boolean isDownPayment;

    public SelectedInvoiceAdapter(Context context, InvoiceFragment invoiceFragment) {
        this.context = context;
        this.invoiceFragment = invoiceFragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_selected_invoice, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        DataInvoice.Ordersget_invoice invoice = data.get(position);
        holder.txtReservationNumber.setText(invoice.getTransactionItemCode());
        holder.txtOwnerPetName.setText(invoice.getCustomerName());

        if (invoice.getTransactionItemCode().contains("R#") || isDownPayment) {
            holder.txtCancel.setVisibility(View.GONE);
        }
        holder.txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                invoiceFragment.removeOrder(invoice.getTransactionId() + "", invoice.getTransactionItemCode());
            }
        });
        DetailSelectedInvoice adapter = new DetailSelectedInvoice(context, invoice.getTransactionItemCode(), isDownPayment, invoiceFragment, invoice.getTransactionId());
        adapter.setDatas(invoice.getItems());
        holder.rvDetail.setLayoutManager(new LinearLayoutManager(context));
        holder.rvDetail.setAdapter(adapter);

    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setDatas(List<DataInvoice.Ordersget_invoice> data, boolean isDownPayment) {
        this.isDownPayment = isDownPayment;
        this.data.clear();
        this.data = data;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_reservation_number)
        TextView txtReservationNumber;
        @BindView(R.id.txt_owner_pet_name)
        TextView txtOwnerPetName;
        @BindView(R.id.txt_cancel)
        ImageView txtCancel;
        @BindView(R.id.rv_detail)
        RecyclerView rvDetail;
        @BindView(R.id.ll_background)
        LinearLayout llBackground;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}


class DetailSelectedInvoice extends RecyclerView.Adapter<DetailSelectedInvoice.ViewHolder> {


    private List<DataInvoice.Ordersget_invoice.Itemsget_invoice> data;
    Context context;
    private String transactionItemCode;
    private boolean isDownPayment;
    private InvoiceFragment invoiceFragment;
    private int transactionId;

    DetailSelectedInvoice(Context context, String transactionItemCode, boolean isDownPayment, InvoiceFragment invoiceFragment, int transactionId) {

        this.context = context;
        this.transactionItemCode = transactionItemCode;
        this.isDownPayment = isDownPayment;
        this.invoiceFragment = invoiceFragment;
        this.transactionId = transactionId;
    }


    @NonNull
    @Override
    public DetailSelectedInvoice.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_detail_selected_invoice, parent, false);

        return new DetailSelectedInvoice.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DetailSelectedInvoice.ViewHolder holder, int position) {
        DataInvoice.Ordersget_invoice.Itemsget_invoice detailInvoice = data.get(position);
        if (transactionItemCode.contains("R#") || isDownPayment) {
            holder.txtCancel.setVisibility(View.GONE);
        }
        holder.txtItemName.setText(detailInvoice.getItemName());
        holder.txt_item_desc.setText(detailInvoice.getItemNote());
        holder.txtQty.setText(detailInvoice.getItemQty());
        holder.txtPrice.setText(Utils.toRupiah(detailInvoice.getItemPrice()));
        holder.txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                invoiceFragment.removeOrderDetail(transactionId + "", transactionItemCode, detailInvoice.getTransactionItemDetailId() + "");
            }
        });
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setDatas(List<DataInvoice.Ordersget_invoice.Itemsget_invoice> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_item_name)
        TextView txtItemName;
        @BindView(R.id.txt_qty)
        TextView txtQty;
        @BindView(R.id.txt_price)
        TextView txtPrice;
        @BindView(R.id.txt_cancel)
        TextView txtCancel;
        @BindView(R.id.ll_background)
        LinearLayout llBackground;
        @BindView(R.id.txt_item_desc)
        TextView txt_item_desc;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

