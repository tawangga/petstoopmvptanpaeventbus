package com.tawangga.petstoopmvp2.Modul.Shop;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Model.DetailCart;
import com.tawangga.petstoopmvp2.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectedProductAdapter extends RecyclerView.Adapter<SelectedProductAdapter.ViewHolder> {

    private List<DetailCart> data;
    private Context context;
    private AdapterView.OnItemClickListener onItemClickListener;


    public SelectedProductAdapter(Context context) {
        data = new ArrayList<>();
        this.context = context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_selected_product, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.position = position;


        if (position % 2 == 1) {
            holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.gray_light));
        } else {
            holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.gray_light_2));
        }

        DetailCart selectedData = data.get(position);
        holder.txtItemName.setText(selectedData.getProductName());
        holder.txtQty.setText(selectedData.getProductQty() + "");
        holder.txtPrice.setText(Utils.toRupiah(selectedData.getProductPrice().replace(".00", "")));

    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setDatas(List<DetailCart> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public List<DetailCart> getDatas() {
        return data;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.txt_item_name)
        TextView txtItemName;
        @BindView(R.id.txt_qty)
        TextView txtQty;
        @BindView(R.id.txt_price)
        TextView txtPrice;
        @BindView(R.id.ll_background)
        LinearLayout llBackground;
        int position;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(null, v, position, 0);
            }

        }
    }
}
