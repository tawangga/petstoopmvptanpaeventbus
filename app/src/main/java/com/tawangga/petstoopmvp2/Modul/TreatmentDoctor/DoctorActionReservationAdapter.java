package com.tawangga.petstoopmvp2.Modul.TreatmentDoctor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.ModelNew.GetTreatment;
import com.tawangga.petstoopmvp2.Modul.TreatmentFragment.TreatmentDetailAdapter;
import com.tawangga.petstoopmvp2.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DoctorActionReservationAdapter extends RecyclerView.Adapter<DoctorActionReservationAdapter.ViewHolder> {

    private Context context;
    private List<GetTreatment.DATABean.ItemsBeanX> data;
    int height = 0;
    private int width = 0;

    public DoctorActionReservationAdapter(Context context) {
        this.context = context;
        data = new ArrayList<>();
    }

    @NonNull
    @Override
    public DoctorActionReservationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_treatment, parent, false);

        return new DoctorActionReservationAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final DoctorActionReservationAdapter.ViewHolder holder, final int position) {
        GetTreatment.DATABean.ItemsBeanX selectedData = data.get(position);

        if (position % 2 == 1) {
            holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.brown_light_2));
        } else {
            holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.white));
        }
        if (width == 0 || height == 0) {
            if (!selectedData.isShowingDetail()) {
                holder.llBackground.measure(0, 0);
                height = holder.llBackground.getMeasuredHeight();
                holder.rowBG.getLayoutParams().height = holder.llBackground.getMeasuredHeight();
                holder.rowBG.requestLayout();

                holder.delete_task.measure(0, 0);
                width = holder.delete_task.getMeasuredWidth();
                holder.edit_task.getLayoutParams().width = holder.delete_task.getMeasuredWidth();
                holder.edit_task.requestLayout();
            }
        } else {
            holder.rowBG.getLayoutParams().height = height;
            holder.rowBG.requestLayout();

            holder.edit_task.getLayoutParams().width = width;
            holder.edit_task.requestLayout();

        }


        holder.txtReservationNumber.setTextColor(context.getResources().getColor(R.color.brown_2));
        holder.llBackground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedData.isShowingDetail()) {
                    selectedData.setShowingDetail(false);
                    setShowingDetail(holder, selectedData.isShowingDetail());

                } else {
                    selectedData.setShowingDetail(true);
                    setShowingDetail(holder, selectedData.isShowingDetail());

                }
            }
        });

        List<GetTreatment.DATABean.ItemsBeanX.ItemsBean> items = new ArrayList<>();
        for (GetTreatment.DATABean.ItemsBeanX.ItemsBean item : selectedData.getItems()) {
            if (item.getServiceType().equals("1")) {
                items.add(item);
            }
        }


        holder.txtReservationNumber.setText(selectedData.getOrderCode());
        holder.txtOwnerPetName.setText(selectedData.getPet().getCustomerName() + " - " + selectedData.getPet().getPetName());
        holder.txtServices.setText(selectedData.getItems().size() + " SERVICES");

        TreatmentDetailAdapter adapter = new TreatmentDetailAdapter(context);
        adapter.setDatas(items, selectedData);
        adapter.setFromOrder(false);
        holder.rvDetail.setLayoutManager(new LinearLayoutManager(context));
        holder.rvDetail.setAdapter(adapter);

    }

    private void setShowingDetail(ViewHolder holder, boolean isShowingDetail) {
        if (!isShowingDetail) {
            holder.rlButton.setVisibility(View.GONE);
            holder.btnAddtorecipe.setVisibility(View.GONE);
            holder.btnCompleteorder.setVisibility(View.GONE);
            holder.btnMakeOrder.setVisibility(View.GONE);
            holder.rvDetail.setVisibility(View.GONE);

        } else {
            holder.rlButton.setVisibility(View.GONE);
            holder.btnCompleteorder.setVisibility(View.GONE);
            holder.btnAddtorecipe.setVisibility(View.GONE);
            holder.btnMakeOrder.setVisibility(View.GONE);
            holder.rvDetail.setVisibility(View.VISIBLE);
        }

        holder.llBackground.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                holder.llBackground.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                holder.rowBG.getLayoutParams().height = holder.llBackground.getHeight();
                holder.rowBG.requestLayout();

            }
        });
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setDatas(List<GetTreatment.DATABean.ItemsBeanX> data) {
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    public List<GetTreatment.DATABean.ItemsBeanX> getData() {
        return data;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_reservation_number)
        TextView txtReservationNumber;
        @BindView(R.id.txt_owner_pet_name)
        TextView txtOwnerPetName;
        @BindView(R.id.txt_services)
        TextView txtServices;
        @BindView(R.id.rv_detail)
        RecyclerView rvDetail;
        @BindView(R.id.btn_completeorder)
        Button btnCompleteorder;
        @BindView(R.id.btn_addtorecipe)
        LinearLayout btnAddtorecipe;
        @BindView(R.id.rl_button)
        RelativeLayout rlButton;
        @BindView(R.id.ll_background)
        LinearLayout llBackground;
        @BindView(R.id.btn_make_order)
        LinearLayout btnMakeOrder;
        @BindView(R.id.rowBG)
        LinearLayout rowBG;
        @BindView(R.id.edit_task)
        RelativeLayout edit_task;
        @BindView(R.id.delete_task)
        RelativeLayout delete_task;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
