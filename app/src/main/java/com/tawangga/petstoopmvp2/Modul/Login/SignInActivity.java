package com.tawangga.petstoopmvp2.Modul.Login;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.Helper.SharedPreference;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Helper.ViewUtils;
import com.tawangga.petstoopmvp2.Model.SignInModel;
import com.tawangga.petstoopmvp2.Modul.Main.MainActivity;
import com.tawangga.petstoopmvp2.Modul.SelectClientSite.SelectClientSiteActivity;
import com.tawangga.petstoopmvp2.R;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignInActivity extends AppCompatActivity implements SignInView {

    private static final String TAG = SignInActivity.class.getSimpleName();
    @BindView(R.id.btn_select_client_site)
    ImageView btnSelectClientSite;
    @BindView(R.id.txt_client)
    TextView txtClient;
    @BindView(R.id.txt_site)
    TextView txtSite;
    @BindView(R.id.txt_email)
    EditText txtEmail;
    @BindView(R.id.txt_password)
    EditText txtPassword;
    @BindView(R.id.pb_loading)
    ProgressBar pbLoading;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.ll_main_login)
    LinearLayout llMainLogin;

    private SignInPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);

        presenter = new SignInPresenter(this, TAG, this);
        initView();

    }

    private void initView() {
        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.50);
        llMainLogin.getLayoutParams().width = width;
        llMainLogin.requestLayout();

        if (new SharedPreference(this).getIsLogin()) {
            startActivity(new Intent(SignInActivity.this, MainActivity.class));
            finish();
        }

        txtEmail.setText("samplestaff");
        txtPassword.setText("secretpwd");
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (new SharedPreference(this).getSavedClientSite() == null)
            selectClientSite();
    }


    void selectClientSite() {
        Toast.makeText(this, "Pilih client dan site", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(SignInActivity.this, SelectClientSiteActivity.class));
        finish();
    }


    @OnClick(R.id.btn_select_client_site)
    void toSelectClient() {
        startActivity(new Intent(SignInActivity.this, SelectClientSiteActivity.class));

    }

    @OnClick(R.id.btn_login)
    public void onLoginClicked() {
        ViewUtils.changeVisibility(pbLoading);
        ViewUtils.changeVisibility(btnLogin);

        presenter.doLogin(txtEmail.getText().toString().trim(), txtPassword.getText().toString().trim(), new SharedPreference(this).getSavedClientSite().getSiteId());

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Connector.newInstance(this).close(TAG);
    }

    @Override
    public void onSuccessSignIn(SignInModel.DATABean dataBean) {
        ViewUtils.changeVisibility(btnLogin);
        ViewUtils.changeVisibility(pbLoading);

        toMainActivity(dataBean);

    }

    private void toMainActivity(SignInModel.DATABean data) {
        new SharedPreference(this).setIsLogin(true);
        new SharedPreference(this).setStaffId(String.valueOf(data.getStaffId()));
        new SharedPreference(this).setStaffName(data.getName());
        new SharedPreference(this).setStaffEmail(data.getEmail());
        new SharedPreference(this).setStaffPhoto(data.getPhoto());
        new SharedPreference(this).setStaffToken(data.getToken());
        new SharedPreference(this).setStaffPhone(data.getPhone());
        new SharedPreference(this).setStaffUsername(data.getUsername());
        new SharedPreference(this).setStaffCategory(data.getCategoryName());
        new SharedPreference(this).setStaffCode(data.getStaffCode());
        new SharedPreference(this).setPhotoOld(data.getPhotoOld());
        new SharedPreference(this).setIsCustomer(false);
        new SharedPreference(this).setSite(new Gson().toJson(data.getSite()));
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void onFailGettingData(OnFailGettingData data) {
        Utils.showToast(this, data.getThrowable().getMessage());
        ViewUtils.changeVisibility(btnLogin);
        ViewUtils.changeVisibility(pbLoading);
        data.getThrowable().printStackTrace();

    }
}
