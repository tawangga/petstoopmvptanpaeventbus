package com.tawangga.petstoopmvp2.Modul.RegisterCustomer;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import com.tawangga.petstoopmvp2.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class RegisterCustomerActivity extends AppCompatActivity {

    @BindView(R.id.iv_pic_owner_2)
    CircleImageView ivPic;
    @BindView(R.id.pb_loading_iv_customer)
    ProgressBar pbLoadingIv;
    @BindView(R.id.ed_owner_name)
    EditText edOwnerName;
    @BindView(R.id.ed_email)
    EditText edEmail;
    @BindView(R.id.ed_password)
    EditText edPassword;
    @BindView(R.id.ed_phone)
    EditText edPhone;
    @BindView(R.id.ed_address)
    EditText edAddress;
    @BindView(R.id.ed_retype_password)
    EditText edRetypePassword;
    @BindView(R.id.btn_daftar)
    Button btnDaftar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_customer);
        ButterKnife.bind(this);
    }
}