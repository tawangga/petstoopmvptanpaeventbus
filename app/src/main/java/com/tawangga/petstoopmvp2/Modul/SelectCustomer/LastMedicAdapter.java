package com.tawangga.petstoopmvp2.Modul.SelectCustomer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.Helper.GlobalVariable;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.ModelNew.GetTreatmentRecord;
import com.tawangga.petstoopmvp2.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LastMedicAdapter extends RecyclerView.Adapter<LastMedicAdapter.ViewHolder> {

    private List<GetTreatmentRecord.DATABean> data;
    Context context;

    public LastMedicAdapter(Context context) {
        this.context = context;
        data = new ArrayList<>();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_last_medic, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GetTreatmentRecord.DATABean dataBean = data.get(position);
        holder.txtServiceName.setText(dataBean.getOrderCode() + " - " + dataBean.getCustomerName());
        holder.txtTreatmentType.setText(dataBean.getServiceText());
        holder.txtDateService.setText(Utils.changeDateFormat(dataBean.getOrderDate(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME_FULL));
    }

    @Override
    public int getItemCount() {
        if (data != null) return data.size();
        else return 0;
    }

    public void setDatas(List<GetTreatmentRecord.DATABean> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public List<GetTreatmentRecord.DATABean> getDatas() {
        return data;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_service_name)
        TextView txtServiceName;
        @BindView(R.id.txt_treatment_type)
        TextView txtTreatmentType;
        @BindView(R.id.txt_date_service)
        TextView txtDateService;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
