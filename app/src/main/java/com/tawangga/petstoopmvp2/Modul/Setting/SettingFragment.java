package com.tawangga.petstoopmvp2.Modul.Setting;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.Helper.SharedPreference;
import com.tawangga.petstoopmvp2.Modul.LoginCustomer.SignInCustomerActivity;
import com.tawangga.petstoopmvp2.Modul.Main.MainActivity;
import com.tawangga.petstoopmvp2.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SettingFragment extends Fragment {

    @BindView(R.id.rv_setting)
    RecyclerView rvSetting;
    Unbinder unbinder;
    @BindView(R.id.btn_logout)
    LinearLayout btnLogout;


    public SettingFragment() {
        // Required empty public constructor
    }


    public static SettingFragment newInstance() {
        SettingFragment fragment = new SettingFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_setting, container, false);
        unbinder = ButterKnife.bind(this, v);

        initView();
        return v;
    }

    private void initView() {
        List<String> data = new ArrayList<>();
        data.add("About Application");

        SettingAdapter adapter = new SettingAdapter(getActivity());
        adapter.setDatas(data);
        adapter.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        ((MainActivity) getActivity()).toAboutApplication();
                        break;
                }
            }
        });

        rvSetting.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvSetting.setAdapter(adapter);
    }

    @OnClick(R.id.btn_logout)
    public void onClickLogout() {
        new SharedPreference(getActivity()).clearSession();
        startActivity(new Intent(getActivity(), SignInCustomerActivity.class));
        getActivity().finish();
    }
}