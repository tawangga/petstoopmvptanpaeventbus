package com.tawangga.petstoopmvp2.Modul.Invoice;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.Helper.GlobalVariable;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Model.GetOrders;
import com.tawangga.petstoopmvp2.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InvoiceAdapter extends RecyclerView.Adapter<InvoiceAdapter.ViewHolder> {

    private List<GetOrders.DATABean> data;
    private List<GetOrders.DATABean> orig;
    private Context context;
    private SparseBooleanArray listState = new SparseBooleanArray();


    public InvoiceAdapter(Context context) {
        this.context = context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_invoice, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        if (position % 2 == 1) {
            holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.brown_light_2));
        } else {
            holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

        GetOrders.DATABean dataBean = data.get(position);

        if (!listState.get(position)) {
            holder.rlButton.setVisibility(View.GONE);
            holder.btnMakeOrder.setVisibility(View.VISIBLE);
            holder.rvDetail.setVisibility(View.GONE);
        } else {
            holder.rlButton.setVisibility(View.GONE);
            holder.btnMakeOrder.setVisibility(View.GONE);
            holder.rvDetail.setVisibility(View.VISIBLE);
        }

        // TODO: 04/03/19 Owner name - Pet Name 
        holder.txtReservationNumber.setTextColor(context.getResources().getColor(R.color.black));
        int countItem = dataBean.getItems().size();
        if (dataBean.getOrderType() == 2) {
            holder.txtReservationNumber.setText(String.format("%s - %s", dataBean.getCustomerName(), dataBean.getPetName()));
            holder.txtServices.setText(context.getResources().getQuantityString(R.plurals.services, countItem, countItem));
        } else if (dataBean.getOrderType() == 1) {
            holder.txtReservationNumber.setText(String.format("%s", dataBean.getCustomerName()));
            holder.txtServices.setText(context.getResources().getQuantityString(R.plurals.item, countItem, countItem));
        }


        holder.txtOwnerPetName.setTextColor(context.getResources().getColor(R.color.red_light));
        holder.txtOwnerPetName.setText(dataBean.getOrderCode());
        holder.txtOrderDate.setText(" - " + Utils.changeDateFormat(dataBean.getOrderDate(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME));


        DetailInvoiceAdapter adapter = new DetailInvoiceAdapter(context);
        adapter.setDatas(dataBean.getItems());
        holder.rvDetail.setLayoutManager(new LinearLayoutManager(context));
        holder.rvDetail.setAdapter(adapter);

    }


    @Override
    public int getItemCount() {
        if (data != null) {
            return data.size();
        } else return 0;
    }

    public void setDatas(List<GetOrders.DATABean> datas) {
        data = new ArrayList<>();
        this.data.clear();
        listState.clear();
        if (orig != null) {
            orig = null;
        }
        this.data = datas;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_reservation_number)
        TextView txtReservationNumber;
        @BindView(R.id.txt_owner_pet_name)
        TextView txtOwnerPetName;
        @BindView(R.id.txt_services)
        TextView txtServices;
        @BindView(R.id.rv_detail)
        RecyclerView rvDetail;
        @BindView(R.id.btn_make_order)
        LinearLayout btnMakeOrder;
        @BindView(R.id.rl_button)
        RelativeLayout rlButton;
        @BindView(R.id.ll_background)
        LinearLayout llBackground;
        @BindView(R.id.txt_order_date)
        TextView txtOrderDate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            rvDetail.setVisibility(View.GONE);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!listState.get(getAdapterPosition(), false)) {
                        listState.put(getAdapterPosition(), true);

                    } else if (listState.get(getAdapterPosition(), false)) {
                        listState.put(getAdapterPosition(), false);
                    }
                    notifyDataSetChanged();

                }
            });
        }
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<GetOrders.DATABean> results = new ArrayList<>();
                if (orig == null) {
                    orig = data;
                }
                if (constraint != null) {
                    if (orig != null && orig.size() > 0) {
                        for (final GetOrders.DATABean g : orig) {
                            if (g.getPetName().toLowerCase().contains(constraint.toString().toLowerCase()) || g.getCustomerName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                                results.add(g);
                            }
                        }
                        oReturn.values = results;
                    }
                }
                return oReturn;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                data = (ArrayList<GetOrders.DATABean>) results.values;
                notifyDataSetChanged();

            }
        };
    }

}


class DetailInvoiceAdapter extends RecyclerView.Adapter<DetailInvoiceAdapter.ViewHolder> {
    Context context;
    List<GetOrders.DATABean.ItemsBean> data;

    DetailInvoiceAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public DetailInvoiceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_detail_invoice, parent, false);

        return new DetailInvoiceAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DetailInvoiceAdapter.ViewHolder holder, int position) {
        GetOrders.DATABean.ItemsBean dataBean = data.get(position);
        holder.txtServiceName.setText(dataBean.getItemName());
        if (dataBean.isChecked()) {
            holder.cbOrder.setChecked(true);
        } else holder.cbOrder.setChecked(false);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setDatas(List<GetOrders.DATABean.ItemsBean> data) {
        this.data = data;
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_progress)
        TextView txtProgress;
        @BindView(R.id.txt_service_name)
        TextView txtServiceName;
        @BindView(R.id.rl_progress)
        RelativeLayout rlProgress;
        @BindView(R.id.checkbox)
        CheckBox cbOrder;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            cbOrder.setId(getAdapterPosition());
            cbOrder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                    if (checked) {
                        data.get(getAdapterPosition()).setChecked(true);
                    } else {
                        data.get(getAdapterPosition()).setChecked(false);
                    }
                }
            });
        }


    }
}

