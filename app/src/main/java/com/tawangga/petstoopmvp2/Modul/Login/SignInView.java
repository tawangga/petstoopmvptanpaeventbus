package com.tawangga.petstoopmvp2.Modul.Login;

import com.tawangga.petstoopmvp2.Model.SignInModel;
import com.tawangga.petstoopmvp2.Modul.BaseView;

public interface SignInView extends BaseView {
    void onSuccessSignIn(SignInModel.DATABean dataBean);
}
