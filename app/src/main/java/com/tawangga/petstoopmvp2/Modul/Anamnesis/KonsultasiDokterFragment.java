package com.tawangga.petstoopmvp2.Modul.Anamnesis;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.tawangga.petstoopmvp2.Dialog.DialogSeeAllTreatment.AdapterMedicalItems;
import com.tawangga.petstoopmvp2.Dialog.DialogSeeAllTreatment.DialogSeeAllTreatment;
import com.tawangga.petstoopmvp2.Dialog.DialogSeeAllTreatment.TreatmentKonsultasiDokterAdapter;
import com.tawangga.petstoopmvp2.EventBus.OnApiResultSuccess;
import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.Helper.TreatmentHelper;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Model.GetOrderConfirmAnamnesis;
import com.tawangga.petstoopmvp2.Modul.Main.MainActivity;
import com.tawangga.petstoopmvp2.Param.SaveConfirmOrderAnamnesis;
import com.tawangga.petstoopmvp2.R;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class KonsultasiDokterFragment extends Fragment implements KonsultasiDokterView {

    private static final String TAG = KonsultasiDokterFragment.class.getSimpleName();
    @BindView(R.id.iv_pic_pet)
    CircleImageView ivPicPet;
    @BindView(R.id.txt_owner_pet_name)
    TextView txtOwnerPetName;
    @BindView(R.id.txt_reservcode)
    TextView txtReservcode;
    @BindView(R.id.btn_medical_records)
    Button btnMedicalRecords;
    @BindView(R.id.rv_clinical_checkup)
    RecyclerView rvClinicalCheckup;
    @BindView(R.id.ll_clinical_checkup)
    LinearLayout llClinicalCheckup;
    @BindView(R.id.ed_anamnesis)
    EditText edAnamnesis;
    @BindView(R.id.ll_anamnesis)
    LinearLayout llAnamnesis;
    @BindView(R.id.rv_treatment)
    RecyclerView rvTreatment;
    @BindView(R.id.ll_treatment)
    LinearLayout llTreatment;
    @BindView(R.id.ed_diagnosis)
    EditText edDiagnosis;
    @BindView(R.id.ll_diagnosis)
    LinearLayout llDiagnosis;
    @BindView(R.id.rv_medical_item)
    RecyclerView rvMedicalItem;
    @BindView(R.id.ll_medical_items)
    LinearLayout llMedicalItems;
    @BindView(R.id.tv_customer_name)
    TextView tvCustomerName;
    @BindView(R.id.ll_other)
    LinearLayout llOther;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    private AdapterClinicalCheckup adapterClinicalCheckup;
    private AdapterMedicalItems adapterMedicalItems;
    private ProgressDialog dialog;
    private KonsultasiDokterPresenter presenter;
    private TreatmentKonsultasiDokterAdapter konsultasiDokterAdapter;
    private GetOrderConfirmAnamnesis.DATABean dataAnamnesis;

    public static KonsultasiDokterFragment newInstance() {
        KonsultasiDokterFragment fragment = new KonsultasiDokterFragment();
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_konsultasi_dokter, container, false);
        ButterKnife.bind(this, v);
        presenter = new KonsultasiDokterPresenter(getActivity(), TAG, this);
        setDataOwner();
        setAdapter();
        getOrderConfirmAnamnesis();
        return v;
    }

    private void getOrderConfirmAnamnesis() {
        dialog = Utils.showProgressDialog(getActivity(), false, "Loading...");

        String cartServiceId = TreatmentHelper.getInstance().getDataTreatment().getCartServiceId() + "";
        presenter.getOrderConfirmAnamnesis(cartServiceId);
    }


    @Override
    public void onFailGettingData(OnFailGettingData data) {
        dialog.dismiss();
        Toast.makeText(getActivity(), data.getThrowable().getMessage(), Toast.LENGTH_SHORT).show();
        data.getThrowable().printStackTrace();
    }

    private void setDataOwner() {
        txtOwnerPetName.setText(TreatmentHelper.getInstance().getSelectedCustomer().getCustomerName() + " - " + TreatmentHelper.getInstance().getSelectedPet().getPetName());
        txtReservcode.setText(TreatmentHelper.getInstance().getDataTreatment().getOrderCode());
        ivPicPet.setVisibility(View.GONE);
        Picasso.get().load(TreatmentHelper.getInstance().getSelectedPet().getPetPhoto())
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .into(ivPicPet, new Callback() {
                    @Override
                    public void onSuccess() {
                        ivPicPet.setVisibility(View.VISIBLE);

                    }

                    @Override
                    public void onError(Exception e) {
                        ivPicPet.setImageDrawable(getResources().getDrawable(R.drawable.no_image));
                        ivPicPet.setVisibility(View.VISIBLE);
                        e.printStackTrace();
                    }
                });
    }

    private void setAdapter() {
        rvClinicalCheckup.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapterClinicalCheckup = new AdapterClinicalCheckup(getActivity());
        rvClinicalCheckup.setAdapter(adapterClinicalCheckup);

        rvMedicalItem.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapterMedicalItems = new AdapterMedicalItems(getActivity(), true);
        adapterMedicalItems.setDatas(new ArrayList<>());
        rvMedicalItem.setAdapter(adapterMedicalItems);

        llAnamnesis.measure(0, 0);
        llTreatment.getLayoutParams().height = llAnamnesis.getMeasuredHeight();
        llTreatment.requestLayout();

        llDiagnosis.measure(0, 0);
        llMedicalItems.getLayoutParams().height = llDiagnosis.getMeasuredHeight();
        llMedicalItems.requestLayout();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Connector.newInstance(getActivity()).close(TAG);
        EventBus.getDefault().unregister(this);
    }

    @OnClick(R.id.btn_medical_records)
    public void onViewMedicalRecords() {
        DialogSeeAllTreatment dialog = DialogSeeAllTreatment.newInstance(TreatmentHelper.getInstance().getSelectedCustomer(), TreatmentHelper.getInstance().getSelectedPet());
        dialog.show(getChildFragmentManager(), "");

    }

    @OnClick(R.id.btn_submit)
    public void onClickBtnSubmit() {
        SaveConfirmOrderAnamnesis param = new SaveConfirmOrderAnamnesis();
        param.setAnamnesis(edAnamnesis.getText().toString().trim());
        param.setDiagnosis(edDiagnosis.getText().toString().trim());
        List<SaveConfirmOrderAnamnesis.ClinicalCheckupBean> listClinicalCheckUp = new ArrayList<>();
        for (int i = 0; i < adapterClinicalCheckup.getData().size(); i++) {
            GetOrderConfirmAnamnesis.DATABean.ClinicalCheckupsBean clinicalCheckupsBean = adapterClinicalCheckup.getData().get(i);
            if (clinicalCheckupsBean.getClinicalCheckupIsPriority() == 1) {
                SaveConfirmOrderAnamnesis.ClinicalCheckupBean clinicalCheckUpItem = new SaveConfirmOrderAnamnesis.ClinicalCheckupBean();
                clinicalCheckUpItem.setClinicalCheckupId(clinicalCheckupsBean.getClinicalCheckupId());
                clinicalCheckUpItem.setClinicalCheckupName(clinicalCheckupsBean.getClinicalCheckupName());
                clinicalCheckUpItem.setValue(clinicalCheckupsBean.getValue() + "");
                clinicalCheckUpItem.setClinicalCheckupUnit(clinicalCheckupsBean.getClinicalCheckupUnit());
                if (clinicalCheckupsBean.isExam()) {
                    clinicalCheckUpItem.setIsExam(1);
                } else {
                    clinicalCheckUpItem.setIsExam(0);
                }
                listClinicalCheckUp.add(clinicalCheckUpItem);
            }
        }
        param.setIsTakeAction(false);
        param.setClinicalCheckup(listClinicalCheckUp);

        dialog = Utils.showProgressDialog(getActivity(), false, "Loading...");

        presenter.saveOrderConfirmAnamnesis(TreatmentHelper.getInstance().getDataTreatment().getCartServiceId(), param);

    }

    @Override
    public void onGetOrderConfirmAnamnesis(GetOrderConfirmAnamnesis.DATABean data) {
        dialog.dismiss();
        dataAnamnesis = data;
        adapterClinicalCheckup.setDatas(data.getClinicalCheckups(), false);
        List<GetOrderConfirmAnamnesis.DATABean.TreatmentsBean> treatments = new ArrayList<>();
        for (GetOrderConfirmAnamnesis.DATABean.TreatmentsBean allTreatment : data.getTreatments()) {
            if (allTreatment.getServiceType().equals("1")) {
                treatments.add(allTreatment);
            }
        }
        adapterMedicalItems.setDatas(data.getMedicalItems());

        rvTreatment.setLayoutManager(new LinearLayoutManager(getActivity()));
        konsultasiDokterAdapter = new TreatmentKonsultasiDokterAdapter(getActivity(), dataAnamnesis);
        rvTreatment.setAdapter(konsultasiDokterAdapter);
        konsultasiDokterAdapter.setDatas(treatments);

    }

    @Override
    public void onSaveOrderConfirmAnamnesis(OnApiResultSuccess data) {
        if (data.getApi() == OnApiResultSuccess.SAVE_ANAMNESIS) {
            dialog.dismiss();
            ((MainActivity) getActivity()).setClearBackStack(true);
            ((MainActivity) getActivity()).toTreatment();

        }

    }
}
