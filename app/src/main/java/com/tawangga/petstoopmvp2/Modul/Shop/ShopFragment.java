package com.tawangga.petstoopmvp2.Modul.Shop;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.tawangga.petstoopmvp2.Dialog.DialogShop.DialogShop;
import com.tawangga.petstoopmvp2.EventBus.CleaningCart;
import com.tawangga.petstoopmvp2.EventBus.OnCreateCart;
import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.EventBus.OnRefreshSearchView;
import com.tawangga.petstoopmvp2.EventBus.RefreshCart;
import com.tawangga.petstoopmvp2.EventBus.RollbackCart;
import com.tawangga.petstoopmvp2.Helper.GlobalVariable;
import com.tawangga.petstoopmvp2.Helper.SharedPreference;
import com.tawangga.petstoopmvp2.Helper.TreatmentHelper;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Model.CartModel;
import com.tawangga.petstoopmvp2.Model.DataInvoice;
import com.tawangga.petstoopmvp2.Model.DetailCart;
import com.tawangga.petstoopmvp2.Model.ProductCategories;
import com.tawangga.petstoopmvp2.Model.ProductModel;
import com.tawangga.petstoopmvp2.Model.PromoModel;
import com.tawangga.petstoopmvp2.ModelNew.GetDoctorSchedule;
import com.tawangga.petstoopmvp2.Modul.Main.MainActivity;
import com.tawangga.petstoopmvp2.Param.CancelMedicalServiceParam;
import com.tawangga.petstoopmvp2.R;
import com.tawangga.petstoopmvp2.ServerSide.Connector;
import com.wang.avi.AVLoadingIndicatorView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class ShopFragment extends Fragment implements ShopFragmentView {

    private static final String TAG = ShopFragment.class.getSimpleName();

    @BindView(R.id.txt_owner_name)
    TextView txtOwnerName;
    @BindView(R.id.txt_pet_name)
    TextView txtPetName;
    @BindView(R.id.searchView)
    EditText searchView;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    Unbinder unbinder;
    @BindView(R.id.rv_produk)
    RecyclerView rvProduk;
    @BindView(R.id.rv_selected_produk)
    RecyclerView rvSelectedProduk;
    @BindView(R.id.txt_item_found)
    TextView txtItemFound;
    @BindView(R.id.btn_addInvoice)
    Button addtoInvoice;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    @BindView(R.id.ll_product)
    LinearLayout llProduct;

    @BindView(R.id.progressbar)
    AVLoadingIndicatorView loadingIndicatorView;
    @BindView(R.id.refreshPet)
    SwipeRefreshLayout refreshPet;

    @BindView(R.id.lay_header)
    LinearLayout lay_header;
    ProductAdapter adapterProduct;
    @BindView(R.id.iv_pic_owner)
    CircleImageView ivPicOwner;
    @BindView(R.id.txt_total_pay)
    TextView txtTotalPay;
    ProductModel.DATABean dataProduct;
    SelectedProductAdapter selectedProductAdapter;
    private CartModel.DATABean cart;

    private boolean isFirstRun;
    ColorStateList colorStateList;
    List<PromoModel.DATABean.ItemsBean> listPromo = new ArrayList<>();
    List<GetDoctorSchedule.DATABean.ItemsBean> listAllDoctor = new ArrayList<>();
    ArrayList<String> listNameAllDoctor = new ArrayList<>();
    ProductCategories.DATABean dataCategory;
    String cart_service_id = "";
    String category_id = "";
    GridLayoutManager layoutManager;
    private ProgressDialog dialogCreateCart, dialogGetCategory, dialogPromo, dialogGetDoctor, dialogCleanCart, dialogCreateInvoice;


    //for Pagination
    private int page, p, lastpage, cleanCart;
    private boolean isLoading = true;
    private String chart_ID = "";

    Handler handler = new Handler();
    long delay = 1000; // 1 seconds after user stops typing
    long last_text_edit = 0;

    private Runnable input_finish_checker = () -> {
        if (System.currentTimeMillis() > (last_text_edit + delay - 500)) {
            if (searchView.getText().length() == 0) {
                resetPaginationProduct();
                isLoading = true;
                getProduct(category_id, "", "");
                new Handler().postDelayed(() -> {
                    Utils.hideSoftKeyboard(getActivity());
                }, 1000);
            } else {
                if (searchView.getText().length() == 0) {
                    Utils.hideSoftKeyboard(getActivity());
                } else {
                    getProduct(category_id, "", searchView.getText().toString());
                }
            }
        }
    };
    private CancelMedicalServiceParam savedCartMedicalItem;
    private ShopPresenter presenter;

    public ShopFragment() {
        // Required empty public constructor
    }

    public static ShopFragment newInstance() {
        ShopFragment fragment = new ShopFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_shop, container, false);
        unbinder = ButterKnife.bind(this, v);
        isFirstRun = true;
        presenter = new ShopPresenter(getActivity(), TAG, this);
        initView();
        return v;
    }

    private void initView() {
        cleanCart = 1;
        searchView.requestFocus();
        cart_service_id = getArguments().getString("cart_service_id");
        colorStateList = new ColorStateList(
                new int[][]{
                        new int[]{-android.R.attr.state_checked}, //disabled
                        new int[]{android.R.attr.state_enabled} //enabled
                },
                new int[]{
                        getResources().getColor(R.color.brown),//disable
                        getResources().getColor(R.color.brown_2)//enable
                }
        );
        loading(true, false);
        dataCategory = new ProductCategories.DATABean();
        setAdapter();
        getPromo();
        getCategoryProduct();

        if (new SharedPreference(getContext()).isDoctor()) {
            addtoInvoice.setText(getResources().getString(R.string.add_this_items));
            createCartMedicalItem(new RefreshCart(RefreshCart.MEDICAL_CART));
            Log.d("pathImg", TreatmentHelper.getInstance().getSelectedPet().getPetPhoto());
            Picasso.get().load(TreatmentHelper.getInstance().getSelectedPet().getPetPhoto())
                    .tag(TAG)
                    .into(ivPicOwner, new Callback() {
                        @Override
                        public void onSuccess() {
                            ivPicOwner.setVisibility(View.VISIBLE);

                        }

                        @Override
                        public void onError(Exception e) {
                            ivPicOwner.setImageDrawable(getResources().getDrawable(R.drawable.no_image));
                            ivPicOwner.setVisibility(View.VISIBLE);
                            e.printStackTrace();
                        }
                    });
            lay_header.setBackgroundColor(getContext().getResources().getColor(R.color.brown_light));
        } else {
            btnCancel.setText("Clear");
            getAllDoctor();
            createCart(new RefreshCart(RefreshCart.CART));
            Picasso.get().load(TreatmentHelper.getInstance().getSelectedCustomer().getCustomerPhoto())
                    .tag(TAG)
                    .into(ivPicOwner, new Callback() {
                        @Override
                        public void onSuccess() {
                            ivPicOwner.setVisibility(View.VISIBLE);

                        }

                        @Override
                        public void onError(Exception e) {
                            ivPicOwner.setImageDrawable(getResources().getDrawable(R.drawable.no_image));
                            ivPicOwner.setVisibility(View.VISIBLE);
                            e.printStackTrace();
                        }
                    });
            lay_header.setBackgroundColor(getContext().getResources().getColor(R.color.brown_light));
        }


        txtOwnerName.setText(TreatmentHelper.getInstance().getSelectedCustomer().getCustomerName());
        if (TreatmentHelper.getInstance().getSelectedPet() != null) {
            txtPetName.setText(TreatmentHelper.getInstance().getSelectedPet().getPetName());
        }

        searchView.requestFocus();
    }

    private void loading(boolean firstLoad, boolean visible) {
        if (firstLoad) {
            refreshPet.setVisibility(View.GONE);
        } else {
            refreshPet.setVisibility(View.VISIBLE);
        }

        if (visible) {
            loadingIndicatorView.setVisibility(View.VISIBLE);
        } else {
            loadingIndicatorView.setVisibility(View.GONE);
        }

    }

    private void setAdapter() {

        refreshPet.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshPet.setRefreshing(false);
                resetPaginationProduct();
                isLoading = true;
                getProduct(category_id + "", "", "");
            }
        });

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                resetPaginationProduct();
                isLoading = true;
                category_id = tab.getTag().toString();
                adapterProduct.clearData();
                loading(true, false);
                getProduct(tab.getTag() + "", "", "");
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        adapterProduct = new ProductAdapter(getActivity(), this);
        adapterProduct.setDatas(new ArrayList<>());
        adapterProduct.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openDialogOptionProduk(adapterProduct.getData().get(position), null);
            }
        });
        layoutManager = new GridLayoutManager(getActivity(), 4);
        rvProduk.setLayoutManager(layoutManager);
        rvProduk.setAdapter(adapterProduct);
        rvProduk.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (page != lastpage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= adapterProduct.getItemCount()) {

                        if (p != page) {
                            p = page;
                            if (new SharedPreference(getContext()).isDoctor()) {
                                getProduct("1", GlobalVariable.LIMIT_ITEM + "", "");
                            } else {

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        getProduct(category_id, GlobalVariable.LIMIT_ITEM + "", "");
                                    }
                                }, 2000);
                            }

                        }
                    }
                }
            }
        });

        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                handler.removeCallbacks(input_finish_checker);

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    last_text_edit = System.currentTimeMillis();
                    handler.postDelayed(input_finish_checker, delay);
                } else {
                    resetPaginationProduct();
                    isLoading = true;
                    Utils.hideSoftKeyboard(getActivity());
                    getProduct(category_id + "", "", "");
                }


            }
        });

        searchView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Utils.hideSoftKeyboardUsingView(getContext(), searchView);
                    return true;
                }
                return false;
            }
        });

        searchView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (searchView.getRight() - searchView.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        Utils.hideSoftKeyboardUsingView(getContext(), searchView);
                        return true;
                    }
                }
                return false;
            }
        });


        selectedProductAdapter = new SelectedProductAdapter(getActivity());
        selectedProductAdapter.setOnItemClickListener((adapterView, view, pos, l) -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (!new SharedPreference(getContext()).isDoctor()) {
                    openDialogOptionProduk(null, selectedProductAdapter.getDatas().get(pos));
                } else {
                    openDialogOptionProduk(null, selectedProductAdapter.getDatas().get(pos));
                }
            }
        });
        selectedProductAdapter.setDatas(new ArrayList<>());
        rvSelectedProduk.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvSelectedProduk.setAdapter(selectedProductAdapter);
    }

    private void resetPaginationProduct() {
        page = 0;
        lastpage = 1;
        p = 0;
    }

    void getProduct(String category_id, String limit, String search) {

        if (isLoading || search.length() != 0) {
            isLoading = false;
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
            loadingIndicatorView.setLayoutParams(layoutParams);
            loading(true, true);
            adapterProduct.clearData();
        } else {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
            loadingIndicatorView.setLayoutParams(layoutParams);
            loading(false, true);
        }
        int laman = getLaman(search);
        String except_category_id = "";
        if (!new SharedPreference(getActivity()).isDoctor()) {
            except_category_id = "1";
        }

        presenter.getProduct(category_id, limit, laman + "", search, except_category_id);
    }

    private int getLaman(String search) {
        if (search.length() != 0) {
            return 0;
        } else {
            return page + 1;
        }
    }

    @Override
    public void onFailGettingData(OnFailGettingData data) {
        if (data.getApi() == OnFailGettingData.GET_PRODUCT) {
            llProduct.setVisibility(View.VISIBLE);
            loading(false, false);
            resetSearchView(new OnRefreshSearchView());
        } else if (data.getApi() == OnFailGettingData.CREATE_CART) {
            Utils.dismissProgressDialog(dialogCreateCart);
            resetSearchView(new OnRefreshSearchView());
            backToCustomer();
        } else if (data.getApi() == OnFailGettingData.CREATE_CART_MEDICAL) {
            Utils.dismissProgressDialog(dialogCreateCart);
            resetSearchView(new OnRefreshSearchView());

        } else if (data.getApi() == OnFailGettingData.GET_PRODUCT_CATEGORY) {
            Utils.dismissProgressDialog(dialogGetCategory);
            backToCustomer();
        } else if (data.getApi() == OnFailGettingData.GET_PROMO) {
            Utils.dismissProgressDialog(dialogPromo);
            backToCustomer();
        } else if (data.getApi() == OnFailGettingData.GET_ALL_DOCTOR) {
            Utils.dismissProgressDialog(dialogGetDoctor);
        } else if (data.getApi() == OnFailGettingData.CLEAN_CART) {
            Utils.dismissProgressDialog(dialogCleanCart);
            searchView.setSelectAllOnFocus(true);
            searchView.selectAll();
        } else if (data.getApi() == OnFailGettingData.ROLLBACK_CART) {
            Utils.dismissProgressDialog(dialogCreateCart);
        } else if (data.getApi() == OnFailGettingData.CREATE_INVOICE) {
            dialogCreateInvoice.dismiss();
        }

        Toast.makeText(getActivity(), data.getThrowable().getMessage(), Toast.LENGTH_SHORT).show();
        data.getThrowable().printStackTrace();
    }

    private void backToCustomer() {
        ((MainActivity) getActivity()).setClearBackStack(true);
        ((MainActivity) getActivity()).toSelectCustomer(false, "shop", "");
    }

    @Subscribe
    private void resetSearchView(OnRefreshSearchView onRefreshSearchView) {
        searchView.requestFocus();
        searchView.setSelection(searchView.getText().length());
        searchView.selectAll();

    }

    @Override
    public void onGetProduct(ProductModel.DATABean dataBeans) {
        dataProduct = dataBeans;
        page = dataBeans.getPage();
        lastpage = dataBeans.getLastPage();
        llProduct.setVisibility(View.VISIBLE);
        if (page == 1) {
            adapterProduct.setDatas(dataBeans.getItems());
        } else {
            adapterProduct.addData(dataBeans.getItems());
        }
        if (dataBeans.getItems().size() == 1) {
            if (dataBeans.getItems().get(0).getProductCode().equals(dataBeans.getSearch())) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    boolean exist = false;
                    int position = 0;
                    for (DetailCart data : selectedProductAdapter.getDatas()) {
                        if (data.getProductId() == dataBeans.getItems().get(0).getProductId()) {
                            exist = true;
                            break;
                        }
                        position++;
                    }

                    if (exist) {
                        openDialogOptionProduk(null, selectedProductAdapter.getDatas().get(position));
                    } else {
                        openDialogOptionProduk(adapterProduct.getData().get(0), null);
                    }

                }
            }
        }
        loading(false, false);
        searchView.requestFocus();
        searchView.setSelection(searchView.getText().length());
        searchView.selectAll();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        EventBus.getDefault().register(this);
    }

    @Subscribe
    public void createCart(RefreshCart refreshCart) {
        if (refreshCart.getCart() == RefreshCart.CART) {
            FormBody.Builder formBuilder = new FormBody.Builder();
            formBuilder.add("customer_id", TreatmentHelper.getInstance().getSelectedCustomer().getCustomerId() + "");
            formBuilder.add("customer_name", TreatmentHelper.getInstance().getSelectedCustomer().getCustomerName());
            formBuilder.add("clean_cart", cleanCart + "");
            RequestBody formBody = formBuilder.build();
            Log.e("error", "dialog create cart added");
            dialogCreateCart = Utils.showProgressDialog(getActivity(), false, "Loading...");


            presenter.createCart(formBody);
        }
    }


    @Override
    public void onCreateCart(OnCreateCart data) {
        cart = data.getData();
        chart_ID = String.valueOf(cart.getCartProductId());
        selectedProductAdapter.setDatas(cart.getItems());
        txtTotalPay.setText(Utils.toRupiah(String.valueOf(cart.getGrandTotal()).replace(".00", "")));
        Utils.dismissProgressDialog(dialogCreateCart);
        resetSearchView(new OnRefreshSearchView());

        if (data.getTAG().equals("CreateCart")) {
            cleanCart = 0;
            TreatmentHelper.getInstance().setCart(data.getData());
        } else {
            if (isFirstRun) {
                savedCartMedicalItem = createBodyCancelMedical(cart);
                isFirstRun = false;
            }
        }
    }

    @Override
    public void onGetProductCategories(ProductCategories.DATABean dataBean) {
        dataCategory = dataBean;
        tabLayout.addTab(tabLayout.newTab().setTag("").setText("All Categories")); // Untuk menampilkan tab all categories
        for (int i = 0; i < dataBean.getItems().size(); i++) {
            tabLayout.addTab(tabLayout.newTab().setTag(dataBean.getItems().get(i).getCategoryId()).setText(dataBean.getItems().get(i).getCategoryName()));
        }

        Utils.dismissProgressDialog(dialogGetCategory);

    }

    @Subscribe
    public void createCartMedicalItem(RefreshCart cart) {
        if (cart.getCart() == RefreshCart.MEDICAL_CART) {
            dialogCreateCart = Utils.showProgressDialog(getActivity(), false, "Loading...");
            presenter.createCartMedicalItem(cart_service_id);
        }
    }

    private CancelMedicalServiceParam createBodyCancelMedical(CartModel.DATABean cart) {
        CancelMedicalServiceParam param = new CancelMedicalServiceParam();
        param.setCartServiceId(cart.getCartServiceId());
        List<CancelMedicalServiceParam.MedicalItemOldBean> datas = new ArrayList<>();
        for (DetailCart selectedData : cart.getItems()) {
            CancelMedicalServiceParam.MedicalItemOldBean data = new CancelMedicalServiceParam.MedicalItemOldBean();
            data.setCartServiceId(selectedData.getCartServiceId());
            data.setProductId(selectedData.getProductId());
            data.setProductName(selectedData.getProductName());
            data.setDiscountAmount(selectedData.getDiscountAmount());
            data.setProductPrice(selectedData.getProductPrice());
            data.setProductNote(selectedData.getProductNote());
            data.setProductQty(selectedData.getProductQty());
            data.setDiscountId(selectedData.getDiscountId());
            data.setDiscountName(selectedData.getDiscountName());
            data.setDoctorId(selectedData.getDoctorId());
            data.setDoctorName(selectedData.getDoctorName());
            data.setIsInvoice(selectedData.getIsInvoice());
            data.setCreatedBy(selectedData.getCreatedBy());
            data.setUpdatedBy(selectedData.getUpdatedBy());
            data.setDeletedAt(selectedData.getDeletedAt());
            data.setCreatedAt(selectedData.getCreatedAt());
            data.setUpdatedAt(selectedData.getUpdatedAt());
            data.setCategoryId(selectedData.getCategoryId());
            data.setCategoryName(selectedData.getCategoryName());
            data.setProductCode(selectedData.getProductCode());
            if (data.getProductCode() == null || data.getProductCode().equals("")) {
                Toast.makeText(getActivity(), "there is no product code", Toast.LENGTH_SHORT).show();
            }
            datas.add(data);
        }
        param.setMedicalItemOld(datas);
        return param;
    }

    private void getCategoryProduct() {

        dialogGetCategory = Utils.showProgressDialog(getActivity(), false, "Loading...");
        presenter.getProductCategory();
    }


    private void getPromo() {

        dialogPromo = Utils.showProgressDialog(getActivity(), false, "Loading...");
        presenter.getPromo();
    }

    @Override
    public void onGetPromo(PromoModel.DATABean dataBean) {
        listPromo.clear();
        listPromo.addAll(dataBean.getItems());
        Log.d("listPromo", "" + listPromo.size());
        Utils.dismissProgressDialog(dialogPromo);
    }

    private void getAllDoctor() {
        dialogGetDoctor = Utils.showProgressDialog(getActivity(), false, "Loading...");

        Map<String, String> params = new HashMap<>();
        params.put("limit", "999");
        params.put("page", "0");
        params.put("search", "");

        presenter.getAllDoctor(params);
    }

    @Override
    public void onGetAllDoctor(GetDoctorSchedule.DATABean dataBeans) {
        Utils.dismissProgressDialog(dialogGetDoctor);
        listAllDoctor.addAll(dataBeans.getItems());
    }

    void cleanCart() {
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("customer_id", TreatmentHelper.getInstance().getSelectedCustomer().getCustomerId() + "");
        RequestBody formBody = formBuilder.build();
        dialogCleanCart = Utils.showProgressDialog(getActivity(), false, "Loading...");
        presenter.cleanCart(formBody);

    }

    @Override
    public void onCleanCart(CleaningCart cleaningCart) {
        Utils.dismissProgressDialog(dialogCleanCart);
        createCart(new RefreshCart(RefreshCart.CART));
        searchView.setSelectAllOnFocus(true);
        searchView.selectAll();

    }

    @Override
    public void onCreateInvoiceProduct(DataInvoice dataInvoice) {
        Utils.dismissProgressDialog(dialogCreateInvoice);
        Log.d("resultShop", dataInvoice.getOrders().size() + "");
        ((MainActivity) Objects.requireNonNull(getContext())).bundle.putParcelable("dataInvoice", dataInvoice);
        ((MainActivity) Objects.requireNonNull(getContext())).position = 3;
        ((MainActivity) Objects.requireNonNull(getContext())).changePosition();

    }

    @Subscribe
    public void onRollbackCart(RollbackCart rollbackCart) {
        Utils.dismissProgressDialog(dialogCreateCart);
        getActivity().onBackPressed();


    }

    private void rollBackMedicalItem() {
        dialogCreateCart = Utils.showProgressDialog(getActivity(), false, "Loading...");
        Connector.newInstance(getActivity()).rollbackMedicalItem(savedCartMedicalItem, new Connector.ApiCallback<String>() {
            @Override
            public String getKey() {
                return TAG;
            }

            @Override
            public void success(String s, String messages) {
                EventBus.getDefault().post(new RollbackCart());

            }

            @Override
            public void onFailure(Throwable t) {
                EventBus.getDefault().post(new OnFailGettingData(OnFailGettingData.ROLLBACK_CART, t));
            }
        });

    }

    @OnClick(R.id.btn_cancel)
    public void onClickBtnCancel() {
        if (new SharedPreference(getContext()).isDoctor()) {
            rollBackMedicalItem();
        } else {
            cleanCart();
        }
    }


    private void openDialogOptionProduk(ProductModel.DATABean.ItemsBean productItem, DetailCart productCart) {
        DialogShop dialogShop = DialogShop.newInstance(listAllDoctor, productItem, productCart, cart, listPromo, cart_service_id);
        dialogShop.setCancelable(false);
        dialogShop.show(getChildFragmentManager(), "");

    }

    @OnClick(R.id.btn_addInvoice)
    public void addToInvoice() {
        if (new SharedPreference(getContext()).isDoctor()) {
            getActivity().onBackPressed();

//            ((MainActivity) Objects.requireNonNull(getActivity())).toDoctorTakeAction(TreatmentHelper.getInstance().getSavedAnamnesis());
        } else createInvoiceProduct();
    }

    private void createInvoiceProduct() {
        dialogCreateInvoice = Utils.showProgressDialog(getContext(), false, "Loading...");
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("cart_product_id", chart_ID);
        RequestBody formBody = formBuilder.build();

        presenter.createInvoiceProduct(formBody);
    }

}