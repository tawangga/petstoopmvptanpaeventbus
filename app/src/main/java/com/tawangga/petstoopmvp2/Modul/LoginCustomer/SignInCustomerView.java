package com.tawangga.petstoopmvp2.Modul.LoginCustomer;

import com.tawangga.petstoopmvp2.Model.SignInCustomer;
import com.tawangga.petstoopmvp2.Modul.BaseView;

public interface SignInCustomerView extends BaseView {
    void onSuccessSignIn(SignInCustomer.DATA dataBean);
}
