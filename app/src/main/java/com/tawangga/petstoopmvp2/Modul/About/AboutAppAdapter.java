package com.tawangga.petstoopmvp2.Modul.About;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.ModelNew.GetAppVersion;
import com.tawangga.petstoopmvp2.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AboutAppAdapter extends RecyclerView.Adapter<AboutAppAdapter.ViewHolder> {

    private List<GetAppVersion.DATABean.ItemsBean> data;
    private Context context;
    private AdapterView.OnItemClickListener onItemClickListener;

    public AboutAppAdapter(Context context) {

        this.context = context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_about_application, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.position = position;
        final long identity = System.currentTimeMillis();
        holder.identity = identity;
        GetAppVersion.DATABean.ItemsBean selectedData = data.get(position);
        holder.txtVersionName.setText(selectedData.getVersionName());
        holder.txtVersionDesc.setText(selectedData.getChangeLog());

    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setDatas(List<GetAppVersion.DATABean.ItemsBean> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        int position;
        long identity;
        @BindView(R.id.txt_version_name)
        TextView txtVersionName;
        @BindView(R.id.txt_version_desc)
        TextView txtVersionDesc;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(null, v, position, 0);
            }

        }
    }
}