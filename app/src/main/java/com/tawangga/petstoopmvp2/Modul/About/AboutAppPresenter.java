package com.tawangga.petstoopmvp2.Modul.About;

import android.content.Context;

import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.ModelNew.GetAppVersion;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import java.util.Map;

public class AboutAppPresenter {
    private final Context context;
    private final String tag;
    private AboutAppView view;

    public AboutAppPresenter(Context context, String TAG, AboutAppView view) {

        this.context = context;
        tag = TAG;
        this.view = view;
    }

    public void getAppVersion(Map<String, String> params) {
        Connector.newInstance(context).getAppVersion(params, new Connector.ApiCallback<GetAppVersion.DATABean>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(GetAppVersion.DATABean dataBean, String messages) {
                view.onSuccessAboutApp(dataBean);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.GET_APP_VERSION, t));
            }
        });

    }
}
