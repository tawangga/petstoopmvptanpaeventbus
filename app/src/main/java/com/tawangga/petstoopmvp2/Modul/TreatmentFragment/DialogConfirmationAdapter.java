package com.tawangga.petstoopmvp2.Modul.TreatmentFragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.ModelNew.CreateCart;
import com.tawangga.petstoopmvp2.R;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class DialogConfirmationAdapter extends RecyclerView.Adapter<DialogConfirmationAdapter.ViewHolder> {
    private Context context;
    private CreateCart.DATABean cartData;
    private TreatmentReservationAdapter reservationAdapter;
    private List<CreateCart.DATABean.ServiceBean> data;

    public DialogConfirmationAdapter(Context context, CreateCart.DATABean cartData, TreatmentReservationAdapter reservationAdapter) {

        this.context = context;
        this.cartData = cartData;
        this.reservationAdapter = reservationAdapter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_dialog_confirmation, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CreateCart.DATABean.ServiceBean selectedData = data.get(position);
        holder.txtServiceName.setText(selectedData.getServiceName());
        holder.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (reservationAdapter != null) {
                    if (selectedData.getServiceId() == 1 && selectedData.getServiceType().equals("1") && data.size() > 1) {
                        Toast.makeText(context, "Can't delete konsultasi dokter", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    reservationAdapter.addDeletedServices(selectedData.getCartServiceDetailId());
                    data.remove(position);
                    notifyDataSetChanged();
                } else {
                    if (selectedData.getServiceId() != 0) {
                        deleteServiceDetail(selectedData);
                    } else {
                        deleteBoarding(position);
                    }
                }
            }
        });
    }

    private void deleteBoarding(int position) {
        cartData.getBoardingSchedule().setCageId(0);
        cartData.getBoardingSchedule().setScheduleDateFrom("");
        cartData.getBoardingSchedule().setCageName("");
        cartData.getBoardingSchedule().setScheduleDateUntil("");
        cartData.getBoardingSchedule().setCagePrice("");
        data.remove(position);
        notifyDataSetChanged();
    }

    public void deleteServiceDetail(CreateCart.DATABean.ServiceBean selectedData) {
//        ((MainActivity) context).setIsLoading(true);
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("cart_service_id", selectedData.getCartServiceId() + "");
        formBuilder.add("service_id", selectedData.getServiceId() + "");
        formBuilder.add("service_type", selectedData.getServiceType() + "");
        RequestBody formBody = formBuilder.build();
        ProgressDialog dialog = Utils.showProgressDialog(context, false, "Loading...");

        Connector.newInstance(context).deleteServiceTreatment(formBody, new Connector.ApiCallback<String>() {
            @Override
            public String getKey() {
                return null;
            }

            @Override
            public void success(String dataBean, String messages) {
//                ((MainActivity) context).setIsLoading(false);
                data.remove(selectedData);
                notifyDataSetChanged();
                dialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
//                ((MainActivity) context).setIsLoading(false);
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                t.printStackTrace();

            }
        });
    }

    public List<CreateCart.DATABean.ServiceBean> getData() {
        return data;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setDatas(List<CreateCart.DATABean.ServiceBean> data) {

        this.data = data;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_service_name)
        TextView txtServiceName;
        @BindView(R.id.btn_cancel)
        ImageView btnCancel;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
