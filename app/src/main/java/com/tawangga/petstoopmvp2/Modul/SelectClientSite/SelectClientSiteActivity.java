package com.tawangga.petstoopmvp2.Modul.SelectClientSite;

import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.Helper.SharedPreference;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Helper.ViewUtils;
import com.tawangga.petstoopmvp2.Model.FindSite;
import com.tawangga.petstoopmvp2.R;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SelectClientSiteActivity extends AppCompatActivity implements SelectClientSiteView {

    private static final String TAG = SelectClientSiteActivity.class.getSimpleName();
    @BindView(R.id.txt_client)
    AutoCompleteTextView txtClient;
    @BindView(R.id.firstLayout)
    LinearLayout firstLayout;
    @BindView(R.id.txt_site)
    AutoCompleteTextView txtSite;
    @BindView(R.id.secondLayout)
    LinearLayout secondLayout;
    @BindView(R.id.pb_loading)
    ProgressBar pbLoading;
    @BindView(R.id.btn_pilih)
    Button btnPilih;
    @BindView(R.id.ll_main_login)
    LinearLayout llMainLogin;
    private SelectClientSitePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_client_site);
        ButterKnife.bind(this);
        presenter = new SelectClientSitePresenter(this, TAG, this);
        initView();
    }

    private void initView() {
        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.50);
        llMainLogin.getLayoutParams().width = width;
        llMainLogin.requestLayout();

        txtClient.setText("C000002");
        txtSite.setText("S000003");
//        presenter.findSite(txtClient.getText().toString().trim(),txtSite.getText().toString().trim());

        if (new SharedPreference(this).getSavedClientSite() != null) {
            txtClient.setText(new SharedPreference(this).getSavedClientSite().getClientCode());
            txtSite.setText(new SharedPreference(this).getSavedClientSite().getSiteCode());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Connector.newInstance(this).close(TAG);
    }

    @OnClick(R.id.btn_pilih)
    void onClickPilih() {
        pbLoading.setVisibility(View.VISIBLE);
        btnPilih.setVisibility(View.GONE);

        presenter.findSite(txtClient.getText().toString().trim(), txtSite.getText().toString().trim());
    }

    @Override
    public void onSuccessFindSite(FindSite.DATABean data) {
        ViewUtils.changeVisibility(btnPilih);
        ViewUtils.changeVisibility(pbLoading);
        new SharedPreference(this).setSavedClientSite(new Gson().toJson(data));
//        Intent intent = new Intent(this, SignInActivity.class);
//        startActivity(intent);
        finish();

    }

    @Override
    public void onFailGettingData(OnFailGettingData data) {
        Utils.showToast(this, data.getThrowable().getMessage());
        ViewUtils.changeVisibility(btnPilih);
        ViewUtils.changeVisibility(pbLoading);
        data.getThrowable().printStackTrace();

    }
}
