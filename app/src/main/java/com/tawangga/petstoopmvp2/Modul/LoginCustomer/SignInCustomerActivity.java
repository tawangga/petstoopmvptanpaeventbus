package com.tawangga.petstoopmvp2.Modul.LoginCustomer;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.Helper.SharedPreference;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Helper.ViewUtils;
import com.tawangga.petstoopmvp2.Model.SignInCustomer;
import com.tawangga.petstoopmvp2.Modul.Login.SignInActivity;
import com.tawangga.petstoopmvp2.Modul.Main.MainActivity;
import com.tawangga.petstoopmvp2.Modul.RegisterCustomer.RegisterCustomerActivity;
import com.tawangga.petstoopmvp2.Modul.SelectClientSite.SelectClientSiteActivity;
import com.tawangga.petstoopmvp2.R;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignInCustomerActivity extends AppCompatActivity implements SignInCustomerView{
    private static final String TAG = SignInCustomerActivity.class.getSimpleName();
    @BindView(R.id.btn_select_client_site)
    ImageView btnSelectClientSite;
    @BindView(R.id.txt_client)
    TextView txtClient;
    @BindView(R.id.txt_site)
    TextView txtSite;
    @BindView(R.id.txt_email)
    EditText txtEmail;
    @BindView(R.id.txt_password)
    EditText txtPassword;
    @BindView(R.id.pb_loading)
    ProgressBar pbLoading;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.ll_main_login)
    LinearLayout llMainLogin;
    private SignInCustomerPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in_customer);
        ButterKnife.bind(this);
        presenter = new SignInCustomerPresenter(this, TAG,this);

        initView();
    }

    private void initView() {
        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.50);
        llMainLogin.getLayoutParams().width = width;
        llMainLogin.requestLayout();

        if (new SharedPreference(this).getIsLogin()) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }

        txtEmail.setText("ruditawangga@gmail.com");
        txtPassword.setText("083823163915");
    }



    @Override
    protected void onResume() {
        super.onResume();
        if (new SharedPreference(this).getSavedClientSite() == null)
            selectClientSite();
    }


    void selectClientSite() {
        Toast.makeText(this, "Pilih client dan site", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this, SelectClientSiteActivity.class));
    }


    @OnClick(R.id.btn_select_client_site)
    void toSelectClient() {
        startActivity(new Intent(this, SelectClientSiteActivity.class));

    }

    @OnClick(R.id.btn_daftar)
    void onClickDaftar(){
        startActivity(new Intent(this, RegisterCustomerActivity.class));
    }

    @OnClick(R.id.btn_login_staff)
    void onClickSignInStaff(){
        startActivity(new Intent(this, SignInActivity.class));
    }

    @OnClick(R.id.btn_login)
    public void onLoginClicked() {
        ViewUtils.changeVisibility(pbLoading);
        ViewUtils.changeVisibility(btnLogin);

        presenter.doLogin(txtEmail.getText().toString().trim(), txtPassword.getText().toString().trim(), new SharedPreference(this).getSavedClientSite().getSiteId());

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Connector.newInstance(this).close(TAG);
    }

    @Override
    public void onSuccessSignIn(SignInCustomer.DATA dataBean) {
        ViewUtils.changeVisibility(btnLogin);
        ViewUtils.changeVisibility(pbLoading);

        toMainActivity(dataBean);

    }


    private void toMainActivity(SignInCustomer.DATA data) {
        new SharedPreference(this).setIsLogin(true);
        new SharedPreference(this).setCustomerId(String.valueOf(data.getCustomerId()));
        new SharedPreference(this).setCustomerName(data.getCustomerName());
        new SharedPreference(this).setCustomerEmail(data.getCustomerEmail());
        new SharedPreference(this).setCustomerPhoto(data.getCustomerPhoto());
        new SharedPreference(this).setStaffToken(data.getToken());
        new SharedPreference(this).setCustomerPhone(data.getCustomerPhone());
        new SharedPreference(this).setCustomerAddres(data.getCustomerAddress());
        new SharedPreference(this).setCustomerCode(data.getCustomerCode());
        new SharedPreference(this).setIsCustomer(true);
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void onFailGettingData(OnFailGettingData data) {
        Utils.showToast(this, data.getThrowable().getMessage());
        ViewUtils.changeVisibility(btnLogin);
        ViewUtils.changeVisibility(pbLoading);
        data.getThrowable().printStackTrace();

    }
}