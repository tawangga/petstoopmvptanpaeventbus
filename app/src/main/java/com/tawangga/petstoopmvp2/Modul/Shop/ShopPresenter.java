package com.tawangga.petstoopmvp2.Modul.Shop;

import android.content.Context;

import com.tawangga.petstoopmvp2.EventBus.CleaningCart;
import com.tawangga.petstoopmvp2.EventBus.OnCreateCart;
import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.Model.CartModel;
import com.tawangga.petstoopmvp2.Model.DataInvoice;
import com.tawangga.petstoopmvp2.Model.ProductCategories;
import com.tawangga.petstoopmvp2.Model.ProductModel;
import com.tawangga.petstoopmvp2.Model.PromoModel;
import com.tawangga.petstoopmvp2.ModelNew.GetDoctorSchedule;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import java.util.Map;

import okhttp3.RequestBody;

public class ShopPresenter {
    private final Context context;
    private final String tag;
    private ShopFragmentView view;

    public ShopPresenter(Context context, String TAG, ShopFragmentView view) {

        this.context = context;
        tag = TAG;
        this.view = view;
    }

    public void getProduct(String category_id, String limit, String laman, String search, String except_category_id) {
        Connector.newInstance(context).getProduct(category_id, limit, laman + "", search, except_category_id, new Connector.ApiCallback<ProductModel.DATABean>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(ProductModel.DATABean dataBeans, String messages) {
                dataBeans.setSearch(search);
                view.onGetProduct(dataBeans);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.GET_PRODUCT, t));
            }
        });

    }

    public void createCart(RequestBody formBody) {
        Connector.newInstance(context).createCart(formBody, new Connector.ApiCallback<CartModel.DATABean>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(CartModel.DATABean dataBean, String messages) {
                view.onCreateCart(new OnCreateCart(dataBean, "CreateCart"));
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.CREATE_CART, t));
            }
        });

    }

    public void createCartMedicalItem(String cart_service_id) {
        Connector.newInstance(context).getCartMedicalItems(cart_service_id, new Connector.ApiCallback<CartModel.DATABean>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(CartModel.DATABean dataBean, String messages) {
                view.onCreateCart(new OnCreateCart(dataBean, "CreateCartMedical"));
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.CREATE_CART_MEDICAL, t));
            }
        });

    }

    public void getProductCategory() {
        Connector.newInstance(context).getProdCategory(new Connector.ApiCallback<ProductCategories.DATABean>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(ProductCategories.DATABean dataBean, String messages) {
                view.onGetProductCategories(dataBean);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.GET_PRODUCT_CATEGORY, t));
            }
        });

    }

    public void getPromo() {
        Connector.newInstance(context).getPromo("", "", "", new Connector.ApiCallback<PromoModel.DATABean>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(PromoModel.DATABean dataBean, String messages) {
                view.onGetPromo(dataBean);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.GET_PROMO, t));
            }
        });

    }

    public void getAllDoctor(Map<String, String> params) {
        Connector.newInstance(context).getAllDoctor(params, new Connector.ApiCallback<GetDoctorSchedule.DATABean>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(GetDoctorSchedule.DATABean dataBeans, String messages) {
                view.onGetAllDoctor(dataBeans);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.GET_ALL_DOCTOR, t));
            }
        });
    }

    public void cleanCart(RequestBody formBody) {
        Connector.newInstance(context).deleteShopCart(formBody, new Connector.ApiCallback<String>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(String s, String messages) {
                view.onCleanCart(new CleaningCart());
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.CLEAN_CART, t));
            }
        });
    }

    public void createInvoiceProduct(RequestBody formBody) {
        Connector.newInstance(context).createInvoiceProduct(formBody, new Connector.ApiCallback<DataInvoice>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(DataInvoice dataInvoice, String messages) {
                view.onCreateInvoiceProduct(dataInvoice);

            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.CREATE_INVOICE, t));
            }
        });

    }
}
