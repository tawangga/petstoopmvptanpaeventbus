package com.tawangga.petstoopmvp2.Modul.AddNewTreatment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.Helper.GlobalVariable;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.ModelNew.CreateCart;
import com.tawangga.petstoopmvp2.ModelNew.GetBoarding;
import com.tawangga.petstoopmvp2.ModelNew.GetDoctor;
import com.tawangga.petstoopmvp2.ModelNew.GetGroomingLocation;
import com.tawangga.petstoopmvp2.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SelectTreatmentNewAdapter extends RecyclerView.Adapter<SelectTreatmentNewAdapter.ViewHolder> {

    private AdapterView.OnItemClickListener onItemClickListener;
    private List<GetDoctor.DATABean.ItemsBean> dataDoctor;
    private Context context;
    boolean isDoctor, isBoarding, isGrooming;
    private List<GetBoarding.DATABean.ItemsBean> dataBoarding;
    private List<GetGroomingLocation.DATABean.ItemsBean> dataGroomingLocation;
    private int selectedPosition = -1;
    private List<GetGroomingLocation.DATABean.ItemsBean> items;
    private CreateCart.DATABean cartData;
    private TextView tvCustomerName;

    public SelectTreatmentNewAdapter(Context context) {

        this.context = context;
        dataDoctor = new ArrayList<>();
        dataBoarding = new ArrayList<>();
        dataGroomingLocation = new ArrayList<>();
    }

    @NonNull
    @Override
    public SelectTreatmentNewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_select_treatment, parent, false);

        return new SelectTreatmentNewAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SelectTreatmentNewAdapter.ViewHolder holder, int position) {
        holder.position = position;
        final long identity = System.currentTimeMillis();
        holder.identity = identity;

        if (position % 2 == 1) {
            holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.brown_light_2));
        }

        if (isDoctor) {
            holder.txtTime.setVisibility(View.VISIBLE);
            GetDoctor.DATABean.ItemsBean selectedDataDoctor = dataDoctor.get(position);
            if ((cartData.getDoctorSchedule().getStaffId() == selectedDataDoctor.getStaffId())
                    && (cartData.getDoctorSchedule().getScheduleTime().equals(selectedDataDoctor.getTime()))) {
                holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.brown_selected));
                selectedDataDoctor.setIsAvailable(true);
            }
            holder.txtService.setText(selectedDataDoctor.getName());
            holder.txtTime.setText(Utils.changeDateFormat(selectedDataDoctor.getTime(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));
            if (selectedDataDoctor.isIsAvailable()) {
                holder.txtStatus.setTextColor(context.getResources().getColor(R.color.white));
                holder.bgStatus.setBackground(context.getResources().getDrawable(R.drawable.bg_status_available));
                holder.txtStatus.setText("Available");

            } else {
                holder.txtStatus.setTextColor(context.getResources().getColor(R.color.brown_light));
                holder.bgStatus.setBackground(context.getResources().getDrawable(R.drawable.bg_status_reserved));
                holder.txtStatus.setText("Reserved");
            }
        } else if (isGrooming) {
            holder.txtTime.setVisibility(View.VISIBLE);
            GetGroomingLocation.DATABean.ItemsBean selectedDataGroomingLocation = dataGroomingLocation.get(position);
            if ((cartData.getGroomingLocationSchedule().getGroomingLocationId() == selectedDataGroomingLocation.getGroomingLocationId()) &&
                    (cartData.getGroomingLocationSchedule().getScheduleTime().equals(selectedDataGroomingLocation.getTime()))) {
                holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.brown_selected));
                selectedDataGroomingLocation.setIsAvailable(true);
            }
            holder.txtService.setText(selectedDataGroomingLocation.getName());
            holder.txtTime.setText(Utils.changeDateFormat(selectedDataGroomingLocation.getTime(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));

            if (selectedDataGroomingLocation.isIsAvailable()) {
                holder.txtStatus.setTextColor(context.getResources().getColor(R.color.white));
                holder.bgStatus.setBackground(context.getResources().getDrawable(R.drawable.bg_status_available));
                holder.txtStatus.setText("Available");
            } else {
                holder.txtStatus.setTextColor(context.getResources().getColor(R.color.brown_light));
                holder.bgStatus.setBackground(context.getResources().getDrawable(R.drawable.bg_status_reserved));
                holder.txtStatus.setText("Reserved");
            }
        } else if (isBoarding) {
            holder.txtTime.setVisibility(View.GONE);
            GetBoarding.DATABean.ItemsBean selectedDataBoarding = dataBoarding.get(position);
            if ((cartData.getBoardingSchedule().getCageId() == selectedDataBoarding.getCageId()) && (cartData.getCartServiceId() == selectedDataBoarding.getCartServiceId() || selectedDataBoarding.getCartServiceId() == 0)) {
                holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.brown_selected));
                selectedDataBoarding.setIsAvailable(true);
            }

            if ((cartData.getBoardingSchedule().getCageId() == selectedDataBoarding.getCageId()) && (cartData.getCartServiceId() != selectedDataBoarding.getCartServiceId() && selectedDataBoarding.getCartServiceId() != 0)) {
                cartData.getBoardingSchedule().setCageId(0);
                cartData.getBoardingSchedule().setScheduleDateFrom("");
                cartData.getBoardingSchedule().setCageName("");
                cartData.getBoardingSchedule().setScheduleDateUntil("");
                cartData.getBoardingSchedule().setCagePrice("");
                if (tvCustomerName != null) {
                    tvCustomerName.setText(cartData.getBoardingSchedule().getCageName());
                }
            }
            holder.txtService.setText(selectedDataBoarding.getCageName());
            if (selectedDataBoarding.isIsAvailable()) {
                holder.txtStatus.setTextColor(context.getResources().getColor(R.color.white));
                holder.bgStatus.setBackground(context.getResources().getDrawable(R.drawable.bg_status_available));
                holder.txtStatus.setText("Available");

            } else {
                holder.txtStatus.setTextColor(context.getResources().getColor(R.color.brown_light));
                holder.bgStatus.setBackground(context.getResources().getDrawable(R.drawable.bg_status_reserved));
                holder.txtStatus.setText("Reserved");
            }
        }
    }

    @Override
    public int getItemCount() {
        if (isDoctor) {
            return dataDoctor.size();
        } else if (isGrooming) {
            return dataGroomingLocation.size();
        } else if (isBoarding) {
            return dataBoarding.size();
        }
        return 0;
    }

    public List<GetDoctor.DATABean.ItemsBean> getDataDoctor() {
        return dataDoctor;
    }

    public void clearAllData() {
        dataDoctor.clear();
        dataBoarding.clear();
        dataGroomingLocation.clear();
        notifyDataSetChanged();
    }

    public void setDataDoctor(List<GetDoctor.DATABean.ItemsBean> dataDoctor, CreateCart.DATABean cartData) {
        this.cartData = cartData;
        this.dataDoctor.addAll(dataDoctor);
        isDoctor = true;
        isBoarding = false;
        isGrooming = false;
        notifyDataSetChanged();
    }

    public void setDataBoarding(List<GetBoarding.DATABean.ItemsBean> items, CreateCart.DATABean cartData, TextView tvCustomerName) {
        this.cartData = cartData;
        this.tvCustomerName = tvCustomerName;
        this.dataBoarding.addAll(items);
        isBoarding = true;
        isGrooming = false;
        isDoctor = false;
        notifyDataSetChanged();
    }


    public List<GetBoarding.DATABean.ItemsBean> getDataBoarding() {
        return dataBoarding;
    }

    public void setDataGroomingLocation(List<GetGroomingLocation.DATABean.ItemsBean> items, CreateCart.DATABean cartData) {
        this.cartData = cartData;
        this.dataGroomingLocation.addAll(items);
        isBoarding = false;
        isGrooming = true;
        isDoctor = false;
        notifyDataSetChanged();
    }

    public List<GetGroomingLocation.DATABean.ItemsBean> getDataGroomingLocation() {
        return dataGroomingLocation;
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.txt_time)
        TextView txtTime;
        @BindView(R.id.txt_service)
        TextView txtService;
        @BindView(R.id.txt_status)
        TextView txtStatus;
        @BindView(R.id.bg_status)
        LinearLayout bgStatus;
        @BindView(R.id.ll_background)
        RelativeLayout llBackground;
        int position;
        long identity;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(null, v, position, 0);
            }

        }
    }
}
