package com.tawangga.petstoopmvp2.Modul.AddNewTreatment;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.ModelNew.CreateCart;
import com.tawangga.petstoopmvp2.R;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class SelectedServiceAdapter extends RecyclerView.Adapter<SelectedServiceAdapter.ViewHolder> {
    private Context context;
    private TextView txt_estimated_prie;
    int estimatedPrice = 0;
    private List<CreateCart.DATABean.ServiceBean> datas;

    public SelectedServiceAdapter(Context context, TextView txt_estimated_prie) {

        this.context = context;
        this.txt_estimated_prie = txt_estimated_prie;
        datas = new ArrayList<>();
    }

    @NonNull
    @Override
    public SelectedServiceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_selected_services, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SelectedServiceAdapter.ViewHolder holder, int position) {
        CreateCart.DATABean.ServiceBean data = datas.get(position);
        holder.txtServiceName.setText(data.getServiceName());
        holder.txtServicePrice.setText(Utils.toRupiah2(data.getServicePrice()));
        holder.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteService(data);
            }
        });
        estimatedPrice += Integer.parseInt(data.getServicePrice().replace(".00", ""));
        txt_estimated_prie.setText(Utils.toRupiah2(String.valueOf(estimatedPrice)));

    }

    private void deleteService(CreateCart.DATABean.ServiceBean data) {
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("cart_service_id", data.getCartServiceId() + "");
        formBuilder.add("service_id", data.getServiceId() + "");
        formBuilder.add("service_type", data.getServiceType() + "");
        RequestBody formBody = formBuilder.build();
        ProgressDialog dialog = Utils.showProgressDialog(context, false, "Loading...");

        Connector.newInstance(context).deleteServiceTreatment(formBody, new Connector.ApiCallback<String>() {
            @Override
            public String getKey() {
                return null;
            }

            @Override
            public void success(String dataBean, String messages) {
                datas.remove(data);
                notifyDataChanged();
                dialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                t.printStackTrace();

            }
        });
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public void notifyDataChanged() {
        estimatedPrice = 0;
        txt_estimated_prie.setText(Utils.toRupiah2(String.valueOf(estimatedPrice)));
        notifyDataSetChanged();
    }

    public void setDatas(List<CreateCart.DATABean.ServiceBean> datas) {

        this.datas = datas;
        notifyDataChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_service_name)
        TextView txtServiceName;
        @BindView(R.id.txt_service_price)
        TextView txtServicePrice;
        @BindView(R.id.btn_cancel)
        ImageView btnCancel;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
