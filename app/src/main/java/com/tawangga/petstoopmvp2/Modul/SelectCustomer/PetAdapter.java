package com.tawangga.petstoopmvp2.Modul.SelectCustomer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.tawangga.petstoopmvp2.Model.CustomerModel;
import com.tawangga.petstoopmvp2.Model.PetListModel;
import com.tawangga.petstoopmvp2.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class PetAdapter extends RecyclerView.Adapter<PetAdapter.ViewHolder> {
    private List<PetListModel.DATABean.ItemsBean> datas = new ArrayList<>();
    private List<CustomerModel.DATABean.ItemsBean> datasCustomer = new ArrayList<>();

    Context context;
    private AdapterView.OnItemClickListener onItemClickListener;

    public PetAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_pet, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.position = position;
        holder.identity = System.currentTimeMillis();

        if (position % 2 == 1) {
            holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.brown_light_2));
        } else {
            holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

        if (datas != null && datas.size() != 0) {
            PetListModel.DATABean.ItemsBean data = datas.get(position);
            holder.txtOwnerName.setText(data.getCustomerName());
            holder.txtPetName.setText(data.getPetName());
            holder.tvId.setText(data.getPetCode() + "");
            Glide.with(context)
                    .load(data.getPetPhoto())
                    .error(context.getResources().getDrawable(R.drawable.pic_pet))
                    .into(holder.ivPetPic);
        } else if (datasCustomer != null && datasCustomer.size() != 0) {
            CustomerModel.DATABean.ItemsBean datAcustomer = datasCustomer.get(position);
            holder.txtPetName.setText(datAcustomer.getCustomerName());
            holder.tvId.setText(datAcustomer.getCustomerCode());
            holder.txtOwnerName.setVisibility(View.GONE);
            Glide.with(context)
                    .load(datAcustomer.getCustomerPhoto())
                    .error(context.getResources().getDrawable(R.drawable.pic))
                    .into(holder.ivPetPic);
        }

    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public int getItemCount() {
        if (datas != null && datas.size() != 0) return datas.size();
        else if (datasCustomer != null && datasCustomer.size() != 0) return datasCustomer.size();
        else return 0;
    }

    public void setDatas(List<PetListModel.DATABean.ItemsBean> data, List<CustomerModel.DATABean.ItemsBean> datAcustomer) {
        if (data != null) {
            this.datas.addAll(data);
        } else if (datAcustomer != null) {
            this.datasCustomer.addAll(datAcustomer);
        }
        notifyDataSetChanged();
    }

    public void clearData() {
        if (datasCustomer.size() != 0) this.datasCustomer.clear();
        else if (datas.size() != 0) this.datas.clear();

        notifyDataSetChanged();
    }

    public List<PetListModel.DATABean.ItemsBean> getDatas() {
        return datas;
    }

    public List<CustomerModel.DATABean.ItemsBean> getDatasCustomer() {
        return datasCustomer;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.iv_pet_pic)
        CircleImageView ivPetPic;
        @BindView(R.id.txt_owner_name)
        TextView txtOwnerName;
        @BindView(R.id.txt_pet_name)
        TextView txtPetName;
        @BindView(R.id.tv_id)
        TextView tvId;
        @BindView(R.id.ll_background)
        LinearLayout llBackground;
        int position;
        long identity;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(null, v, position, 0);
            }

        }
    }
}
