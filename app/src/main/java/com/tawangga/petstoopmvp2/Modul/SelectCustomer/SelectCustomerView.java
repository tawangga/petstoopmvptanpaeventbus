package com.tawangga.petstoopmvp2.Modul.SelectCustomer;

import com.tawangga.petstoopmvp2.Helper.SaveOwner;
import com.tawangga.petstoopmvp2.Model.CustomerModel;
import com.tawangga.petstoopmvp2.Model.DetailCustomerModel;
import com.tawangga.petstoopmvp2.Model.PetListModel;
import com.tawangga.petstoopmvp2.ModelNew.GetTreatmentRecord;
import com.tawangga.petstoopmvp2.Modul.BaseView;

import java.util.List;

public interface SelectCustomerView extends BaseView {
    void onGetPetTreatmentRecord(List<GetTreatmentRecord.DATABean> dataBeans);

    void onGetCustomerByPet(CustomerModel.DATABean dataBeans);

    void onGetPetList(PetListModel.DATABean dataBeans);

    void onGetCustomerPage(PetListModel.DATABean dataBeans);

    void onGetCustomer(CustomerModel.DATABean dataBeans);

    void onGetDetailCustomer(DetailCustomerModel.DATABean dataBean);

    void onSaveOwner(SaveOwner dataBean);
}
