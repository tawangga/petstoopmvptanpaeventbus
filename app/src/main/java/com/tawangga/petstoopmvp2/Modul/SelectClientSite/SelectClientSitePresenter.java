package com.tawangga.petstoopmvp2.Modul.SelectClientSite;

import android.content.Context;

import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.Model.FindSite;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import okhttp3.FormBody;
import okhttp3.RequestBody;

public class SelectClientSitePresenter {
    private final Context context;
    private final String TAG;
    private SelectClientSiteView view;

    public SelectClientSitePresenter(Context context, String TAG, SelectClientSiteView view) {
        this.context = context;
        this.TAG = TAG;
        this.view = view;
    }

    public void findSite(String client, String site) {
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("client_code", client);
        formBuilder.add("site_code", site);
        RequestBody formBody = formBuilder.build();
        Connector.newInstance(context).findSite(formBody, new Connector.ApiCallback<FindSite.DATABean>() {
            @Override
            public String getKey() {
                return TAG;
            }

            @Override
            public void success(FindSite.DATABean dataBean, String messages) {
                view.onSuccessFindSite(dataBean);

            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.CLEAN_CART, t));
            }
        });

    }

}
