package com.tawangga.petstoopmvp2.Modul.Invoice;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.tawangga.petstoopmvp2.Dialog.DialogInvoiceDP.DialogInvoiceDP;
import com.tawangga.petstoopmvp2.Dialog.DialogPilihCustomer.DialogPilihCustomerInvoice;
import com.tawangga.petstoopmvp2.EventBus.OnApiResultSuccess;
import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.EventBus.RefreshOrder;
import com.tawangga.petstoopmvp2.Helper.GlobalVariable;
import com.tawangga.petstoopmvp2.Helper.SharedPreference;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Model.DataInvoice;
import com.tawangga.petstoopmvp2.Model.GetOrders;
import com.tawangga.petstoopmvp2.Model.ProcessInvoice;
import com.tawangga.petstoopmvp2.Model.SignInModel;
import com.tawangga.petstoopmvp2.Modul.Shop.ShopFragment;
import com.tawangga.petstoopmvp2.Param.AddCartToInvoice;
import com.tawangga.petstoopmvp2.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class InvoiceFragment extends Fragment implements InvoiceFragmentView {
    private static final String TAG = ShopFragment.class.getSimpleName();

    @BindView(R.id.txt_all)
    TextView txtAll;
    @BindView(R.id.btn_all)
    LinearLayout btnAll;
    @BindView(R.id.txt_treatment)
    TextView txtTreatment;
    @BindView(R.id.btn_treatment)
    LinearLayout btnTreatment;
    @BindView(R.id.txt_shop)
    TextView txtShop;
    @BindView(R.id.btn_shop)
    LinearLayout btnShop;
    @BindView(R.id.et_search)
    EditText etSearch;
    @BindView(R.id.tab)
    LinearLayout tab;
    @BindView(R.id.rv_invoice)
    RecyclerView rvInvoice;
    @BindView(R.id.txt_treatment_selected_count)
    TextView txtTreatmentSelectedCount;
    @BindView(R.id.btn_add_to_invoice)
    LinearLayout btnAddToInvoice;
    @BindView(R.id.rl_button)
    RelativeLayout rlButton;
    @BindView(R.id.txt_invoiceCode)
    TextView txtInvoiceCode;
    @BindView(R.id.btn_open_invoice)
    LinearLayout btnOpenInvoice;
    @BindView(R.id.txt_site_name)
    TextView txtSiteName;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.txt_site_address)
    TextView txtSiteAddress;
    @BindView(R.id.llPetshopdesc)
    LinearLayout llPetshopdesc;
    @BindView(R.id.rv_selected_invoice)
    RecyclerView rvSelectedInvoice;
    @BindView(R.id.txt_total)
    TextView txtTotal;
    @BindView(R.id.txt_downpayment)
    TextView txtDownpayment;
    @BindView(R.id.rl_down)
    RelativeLayout rlDown;
    @BindView(R.id.txt_remaining)
    TextView txtRemaining;
    @BindView(R.id.rl_remain)
    RelativeLayout rlRemain;
    @BindView(R.id.btn_proceed_payment)
    Button btnProceedPayment;
    @BindView(R.id.btn_down_payment)
    Button btnDownPayment;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    @BindView(R.id.lay_Invoice)
    LinearLayout layInvoice;
    private DataInvoice dataInvoice;
    private String invoiceID = "";
    private InvoiceAdapter adapterOrder;
    private SelectedInvoiceAdapter selectedInvoiceAdapter;
    private InvoicePresenter presenter;
    private ProgressDialog dialog;
    private ProgressDialog dialogGetOrder;
    private List<GetOrders.DATABean> listOrder = new ArrayList<>();
    private boolean state = true;
    private boolean isDp = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_invoice, container, false);
        ButterKnife.bind(this, v);
        presenter = new InvoicePresenter(getActivity(), TAG, this);
        showClientSite();
        setAdapter();
        initView();
        return v;
    }

    private void initView() {
        Bundle bundle = getArguments();
        txtDate.setText(Utils.changeDateFormat(Utils.getTodayDate(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME_FULL));

        dataInvoice = Objects.requireNonNull(bundle).getParcelable("dataInvoice");
        if (dataInvoice != null) {

            float total = Float.parseFloat(dataInvoice.getTotalTransaction());
            float downpayment = Float.parseFloat(dataInvoice.getTransactionDp());
            float remainingPayment = total - downpayment;


            rlDown.setVisibility(View.VISIBLE);
            rlRemain.setVisibility(View.VISIBLE);

            txtInvoiceCode.setText(dataInvoice.getTransactionInvoiceCode());
            txtTotal.setText(Utils.toRupiah(String.valueOf(total)));

            txtDownpayment.setText(Utils.toRupiah(String.valueOf(downpayment)));
            txtRemaining.setText(Utils.toRupiah(String.valueOf(remainingPayment)));

            boolean isDownPayment = false;
            if (dataInvoice.getPaymentStatus().equals("2")) {
                isDownPayment = true;
            }
            selectedInvoiceAdapter.setDatas(dataInvoice.getOrders(), isDownPayment);
            invoiceID = String.valueOf(dataInvoice.getTransactionId());
            txtDate.setText(Utils.changeDateFormat(dataInvoice.getCreatedAt(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME_FULL));
            if (dataInvoice.getPaymentStatus().equals("2")) {
                btnDownPayment.setVisibility(View.GONE);
                rvInvoice.setVisibility(View.GONE);
                btnCancel.setVisibility(View.GONE);
            } else {
                btnDownPayment.setVisibility(View.VISIBLE);
                btnCancel.setVisibility(View.VISIBLE);

            }

        }

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (adapterOrder != null) {
                    adapterOrder.getFilter().filter(s.toString());
                }
                if (s.toString().length() == 0) {
                    Utils.hideSoftKeyboard(getActivity());
                }
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Utils.hideSoftKeyboardUsingView(getContext(), etSearch);
                    return true;
                }
                return false;
            }
        });

        etSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (etSearch.getRight() - etSearch.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        Utils.hideSoftKeyboardUsingView(getContext(), etSearch);
                        return true;
                    }
                }
                return false;
            }
        });

        getOrders("");

    }

    private void showClientSite() {
        String json = new SharedPreference(getActivity()).getSite();
        SignInModel.DATABean.SiteBean site = new Gson().fromJson(json, SignInModel.DATABean.SiteBean.class);
        txtSiteName.setText(site.getSiteName());
        txtSiteAddress.setText(site.getSiteAddress());

    }


    @Override
    public void onFailGettingData(OnFailGettingData data) {
        Utils.dismissProgressDialog(dialog);
        Utils.showToast(getActivity(), data.getThrowable().getMessage());
        data.getThrowable().printStackTrace();
    }

    private void getOrders(String order_type) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                dialogGetOrder = Utils.showProgressDialog(getContext(), false, "Loading...");
                presenter.getOrders(order_type);
            }
        }, 500);
    }

    private void setAdapter() {
        adapterOrder = new InvoiceAdapter(getContext());
        rvInvoice.setLayoutManager(new LinearLayoutManager(getContext()));
        rvInvoice.setAdapter(adapterOrder);

        selectedInvoiceAdapter = new SelectedInvoiceAdapter(getContext(), this);
        rvSelectedInvoice.setLayoutManager(new LinearLayoutManager(getContext()));
        rvSelectedInvoice.setAdapter(selectedInvoiceAdapter);
    }


    @OnClick({R.id.btn_all, R.id.btn_treatment, R.id.btn_shop, R.id.btn_cancel})
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_all:
                txtAll.setTextColor(getResources().getColor(R.color.white));
                btnAll.setBackgroundColor(getResources().getColor(R.color.brown_2));
                txtTreatment.setTextColor(getResources().getColor(R.color.black));
                btnTreatment.setBackgroundColor(getResources().getColor(R.color.white));
                txtShop.setTextColor(getResources().getColor(R.color.black));
                btnShop.setBackgroundColor(getResources().getColor(R.color.white));
                getOrders("");
                break;
            case R.id.btn_treatment:
                txtAll.setTextColor(getResources().getColor(R.color.black));
                btnAll.setBackgroundColor(getResources().getColor(R.color.white));
                txtTreatment.setTextColor(getResources().getColor(R.color.white));
                btnTreatment.setBackgroundColor(getResources().getColor(R.color.brown_2));
                txtShop.setTextColor(getResources().getColor(R.color.black));
                btnShop.setBackgroundColor(getResources().getColor(R.color.white));
                getOrders("2");
                break;
            case R.id.btn_shop:

                txtAll.setTextColor(getResources().getColor(R.color.black));
                btnAll.setBackgroundColor(getResources().getColor(R.color.white));
                txtTreatment.setTextColor(getResources().getColor(R.color.black));
                btnTreatment.setBackgroundColor(getResources().getColor(R.color.white));
                txtShop.setTextColor(getResources().getColor(R.color.white));
                btnShop.setBackgroundColor(getResources().getColor(R.color.brown_2));
                getOrders("1");
                break;
            case R.id.btn_cancel:
                if (!invoiceID.equals("")) {
                    cancelInvoice(invoiceID);
                    invoiceID = "";
                    txtTotal.setText(Utils.toRupiah("0"));
                    btnDownPayment.setVisibility(View.VISIBLE);
                    rvInvoice.setVisibility(View.VISIBLE);
                }
                break;
        }
    }


    @OnClick({R.id.btn_proceed_payment, R.id.btn_down_payment})
    public void onPayment(View view) {
        if (view.getId() == R.id.btn_down_payment) {
            if (invoiceID.equals("")) {
                Toast.makeText(getContext(), "Please Add Some Orders to Invoice !", Toast.LENGTH_SHORT).show();
            } else {
                proccessInvoice(invoiceID, true);
            }
        } else {
            if (invoiceID.equals("")) {
                Toast.makeText(getContext(), "Please Add Some Orders to Invoice !", Toast.LENGTH_SHORT).show();
            } else {
                proccessInvoice(invoiceID, false);
            }
        }
    }

    private void cancelInvoice(String trID) {
        if (!new SharedPreference(getActivity()).isCustomer()) {
            FormBody.Builder formBuilder = new FormBody.Builder();
            formBuilder.add("transaction_id", trID);
            RequestBody formBody = formBuilder.build();
            dialog = Utils.showProgressDialog(getContext(), false, "Loading...");
            presenter.cancelInvoice(formBody);
        }
    }


    @OnClick(R.id.btn_add_to_invoice)
    public void onClickAddToInvoice() {

        rlDown.setVisibility(View.GONE);
        rlRemain.setVisibility(View.GONE);

        AddCartToInvoice param = new AddCartToInvoice();
        if (invoiceID.equals("")) {
            param.setTransactionId(0);
        } else {
            param.setTransactionId(Integer.parseInt(invoiceID));
        }
        List<GetOrders.DATABean> order = new ArrayList<>();
        for (int i = 0; i < listOrder.size(); i++) {
//            order.add(listOrder.get(i));
            List<GetOrders.DATABean.ItemsBean> items = new ArrayList<>();
            for (int j = 0; j < listOrder.get(i).getItems().size(); j++) {
                if (listOrder.get(i).getItems().get(j).isChecked()) {
                    items.add(listOrder.get(i).getItems().get(j));
                }
            }

            if (items.size() > 0) {
                GetOrders.DATABean dataOrder = listOrder.get(i);
                dataOrder.setItems(items);
                order.add(dataOrder);
            }
        }

        param.setItems(order);

        dialog = Utils.showProgressDialog(getContext(), false, "Loading...");

        presenter.addOrderToInvoice(param);

    }


    private void proccessInvoice(String trID, boolean dp) {
        dialog = Utils.showProgressDialog(getContext(), false, "Loading...");
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("transaction_id", trID);
        RequestBody formBody = formBuilder.build();
        isDp = dp;
        presenter.processInvoice(formBody);

    }

    @Override
    public void onGetOrder(RefreshOrder data) {
        dialogGetOrder.dismiss();
        listOrder.clear();
        adapterOrder.setDatas(data.getData());
        listOrder.addAll(data.getData());

    }

    @Override
    public void onCancelInvoice(OnApiResultSuccess data) {
        dialog.dismiss();
        if (data.getApi() == OnApiResultSuccess.API_CANCEL_INVOICE) {
            rvSelectedInvoice.setVisibility(View.GONE);
            txtInvoiceCode.setText("");

            getOrders("");
        }
    }

    @Override
    public void onAddOrderToInvoice(DataInvoice data) {
        Utils.dismissProgressDialog(dialog);
        rvSelectedInvoice.setVisibility(View.VISIBLE);
        boolean isDownPayment = false;
        if (data.getPaymentStatus().equals("2")) {
            isDownPayment = true;
        }
        selectedInvoiceAdapter.setDatas(data.getOrders(), isDownPayment);
        layInvoice.setVisibility(View.VISIBLE);
        invoiceID = String.valueOf(data.getTransactionId());
        txtInvoiceCode.setText(data.getTransactionInvoiceCode());
        txtTotal.setText(Utils.toRupiah(data.getTotalTransaction()));
        txtDate.setText(Utils.changeDateFormat(data.getCreatedAt(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME_FULL));

        getOrders("");
        if (data.getPaymentStatus().equals("2")) {
            btnDownPayment.setVisibility(View.GONE);
            rvInvoice.setVisibility(View.GONE);
        } else btnDownPayment.setVisibility(View.VISIBLE);


    }

    @Override
    public void onRemoveOrder(DataInvoice data) {
        Utils.dismissProgressDialog(dialog);
        rvSelectedInvoice.setVisibility(View.VISIBLE);
        boolean isDownPayment = false;
        if (data.getPaymentStatus().equals("2")) {
            isDownPayment = true;
        }
        selectedInvoiceAdapter.setDatas(data.getOrders(), isDownPayment);
        layInvoice.setVisibility(View.VISIBLE);
        invoiceID = String.valueOf(data.getTransactionId());
        txtInvoiceCode.setText(data.getTransactionInvoiceCode());
        txtTotal.setText(Utils.toRupiah(data.getTotalTransaction()));
        txtDate.setText(Utils.changeDateFormat(data.getCreatedAt(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME_FULL));

        getOrders("");
        if (data.getPaymentStatus().equals("2")) {
            btnDownPayment.setVisibility(View.GONE);
            rvInvoice.setVisibility(View.GONE);
        } else btnDownPayment.setVisibility(View.VISIBLE);


    }

    @Override
    public void onRemoveOrderDetail(DataInvoice data) {
        Utils.dismissProgressDialog(dialog);
        rvSelectedInvoice.setVisibility(View.VISIBLE);
        boolean isDownPayment = false;
        if (data.getPaymentStatus().equals("2")) {
            isDownPayment = true;
        }
        selectedInvoiceAdapter.setDatas(data.getOrders(), isDownPayment);
        layInvoice.setVisibility(View.VISIBLE);
        invoiceID = String.valueOf(data.getTransactionId());
        txtInvoiceCode.setText(data.getTransactionInvoiceCode());
        txtTotal.setText(Utils.toRupiah(data.getTotalTransaction()));
        txtDate.setText(Utils.changeDateFormat(data.getCreatedAt(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME_FULL));

        getOrders("");
        if (data.getPaymentStatus().equals("2")) {
            btnDownPayment.setVisibility(View.GONE);
            rvInvoice.setVisibility(View.GONE);
        } else btnDownPayment.setVisibility(View.VISIBLE);


    }

    @Override
    public void onProcessInvoice(ProcessInvoice.DATABean data) {
        Utils.dismissProgressDialog(dialog);
        state = false;
        dataInvoice = data.getInvoice();
        showPopUpCustomerInvoice(data.getCustomers(), isDp);

    }

    @Override
    public void onGetInvoiceDP(List<DataInvoice> data) {
        Utils.dismissProgressDialog(dialog);
        if (data.size() > 0) {
            showInvoiceDP(data);
        } else {
            Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();
        }
    }


    public void removeOrder(String transaction_id, String order_code) {
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("transaction_id", transaction_id);
        formBuilder.add("order_code", order_code);
        RequestBody param = formBuilder.build();
        dialog = Utils.showProgressDialog(getContext(), false, "Loading...");

        presenter.removeOrder(param);
    }

    public void removeOrderDetail(String transaction_id, String order_code, String transaction_item_detail_id) {
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("transaction_id", transaction_id);
        formBuilder.add("order_code", order_code);
        formBuilder.add("transaction_item_detail_id", transaction_item_detail_id);
        RequestBody param = formBuilder.build();
        dialog = Utils.showProgressDialog(getContext(), false, "Loading...");

        presenter.removeOrderDetail(param);
    }

    private void showPopUpCustomerInvoice(List<ProcessInvoice.DATABean.CustomersBean> customers, boolean dp) {

        DialogPilihCustomerInvoice dialogPilihCustomer = DialogPilihCustomerInvoice.newInstance(customers, dp, dataInvoice);
        dialogPilihCustomer.show(getChildFragmentManager(), "");
    }


    @OnClick(R.id.btn_open_invoice)
    public void onViewClicked() {
        getInvoiceDP();
    }

    private void getInvoiceDP() {
        dialog = Utils.showProgressDialog(getContext(), false, "Loading...");
        presenter.getInvoiceDP();
    }

    private void showInvoiceDP(List<DataInvoice> data) {
        DialogInvoiceDP dialog = DialogInvoiceDP.newInstance(data, rvInvoice);
        dialog.show(getChildFragmentManager(), "");
    }

}
