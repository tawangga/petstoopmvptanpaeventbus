package com.tawangga.petstoopmvp2.Modul.LoginCustomer;

import android.content.Context;

import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.Model.SignInCustomer;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import okhttp3.FormBody;
import okhttp3.RequestBody;

public class SignInCustomerPresenter {
    private final Context context;
    private final String TAG;
    private SignInCustomerView view;

    public SignInCustomerPresenter(Context context, String TAG, SignInCustomerView view) {
        this.context = context;
        this.TAG = TAG;
        this.view = view;
    }

    public void doLogin(String username, String password, int siteId) {
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("email", username);
        formBuilder.add("password", password);
        formBuilder.add("site_id", siteId + "");
        RequestBody formBody = formBuilder.build();
        Connector.newInstance(context).loginCustomer(formBody, new Connector.ApiCallback<SignInCustomer.DATA>() {
            @Override
            public String getKey() {
                return TAG;
            }

            @Override
            public void success(SignInCustomer.DATA dataBean, String messages) {

                view.onSuccessSignIn(dataBean);

            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.GET_APP_VERSION,t));

            }
        });

    }

}
