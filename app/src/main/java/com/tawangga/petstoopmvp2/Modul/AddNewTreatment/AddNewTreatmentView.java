package com.tawangga.petstoopmvp2.Modul.AddNewTreatment;

import com.tawangga.petstoopmvp2.EventBus.OnCancelTreatmentService;
import com.tawangga.petstoopmvp2.EventBus.OnSuccessReservTreatment;
import com.tawangga.petstoopmvp2.ModelNew.CreateCart;
import com.tawangga.petstoopmvp2.ModelNew.GetBoarding;
import com.tawangga.petstoopmvp2.ModelNew.GetDoctor;
import com.tawangga.petstoopmvp2.ModelNew.GetDoctorSchedule;
import com.tawangga.petstoopmvp2.ModelNew.GetGroomingLocation;
import com.tawangga.petstoopmvp2.Modul.BaseView;

public interface AddNewTreatmentView extends BaseView {
    void onGetDoctor(GetDoctor.DATABean dataBean);

    void onGetGroomingLocation(GetGroomingLocation.DATABean dataBean);

    void onGetBoarding(GetBoarding.DATABean dataBean);

    void onRollbackService(OnCancelTreatmentService dataBean);

    void onDeleteService(OnCancelTreatmentService dataBean);

    void onCreateCartTreatment(CreateCart.DATABean dataBean);

    void onReservTreatment(OnSuccessReservTreatment dataBean);

    void onGetAllDoctor(GetDoctorSchedule.DATABean dataBean);
}
