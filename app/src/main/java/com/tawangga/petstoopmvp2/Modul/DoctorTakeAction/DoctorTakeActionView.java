package com.tawangga.petstoopmvp2.Modul.DoctorTakeAction;

import com.tawangga.petstoopmvp2.EventBus.OnApiResultSuccess;
import com.tawangga.petstoopmvp2.Model.GetOrderConfirmAnamnesis;
import com.tawangga.petstoopmvp2.Modul.BaseView;

public interface DoctorTakeActionView extends BaseView {
    void onGetOrderConfirmAnamnesis(GetOrderConfirmAnamnesis.DATABean dataBean);

    void onSaveOrderConfirmAnamnesis(OnApiResultSuccess dataBean);
}
