package com.tawangga.petstoopmvp2.Modul.TreatmentFragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.tawangga.petstoopmvp2.EventBus.RefreshTreatment;
import com.tawangga.petstoopmvp2.Helper.TreatmentHelper;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Model.DetailCustomerModel;
import com.tawangga.petstoopmvp2.ModelNew.CreateCart;
import com.tawangga.petstoopmvp2.ModelNew.GetTreatment;
import com.tawangga.petstoopmvp2.Modul.Main.MainActivity;
import com.tawangga.petstoopmvp2.Param.ConvertToOrderParam;
import com.tawangga.petstoopmvp2.R;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class TreatmentReservationAdapter extends RecyclerView.Adapter<TreatmentReservationAdapter.ViewHolder> {
    public static final String TAG = "DIALOG CONFIRMATION";
    @BindView(R.id.txt_owner_name)
    TextView txtOwnerName;
    @BindView(R.id.txt_pet_name)
    TextView txtPetName;
    @BindView(R.id.rv_medical)
    RecyclerView rvMedical;
    @BindView(R.id.rv_grooming)
    RecyclerView rvGrooming;
    @BindView(R.id.rv_boarding)
    RecyclerView rvBoarding;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;
    @BindView(R.id.iv_pic_pet)
    CircleImageView ivPicPet;
    @BindView(R.id.ll_medical)
    LinearLayout llMedical;
    @BindView(R.id.ll_grooming)
    LinearLayout llGrooming;
    @BindView(R.id.ll_boarding)
    LinearLayout llBoarding;
    private List<GetTreatment.DATABean.ItemsBeanX> data;
    private Context context;
    private Fragment fragment;
    List<Integer> deletedServices;

    public TreatmentReservationAdapter(Context context, Fragment fragment) {
        this.context = context;
        this.fragment = fragment;
        data = new ArrayList<>();
        deletedServices = new ArrayList<>();
    }

    public void addDeletedServices(Integer cart_service_detail_id) {
        deletedServices.add(cart_service_detail_id);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_treatment, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        GetTreatment.DATABean.ItemsBeanX selectedData = data.get(position);
        if (position % 2 == 1) {
            holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.brown_light_2));
        } else {
            holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

        holder.llBackground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedData.isShowingDetail()) {
                    selectedData.setShowingDetail(false);
                    setShowingDetail(holder, selectedData);

                } else {
                    selectedData.setCount(selectedData.getCount() + 1);
                    selectedData.setShowingDetail(true);
                    setShowingDetail(holder, selectedData);

                }
            }
        });

        setShowingDetail(holder, selectedData);

        holder.txtReservationNumber.setText(selectedData.getOrderCode());
        holder.txtReservationNumber.setTextColor(context.getResources().getColor(R.color.brown_2));
        holder.txtOwnerPetName.setText(selectedData.getPet().getCustomerName() + " - " + selectedData.getPet().getPetName());
        holder.txtServices.setText(selectedData.getItems().size() + " SERVICES");

        TreatmentDetailAdapter adapter = new TreatmentDetailAdapter(context);
        adapter.setDatas(selectedData.getItems(), selectedData);
        holder.rvDetail.setLayoutManager(new LinearLayoutManager(context));
        holder.rvDetail.setAdapter(adapter);

        holder.btnMakeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openConfirmationDialog(selectedData.getItems(), selectedData);
            }
        });

    }

    private void setShowingDetail(ViewHolder holder, GetTreatment.DATABean.ItemsBeanX data) {
        boolean isShowingDetail = data.isShowingDetail();

        if (!isShowingDetail) {
            holder.rlButton.setVisibility(View.GONE);
            holder.btnAddtorecipe.setVisibility(View.GONE);
            holder.btnCompleteorder.setVisibility(View.GONE);
            holder.btnMakeOrder.setVisibility(View.GONE);
            holder.rvDetail.setVisibility(View.GONE);

        } else {

            holder.rlButton.setVisibility(View.VISIBLE);
            holder.btnCompleteorder.setVisibility(View.GONE);
            holder.btnAddtorecipe.setVisibility(View.GONE);
            holder.btnMakeOrder.setVisibility(View.VISIBLE);
            holder.rvDetail.setVisibility(View.VISIBLE);
//            holder.btnAddtorecipe.setVisibility(View.VISIBLE);


        }

        holder.llBackground.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                holder.llBackground.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                holder.rowBG.getLayoutParams().height = holder.llBackground.getHeight();
                holder.rowBG.requestLayout();

            }
        });

        holder.delete_task.measure(0, 0);
        holder.edit_task.getLayoutParams().width = holder.delete_task.getMeasuredWidth();
        holder.edit_task.requestLayout();


    }

    private void openConfirmationDialog(List<GetTreatment.DATABean.ItemsBeanX.ItemsBean> items, GetTreatment.DATABean.ItemsBeanX selectedData) {
        deletedServices = new ArrayList<>();
        final AlertDialog dialog = new AlertDialog.Builder(context).create();
        View v = View.inflate(context, R.layout.dialog_confirmation, null);
        ButterKnife.bind(this, v);
        txtOwnerName.setText(selectedData.getPet().getCustomerName());
        txtPetName.setText(selectedData.getPet().getPetName());
        Picasso.get().load(selectedData.getPet().getPetPhoto())
                .tag("DIALOG CONFIRMATION")
                .into(ivPicPet, new Callback() {
                    @Override
                    public void onSuccess() {
                        ivPicPet.setVisibility(View.VISIBLE);

                    }

                    @Override
                    public void onError(Exception e) {
                        ivPicPet.setImageDrawable(context.getResources().getDrawable(R.drawable.no_image));
                        ivPicPet.setVisibility(View.VISIBLE);
                        e.printStackTrace();
                    }
                });
        LinearLayout ll_boarding = v.findViewById(R.id.ll_boarding);
        LinearLayout ll_grooming = v.findViewById(R.id.ll_grooming);
        LinearLayout ll_medical = v.findViewById(R.id.ll_medical);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Picasso.get().cancelTag(TAG);
                ((TreatmentFragment) fragment).getReservation("");
            }
        });
        List<CreateCart.DATABean.ServiceBean> dataMedic = new ArrayList<>();
        List<CreateCart.DATABean.ServiceBean> dataGrooming = new ArrayList<>();
        List<CreateCart.DATABean.ServiceBean> dataBoarding = new ArrayList<>();

        for (int i = 0; i < items.size(); i++) {
            GetTreatment.DATABean.ItemsBeanX.ItemsBean itemsBean = items.get(i);
            if (itemsBean.getServiceType().equals("1")) {
                CreateCart.DATABean.ServiceBean serviceBean = new CreateCart.DATABean.ServiceBean();
                serviceBean.setServiceId(itemsBean.getServiceId());
                serviceBean.setServiceName(itemsBean.getServiceName());
                serviceBean.setCartServiceId(itemsBean.getCartServiceId());
                serviceBean.setServiceType(itemsBean.getServiceType());
                serviceBean.setServicePrice(itemsBean.getServicePrice());
                serviceBean.setCartServiceDetailId(itemsBean.getCartServiceDetailId());
                dataMedic.add(serviceBean);
            }
            if (itemsBean.getServiceType().equals("2")) {
                CreateCart.DATABean.ServiceBean serviceBean = new CreateCart.DATABean.ServiceBean();
                serviceBean.setServiceId(itemsBean.getServiceId());
                serviceBean.setServiceName(itemsBean.getServiceName());
                serviceBean.setCartServiceId(itemsBean.getCartServiceId());
                serviceBean.setServiceType(itemsBean.getServiceType());
                serviceBean.setServicePrice(itemsBean.getServicePrice());
                serviceBean.setCartServiceDetailId(itemsBean.getCartServiceDetailId());
                dataGrooming.add(serviceBean);
            }

            if (itemsBean.getServiceType().equals("3")) {
                CreateCart.DATABean.ServiceBean serviceBean = new CreateCart.DATABean.ServiceBean();
                serviceBean.setServiceId(itemsBean.getServiceId());
                serviceBean.setServiceName(itemsBean.getServiceName());
                serviceBean.setCartServiceId(itemsBean.getCartServiceId());
                serviceBean.setServiceType(itemsBean.getServiceType());
                serviceBean.setServicePrice(itemsBean.getServicePrice());
                serviceBean.setCartServiceDetailId(itemsBean.getCartServiceDetailId());
                dataBoarding.add(serviceBean);
            }
        }

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dataMedic.size() == 0 && dataGrooming.size() == 0 && dataBoarding.size() == 0) {
                    Toast.makeText(context, "Can't convert to order because there is nothing service", Toast.LENGTH_SHORT).show();
                    return;
                }
                convertToOrder(selectedData, dialog);

            }
        });


        if (dataMedic.size() == 0) {
            ll_medical.setVisibility(View.GONE);
        }

        if (dataGrooming.size() == 0) {
            ll_grooming.setVisibility(View.GONE);
        }

        if (dataBoarding.size() == 0) {
            ll_boarding.setVisibility(View.GONE);
        }


        DialogConfirmationAdapter adapterMedic = new DialogConfirmationAdapter(context, null, this);
        adapterMedic.setDatas(dataMedic);

        DialogConfirmationAdapter adapterGrooming = new DialogConfirmationAdapter(context, null, this);
        adapterGrooming.setDatas(dataGrooming);

        DialogConfirmationAdapter adapterBoarding = new DialogConfirmationAdapter(context, null, this);
        adapterBoarding.setDatas(dataBoarding);

        rvMedical.setLayoutManager(new LinearLayoutManager(context));
        rvMedical.setAdapter(adapterMedic);

        rvGrooming.setLayoutManager(new LinearLayoutManager(context));
        rvGrooming.setAdapter(adapterGrooming);

        rvBoarding.setLayoutManager(new LinearLayoutManager(context));
        rvBoarding.setAdapter(adapterBoarding);

        dialog.setView(v);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());

        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.80);
        int height = (int) (context.getResources().getDisplayMetrics().heightPixels * 0.80);
        lp.width = width;
//        lp.height = height;
        dialog.show();
        dialog.getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void convertToOrder(GetTreatment.DATABean.ItemsBeanX selectedData, AlertDialog mainDialog) {
        ProgressDialog dialog = Utils.showProgressDialog(context, false, "Loading...");
        ConvertToOrderParam param = new ConvertToOrderParam(selectedData.getCartServiceId(), deletedServices);
        Connector.newInstance(context).convertToOrder(param, new Connector.ApiCallback<String>() {
            @Override
            public String getKey() {
                return null;
            }

            @Override
            public void success(String s, String messages) {
                dialog.dismiss();
                mainDialog.setOnDismissListener(null);
                mainDialog.dismiss();

                DetailCustomerModel.DATABean.PetsBean pet = new DetailCustomerModel.DATABean.PetsBean();
                pet.setPetId(selectedData.getPetId());
                pet.setPetName(selectedData.getPet().getPetName());
                pet.setPetPhoto(selectedData.getPet().getPetPhoto());
                TreatmentHelper.getInstance().setselectedPet(pet);

                DetailCustomerModel.DATABean customer = new DetailCustomerModel.DATABean();
                customer.setCustomerId(selectedData.getPet().getCustomerId());
                customer.setCustomerName(selectedData.getPet().getCustomerName());
                TreatmentHelper.getInstance().setSelectedCustomer(customer);

                CreateCart.DATABean dataTreatment = new CreateCart.DATABean();
                dataTreatment.setCartServiceId(selectedData.getCartServiceId());
                dataTreatment.setOrderCode(selectedData.getOrderCode());
                TreatmentHelper.getInstance().setDataTreatment(dataTreatment);

                boolean isHasKonsultasiDokter = false;
                for (GetTreatment.DATABean.ItemsBeanX.ItemsBean medItem : selectedData.getItems()) {
                    if ((medItem.getServiceId() == 1) && (medItem.getServiceType().equals("1"))) {
                        isHasKonsultasiDokter = true;
                    }
                }
                if (isHasKonsultasiDokter) {
                    ((MainActivity) context).toKonsultasiDokter();
                } else {
                    EventBus.getDefault().post(new RefreshTreatment());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();

            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setDatas(List<GetTreatment.DATABean.ItemsBeanX> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public List<GetTreatment.DATABean.ItemsBeanX> getData() {
        return data;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_reservation_number)
        TextView txtReservationNumber;
        @BindView(R.id.txt_owner_pet_name)
        TextView txtOwnerPetName;
        @BindView(R.id.txt_services)
        TextView txtServices;
        @BindView(R.id.rv_detail)
        RecyclerView rvDetail;
        @BindView(R.id.btn_completeorder)
        Button btnCompleteorder;
        @BindView(R.id.btn_addtorecipe)
        LinearLayout btnAddtorecipe;
        @BindView(R.id.rl_button)
        RelativeLayout rlButton;
        @BindView(R.id.ll_background)
        LinearLayout llBackground;
        @BindView(R.id.btn_make_order)
        LinearLayout btnMakeOrder;
        @BindView(R.id.rowBG)
        LinearLayout rowBG;
        @BindView(R.id.edit_task)
        RelativeLayout edit_task;
        @BindView(R.id.delete_task)
        RelativeLayout delete_task;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
