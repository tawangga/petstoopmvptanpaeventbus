package com.tawangga.petstoopmvp2.Modul.TreatmentFragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.EventBus.UpdateTreatmentData;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.ModelNew.GetTreatment;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import java.util.HashMap;
import java.util.Map;

public class TreatmentPresenter {
    private Context context;
    private String TAG;
    private TreatmentFragmentView view;

    public TreatmentPresenter(Context context, String TAG, TreatmentFragmentView view) {

        this.context = context;
        this.TAG = TAG;
        this.view = view;
    }

    public void getReservation(String date) {
        Map<String, String> param = new HashMap<>();
        param.put("date", date);
        param.put("cart_service_type", "1");
        param.put("page", "1");
        param.put("limit", "999");
        Connector.newInstance(context).getReservation(param, new Connector.ApiCallback<GetTreatment.DATABean>() {
            @Override
            public String getKey() {
                return TAG;
            }

            @Override
            public void success(GetTreatment.DATABean dataBeans, String messages) {
                view.getReservasionOrder(new UpdateTreatmentData(dataBeans, "GetReservation"));
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.GET_RESERVATION, t));
            }
        });
    }

    public void getOrder(String date) {
        Map<String, String> param = new HashMap<>();
        param.put("date", date);
        param.put("cart_service_type", "2");
        param.put("page", "1");
        param.put("limit", "999");
        Connector.newInstance(context).getReservation(param, new Connector.ApiCallback<GetTreatment.DATABean>() {
            @Override
            public String getKey() {
                return TAG;
            }

            @Override
            public void success(GetTreatment.DATABean dataBeans, String messages) {
                view.getReservasionOrder(new UpdateTreatmentData(dataBeans, "GetOrder"));
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.GET_ORDER, t));
            }
        });
    }

    public void deleteService(int cartServiceId, boolean isReservation, DialogInterface dialogMain) {
        ProgressDialog progressDialog = Utils.showProgressDialog(context, false, "Loading...");

        Connector.newInstance(context).deleteService(cartServiceId, new Connector.ApiCallback<String>() {
            @Override
            public String getKey() {
                return TAG;
            }

            @Override
            public void success(String s, String messages) {
                Utils.dismissProgressDialog(progressDialog);
                dialogMain.dismiss();
                if (isReservation) {
                    getReservation("");
                } else {
                    getOrder("");
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Utils.dismissProgressDialog(progressDialog);
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();

            }
        });

    }

}
