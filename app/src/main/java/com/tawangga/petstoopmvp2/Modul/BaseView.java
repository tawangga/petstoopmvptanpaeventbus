package com.tawangga.petstoopmvp2.Modul;

import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;

public interface BaseView {
    void onFailGettingData(OnFailGettingData data);
}
