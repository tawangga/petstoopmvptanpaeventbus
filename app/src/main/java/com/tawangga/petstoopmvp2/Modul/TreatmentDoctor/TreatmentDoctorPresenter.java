package com.tawangga.petstoopmvp2.Modul.TreatmentDoctor;

import android.content.Context;

import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.EventBus.UpdateTreatmentData;
import com.tawangga.petstoopmvp2.ModelNew.GetTreatment;
import com.tawangga.petstoopmvp2.Modul.TreatmentFragment.TreatmentFragmentView;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import java.util.Map;

public class TreatmentDoctorPresenter {
    private String TAG;
    private TreatmentFragmentView view;
    private Context context;

    public TreatmentDoctorPresenter(Context context, String TAG, TreatmentFragmentView view) {
        this.context = context;
        this.TAG = TAG;
        this.view = view;
    }

    public void getOrder(Map<String, String> param) {
        Connector.newInstance(context).getDoctorReservation(param, new Connector.ApiCallback<GetTreatment.DATABean>() {
            @Override
            public String getKey() {
                return TAG;
            }

            @Override
            public void success(GetTreatment.DATABean dataBeans, String messages) {
                view.getReservasionOrder(new UpdateTreatmentData(dataBeans, "GET_ORDER"));
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.GET_ORDER, t));
            }
        });

    }

    public void getReservation(Map<String, String> param) {
        Connector.newInstance(context).getDoctorReservation(param, new Connector.ApiCallback<GetTreatment.DATABean>() {
            @Override
            public String getKey() {
                return TAG;
            }

            @Override
            public void success(GetTreatment.DATABean dataBeans, String messages) {
                view.getReservasionOrder(new UpdateTreatmentData(dataBeans, "GET_RESERVATION"));
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.GET_RESERVATION, t));
            }
        });

    }
}
