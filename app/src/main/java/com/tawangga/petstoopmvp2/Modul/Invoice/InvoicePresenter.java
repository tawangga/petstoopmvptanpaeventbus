package com.tawangga.petstoopmvp2.Modul.Invoice;

import android.content.Context;

import com.tawangga.petstoopmvp2.EventBus.OnApiResultSuccess;
import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.EventBus.RefreshOrder;
import com.tawangga.petstoopmvp2.Model.DataInvoice;
import com.tawangga.petstoopmvp2.Model.GetOrders;
import com.tawangga.petstoopmvp2.Model.ProcessInvoice;
import com.tawangga.petstoopmvp2.Param.AddCartToInvoice;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import java.util.List;

import okhttp3.RequestBody;

public class InvoicePresenter {
    private final Context context;
    private final String tag;
    private InvoiceFragmentView view;

    public InvoicePresenter(Context context, String TAG, InvoiceFragmentView view) {

        this.context = context;
        tag = TAG;
        this.view = view;
    }

    public void getOrders(String order_type) {
        Connector.newInstance(context).getOrders(order_type, new Connector.ApiCallback<List<GetOrders.DATABean>>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(List<GetOrders.DATABean> dataBeans, String messages) {
                view.onGetOrder(new RefreshOrder(dataBeans));
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.GET_INVOICE, t));

            }
        });

    }

    public void cancelInvoice(RequestBody param) {
        Connector.newInstance(context).cancelInvoice(param, new Connector.ApiCallback<String>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(String s, String messages) {
                view.onCancelInvoice(new OnApiResultSuccess(OnApiResultSuccess.API_CANCEL_INVOICE));

            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.CANCEL_INVOICE, t));

            }
        });
    }

    public void addOrderToInvoice(AddCartToInvoice param) {
        Connector.newInstance(context).addOrderToInvoice(param, new Connector.ApiCallback<DataInvoice>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(DataInvoice dataInvoice, String messages) {
                view.onAddOrderToInvoice(dataInvoice);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.ADD_ORDER_TO_INVOICE, t));
            }
        });

    }

    public void removeOrder(RequestBody param) {
        Connector.newInstance(context).removeOrderInvoice(param, new Connector.ApiCallback<DataInvoice>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(DataInvoice dataInvoice, String messages) {
                view.onRemoveOrder(dataInvoice);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.ADD_ORDER_TO_INVOICE, t));
            }
        });

    }

    public void removeOrderDetail(RequestBody param) {
        Connector.newInstance(context).removeOrderDetailInvoice(param, new Connector.ApiCallback<DataInvoice>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(DataInvoice dataInvoice, String messages) {
                view.onRemoveOrderDetail(dataInvoice);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.ADD_ORDER_TO_INVOICE, t));
            }
        });

    }

    public void processInvoice(RequestBody param) {
        Connector.newInstance(context).processInvoice(param, new Connector.ApiCallback<ProcessInvoice.DATABean>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(ProcessInvoice.DATABean datAprocess_invoice, String messages) {
                view.onProcessInvoice(datAprocess_invoice);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.PROCESS_INVOICE, t));
            }
        });
    }

    public void getInvoiceDP() {
        Connector.newInstance(context).getInvoiceDP(new Connector.ApiCallback<List<DataInvoice>>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(List<DataInvoice> dataBeans, String messages) {
                view.onGetInvoiceDP(dataBeans);

            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.INVOICE_DP, t));
            }
        });

    }
}
