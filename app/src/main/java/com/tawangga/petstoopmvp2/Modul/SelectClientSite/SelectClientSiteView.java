package com.tawangga.petstoopmvp2.Modul.SelectClientSite;

import com.tawangga.petstoopmvp2.Model.FindSite;
import com.tawangga.petstoopmvp2.Modul.BaseView;

public interface SelectClientSiteView extends BaseView {
    void onSuccessFindSite(FindSite.DATABean dataBean);
}
