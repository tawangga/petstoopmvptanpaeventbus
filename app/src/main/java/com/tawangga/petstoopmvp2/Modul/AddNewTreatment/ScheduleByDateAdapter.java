package com.tawangga.petstoopmvp2.Modul.AddNewTreatment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.Helper.GlobalVariable;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.ModelNew.GetDoctorSchedule;
import com.tawangga.petstoopmvp2.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ScheduleByDateAdapter extends RecyclerView.Adapter<ScheduleByDateAdapter.ViewHolder> {
    private Context context;
    private List<GetDoctorSchedule.DATABean.ItemsBean> data;
    private String day;

    public ScheduleByDateAdapter(Context context) {

        this.context = context;
        data = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_bydate, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (position % 2 == 1) {
            holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.gray_light));
        } else {
            holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.gray_light_2));
        }

        GetDoctorSchedule.DATABean.ItemsBean selectedData = data.get(position);
        holder.txtDoctorName.setText(selectedData.getName());
        switch (day) {
            case "sun":
                holder.txtTime.setText(Utils.changeDateFormat(selectedData.getDoctor().getSunTimeOperationalStart(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H) + " - " + Utils.changeDateFormat(selectedData.getDoctor().getSunTimeOperationalEnd(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));
                break;
            case "mon":
                holder.txtTime.setText(Utils.changeDateFormat(selectedData.getDoctor().getMonTimeOperationalStart(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H) + " - " + Utils.changeDateFormat(selectedData.getDoctor().getMonTimeOperationalEnd(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));

                break;
            case "tue":
                holder.txtTime.setText(Utils.changeDateFormat(selectedData.getDoctor().getTueTimeOperationalStart(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H) + " - " + Utils.changeDateFormat(selectedData.getDoctor().getTueTimeOperationalEnd(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));

                break;
            case "wed":
                holder.txtTime.setText(Utils.changeDateFormat(selectedData.getDoctor().getWedTimeOperationalStart(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H) + " - " + Utils.changeDateFormat(selectedData.getDoctor().getWedTimeOperationalEnd(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));

                break;
            case "thu":
                holder.txtTime.setText(Utils.changeDateFormat(selectedData.getDoctor().getThuTimeOperationalStart(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H) + " - " + Utils.changeDateFormat(selectedData.getDoctor().getThuTimeOperationalEnd(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));

                break;
            case "fri":
                holder.txtTime.setText(Utils.changeDateFormat(selectedData.getDoctor().getFriTimeOperationalStart(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H) + " - " + Utils.changeDateFormat(selectedData.getDoctor().getFriTimeOperationalEnd(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));

                break;
            case "sat":
                holder.txtTime.setText(Utils.changeDateFormat(selectedData.getDoctor().getSatTimeOperationalStart(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H) + " - " + Utils.changeDateFormat(selectedData.getDoctor().getSatTimeOperationalEnd(), GlobalVariable.TIME_FORMAT_STANDARD, GlobalVariable.TIME_FORMAT_12H));
                break;
            default:
                holder.txtTime.setText("");
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day.substring(0, 3).toLowerCase();
    }

    public void setDatas(List<GetDoctorSchedule.DATABean.ItemsBean> data) {

        this.data = data;
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_time)
        TextView txtTime;
        @BindView(R.id.txt_doctor_name)
        TextView txtDoctorName;
        @BindView(R.id.ll_background)
        RelativeLayout llBackground;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
