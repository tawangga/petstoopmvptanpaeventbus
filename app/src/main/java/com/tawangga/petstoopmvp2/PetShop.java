package com.tawangga.petstoopmvp2;

import android.app.Application;

import com.mazenrashed.printooth.Printooth;

public class PetShop extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Printooth.INSTANCE.init(getApplicationContext());
    }
}
