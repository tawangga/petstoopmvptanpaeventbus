package com.tawangga.petstoopmvp2.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DetailCustomerModel {

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("ERROR")
    private String ERROR;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public String getERROR() {
        return ERROR;
    }

    public void setERROR(String ERROR) {
        this.ERROR = ERROR;
    }

    public static class DATABean {


        @SerializedName("customer_id")
        private int customerId;
        @SerializedName("customer_name")
        private String customerName;
        @SerializedName("customer_email")
        private String customerEmail;
        @SerializedName("customer_phone")
        private String customerPhone;
        @SerializedName("customer_address")
        private String customerAddress;
        @SerializedName("customer_photo")
        private String customerPhoto;
        @SerializedName("created_on")
        private String createdOn;
        @SerializedName("updated_on")
        private String updatedOn;
        @SerializedName("created_by")
        private int createdBy;
        @SerializedName("updated_by")
        private int updatedBy;
        @SerializedName("deleted_at")
        private String deletedAt;
        @SerializedName("created_at")
        private String createdAt;
        @SerializedName("updated_at")
        private String updatedAt;
        @SerializedName("customer_photo_old")
        private String customerPhotoOld;
        @SerializedName("pets")
        private List<PetsBean> pets;

        public int getCustomerId() {
            return customerId;
        }

        public void setCustomerId(int customerId) {
            this.customerId = customerId;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getCustomerEmail() {
            return customerEmail;
        }

        public void setCustomerEmail(String customerEmail) {
            this.customerEmail = customerEmail;
        }

        public String getCustomerPhone() {
            return customerPhone;
        }

        public void setCustomerPhone(String customerPhone) {
            this.customerPhone = customerPhone;
        }

        public String getCustomerAddress() {
            return customerAddress;
        }

        public void setCustomerAddress(String customerAddress) {
            this.customerAddress = customerAddress;
        }

        public String getCustomerPhoto() {
            return customerPhoto;
        }

        public void setCustomerPhoto(String customerPhoto) {
            this.customerPhoto = customerPhoto;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
        }

        public int getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(int createdBy) {
            this.createdBy = createdBy;
        }

        public int getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(String deletedAt) {
            this.deletedAt = deletedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getCustomerPhotoOld() {
            return customerPhotoOld;
        }

        public void setCustomerPhotoOld(String customerPhotoOld) {
            this.customerPhotoOld = customerPhotoOld;
        }

        public List<PetsBean> getPets() {
            return pets;
        }

        public void setPets(List<PetsBean> pets) {
            this.pets = pets;
        }

        public static class PetsBean {

            @SerializedName("pet_color")
            private String petColor;
            @SerializedName("pet_breed")
            private String petBreed;
            @SerializedName("pet_special_notes")
            private String petSpecialNotes;
            @SerializedName("pet_microchip")
            private String petMicrochip;

            public String getPetColor() {
                return petColor;
            }

            public void setPetColor(String petColor) {
                this.petColor = petColor;
            }

            public String getPetBreed() {
                return petBreed;
            }

            public void setPetBreed(String petBreed) {
                this.petBreed = petBreed;
            }

            public String getPetSpecialNotes() {
                return petSpecialNotes;
            }

            public void setPetSpecialNotes(String petSpecialNotes) {
                this.petSpecialNotes = petSpecialNotes;
            }

            public String getPetMicrochip() {
                return petMicrochip;
            }

            public void setPetMicrochip(String petMicrochip) {
                this.petMicrochip = petMicrochip;
            }

            @SerializedName("pet_id")
            private int petId;
            @SerializedName("customer_id")
            private int customerId;
            @SerializedName("pet_name")
            private String petName;
            @SerializedName("pet_race")
            private String petRace;
            @SerializedName("pet_birthday")
            private String petBirthday;
            @SerializedName("pet_gender")
            private String petGender;
            @SerializedName("pet_photo")
            private String petPhoto;
            @SerializedName("created_on")
            private String createdOn;
            @SerializedName("updated_on")
            private String updatedOn;
            @SerializedName("created_by")
            private int createdBy;
            @SerializedName("updated_by")
            private int updatedBy;
            @SerializedName("deleted_at")
            private String deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;
            private boolean isFinish = true;

            public int getPetId() {
                return petId;
            }

            @SerializedName("pet_photo_old")
            private String petPhotoOld;

            public String getPetPhotoOld() {
                return petPhotoOld;
            }

            public void setPetPhotoOld(String petPhotoOld) {
                this.petPhotoOld = petPhotoOld;
            }

            public void setPetId(int petId) {
                this.petId = petId;
            }

            public int getCustomerId() {
                return customerId;
            }

            public void setCustomerId(int customerId) {
                this.customerId = customerId;
            }

            public String getPetName() {
                return petName;
            }

            public void setPetName(String petName) {
                this.petName = petName;
            }

            public String getPetRace() {
                return petRace;
            }

            public void setPetRace(String petRace) {
                this.petRace = petRace;
            }

            public String getPetBirthday() {
                return petBirthday;
            }

            public void setPetBirthday(String petBirthday) {
                this.petBirthday = petBirthday;
            }

            public String getPetGender() {
                return petGender;
            }

            public void setPetGender(String petGender) {
                this.petGender = petGender;
            }

            public String getPetPhoto() {
                return petPhoto;
            }

            public void setPetPhoto(String petPhoto) {
                this.petPhoto = petPhoto;
            }

            public String getCreatedOn() {
                return createdOn;
            }

            public void setCreatedOn(String createdOn) {
                this.createdOn = createdOn;
            }

            public String getUpdatedOn() {
                return updatedOn;
            }

            public void setUpdatedOn(String updatedOn) {
                this.updatedOn = updatedOn;
            }

            public int getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(int createdBy) {
                this.createdBy = createdBy;
            }

            public int getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(int updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(String deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public boolean isFinish() {
                return isFinish;
            }

            public void setFinish(boolean finish) {
                isFinish = finish;
            }
        }
    }
}
