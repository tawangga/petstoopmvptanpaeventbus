package com.tawangga.petstoopmvp2.Model;

import com.google.gson.annotations.SerializedName;

public class GetInvoice {


    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DataInvoice DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DataInvoice getDATA() {
        return DATA;
    }

    public void setDATA(DataInvoice DATA) {
        this.DATA = DATA;
    }


}
