package com.tawangga.petstoopmvp2.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CartModel {


    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {

        @SerializedName("cart_product_id")
        private int cartProductId;
        @SerializedName("order_code")
        private String orderCode;
        @SerializedName("customer_id")
        private int customerId;
        @SerializedName("cart_service_id")
        private int cartServiceId;
        @SerializedName("customer_name")
        private String customerName;
        @SerializedName("is_invoice")
        private int isInvoice;
        @SerializedName("created_by")
        private int createdBy;
        @SerializedName("updated_by")
        private int updatedBy;
        @SerializedName("deleted_at")
        private String deletedAt;
        @SerializedName("created_at")
        private String createdAt;
        @SerializedName("updated_at")
        private String updatedAt;
        @SerializedName("grand_total")
        private int grandTotal;
        @SerializedName("items")
//        @SerializedName("medical_items")
        private List<DetailCart> items;

        public int getCartProductId() {
            return cartProductId;
        }

        public void setCartProductId(int cartProductId) {
            this.cartProductId = cartProductId;
        }

        public String getOrderCode() {
            return orderCode;
        }

        public void setOrderCode(String orderCode) {
            this.orderCode = orderCode;
        }

        public int getCustomerId() {
            return customerId;
        }

        public void setCustomerId(int customerId) {
            this.customerId = customerId;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public int getIsInvoice() {
            return isInvoice;
        }

        public void setIsInvoice(int isInvoice) {
            this.isInvoice = isInvoice;
        }

        public int getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(int createdBy) {
            this.createdBy = createdBy;
        }

        public int getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(String deletedAt) {
            this.deletedAt = deletedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public int getGrandTotal() {
            return grandTotal;
        }

        public void setGrandTotal(int grandTotal) {
            this.grandTotal = grandTotal;
        }

        public List<DetailCart> getItems() {
            return items;
        }

        public void setItems(List<DetailCart> items) {
            this.items = items;
        }

        public int getCartServiceId() {
            return cartServiceId;
        }

        public void setCartServiceId(int cartServiceId) {
            this.cartServiceId = cartServiceId;
        }
    }
}



