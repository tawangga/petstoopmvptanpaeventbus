package com.tawangga.petstoopmvp2.Model;

import android.view.KeyEvent;

public class EventBusClass {
    public static class OnKeyUp {
        public int keyCode;
        public KeyEvent event;

        public OnKeyUp(int keyCode, KeyEvent event) {
            this.keyCode = keyCode;
            this.event = event;
        }
    }
}
