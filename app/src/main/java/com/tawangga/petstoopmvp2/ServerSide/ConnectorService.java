package com.tawangga.petstoopmvp2.ServerSide;

import com.tawangga.petstoopmvp2.Model.CartModel;
import com.tawangga.petstoopmvp2.Model.CustomerModel;
import com.tawangga.petstoopmvp2.Model.DetailCustomerModel;
import com.tawangga.petstoopmvp2.Model.FindSite;
import com.tawangga.petstoopmvp2.Model.GetErrorMessage;
import com.tawangga.petstoopmvp2.Model.GetExamModel;
import com.tawangga.petstoopmvp2.Model.GetInvoice;
import com.tawangga.petstoopmvp2.Model.GetInvoiceDP;
import com.tawangga.petstoopmvp2.Model.GetOrderConfirmAnamnesis;
import com.tawangga.petstoopmvp2.Model.GetOrders;
import com.tawangga.petstoopmvp2.Model.PetListModel;
import com.tawangga.petstoopmvp2.Model.ProcessInvoice;
import com.tawangga.petstoopmvp2.Model.ProductCategories;
import com.tawangga.petstoopmvp2.Model.ProductModel;
import com.tawangga.petstoopmvp2.Model.PromoModel;
import com.tawangga.petstoopmvp2.Model.SignInCustomer;
import com.tawangga.petstoopmvp2.Model.SignInModel;
import com.tawangga.petstoopmvp2.ModelNew.AddServiceTreatment;
import com.tawangga.petstoopmvp2.ModelNew.CreateCart;
import com.tawangga.petstoopmvp2.ModelNew.GetAppVersion;
import com.tawangga.petstoopmvp2.ModelNew.GetBoarding;
import com.tawangga.petstoopmvp2.ModelNew.GetBoardingTreatmentRecord;
import com.tawangga.petstoopmvp2.ModelNew.GetCurrentVersion;
import com.tawangga.petstoopmvp2.ModelNew.GetDoctor;
import com.tawangga.petstoopmvp2.ModelNew.GetDoctorSchedule;
import com.tawangga.petstoopmvp2.ModelNew.GetGroomingLocation;
import com.tawangga.petstoopmvp2.ModelNew.GetGroomingService;
import com.tawangga.petstoopmvp2.ModelNew.GetGroomingTreatmentRecord;
import com.tawangga.petstoopmvp2.ModelNew.GetMedicalService;
import com.tawangga.petstoopmvp2.ModelNew.GetMedicalTreatmentRecord;
import com.tawangga.petstoopmvp2.ModelNew.GetTreatment;
import com.tawangga.petstoopmvp2.ModelNew.GetTreatmentRecord;
import com.tawangga.petstoopmvp2.Param.AddCartToInvoice;
import com.tawangga.petstoopmvp2.Param.CancelMedicalServiceParam;
import com.tawangga.petstoopmvp2.Param.CancelServiceParam;
import com.tawangga.petstoopmvp2.Param.ConvertToOrderParam;
import com.tawangga.petstoopmvp2.Param.SaveConfirmOrderAnamnesis;

import java.util.HashMap;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;
import rx.Observable;

interface ConnectorService {
    @POST("login")
    Observable<SignInModel> loginPost(@Body RequestBody body);

    @POST("customer/login")
    Observable<SignInCustomer> loginCustomer(@Body RequestBody body);

    @GET("customers/pet")
    Observable<PetListModel> getPetList(@Query("limit") String limit,
                                        @Query("page") String page,
                                        @Query("search") String search);

    @GET("customers-pet")
    Observable<PetListModel> getCustomerPage(@Query("limit") String limit,
                                             @Query("page") String page,
                                             @Query("search") String search);

    @GET("customers")
    Observable<CustomerModel> getCustomerList(@Query("limit") String limit,
                                              @Query("page") String page,
                                              @Query("search") String search);

    @GET("customers")
    Observable<CustomerModel> getCustomerListByPet(@Query("limit") String limit,
                                                   @Query("page") String page,
                                                   @Query("search") String search,
                                                   @Query("pet") String pet);

    @GET("products")
    Observable<ProductModel> getProduct(@Query("category_id") String category_id,
                                        @Query("limit") String limit,
                                        @Query("page") String page,
                                        @Query("search") String search,
                                        @Query("is_mobile") String isMobile,
                                        @Query("except_category_id") String except_category_id);

    @GET("products/categories")
    Observable<ProductCategories> getProdCategory();

    @Multipart
    @POST
    Observable<DetailCustomerModel> addCustomer(@Url String url,
                                                @PartMap HashMap<String, RequestBody> data,
                                                @Part MultipartBody.Part file);

    @GET
    Observable<DetailCustomerModel> detailCustomer(@Url String url);

    @POST("cart/product")
    Observable<CartModel> createCart(@Body RequestBody body);

    @GET
    Observable<CartModel> getCartMedicalItems(@Url String body);

    @GET
    Observable<GetTreatmentRecord> getPetTreatmentRecord(@Url String body);

    @GET
    Observable<GetMedicalTreatmentRecord> getPetMedicalTreatmentRecord(@Url String body, @QueryMap Map<String, String> param);

    @GET
    Observable<GetGroomingTreatmentRecord> getPetGroomingTreatmentRecord(@Url String body, @QueryMap Map<String, String> param);

    @GET
    Observable<GetBoardingTreatmentRecord> getPetBoardingTreatmentRecord(@Url String body, @QueryMap Map<String, String> param);

    @GET("promos")
    Observable<PromoModel> getPromo(@Query("limit") String limit,
                                    @Query("page") String page,
                                    @Query("search") String search);

    @POST("cart/product/add")
    Observable<GetErrorMessage> addProductToCart(@Body RequestBody body);

    @GET("doctors")
    Observable<GetDoctor> getDoctor(@QueryMap Map<String, String> param);

    @GET("doctors/schedules")
    Observable<GetDoctorSchedule> getAllDoctor(@QueryMap Map<String, String> param);

    @GET("groomings/locations")
    Observable<GetGroomingLocation> getGroomingLocation(@QueryMap Map<String, String> param);

    @GET("cages")
    Observable<GetBoarding> getBoarding(@QueryMap Map<String, String> param);

    @GET("groomings")
    Observable<GetGroomingService> getGroomingService(@QueryMap Map<String, String> param);

    @GET("settings/versions")
    Observable<GetAppVersion> getAppVersion(@QueryMap Map<String, String> param);

    @GET("medicals")
    Observable<GetMedicalService> getMedicalService(@QueryMap Map<String, String> param);

    @POST("cart/service")
    Observable<CreateCart> createCartTreatment(@Body RequestBody requestBody);

    @POST("cart/service/add")
    Observable<AddServiceTreatment> addServiceTreatment(@Body RequestBody requestBody);

    @POST("doctor/cart/service/add")
    Observable<AddServiceTreatment> addServiceTreatmentDoctor(@Body RequestBody requestBody);

    @POST("cart/product/clean")
    Observable<GetErrorMessage> deleteShopCart(@Body RequestBody requestBody);

    @POST("cart/service/delete")
    Observable<GetErrorMessage> deleteServiceTreatment(@Body RequestBody requestBody);

    @POST
    Observable<GetErrorMessage> deleteMedicalItems(@Url String url, @Body RequestBody requestBody);

    @POST("cart/service/reservation-to-order")
    Observable<GetErrorMessage> convertToOrder(@Body ConvertToOrderParam requestBody);

    @POST("cart/service/reservation")
    Observable<CreateCart> reservTreatment(@Body RequestBody requestBody);

    @GET("cart/service")
    Observable<GetTreatment> getReservation(@QueryMap Map<String, String> param);

    @POST("cart/service/set-status")
    Observable<GetErrorMessage> changeOrderStatus(@Body RequestBody requestBody);

    @GET
    Observable<GetErrorMessage> deleteService(@Url String url);

    @GET
    Observable<GetOrderConfirmAnamnesis> getOrderConfirmAnamnesis(@Url String url, @Query("is_record") String is_record);

    @POST
    Observable<GetErrorMessage> saveOrderConfirmAnamnesis(@Url String url, @Body SaveConfirmOrderAnamnesis body);

    @POST("transaction/invoice/services")
    Observable<GetInvoice> addToInvoice(@Body RequestBody requestBody);

    @POST("transaction/invoice/products")
    Observable<GetInvoice> createInvoiceProduct(@Body RequestBody body);

    @GET("orders")
    Observable<GetOrders> getOrders(@Query("order_type") String order_type);

    @POST("transaction/invoice/orders")
    Observable<GetInvoice> addOrderToInvoice(@Body AddCartToInvoice requestBody);

    @POST("transaction/remove/order")
    Observable<GetInvoice> removeOrderInvoice(@Body RequestBody requestBody);

    @POST("transaction/remove/order/item")
    Observable<GetInvoice> removeOrderDetailInvoice(@Body RequestBody requestBody);

    @POST("transaction/cancel")
    Observable<GetErrorMessage> cancelInvoice(@Body RequestBody requestBody);

    @POST("transaction/payment/step1")
    Observable<ProcessInvoice> processInvoice(@Body RequestBody requestBody);

    @POST("transaction/payment/step2")
    Observable<GetErrorMessage> processInvoice2(@Body RequestBody requestBody);


    @GET("transaction/invoice-dp")
    Observable<GetInvoiceDP> getInvoiceDP();


    @GET("doctor/cart/service")
    Observable<GetTreatment> getDoctorReservation(@QueryMap Map<String, String> formBody);

    @GET
    Observable<GetOrderConfirmAnamnesis> getOrderConfirmAnamnesisDoctor(@Url String url, @Query("is_record") String is_record);

    @POST("cart/product/delete")
    Observable<GetErrorMessage> deleteProductCart(@Body RequestBody body);

    @POST("doctor/cart/service/anamnesis/{cart_service_id}/medical-items/delete")
    Observable<GetErrorMessage> deleteProductCartDoctor(@Body RequestBody body, @Path("cart_service_id") String cart_service_id);

    @POST
    Observable<GetErrorMessage> addMedicalItemToCart(@Url String url, @Body RequestBody body);

    @GET("doctor/exams")
    Observable<GetExamModel> getExam(@QueryMap Map<String, String> param);

    @POST("find-site")
    Observable<FindSite> findSite(@Body RequestBody requestBody);

    @POST("cart/service/rollback")
    Observable<GetErrorMessage> rollbackService(@Body CancelServiceParam param);

    @POST("doctor/cart/service/anamnesis/medical-items/rollback")
    Observable<GetErrorMessage> rollbackMedicalItem(@Body CancelMedicalServiceParam param);

    @GET("versions/latest")
    Observable<GetCurrentVersion> getVersion();

}
