package com.tawangga.petstoopmvp2.ServerSide;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tawangga.petstoopmvp2.BuildConfig;
import com.tawangga.petstoopmvp2.Helper.GlobalVariable;
import com.tawangga.petstoopmvp2.Helper.SharedPreference;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Model.CartModel;
import com.tawangga.petstoopmvp2.Model.CustomerModel;
import com.tawangga.petstoopmvp2.Model.DataInvoice;
import com.tawangga.petstoopmvp2.Model.DetailCustomerModel;
import com.tawangga.petstoopmvp2.Model.FindSite;
import com.tawangga.petstoopmvp2.Model.GetExamModel;
import com.tawangga.petstoopmvp2.Model.GetOrderConfirmAnamnesis;
import com.tawangga.petstoopmvp2.Model.GetOrders;
import com.tawangga.petstoopmvp2.Model.PetListModel;
import com.tawangga.petstoopmvp2.Model.ProcessInvoice;
import com.tawangga.petstoopmvp2.Model.ProductCategories;
import com.tawangga.petstoopmvp2.Model.ProductModel;
import com.tawangga.petstoopmvp2.Model.PromoModel;
import com.tawangga.petstoopmvp2.Model.SignInCustomer;
import com.tawangga.petstoopmvp2.Model.SignInModel;
import com.tawangga.petstoopmvp2.ModelNew.CreateCart;
import com.tawangga.petstoopmvp2.ModelNew.GetAppVersion;
import com.tawangga.petstoopmvp2.ModelNew.GetBoarding;
import com.tawangga.petstoopmvp2.ModelNew.GetBoardingTreatmentRecord;
import com.tawangga.petstoopmvp2.ModelNew.GetCurrentVersion;
import com.tawangga.petstoopmvp2.ModelNew.GetDoctor;
import com.tawangga.petstoopmvp2.ModelNew.GetDoctorSchedule;
import com.tawangga.petstoopmvp2.ModelNew.GetGroomingLocation;
import com.tawangga.petstoopmvp2.ModelNew.GetGroomingService;
import com.tawangga.petstoopmvp2.ModelNew.GetGroomingTreatmentRecord;
import com.tawangga.petstoopmvp2.ModelNew.GetMedicalService;
import com.tawangga.petstoopmvp2.ModelNew.GetMedicalTreatmentRecord;
import com.tawangga.petstoopmvp2.ModelNew.GetTreatment;
import com.tawangga.petstoopmvp2.ModelNew.GetTreatmentRecord;
import com.tawangga.petstoopmvp2.Modul.Login.SignInActivity;
import com.tawangga.petstoopmvp2.Param.AddCartToInvoice;
import com.tawangga.petstoopmvp2.Param.CancelMedicalServiceParam;
import com.tawangga.petstoopmvp2.Param.CancelServiceParam;
import com.tawangga.petstoopmvp2.Param.ConvertToOrderParam;
import com.tawangga.petstoopmvp2.Param.SaveConfirmOrderAnamnesis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class Connector {
    private static final String CODELABS_URL = GlobalVariable.ROOT_URL;
    static Connector fragment;
    private Map<String, List<Subscription>> subscriptionMap;
    private ConnectorService codelabsService;
    private Context context;

    public static Connector newInstance(Context context) {
        if (fragment == null) {
            fragment = new Connector();
        }
        fragment.context = context;
        return fragment;
    }

    private GsonConverterFactory buildGsonConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson myGson = gsonBuilder.create();
        return GsonConverterFactory.create(myGson);
    }

    private Connector() {
        subscriptionMap = new HashMap<>();
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();
                        Request.Builder newReq = request.newBuilder()
                                .addHeader("APP_TOKEN", GlobalVariable.APP_TOKEN)
                                .addHeader("USER_TOKEN", new SharedPreference(context).getStaffToken());
                        okhttp3.Response response = chain.proceed(newReq.build());

                        // todo deal with the issues the way you need to
                        if (response.code() == 401) {
                            new SharedPreference(context).clearSession();
                            context.startActivity(new Intent(context, SignInActivity.class));
                            if (context instanceof Activity) {
                                ((Activity) context).finish();
                            }

                            return response;
                        }
                        return response;

                    }
                });

        if (BuildConfig.DEBUG) {
            builder.addNetworkInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        }

        final OkHttpClient okHttpClient = builder.build();

        Retrofit whiteListAdapter = new Retrofit.Builder()
                .baseUrl(CODELABS_URL)
                .addConverterFactory(buildGsonConverter())
                .addCallAdapterFactory(LabsRxErrorHandlingCallAdapterFactory.create())
                .client(okHttpClient)
                .build();
        codelabsService = whiteListAdapter.create(ConnectorService.class);

    }


    private void addSubscriptionToMap(String key, Subscription subs) {
        List<Subscription> listSub;
        if (subscriptionMap.get(key) == null) {
            listSub = new ArrayList<>();
            subscriptionMap.put(key, listSub);
        }
        subscriptionMap.get(key).add(subs);
    }


    Interceptor requestInterceptor = chain -> {
        Request.Builder newReq = chain.request().newBuilder()
                .addHeader("APP_TOKEN", GlobalVariable.APP_TOKEN)
                .addHeader("USER_TOKEN", new SharedPreference(context).getStaffToken());
        return chain.proceed(newReq.build());
    };

    public void loginPost(RequestBody body, final Connector.ApiCallback<SignInModel.DATABean> callback) {
        Subscription subscribe = codelabsService.loginPost(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getDATA(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);

    }

    public void loginCustomer(RequestBody body, final Connector.ApiCallback<SignInCustomer.DATA> callback) {
        Subscription subscribe = codelabsService.loginCustomer(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getDATA(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);

    }


    public void createCart(RequestBody body, final Connector.ApiCallback<CartModel.DATABean> callback) {
        Subscription subscribe = codelabsService.createCart(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getDATA() != null) {
                            if (response.getSTATUSCODE().equals("200")) {
                                callback.success(response.getDATA(), "");
                            } else {
                                callback.onFailure(new Exception(response.getMESSAGE()));
                            }
                        } else {
                            callback.onFailure(new Exception(Utils.getNoDataAvailableString()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);

    }

    public void getPromo(String limit, String page, String search, final ApiCallback<PromoModel.DATABean> callback) {
        Subscription subscribe = codelabsService.getPromo(limit, page, search)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getDATA() != null) {
                            if (response.getSTATUSCODE().equals("200")) {
                                callback.success(response.getDATA(), "");
                            } else {
                                callback.onFailure(new Exception(response.getMESSAGE()));
                            }
                        } else {
                            callback.onFailure(new Exception(Utils.getNoDataAvailableString()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);

    }


    public void getExam(Map<String, String> param, final Connector.ApiCallback<GetExamModel.DATABean> callback) {
        Subscription subscribe = codelabsService.getExam(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getDATA(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);

    }

    public void getOrders(String order_type, final Connector.ApiCallback<List<GetOrders.DATABean>> callback) {
        Subscription subscribe = codelabsService.getOrders(order_type)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getDATA() != null) {
                            if (response.getSTATUSCODE().equals("200")) {
                                callback.success(response.getDATA(), "");
                            } else if (response.getSTATUSCODE().equals("203")) {
                                callback.success(response.getDATA(), "");
                            } else {
                                callback.onFailure(new Exception(response.getMESSAGE()));
                            }
                        } else {
                            callback.onFailure(new Exception(Utils.getNoDataAvailableString()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);

    }


    public void detailCustomer(String customerId, final Connector.ApiCallback<DetailCustomerModel.DATABean> callback) {
        String url = CODELABS_URL + "customers/" + customerId;
        Subscription subscribe = codelabsService.detailCustomer(url)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getDATA() != null) {
                            if (response.getSTATUSCODE().equals("200")) {
                                callback.success(response.getDATA(), "");
                            } else {
                                callback.onFailure(new Exception(response.getMESSAGE()));
                            }
                        } else {
                            callback.onFailure(new Exception(Utils.getNoDataAvailableString()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);

    }

    public void addProductToCart(RequestBody body, final Connector.ApiCallback<String> callback) {
        Subscription subscribe = codelabsService.addProductToCart(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {

                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getMESSAGE(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);

    }

    public void getCustomerListByPet(String limit, String page, String search, final Connector.ApiCallback<CustomerModel.DATABean> callback) {
        Subscription subscribe = codelabsService.getCustomerListByPet(limit, page, search, search)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getDATA() != null) {
                            if (response.getSTATUSCODE().equals("200")) {
                                callback.success(response.getDATA(), "");
                            } else {
                                callback.onFailure(new Exception(response.getMESSAGE()));
                            }
                        } else {
                            callback.onFailure(new Exception(Utils.getNoDataAvailableString()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);
    }

    public void getCustomerList(String limit, String page, String search, final Connector.ApiCallback<CustomerModel.DATABean> callback) {
        Subscription subscribe = codelabsService.getCustomerList(limit, page, search)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getDATA() != null) {
                            if (response.getSTATUSCODE().equals("200")) {
                                callback.success(response.getDATA(), "");
                            } else {
                                callback.onFailure(new Exception(response.getMESSAGE()));
                            }
                        } else {
                            callback.onFailure(new Exception(Utils.getNoDataAvailableString()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);

    }

    public void getPetList(String limit, String page, String search, final Connector.ApiCallback<PetListModel.DATABean> callback) {
        Subscription subscribe = codelabsService.getPetList(limit, page, search)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getDATA() != null) {
                            if (response.getSTATUSCODE().equals("200")) {
                                callback.success(response.getDATA(), "");
                            } else {
                                callback.onFailure(new Exception(response.getMESSAGE()));
                            }
                        } else {
                            callback.onFailure(new Exception(Utils.getNoDataAvailableString()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);

    }

    public void getCustomerPage(String limit, String page, String search, final Connector.ApiCallback<PetListModel.DATABean> callback) {
        Subscription subscribe = codelabsService.getCustomerPage(limit, page, search)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getDATA() != null) {
                            if (response.getSTATUSCODE().equals("200")) {
                                callback.success(response.getDATA(), "");
                            } else {
                                callback.onFailure(new Exception(response.getMESSAGE()));
                            }
                        } else {
                            callback.onFailure(new Exception(Utils.getNoDataAvailableString()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);

    }

    public void getProduct(String category_id, String limit, String page, String search, String except_category_id, final Connector.ApiCallback<ProductModel.DATABean> callback) {
        Subscription subscribe = codelabsService.getProduct(category_id, limit, page, search, "1", except_category_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getDATA() != null) {
                            if (response.getSTATUSCODE().equals("200")) {
                                if (response.getDATA().getItems().size() != 0) {
                                    callback.success(response.getDATA(), "");
                                } else {
                                    callback.onFailure(new Exception(Utils.getNoDataAvailableString()));
                                }
                            } else {
                                callback.onFailure(new Exception(response.getMESSAGE()));
                            }
                        } else {
                            callback.onFailure(new Exception(Utils.getNoDataAvailableString()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);
    }

    public void getDoctor(Map<String, String> param, final ApiCallback<GetDoctor.DATABean> callback) {
        Subscription subscribe = codelabsService.getDoctor(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getDATA() != null) {
                            if (response.getSTATUSCODE().equals("200")) {
                                callback.success(response.getDATA(), "");
                            } else {
                                callback.onFailure(new Exception(response.getMESSAGE()));
                            }
                        } else {
                            callback.onFailure(new Exception(Utils.getNoDataAvailableString()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);
    }

    public void getBoarding(Map<String, String> param, final ApiCallback<GetBoarding.DATABean> callback) {
        Subscription subscribe = codelabsService.getBoarding(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getDATA() != null) {
                            if (response.getSTATUSCODE().equals("200")) {
                                callback.success(response.getDATA(), "");
                            } else {
                                callback.onFailure(new Exception(response.getMESSAGE()));
                            }
                        } else {
                            callback.onFailure(new Exception(Utils.getNoDataAvailableString()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);
    }


    public void getAppVersion(Map<String, String> param, final ApiCallback<GetAppVersion.DATABean> callback) {
        Subscription subscribe = codelabsService.getAppVersion(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getDATA() != null) {
                            if (response.getSTATUSCODE().equals("200")) {
                                callback.success(response.getDATA(), "");
                            } else {
                                callback.onFailure(new Exception(response.getMESSAGE()));
                            }
                        } else {
                            callback.onFailure(new Exception(Utils.getNoDataAvailableString()));
                        }
                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);
    }

    public void getAllDoctor(Map<String, String> param, final ApiCallback<GetDoctorSchedule.DATABean> callback) {
        Subscription subscribe = codelabsService.getAllDoctor(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getDATA() != null) {
                            if (response.getSTATUSCODE().equals("200")) {
                                callback.success(response.getDATA(), "");
                            } else {
                                callback.onFailure(new Exception(response.getMESSAGE()));
                            }
                        } else {
                            callback.onFailure(new Exception(Utils.getNoDataAvailableString()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);
    }

    public void getProdCategory(final ApiCallback<ProductCategories.DATABean> callback) {
        Subscription subscribe = codelabsService.getProdCategory()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getDATA() != null) {
                            if (response.getSTATUSCODE().equals("200")) {
                                if (response.getDATA().getItems().size() != 0) {
                                    callback.success(response.getDATA(), "");
                                } else {
                                    callback.onFailure(new Exception(Utils.getNoDataAvailableString()));
                                }
                            } else {
                                callback.onFailure(new Exception(response.getMESSAGE()));
                            }
                        } else {
                            callback.onFailure(new Exception(Utils.getNoDataAvailableString()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);
    }

    public void getGroomingLocation(Map<String, String> param, final ApiCallback<GetGroomingLocation.DATABean> callback) {
        Subscription subscribe = codelabsService.getGroomingLocation(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getDATA() != null) {
                            if (response.getSTATUSCODE().equals("200")) {
                                callback.success(response.getDATA(), "");
                            } else {
                                callback.onFailure(new Exception(response.getMESSAGE()));
                            }
                        } else {
                            callback.onFailure(new Exception(Utils.getNoDataAvailableString()));
                        }
                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);
    }


    public void getGroomingService(Map<String, String> param, final ApiCallback<GetGroomingService.DATABean> callback) {
        Subscription subscribe = codelabsService.getGroomingService(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getDATA() != null) {
                            if (response.getSTATUSCODE().equals("200")) {
                                callback.success(response.getDATA(), "");
                            } else {
                                callback.onFailure(new Exception(response.getMESSAGE()));
                            }
                        } else {
                            callback.onFailure(new Exception(Utils.getNoDataAvailableString()));
                        }
                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);
    }


    public void getMedicalService(Map<String, String> param, final ApiCallback<GetMedicalService.DATABean> callback) {
        Subscription subscribe = codelabsService.getMedicalService(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getDATA() != null) {
                            if (response.getSTATUSCODE().equals("200")) {
                                callback.success(response.getDATA(), "");
                            } else {
                                callback.onFailure(new Exception(response.getMESSAGE()));
                            }
                        } else {
                            callback.onFailure(new Exception(Utils.getNoDataAvailableString()));
                        }
                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);
    }

    public void getReservation(Map<String, String> param, final ApiCallback<GetTreatment.DATABean> callback) {
        Subscription subscribe = codelabsService.getReservation(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getDATA(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);

        addSubscriptionToMap(callback.getKey(), subscribe);
    }

    public void getDoctorReservation(Map<String, String> formBody, final ApiCallback<GetTreatment.DATABean> callback) {
        Subscription subscribe = codelabsService.getDoctorReservation(formBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getDATA(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);
    }


    public void addCustomer(HashMap<String, RequestBody> data
            , MultipartBody.Part file, String id, final Connector.ApiCallback<DetailCustomerModel.DATABean> callback) {
        String url = CODELABS_URL + "customers" + id;

        Subscription sort = codelabsService.addCustomer(url, data, file)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getDATA(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE() + " \n" + response.getERROR()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);
    }

    public void createCartTreatment(RequestBody requestBody, final Connector.ApiCallback<CreateCart.DATABean> callback) {
        Subscription sort = codelabsService.createCartTreatment(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getDATA(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);
    }

    public void addServiceTreatment(RequestBody requestBody, final Connector.ApiCallback<CreateCart.DATABean.ServiceBean> callback) {
        Subscription sort = codelabsService.addServiceTreatment(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getDATA(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);
    }

    public void addServiceTreatmentDoctor(RequestBody requestBody, final Connector.ApiCallback<CreateCart.DATABean.ServiceBean> callback) {
        Subscription sort = codelabsService.addServiceTreatmentDoctor(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getDATA(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);
    }

    public void addOrderToInvoice(AddCartToInvoice requestBody, final Connector.ApiCallback<DataInvoice> callback) {
        Subscription sort = codelabsService.addOrderToInvoice(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getDATA(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);
    }

    public void removeOrderInvoice(RequestBody requestBody, final Connector.ApiCallback<DataInvoice> callback) {
        Subscription sort = codelabsService.removeOrderInvoice(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getDATA(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);
    }

    public void removeOrderDetailInvoice(RequestBody requestBody, final Connector.ApiCallback<DataInvoice> callback) {
        Subscription sort = codelabsService.removeOrderDetailInvoice(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getDATA(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);
    }

    public void deleteShopCart(RequestBody requestBody, final Connector.ApiCallback<String> callback) {
        Subscription sort = codelabsService.deleteShopCart(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getMESSAGE(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);
    }

    public void deleteServiceTreatment(RequestBody requestBody, final Connector.ApiCallback<String> callback) {
        Subscription sort = codelabsService.deleteServiceTreatment(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getMESSAGE(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);
    }

    public void deleteMedicalItems(RequestBody requestBody, String cartServiceId, final Connector.ApiCallback<String> callback) {
        String url = CODELABS_URL + "doctor/cart/service/anamnesis/" + cartServiceId + "/medical-items/delete";
        Subscription sort = codelabsService.deleteMedicalItems(url, requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getMESSAGE(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);
    }

    public void addToInvoice(RequestBody requestBody, final Connector.ApiCallback<DataInvoice> callback) {
        Subscription sort = codelabsService.addToInvoice(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getDATA(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);
    }

    public void createInvoiceProduct(RequestBody requestBody, final Connector.ApiCallback<DataInvoice> callback) {
        Subscription sort = codelabsService.createInvoiceProduct(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getDATA(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);
    }


    public void convertToOrder(ConvertToOrderParam requestBody, final Connector.ApiCallback<String> callback) {
        Subscription sort = codelabsService.convertToOrder(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getMESSAGE(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);
    }

    public void changeOrderStatus(RequestBody requestBody, final Connector.ApiCallback<String> callback) {
        Subscription sort = codelabsService.changeOrderStatus(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getMESSAGE(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);
    }


    public void reservTreatment(RequestBody requestBody, final Connector.ApiCallback<CreateCart.DATABean> callback) {
        Subscription sort = codelabsService.reservTreatment(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getDATA(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);
    }

    public void deleteService(int cartServiceId, final Connector.ApiCallback<String> callback) {
        String url = CODELABS_URL + "cart/service/" + cartServiceId + "/delete";
        Subscription sort = codelabsService.deleteService(url)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getMESSAGE(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);
    }

    public void saveOrderConfirmAnamnesis(int cartServiceId, SaveConfirmOrderAnamnesis body, final Connector.ApiCallback<String> callback) {
        String url = CODELABS_URL + "cart/service/anamnesis/" + cartServiceId;
        Subscription sort = codelabsService.saveOrderConfirmAnamnesis(url, body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getMESSAGE(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);
    }

    public void saveOrderConfirmAnamnesisDokter(int cartServiceId, SaveConfirmOrderAnamnesis body, final Connector.ApiCallback<String> callback) {
        String url = CODELABS_URL + "doctor/cart/service/anamnesis/" + cartServiceId;
        Subscription sort = codelabsService.saveOrderConfirmAnamnesis(url, body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getMESSAGE(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);
    }


    public void deleteProductCart(RequestBody requestBody, final Connector.ApiCallback<String> callback) {
        Subscription sort = codelabsService.deleteProductCart(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getMESSAGE(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);
    }

    public void deleteProductCartDoctor(RequestBody requestBody, String cart_service_id, final Connector.ApiCallback<String> callback) {
        Subscription sort = codelabsService.deleteProductCartDoctor(requestBody, cart_service_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getMESSAGE(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);
    }


    public void getOrderConfirmAnamnesis(String serviceID, int is_record, final Connector.ApiCallback<GetOrderConfirmAnamnesis.DATABean> callback) {
        String url = CODELABS_URL + "cart/service/anamnesis/" + serviceID;
        Subscription sort = codelabsService.getOrderConfirmAnamnesis(url, String.valueOf(is_record))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getDATA(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);
    }

    public void getOrderConfirmAnamnesisDoctor(String serviceID, int is_record, ApiCallback<GetOrderConfirmAnamnesis.DATABean> callback) {
        String url = CODELABS_URL + "doctor/cart/service/anamnesis/" + serviceID;
        Subscription sort = codelabsService.getOrderConfirmAnamnesisDoctor(url, String.valueOf(is_record))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getDATA(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);

    }


    public interface ApiCallback<T> {
        String getKey();

        void success(T t, String messages);

        void onFailure(Throwable t);

    }


    public void close(String key) {
        List<Subscription> lss = subscriptionMap.get(key);
        if (lss != null) {
            for (Subscription subs : lss) {
                if (!subs.isUnsubscribed()) {
                    subs.unsubscribe();
                }
            }
        }
        subscriptionMap.remove(key);
    }


    public void cancelInvoice(RequestBody requestBody, final Connector.ApiCallback<String> callback) {
        Subscription sort = codelabsService.cancelInvoice(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getMESSAGE(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);
    }


    public void processInvoice(RequestBody requestBody, final Connector.ApiCallback<ProcessInvoice.DATABean> callback) {
        Subscription sort = codelabsService.processInvoice(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getDATA(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);
    }

    public void processInvoice2(RequestBody requestBody, final Connector.ApiCallback<String> callback) {
        Subscription sort = codelabsService.processInvoice2(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getMESSAGE(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);
    }

    public void getInvoiceDP(final Connector.ApiCallback<List<DataInvoice>> callback) {
        Subscription subscribe = codelabsService.getInvoiceDP()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getDATA() != null) {
                            if (response.getSTATUSCODE().equals("200")) {
                                callback.success(response.getDATA(), "");
                            } else {
                                callback.onFailure(new Exception(response.getMESSAGE()));
                            }
                        } else {
                            callback.onFailure(new Exception(Utils.getNoDataAvailableString()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);

    }

    public void getCartMedicalItems(String cartServiceId, final Connector.ApiCallback<CartModel.DATABean> callback) {
        String url = CODELABS_URL + "doctor/cart/service/anamnesis/" + cartServiceId + "/medical-items";
        Subscription subscribe = codelabsService.getCartMedicalItems(url)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getDATA() != null) {
                            if (response.getSTATUSCODE().equals("200")) {
                                callback.success(response.getDATA(), "");
                            } else {
                                callback.onFailure(new Exception(response.getMESSAGE()));
                            }
                        } else {
                            callback.onFailure(new Exception(Utils.getNoDataAvailableString()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);

    }

    public void getPetTreatmentRecord(String petId, final Connector.ApiCallback<List<GetTreatmentRecord.DATABean>> callback) {
        String url = CODELABS_URL + "cart/service/pet/" + petId;
        Subscription subscribe = codelabsService.getPetTreatmentRecord(url)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getDATA() != null) {
                            if (response.getSTATUSCODE().equals("200")) {
                                callback.success(response.getDATA(), "");
                            } else {
                                callback.onFailure(new Exception(response.getMESSAGE()));
                            }
                        } else {
                            callback.onFailure(new Exception(Utils.getNoDataAvailableString()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);

    }

    public void getPetMedicalTreatmentRecord(String petId, Map<String, String> param, final Connector.ApiCallback<GetMedicalTreatmentRecord.DATABean> callback) {
        String url = CODELABS_URL + "cart/service/medical/pet/" + petId;
        Subscription subscribe = codelabsService.getPetMedicalTreatmentRecord(url, param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getDATA() != null) {
                            if (response.getSTATUSCODE().equals("200")) {
                                callback.success(response.getDATA(), "");
                            } else {
                                callback.onFailure(new Exception(response.getMESSAGE()));
                            }
                        } else {
                            callback.onFailure(new Exception(Utils.getNoDataAvailableString()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);

    }

    public void getPetGroomingTreatmentRecord(String petId, Map<String, String> param, final Connector.ApiCallback<GetGroomingTreatmentRecord.DATABean> callback) {
        String url = CODELABS_URL + "cart/service/grooming/pet/" + petId;
        Subscription subscribe = codelabsService.getPetGroomingTreatmentRecord(url, param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getDATA() != null) {
                            if (response.getSTATUSCODE().equals("200")) {
                                callback.success(response.getDATA(), "");
                            } else {
                                callback.onFailure(new Exception(response.getMESSAGE()));
                            }
                        } else {
                            callback.onFailure(new Exception(Utils.getNoDataAvailableString()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);

    }

    public void getPetBoardingTreatmentRecord(String petId, Map<String, String> param, final Connector.ApiCallback<GetBoardingTreatmentRecord.DATABean> callback) {
        String url = CODELABS_URL + "cart/service/boarding/pet/" + petId;
        Subscription subscribe = codelabsService.getPetBoardingTreatmentRecord(url, param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getDATA() != null) {
                            if (response.getSTATUSCODE().equals("200")) {
                                callback.success(response.getDATA(), "");
                            } else {
                                callback.onFailure(new Exception(response.getMESSAGE()));
                            }
                        } else {
                            callback.onFailure(new Exception(Utils.getNoDataAvailableString()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);

    }

    public void addMedicalItemToCart(String cart_service_id, RequestBody body, final Connector.ApiCallback<String> callback) {
        String url = CODELABS_URL + "doctor/cart/service/anamnesis/" + cart_service_id + "/medical-items";
        Subscription subscribe = codelabsService.addMedicalItemToCart(url, body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {

                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getMESSAGE(), "");
                        } else if (response.getSTATUSCODE().equals("400")) {
                            callback.success(response.getMESSAGE(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);

    }


    public void findSite(RequestBody requestBody, final Connector.ApiCallback<FindSite.DATABean> callback) {
        Subscription subscribe = codelabsService.findSite(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getDATA(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);
    }

    public void rollbackService(CancelServiceParam param, final Connector.ApiCallback<String> callback) {
        Subscription sort = codelabsService.rollbackService(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getMESSAGE(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);
    }

    public void rollbackMedicalItem(CancelMedicalServiceParam param, final Connector.ApiCallback<String> callback) {
        Subscription sort = codelabsService.rollbackMedicalItem(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getMESSAGE(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), sort);
    }

    public void getVersion(final Connector.ApiCallback<GetCurrentVersion.DATABean> callback) {
        Subscription subscribe = codelabsService.getVersion()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    try {
                        if (response.getSTATUSCODE().equals("200")) {
                            callback.success(response.getDATA(), "");
                        } else {
                            callback.onFailure(new Exception(response.getMESSAGE()));
                        }

                    } catch (Exception e) {
                        callback.onFailure(e);
                    }
                }, callback::onFailure);
        addSubscriptionToMap(callback.getKey(), subscribe);

    }

}
