package com.tawangga.petstoopmvp2.Dialog.DialogDoctorSchedule;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.tabs.TabLayout;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.ModelNew.GetDoctorSchedule;
import com.tawangga.petstoopmvp2.Modul.AddNewTreatment.ScheduleByDateAdapter;
import com.tawangga.petstoopmvp2.Modul.AddNewTreatment.ScheduleByDoctorAdapter;
import com.tawangga.petstoopmvp2.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogDoctorSchedule extends DialogFragment {

    @BindView(R.id.txt_bydate)
    TextView txtBydate;
    @BindView(R.id.btn_bydate)
    LinearLayout btnBydate;
    @BindView(R.id.txt_bydoctor)
    TextView txtBydoctor;
    @BindView(R.id.btn_bydoctor)
    LinearLayout btnBydoctor;
    @BindView(R.id.tabByDate)
    TabLayout tabByDate;
    @BindView(R.id.rv_bydate)
    RecyclerView rvBydate;
    @BindView(R.id.llByDate)
    LinearLayout llByDate;
    @BindView(R.id.rv_bydoctor)
    RecyclerView rvBydoctor;
    @BindView(R.id.llByDoctor)
    LinearLayout llByDoctor;
    private List<GetDoctorSchedule.DATABean.ItemsBean> data;

    public static DialogDoctorSchedule newInstance(List<GetDoctorSchedule.DATABean.ItemsBean> data) {

        Bundle args = new Bundle();

        DialogDoctorSchedule fragment = new DialogDoctorSchedule();
        fragment.data = data;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = View.inflate(getActivity(), R.layout.layout_doctor_schedule, null);
        ButterKnife.bind(this, v);
        initView();
        return v;

    }

    @Override
    public void onResume() {
        super.onResume();
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((WindowManager.LayoutParams) params);

    }

    private void initView() {
        View.OnClickListener onCLickByDate = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtBydate.setTextColor(getResources().getColor(R.color.white));
                btnBydate.setBackgroundColor(getResources().getColor(R.color.brown_2));
                txtBydoctor.setTextColor(getResources().getColor(R.color.black));
                btnBydoctor.setBackgroundColor(getResources().getColor(R.color.white));
                llByDate.setVisibility(View.VISIBLE);
                llByDoctor.setVisibility(View.GONE);
            }
        };
        View.OnClickListener onCLickByDoctor = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtBydate.setTextColor(getResources().getColor(R.color.black));
                btnBydate.setBackgroundColor(getResources().getColor(R.color.white));
                txtBydoctor.setTextColor(getResources().getColor(R.color.white));
                btnBydoctor.setBackgroundColor(getResources().getColor(R.color.brown_2));
                llByDate.setVisibility(View.GONE);
                llByDoctor.setVisibility(View.VISIBLE);
            }
        };

        btnBydate.setOnClickListener(onCLickByDate);
        txtBydate.setOnClickListener(onCLickByDate);
        btnBydoctor.setOnClickListener(onCLickByDoctor);
        txtBydoctor.setOnClickListener(onCLickByDoctor);
        for (int i = 0; i < 7; i++) {
            tabByDate.addTab(tabByDate.newTab().setText(Utils.getInstance().getDateName()[i]));
        }
        ScheduleByDateAdapter scheduleByDateAdapter = new ScheduleByDateAdapter(getActivity());
        rvBydate.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvBydate.setAdapter(scheduleByDateAdapter);

        List<GetDoctorSchedule.DATABean.ItemsBean> listDoctorDay = new ArrayList<>();
        for (GetDoctorSchedule.DATABean.ItemsBean doctor : data) {
            if (doctor.getDoctor().getWorkingdays().toLowerCase().contains(Utils.getInstance().getDateName()[0].toLowerCase()))
                listDoctorDay.add(doctor);
        }
        scheduleByDateAdapter.setDatas(listDoctorDay);

        tabByDate.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                List<GetDoctorSchedule.DATABean.ItemsBean> listDoctorDay = new ArrayList<>();
                for (GetDoctorSchedule.DATABean.ItemsBean doctor : data) {
                    if (doctor.getDoctor().getWorkingdays().toLowerCase().contains(tab.getText().toString().toLowerCase()))
                        listDoctorDay.add(doctor);

                }
                scheduleByDateAdapter.setDay(tab.getText().toString());
                scheduleByDateAdapter.setDatas(listDoctorDay);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        for (int i = 0; i < tabByDate.getTabCount(); i++) {
            if (tabByDate.getTabAt(i).getText().toString().toLowerCase().contains(Utils.getInstance().getTodayDay().toLowerCase())) {
                tabByDate.getTabAt(i).select();
            }
        }


        ScheduleByDoctorAdapter scheduleByDoctorAdapter = new ScheduleByDoctorAdapter(getActivity());
        scheduleByDoctorAdapter.setDatas(data);
        rvBydoctor.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvBydoctor.setAdapter(scheduleByDoctorAdapter);

    }
}
