package com.tawangga.petstoopmvp2.Dialog.DialogSearchService;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Model.GetOrderConfirmAnamnesis;
import com.tawangga.petstoopmvp2.ModelNew.CreateCart;
import com.tawangga.petstoopmvp2.ModelNew.GetGroomingService;
import com.tawangga.petstoopmvp2.ModelNew.GetMedicalService;
import com.tawangga.petstoopmvp2.R;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class ServiceSelectionAdapter extends RecyclerView.Adapter<ServiceSelectionAdapter.ViewHolder> {

    private boolean isCart;
    private List<GetGroomingService.DATABean.ItemsBean> dataGrooming;
    private List<GetMedicalService.DATABean.ItemsBean> dataMedical;
    private boolean isGrooming, isMedical;
    private Context context;
    private TextView txt_treatment_selected;
    private GetOrderConfirmAnamnesis.DATABean dataAnamnesis;
    private CreateCart.DATABean cartData;

    public ServiceSelectionAdapter(Context context, TextView txt_treatment_selected, CreateCart.DATABean cartData) {

        this.context = context;
        this.txt_treatment_selected = txt_treatment_selected;
        this.cartData = cartData;
        dataGrooming = new ArrayList<>();
        dataMedical = new ArrayList<>();
        this.isCart = true;
    }

    public ServiceSelectionAdapter(Context activity, TextView txt_treatment_selected, GetOrderConfirmAnamnesis.DATABean dataAnamnesis) {

        context = activity;
        this.txt_treatment_selected = txt_treatment_selected;
        this.dataAnamnesis = dataAnamnesis;
        dataMedical = new ArrayList<>();
        dataGrooming = new ArrayList<>();
        this.isCart = false;
    }

    @NonNull
    @Override
    public ServiceSelectionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_treatment_selection, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceSelectionAdapter.ViewHolder holder, int position) {

        if (isMedical) {
            GetMedicalService.DATABean.ItemsBean data = dataMedical.get(position);
            holder.txtTreatmentName.setText(data.getMedicalName());
            holder.txtTreatmentPrice.setText(Utils.toRupiah2(data.getMedicalPrice() + ""));
            if (data.isAdded()) {
                holder.tvAdd.setText("Added");
                holder.tvAdd.setTextColor(context.getResources().getColor(R.color.brown_light));
                holder.tvAdd.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
            } else {
                holder.tvAdd.setText("+");
                holder.tvAdd.setTextColor(context.getResources().getColor(R.color.white));
                holder.tvAdd.setBackgroundColor(context.getResources().getColor(R.color.brown_light));
            }
        }
        if (isGrooming) {
            GetGroomingService.DATABean.ItemsBean data = dataGrooming.get(position);
            holder.txtTreatmentName.setText(data.getGroomingName());
            holder.txtTreatmentPrice.setText(Utils.toRupiah2(data.getGroomingPrice() + ""));
            if (data.isAdded()) {
                holder.tvAdd.setText("Added");
                holder.tvAdd.setTextColor(context.getResources().getColor(R.color.brown_light));
                holder.tvAdd.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));

            } else {
                holder.tvAdd.setText("+");
                holder.tvAdd.setTextColor(context.getResources().getColor(R.color.white));
                holder.tvAdd.setBackgroundColor(context.getResources().getColor(R.color.brown_light));
            }
        }
    }

    private void addServiceTreatment(GetMedicalService.DATABean.ItemsBean data, GetGroomingService.DATABean.ItemsBean dataGrooming, TextView tvAdd) {
        FormBody.Builder formBuilder = new FormBody.Builder();
        if (isMedical) {
            if (isCart) {
                formBuilder.add("cart_service_id", cartData.getCartServiceId() + "");
            } else {
                formBuilder.add("cart_service_id", dataAnamnesis.getCartServiceId() + "");
            }
            formBuilder.add("service_id", data.getMedicalId() + "");
            formBuilder.add("service_type", "1");
            formBuilder.add("service_name", data.getMedicalName() + "");
            formBuilder.add("service_price", data.getMedicalPrice());
        } else {
            formBuilder.add("cart_service_id", cartData.getCartServiceId() + "");
            formBuilder.add("service_id", dataGrooming.getGroomingId() + "");
            formBuilder.add("service_type", "2");
            formBuilder.add("service_name", dataGrooming.getGroomingName() + "");
            formBuilder.add("service_price", dataGrooming.getGroomingPrice());

        }
        RequestBody formBody = formBuilder.build();
        ProgressDialog dialog = Utils.showProgressDialog(context, false, "Loading...");

        if (isCart) {
            Connector.newInstance(context).addServiceTreatment(formBody, new Connector.ApiCallback<CreateCart.DATABean.ServiceBean>() {
                @Override
                public String getKey() {
                    return null;
                }

                @Override
                public void success(CreateCart.DATABean.ServiceBean dataBean, String messages) {
                    if (isMedical) {
                        data.setAdded(true);
                        cartData.getServiceMedical().add(dataBean);
                        setSelectedMedical();
                    } else {
                        dataGrooming.setAdded(true);
                        cartData.getServiceGrooming().add(dataBean);
                        txt_treatment_selected.setText("( " + cartData.getServiceGrooming().size() + " Treatment Selected )");
                    }
                    tvAdd.setOnClickListener(null);
                    notifyDataSetChanged();
                    dialog.dismiss();
                }

                @Override
                public void onFailure(Throwable t) {
                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    t.printStackTrace();

                }
            });
        } else {
            Connector.newInstance(context).addServiceTreatmentDoctor(formBody, new Connector.ApiCallback<CreateCart.DATABean.ServiceBean>() {
                @Override
                public String getKey() {
                    return null;
                }

                @Override
                public void success(CreateCart.DATABean.ServiceBean dataBean, String messages) {
                    if (isMedical) {
                        data.setAdded(true);
                        GetOrderConfirmAnamnesis.DATABean.TreatmentsBean dataTreatment = new GetOrderConfirmAnamnesis.DATABean.TreatmentsBean();
                        dataTreatment.setCartServiceId(dataBean.getCartServiceId());
                        dataTreatment.setCartServiceDetailId(dataBean.getCartServiceDetailId());
                        dataTreatment.setServiceId(dataBean.getServiceId());
                        dataTreatment.setServiceName(dataBean.getServiceName());
                        dataTreatment.setServiceType(dataBean.getServiceType());
                        dataTreatment.setServicePrice(dataBean.getServicePrice());
                        dataAnamnesis.getTreatments().add(dataTreatment);
                        setSelectedMedical();
                    } else {
                        dataGrooming.setAdded(true);
                        cartData.getServiceGrooming().add(dataBean);
                        txt_treatment_selected.setText("( " + cartData.getServiceGrooming().size() + " Treatment Selected )");
                    }
                    tvAdd.setOnClickListener(null);
                    notifyDataSetChanged();
                    dialog.dismiss();
                }

                @Override
                public void onFailure(Throwable t) {
                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    t.printStackTrace();

                }
            });
        }

    }

    @Override
    public int getItemCount() {
        if (isGrooming)
            return dataGrooming.size();
        else if (isMedical)
            return dataMedical.size();
        return 0;
    }

    public void setDataGrooming(List<GetGroomingService.DATABean.ItemsBean> dataGrooming) {
        this.dataGrooming.addAll(dataGrooming);
        txt_treatment_selected.setText("( " + cartData.getServiceGrooming().size() + " Treatment Selected )");
        isGrooming = true;
        isMedical = false;
        notifyDataSetChanged();
    }

    public void setDataMedical(List<GetMedicalService.DATABean.ItemsBean> dataMedical) {
        this.dataMedical.addAll(dataMedical);
        isGrooming = false;
        isMedical = true;
        setSelectedMedical();
        notifyDataSetChanged();
    }

    void setSelectedMedical() {
        if (isCart) {
            txt_treatment_selected.setText("( " + cartData.getServiceMedical().size() + " Treatment Selected )");
        } else {
            List<GetOrderConfirmAnamnesis.DATABean.TreatmentsBean> treatments = new ArrayList<>();
            for (GetOrderConfirmAnamnesis.DATABean.TreatmentsBean allTreatment : dataAnamnesis.getTreatments()) {
                if (allTreatment.getServiceType().equals("1")) {
                    treatments.add(allTreatment);
                }
            }

            txt_treatment_selected.setText("( " + treatments.size() + " Treatment Selected )");

        }
    }

    public List<GetGroomingService.DATABean.ItemsBean> getDataGrooming() {
        return dataGrooming;
    }

    public List<GetMedicalService.DATABean.ItemsBean> getDataMedical() {
        return dataMedical;
    }

    public void clearAllData() {
        dataGrooming.clear();
        dataMedical.clear();
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_treatment_name)
        TextView txtTreatmentName;
        @BindView(R.id.txt_treatment_price)
        TextView txtTreatmentPrice;
        @BindView(R.id.tv_add)
        TextView tvAdd;
        @BindView(R.id.ll_background)
        RelativeLayout llBackground;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isMedical) {
                        GetMedicalService.DATABean.ItemsBean data = dataMedical.get(getAdapterPosition());
                        if (!data.isAdded())
                            addServiceTreatment(data, null, tvAdd);
                    } else if (isGrooming) {
                        GetGroomingService.DATABean.ItemsBean data = dataGrooming.get(getAdapterPosition());
                        if (!data.isAdded())
                            addServiceTreatment(null, data, tvAdd);
                    }
                }
            });
        }
    }
}
