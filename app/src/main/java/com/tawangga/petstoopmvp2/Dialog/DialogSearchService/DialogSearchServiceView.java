package com.tawangga.petstoopmvp2.Dialog.DialogSearchService;

import com.tawangga.petstoopmvp2.ModelNew.GetGroomingService;
import com.tawangga.petstoopmvp2.ModelNew.GetMedicalService;
import com.tawangga.petstoopmvp2.Modul.BaseView;

public interface DialogSearchServiceView extends BaseView {
    void onSuccessGetMedicalService(GetMedicalService.DATABean data);

    void onSuccessGetGroomingService(GetGroomingService.DATABean data);
}
