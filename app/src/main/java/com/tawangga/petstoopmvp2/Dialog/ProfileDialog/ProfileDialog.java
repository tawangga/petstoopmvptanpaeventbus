package com.tawangga.petstoopmvp2.Dialog.ProfileDialog;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.michael.easydialog.EasyDialog;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.tawangga.petstoopmvp2.Helper.SharedPreference;
import com.tawangga.petstoopmvp2.Modul.LoginCustomer.SignInCustomerActivity;
import com.tawangga.petstoopmvp2.R;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileDialog extends EasyDialog {
    private static final String TAG = ProfileDialog.class.getSimpleName();
    @BindView(R.id.iv_pic)
    CircleImageView ivPic;
    @BindView(R.id.pb_pic)
    ProgressBar pbPic;
    @BindView(R.id.txt_username)
    TextView txtUsername;
    @BindView(R.id.txt_userid)
    TextView txtUserid;
    @BindView(R.id.txt_name)
    TextView txtName;
    @BindView(R.id.ll_tv_name)
    LinearLayout llTvName;
    @BindView(R.id.txt_phone)
    TextView txtPhone;
    @BindView(R.id.ed_phone)
    EditText edPhone;
    @BindView(R.id.tv_edit_phone)
    TextView tvEditPhone;
    @BindView(R.id.ll_tv_phone)
    LinearLayout llTvPhone;
    @BindView(R.id.txt_email)
    TextView txtEmail;
    @BindView(R.id.ed_email)
    EditText edEmail;
    @BindView(R.id.tv_edit_email)
    TextView tvEditEmail;
    @BindView(R.id.ll_tv_email)
    LinearLayout llTvEmail;
    @BindView(R.id.tv_edit_password)
    TextView tvEditPassword;
    @BindView(R.id.ed_password)
    EditText edPassword;
    @BindView(R.id.ll_tv_password)
    LinearLayout llTvPassword;
    @BindView(R.id.btn_logout)
    Button btnLogout;
    @BindView(R.id.btn_save)
    Button btnSave;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    private View view;
    private Context context;
    private int width;
    private ProfileDialogPresenter presenter;
    private SharedPreference data;
    private String selectedImagePath = "";


    public ProfileDialog(Context context, int width) {
        super(context);
        this.context = context;
        this.width = width;
        view = View.inflate(context, R.layout.dialog_profile_account, null);
        ButterKnife.bind(this, view);
        presenter = new ProfileDialogPresenter(context);
        setLayout(view);
        initView();
    }

    private void initView() {
        setBackgroundColor(context.getResources().getColor(R.color.white));
        setOutsideColor(context.getResources().getColor(R.color.background_color_black));
        setGravity(EasyDialog.GRAVITY_RIGHT);
        setTouchOutsideDismiss(true);
        setMatchParent(false);

        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                view.getLayoutParams().width = width;
                view.requestLayout();
                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });


        data = new SharedPreference(context);

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (data.isCustomer()){
                    data.clearSession();
                    Intent intent = new Intent(context, SignInCustomerActivity.class);
                    context.startActivity(intent);
                    ((Activity) context).finish();

                }else {
                    if (!data.isDoctor()) {
                        presenter.cancelInvoice();
                    } else {
                        data.clearSession();
                        Intent intent = new Intent(context, SignInCustomerActivity.class);
                        context.startActivity(intent);
                        ((Activity) context).finish();
                    }
                }
            }
        });



        if (data.isCustomer()){
            txtUsername.setText(data.getCustomerName());
            txtName.setText(data.getCustomerName());
            txtUserid.setText(data.getCustomerId());
            txtPhone.setText(data.getCustomerPhone());
            txtEmail.setText(data.getCustomerEmail());
            Picasso.get().load(data.getCustomerPhoto())
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .tag(TAG)
                    .networkPolicy(NetworkPolicy.NO_CACHE).into(ivPic, new Callback() {
                @Override
                public void onSuccess() {
                    pbPic.setVisibility(View.GONE);
                    ivPic.setVisibility(View.VISIBLE);

                }

                @Override
                public void onError(Exception e) {
                    ivPic.setImageDrawable(context.getResources().getDrawable(R.drawable.pic));
                    pbPic.setVisibility(View.GONE);
                    ivPic.setVisibility(View.VISIBLE);
                    e.printStackTrace();
                    Log.e("Staff Photo", data.getStaffPhoto());
                }
            });


            tvEditPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    txtPhone.setVisibility(View.GONE);
                    tvEditPhone.setVisibility(View.GONE);
                    edPhone.setVisibility(View.VISIBLE);
                }
            });

            tvEditEmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    txtEmail.setVisibility(View.GONE);
                    tvEditEmail.setVisibility(View.GONE);
                    edEmail.setVisibility(View.VISIBLE);

                }
            });

            tvEditPassword.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    tvEditPassword.setVisibility(View.GONE);
                    edPassword.setVisibility(View.VISIBLE);

                }
            });


            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });


            ivPic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((Activity) context).registerForContextMenu(ivPic);
                    ((Activity) context).openContextMenu(ivPic);
                    ((Activity) context).unregisterForContextMenu(ivPic);

                }
            });

            btnSave.setVisibility(View.GONE);
            btnCancel.setVisibility(View.GONE);
            tvEditPhone.setVisibility(View.GONE);
            tvEditPassword.setVisibility(View.GONE);
            tvEditEmail.setVisibility(View.GONE);
            llTvPassword.setVisibility(View.GONE);
        }
        else{
            txtUsername.setText(data.getStaffUsername());
            txtName.setText(data.getStaffName());
            txtUserid.setText(data.getStaffCode());
            txtPhone.setText(data.getStaffPhone());
            txtEmail.setText(data.getStaffEmail());
            Picasso.get().load(data.getStaffPhoto())
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .tag(TAG)
                    .networkPolicy(NetworkPolicy.NO_CACHE).into(ivPic, new Callback() {
                @Override
                public void onSuccess() {
                    pbPic.setVisibility(View.GONE);
                    ivPic.setVisibility(View.VISIBLE);

                }

                @Override
                public void onError(Exception e) {
                    ivPic.setImageDrawable(context.getResources().getDrawable(R.drawable.pic));
                    pbPic.setVisibility(View.GONE);
                    ivPic.setVisibility(View.VISIBLE);
                    e.printStackTrace();
                    Log.e("Staff Photo", data.getStaffPhoto());
                }
            });


            tvEditPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    txtPhone.setVisibility(View.GONE);
                    tvEditPhone.setVisibility(View.GONE);
                    edPhone.setVisibility(View.VISIBLE);
                }
            });

            tvEditEmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    txtEmail.setVisibility(View.GONE);
                    tvEditEmail.setVisibility(View.GONE);
                    edEmail.setVisibility(View.VISIBLE);

                }
            });

            tvEditPassword.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    tvEditPassword.setVisibility(View.GONE);
                    edPassword.setVisibility(View.VISIBLE);

                }
            });


            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });


            ivPic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((Activity) context).registerForContextMenu(ivPic);
                    ((Activity) context).openContextMenu(ivPic);
                    ((Activity) context).unregisterForContextMenu(ivPic);

                }
            });
        }

    }

    public void dismiss() {
        super.dismiss();
    }


    public void setImage(String image, File file) {
        Toast.makeText(context, "panggil", Toast.LENGTH_SHORT).show();
        Picasso.get().load(file).into(ivPic);

        selectedImagePath = image;
    }

    public void setImage(String image, Bitmap file) {
        Toast.makeText(context, "panggil", Toast.LENGTH_SHORT).show();
        ivPic.setImageBitmap(file);

        selectedImagePath = image;
    }


}
