package com.tawangga.petstoopmvp2.Dialog.DialogCreateOrder;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.EventBus.OnSuccessReservTreatment;
import com.tawangga.petstoopmvp2.Helper.TreatmentHelper;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.ModelNew.CreateCart;
import com.tawangga.petstoopmvp2.Modul.AddNewTreatment.SelectTreatmentNewAdapter;
import com.tawangga.petstoopmvp2.Modul.AddNewTreatment.SelectedServiceAdapter;
import com.tawangga.petstoopmvp2.Modul.Main.MainActivity;
import com.tawangga.petstoopmvp2.Modul.TreatmentFragment.DialogConfirmationAdapter;
import com.tawangga.petstoopmvp2.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class DialogCreateOrder extends DialogFragment implements DialogCreateOrderView {
    private static final String TAG = DialogCreateOrder.class.getSimpleName();
    @BindView(R.id.iv_pic_pet)
    CircleImageView ivPicPet;
    @BindView(R.id.txt_owner_name)
    TextView txtOwnerName;
    @BindView(R.id.txt_pet_name)
    TextView txtPetName;
    @BindView(R.id.rv_medical)
    RecyclerView rvMedical;
    @BindView(R.id.ll_medical)
    LinearLayout llMedical;
    @BindView(R.id.rv_grooming)
    RecyclerView rvGrooming;
    @BindView(R.id.ll_grooming)
    LinearLayout llGrooming;
    @BindView(R.id.rv_boarding)
    RecyclerView rvBoarding;
    @BindView(R.id.ll_boarding)
    LinearLayout llBoarding;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;
    @BindView(R.id.ll_button)
    LinearLayout llButton;
    private CreateCart.DATABean cartData;
    private boolean isHasKonsultasiDokter;
    List<CreateCart.DATABean.ServiceBean> dataMedic, dataGrooming, dataBoarding;
    private SelectTreatmentNewAdapter adapter;
    private SelectedServiceAdapter adapterServices;
    private boolean isResultSuccess = false;
    private ProgressDialog dialog;
    private DialogCreateOrderPresenter presenter;

    public static DialogCreateOrder newInstance(CreateCart.DATABean cartdata, SelectTreatmentNewAdapter adapter, SelectedServiceAdapter adapterServices) {


        DialogCreateOrder fragment = new DialogCreateOrder();
        fragment.cartData = cartdata;
        fragment.adapter = adapter;
        fragment.adapterServices = adapterServices;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = View.inflate(getActivity(), R.layout.dialog_confirmation, null);
        ButterKnife.bind(this, v);
        presenter = new DialogCreateOrderPresenter(getActivity(), TAG, this);
        initView();
        return v;
    }

    private void initView() {
        dataMedic = cartData.getServiceMedical();
        dataGrooming = cartData.getServiceGrooming();
        dataBoarding = new ArrayList<>();

        txtOwnerName.setText(cartData.getPet().getCustomerName());
        txtPetName.setText(cartData.getPet().getPetName());
        Picasso.get().load(cartData.getPet().getPetPhoto())
                .into(ivPicPet, new Callback() {
                    @Override
                    public void onSuccess() {
                        ivPicPet.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError(Exception e) {
                        ivPicPet.setImageDrawable(getResources().getDrawable(R.drawable.no_image));
                        ivPicPet.setVisibility(View.VISIBLE);
                        e.printStackTrace();
                    }
                });

        isHasKonsultasiDokter = isHasKonsultasiDokter(dataMedic);
        if (cartData.getServiceMedical().size() == 0 && cartData.getServiceGrooming().size() == 0 && cartData.getBoardingSchedule().getCageId() == 0) {
            Toast.makeText(getActivity(), "Tidak ada service yang dipilih", Toast.LENGTH_SHORT).show();
            return;
        }


        if (cartData.getBoardingSchedule().getCageId() != 0) {
            CreateCart.DATABean.ServiceBean item = new CreateCart.DATABean.ServiceBean();
            item.setServiceType("3");
            item.setCartServiceId(cartData.getCartServiceId());
            item.setServiceName(cartData.getBoardingSchedule().getCageName());
            item.setServiceId(0);
            dataBoarding.add(item);
        }

        if (dataMedic.size() == 0) {
            llMedical.setVisibility(View.GONE);
        }

        if (dataGrooming.size() == 0) {
            llGrooming.setVisibility(View.GONE);
        }

        if (dataBoarding.size() == 0) {
            llBoarding.setVisibility(View.GONE);
        }


        DialogConfirmationAdapter adapterMedic = new DialogConfirmationAdapter(getActivity(), cartData, null);
        adapterMedic.setDatas(dataMedic);

        DialogConfirmationAdapter adapterGrooming = new DialogConfirmationAdapter(getActivity(), cartData, null);
        adapterGrooming.setDatas(dataGrooming);

        DialogConfirmationAdapter adapterBoarding = new DialogConfirmationAdapter(getActivity(), cartData, null);
        adapterBoarding.setDatas(dataBoarding);

        rvMedical.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvMedical.setAdapter(adapterMedic);

        rvGrooming.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvGrooming.setAdapter(adapterGrooming);

        rvBoarding.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvBoarding.setAdapter(adapterBoarding);
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        if (!isResultSuccess) {
            adapter.notifyDataSetChanged();
            adapterServices.notifyDataChanged();
        }
    }

    @OnClick(R.id.btn_confirm)
    void onClickBtnConfirm() {
        if (cartData.getDoctorSchedule().getStaffId() == 0 && cartData.getServiceMedical().size() > 0) {
            Toast.makeText(getActivity(), "Pilih dokter untuk medical service", Toast.LENGTH_SHORT).show();
            return;
        }
        if (cartData.getDoctorSchedule().getStaffId() != 0) {
            isHasKonsultasiDokter = isHasKonsultasiDokter(cartData.getServiceMedical());
            if (!isHasKonsultasiDokter) {
                Toast.makeText(getActivity(), "Tambahkan konsultasi dokter pada medical service", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        if (cartData.getGroomingLocationSchedule().getGroomingLocationId() == 0 && cartData.getServiceGrooming().size() > 0) {
            Toast.makeText(getActivity(), "Pilih grooming location untuk grooming service", Toast.LENGTH_SHORT).show();
            return;
        }

        if (cartData.getGroomingLocationSchedule().getGroomingLocationId() != 0 && cartData.getServiceGrooming().size() == 0) {
            Toast.makeText(getActivity(), "Tambahkan service pada grooming", Toast.LENGTH_SHORT).show();
            return;
        }

        if (cartData.getBoardingSchedule().getCageId() != 0 && cartData.getBoardingSchedule().getScheduleDateUntil().equals("")) {
            Toast.makeText(getActivity(), "Tentukan tanggal pengambilan hewan", Toast.LENGTH_SHORT).show();
            return;

        }

        reservTreatment();

    }

    private void reservTreatment() {
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("cart_service_id", cartData.getCartServiceId() + "");
        formBuilder.add("pet_id", cartData.getPetId() + "");
        formBuilder.add("doctor_id", cartData.getDoctorSchedule().getStaffId() + "");
        formBuilder.add("doctor_schedule_date", cartData.getDoctorSchedule().getScheduleDate() + "");
        formBuilder.add("doctor_schedule_time", cartData.getDoctorSchedule().getScheduleTime() + "");
        formBuilder.add("grooming_location_id", cartData.getGroomingLocationSchedule().getGroomingLocationId() + "");
        formBuilder.add("grooming_location_date", cartData.getGroomingLocationSchedule().getScheduleDate() + "");
        formBuilder.add("grooming_location_time", cartData.getGroomingLocationSchedule().getScheduleTime() + "");
        formBuilder.add("cage_id", cartData.getBoardingSchedule().getCageId() + "");
        formBuilder.add("cage_schedule_date_from", cartData.getBoardingSchedule().getScheduleDateFrom() + "");
        formBuilder.add("cage_schedule_date_until", cartData.getBoardingSchedule().getScheduleDateUntil() + "");
        formBuilder.add("cage_price", cartData.getBoardingSchedule().getCagePrice() + "");
        formBuilder.add("cage_name", cartData.getBoardingSchedule().getCageName() + "");
        formBuilder.add("order_date", getServiceDate());
        formBuilder.add("cart_service_type", "2");
        RequestBody formBody = formBuilder.build();
        dialog = Utils.showProgressDialog(getActivity(), false, "Loading...");

        presenter.reservTreatment(formBody);

    }


    private String getServiceDate() {
        String date = Utils.getTodayDate();
        if (cartData.getDoctorSchedule().getStaffId() != 0) {
            date = cartData.getDoctorSchedule().getScheduleDate();
        } else if (cartData.getGroomingLocationSchedule().getGroomingLocationId() != 0) {
            date = cartData.getGroomingLocationSchedule().getScheduleDate();
        } else if (cartData.getBoardingSchedule().getCageId() != 0) {
            date = cartData.getBoardingSchedule().getScheduleDateFrom();
        }
        return date;
    }

    private boolean isHasKonsultasiDokter(List<CreateCart.DATABean.ServiceBean> dataMedic) {
        boolean hasKonsultasiDokter = false;
        for (CreateCart.DATABean.ServiceBean medicService : dataMedic) {
            if (medicService.getServiceId() == 1) {
                hasKonsultasiDokter = true;
            }
        }
        return hasKonsultasiDokter;
    }

    @OnClick(R.id.btn_cancel)
    void onCancelClick() {
        dismiss();
    }

    @Override
    public void onResume() {
        super.onResume();
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((WindowManager.LayoutParams) params);

    }

    @Override
    public void onSuccessReservTreatment(OnSuccessReservTreatment data) {
        dialog.dismiss();

        isResultSuccess = true;
        TreatmentHelper.getInstance().setDataTreatment(cartData);
        if (isHasKonsultasiDokter) {
            ((MainActivity) getActivity()).toKonsultasiDokter();
        } else {
            ((MainActivity) getActivity()).setClearBackStack(true);
            ((MainActivity) getActivity()).toTreatment();
        }
        dismiss();
    }

    @Override
    public void onFailGettingData(OnFailGettingData data) {
        if (data.getApi() == OnFailGettingData.RESERV_TREATMENT) {
            Toast.makeText(getActivity(), data.getThrowable().getMessage(), Toast.LENGTH_SHORT).show();
            ((MainActivity) getActivity()).toSelectCustomer(false, "treatment", "");
            dialog.dismiss();
            data.getThrowable().printStackTrace();

        }
    }

}
