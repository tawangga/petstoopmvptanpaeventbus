package com.tawangga.petstoopmvp2.Dialog.DialogSeeAllTreatment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.Model.GetOrderConfirmAnamnesis;
import com.tawangga.petstoopmvp2.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterClinicalCheckUp2 extends RecyclerView.Adapter<AdapterClinicalCheckUp2.ViewHolder> {

    private List<GetOrderConfirmAnamnesis.DATABean.ClinicalCheckupsBean> data;
    private Context context;
    private AdapterView.OnItemClickListener onItemClickListener;

    public AdapterClinicalCheckUp2(Context context) {

        this.context = context;
        data = new ArrayList<>();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_clinical_checkup_2, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.position = position;
        final long identity = System.currentTimeMillis();
        holder.identity = identity;

        GetOrderConfirmAnamnesis.DATABean.ClinicalCheckupsBean selectedData = data.get(position);
        holder.tvClinicalCheckup.setText(position + 1 + ". " + selectedData.getClinicalCheckupName());
        holder.txtUnit.setText(selectedData.getClinicalCheckupUnit());
        holder.txtValue.setText(selectedData.getValue() + "");


    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setDatas(List<GetOrderConfirmAnamnesis.DATABean.ClinicalCheckupsBean> data) {
        this.data = data;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        int position;
        long identity;

        @BindView(R.id.tv_clinical_checkup)
        TextView tvClinicalCheckup;
        @BindView(R.id.txt_value)
        TextView txtValue;
        @BindView(R.id.txt_unit)
        TextView txtUnit;
        @BindView(R.id.ll_background)
        LinearLayout llBackground;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(null, v, position, 0);
            }

        }
    }
}