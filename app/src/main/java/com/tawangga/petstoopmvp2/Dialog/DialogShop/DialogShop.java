package com.tawangga.petstoopmvp2.Dialog.DialogShop;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.tawangga.petstoopmvp2.EventBus.OnApiResultSuccess;
import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.EventBus.OnRefreshSearchView;
import com.tawangga.petstoopmvp2.EventBus.RefreshCart;
import com.tawangga.petstoopmvp2.Helper.SharedPreference;
import com.tawangga.petstoopmvp2.Helper.TreatmentHelper;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Model.CartModel;
import com.tawangga.petstoopmvp2.Model.DetailCart;
import com.tawangga.petstoopmvp2.Model.ProductModel;
import com.tawangga.petstoopmvp2.Model.PromoModel;
import com.tawangga.petstoopmvp2.ModelNew.GetDoctorSchedule;
import com.tawangga.petstoopmvp2.R;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class DialogShop extends DialogFragment implements DialogShopView {

    private static final String TAG = DialogShop.class.getSimpleName();
    @BindView(R.id.iv_produk)
    ImageView ivProduk;
    @BindView(R.id.pb_pic_product)
    ProgressBar pbPicProduct;
    @BindView(R.id.txt_produk_code)
    TextView txtProdukCode;
    @BindView(R.id.txt_produk_name)
    TextView txtProdukName;
    @BindView(R.id.txt_produk_price)
    TextView txtProdukPrice;
    @BindView(R.id.txt_produk_desc)
    TextView txtProdukDesc;
    @BindView(R.id.btn_min_qty)
    ImageView btnMinQty;
    @BindView(R.id.txt_qty)
    EditText txtQty;
    @BindView(R.id.btn_plus_qty)
    ImageView btnPlusQty;
    @BindView(R.id.rg_discount)
    RadioGroup rgDiscount;
    @BindView(R.id.txt_recomended)
    TextView txtRecomended;
    @BindView(R.id.sp_doctor)
    Spinner spDoctor;
    @BindView(R.id.txt_note)
    EditText txtNote;
    @BindView(R.id.main)
    LinearLayout main;
    @BindView(R.id.btn_add)
    Button btnAdd;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    @BindView(R.id.btn_delete)
    Button btnDelete;
    private List<String> listNameAllDoctor;
    private List<GetDoctorSchedule.DATABean.ItemsBean> listAllDoctor;
    private String doctor_id = "";
    private String doctor_name = "";
    private String discountID = "";
    private String savedDiscountName = "";
    private String discountName = "";
    private ProductModel.DATABean.ItemsBean productItem;
    private DetailCart productCart;
    private String prodName = "", prodPrice = "", prodDesc = "", prodImg = "", prodCode = "";
    private int qty = 0;
    private CartModel.DATABean cart;
    private List<PromoModel.DATABean.ItemsBean> listPromo = new ArrayList<>();
    private ProgressDialog dialog;
    private DialogShopPresenter presenter;
    private String medicalCartServiceId = "";

    public static DialogShop newInstance(List<GetDoctorSchedule.DATABean.ItemsBean> listAllDoctor, ProductModel.DATABean.ItemsBean productItem, DetailCart productCart, CartModel.DATABean cart, List<PromoModel.DATABean.ItemsBean> listPromo, String medicalCartServiceId) {
        DialogShop dialog = new DialogShop();
        dialog.listNameAllDoctor = new ArrayList<>();
        dialog.listAllDoctor = listAllDoctor;
        dialog.productItem = productItem;
        dialog.productCart = productCart;
        dialog.cart = cart;
        dialog.listPromo = listPromo;
        dialog.medicalCartServiceId = medicalCartServiceId;
        return dialog;
    }

    @Override
    public void onResume() {

        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = View.inflate(getContext(), R.layout.layout_option_product, null);
        ButterKnife.bind(this, v);
        presenter = new DialogShopPresenter(getActivity(), TAG, this);
        initView();
        return v;
    }

    private void initView() {

        if (new SharedPreference(getContext()).isDoctor()) {
            spDoctor.setVisibility(View.GONE);
            txtRecomended.setVisibility(View.GONE);
        }

        ArrayAdapter<String> adapterDoctor = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, listNameAllDoctor);
        listNameAllDoctor.add("-Select doctor");
        for (int u = 0; u < listAllDoctor.size(); u++) {
            listNameAllDoctor.add(listAllDoctor.get(u).getName());
        }

        spDoctor.setAdapter(adapterDoctor);
        spDoctor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    doctor_id = String.valueOf(listAllDoctor.get(position - 1).getStaffId());
                    doctor_name = listAllDoctor.get(position - 1).getName();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (productItem != null) {
            for (int i = 0; i < cart.getItems().size(); i++) {
                if (cart.getItems().get(i).getProductId() == productItem.getProductId()) {
                    Toast.makeText(getContext(), "SORRY, PRODUCT WAS ADDED BEFORE", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
            btnDelete.setVisibility(View.GONE);
            prodName = productItem.getProductName();
            prodCode = productItem.getProductCode();
            prodPrice = Utils.toRupiah(productItem.getProductPrice() + "");
            prodDesc = productItem.getProductDesc();
            prodImg = productItem.getProductImage();
            qty = 1;
            txtNote.setText("");
            btnAdd.setText("Add");
        } else if (productCart != null) {
            prodCode = productCart.getProductCode();
            prodName = productCart.getProductName();
            prodPrice = Utils.toRupiah(productCart.getProductPrice() + "");
            prodDesc = productCart.getProductDesc();
            prodImg = productCart.getProductImage();
            qty = productCart.getProductQty();
            spDoctor.setSelection(adapterDoctor.getPosition(productCart.getDoctorName()));
            txtNote.setText(productCart.getProductNote());
            txtNote.setSelection(productCart.getProductNote().length());
            btnAdd.setText("Edit");
            btnDelete.setVisibility(View.VISIBLE);
        }

        showPromo();

        txtQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!TextUtils.isEmpty(txtQty.getText().toString().trim())) {
                    qty = Integer.valueOf(txtQty.getText().toString());
                } else {
                    txtQty.setText("1");
                    qty = Integer.valueOf(txtQty.getText().toString());
                }

                if (txtQty.getText().toString().trim().equals("0")) {
                    txtQty.setText("1");
                    qty = Integer.valueOf(txtQty.getText().toString());
                }

            }
        });


        txtQty.setText("" + qty);
        txtProdukCode.setText("Code: " + prodCode);
        txtProdukName.setText(prodName);
        txtProdukPrice.setText(prodPrice);
        txtProdukDesc.setText(prodDesc);
        Picasso.get().load(prodImg)
                .into(ivProduk, new Callback() {
                    @Override
                    public void onSuccess() {
                        pbPicProduct.setVisibility(View.GONE);
                        ivProduk.setVisibility(View.VISIBLE);

                    }

                    @Override
                    public void onError(Exception e) {
                        ivProduk.setImageDrawable(getResources().getDrawable(R.drawable.no_image));
                        pbPicProduct.setVisibility(View.GONE);
                        ivProduk.setVisibility(View.VISIBLE);
                        e.printStackTrace();
                    }
                });

        btnMinQty.setOnClickListener(v -> {
            if (qty > 1) {
                txtQty.setText(String.valueOf(--qty));

                discountID = "";
                discountName = "";
                rgDiscount.removeAllViews();
                showPromo();

            }
        });

        btnPlusQty.setOnClickListener(v -> {
            txtQty.setText(String.valueOf(++qty));
            discountID = "";
            discountName = "";
            rgDiscount.removeAllViews();
            showPromo();

        });


        btnAdd.setOnClickListener(v -> {
            if (qty == 0) {
                Toast.makeText(getActivity(), "Insert qty", Toast.LENGTH_SHORT).show();
                return;
            }
            if (productCart != null) {
                if (new SharedPreference(getContext()).isDoctor()) {
                    editProductCartDoctor(productCart, qty, txtNote.getText().toString().trim());
                } else {
                    editProductCart(productCart, qty, txtNote.getText().toString().trim());
                }
            } else {
                if (new SharedPreference(getContext()).isDoctor()) {
                    addMedicalItemtoCart(productItem, qty, txtNote.getText().toString().trim());
                } else {
                    addProductCart(productItem, qty, txtNote.getText().toString().trim());
                }
            }

        });
        btnDelete.setOnClickListener(v -> {
                    if (new SharedPreference(getActivity()).isDoctor()) {
                        deleteProductCartDoctor(String.valueOf(cart.getCartServiceId()), String.valueOf(productCart.getProductId()));
                    } else {
                        deleteProductCart(String.valueOf(productCart.getCartProductId()), String.valueOf(productCart.getProductId()));
                    }
                }
        );
        btnCancel.setOnClickListener(v -> dismiss());
    }


    @SuppressLint("NewApi")
    private void showPromo() {
        for (int i = 0; i < listPromo.size(); i++) {
            if (listPromo.get(i).getPromoType().equals("2")) {
                if (productItem != null) {
                    if (Integer.valueOf(listPromo.get(i).getPromoAmount().replace(".00", "")) >= productItem.getProductPrice() * qty) {
                        continue;
                    }
                } else {
                    if (Integer.valueOf(listPromo.get(i).getPromoAmount().replace(".00", "")) >= Integer.valueOf(productCart.getProductPrice().replace(".00", "")) * qty) {
                        continue;
                    }
                }
            }
            RadioButton rb_discount = new RadioButton(getContext());
            rb_discount.setText(listPromo.get(i).getPromoName());
            rb_discount.setId(listPromo.get(i).getPromoId());
            rb_discount.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen._5ssp));
            rb_discount.setTextColor(ContextCompat.getColorStateList(Objects.requireNonNull(getContext()), R.color.rb_text_color));
            ColorStateList colorStateList;
            colorStateList = new ColorStateList(
                    new int[][]{
                            new int[]{-android.R.attr.state_checked}, //disabled
                            new int[]{android.R.attr.state_enabled} //enabled
                    },
                    new int[]{
                            getResources().getColor(R.color.brown),//disable
                            getResources().getColor(R.color.brown_2)//enable
                    }
            );

            rb_discount.setButtonTintList(colorStateList);
            if (productCart != null) {
                if (savedDiscountName.equals("")) {
                    if (rb_discount.getText().equals(productCart.getDiscountName())) {
                        savedDiscountName = rb_discount.getText().toString();
                        rb_discount.setChecked(true);
                        discountID = String.valueOf(rb_discount.getId());
                        discountName = rb_discount.getText().toString();
                    }
                } else {
                    if (rb_discount.getText().equals(savedDiscountName)) {
                        savedDiscountName = rb_discount.getText().toString();
                        rb_discount.setChecked(true);
                        discountID = String.valueOf(rb_discount.getId());
                        discountName = rb_discount.getText().toString();
                    }
                }
            } else {
                if (rb_discount.getText().equals(savedDiscountName)) {
                    savedDiscountName = rb_discount.getText().toString();
                    rb_discount.setChecked(true);
                    discountID = String.valueOf(rb_discount.getId());
                    discountName = rb_discount.getText().toString();
                }
            }
            rb_discount.setOnCheckedChangeListener((compoundButton, checked) -> {
                if (checked) {
                    savedDiscountName = rb_discount.getText().toString();
                    discountID = String.valueOf(rb_discount.getId());
                    discountName = rb_discount.getText().toString();
                }

            });
            rgDiscount.addView(rb_discount);

        }
    }


    void editProductCart(DetailCart selectedProduct, int qty, String
            note) {
        if (selectedProduct.getProductCode() == null || selectedProduct.getProductCode().equals("")) {
            Toast.makeText(getActivity(), "There is no product code", Toast.LENGTH_SHORT).show();
            return;
        }

        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("cart_product_id", TreatmentHelper.getInstance().getCart().getCartProductId() + "");
        formBuilder.add("product_id", selectedProduct.getProductId() + "");
        formBuilder.add("product_name", selectedProduct.getProductName());
        formBuilder.add("product_qty", qty + "");
        formBuilder.add("product_price", selectedProduct.getProductPrice() + "");
        formBuilder.add("discount_id", discountID);
        formBuilder.add("discount_name", discountName);
        formBuilder.add("doctor_id", doctor_id);
        formBuilder.add("doctor_name", doctor_name);
        formBuilder.add("product_note", note);
        formBuilder.add("category_id", selectedProduct.getCategoryId() + "");
        formBuilder.add("category_name", selectedProduct.getCategoryName() + "");
        formBuilder.add("product_code", selectedProduct.getProductCode() + "");

        RequestBody formBody = formBuilder.build();

        dialog = Utils.showProgressDialog(getActivity(), false, "Loading...");

        presenter.editProductCart(formBody);
        Connector.newInstance(getActivity()).addProductToCart(formBody, new Connector.ApiCallback<String>() {
            @Override
            public String getKey() {
                return TAG;
            }

            @Override
            public void success(String s, String messages) {
            }

            @Override
            public void onFailure(Throwable t) {
            }
        });
    }

    @Override
    public void OnSuccessApi(OnApiResultSuccess onApiResultSuccess) {
        Utils.dismissProgressDialog(dialog);
        if (onApiResultSuccess.getApi() == OnApiResultSuccess.API_EDIT_CART || onApiResultSuccess.getApi() == OnApiResultSuccess.API_ADD_CART) {
            EventBus.getDefault().post(new RefreshCart(RefreshCart.CART));
            EventBus.getDefault().post(new OnRefreshSearchView());
        } else if (onApiResultSuccess.getApi() == OnApiResultSuccess.API_EDIT_MEDICAL_CART || onApiResultSuccess.getApi() == OnApiResultSuccess.API_ADD_MEDICAL_CART) {
            EventBus.getDefault().post(new RefreshCart(RefreshCart.MEDICAL_CART));
            EventBus.getDefault().post(new OnRefreshSearchView());
        } else if (onApiResultSuccess.getApi() == OnApiResultSuccess.API_DELETE_CART) {
            Log.e("OnDeleteCart", "CART DELETED");
            EventBus.getDefault().post(new RefreshCart(RefreshCart.CART));
        } else if (onApiResultSuccess.getApi() == OnApiResultSuccess.API_DELETE_MEDICAL_CART) {
            EventBus.getDefault().post(new RefreshCart(RefreshCart.MEDICAL_CART));
        }
        dismiss();

    }


    @Override
    public void onFailGettingData(OnFailGettingData onFailGettingData) {
        if (onFailGettingData.getApi() == OnFailGettingData.EDIT_PRODUCT || onFailGettingData.getApi() == OnFailGettingData.ADD_PRODUCT) {
            Utils.dismissProgressDialog(dialog);
            EventBus.getDefault().post(new OnRefreshSearchView());
        } else if (onFailGettingData.getApi() == OnFailGettingData.DELETE_PRODUCT) {
            Utils.dismissProgressDialog(dialog);
        }
        Toast.makeText(getActivity(), onFailGettingData.getThrowable().getMessage(), Toast.LENGTH_SHORT).show();
        onFailGettingData.getThrowable().printStackTrace();


    }

    void editProductCartDoctor(DetailCart selectedProduct, int qty, String
            note) {

        if (selectedProduct.getProductCode() == null || selectedProduct.getProductCode().equals("")) {
            Toast.makeText(getActivity(), "There is no product code", Toast.LENGTH_SHORT).show();
            return;
        }


        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("product_id", selectedProduct.getProductId() + "");
        formBuilder.add("product_name", selectedProduct.getProductName());
        formBuilder.add("product_qty", qty + "");
        formBuilder.add("product_price", selectedProduct.getProductPrice() + "");
        formBuilder.add("discount_id", discountID);
        formBuilder.add("discount_name", discountName);
        formBuilder.add("doctor_id", doctor_id);
        formBuilder.add("doctor_name", doctor_name);
        formBuilder.add("product_note", note);
        formBuilder.add("category_id", selectedProduct.getCategoryId() + "");
        formBuilder.add("category_name", selectedProduct.getCategoryName() + "");
        formBuilder.add("product_code", selectedProduct.getProductCode() + "");
        RequestBody formBody = formBuilder.build();

        dialog = Utils.showProgressDialog(getActivity(), false, "Loading...");

        presenter.editProductMedicalCart(cart.getCartServiceId() + "", formBody);

    }


    private void deleteProductCart(String cart_id, String product_id) {

        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("cart_product_id", cart_id);
        formBuilder.add("product_id", product_id);
        RequestBody formBody = formBuilder.build();
        dialog = Utils.showProgressDialog(getActivity(), false, "Loading...");
        presenter.deleteProductCart(formBody);

    }

    private void deleteProductCartDoctor(String cart_id, String product_id) {

        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("cart_service_id", cart_id);
        formBuilder.add("product_id", product_id);
        RequestBody formBody = formBuilder.build();
        dialog = Utils.showProgressDialog(getActivity(), false, "Loading...");

        presenter.deleteProductCartDoctor(formBody, cart_id);
    }

    void addMedicalItemtoCart(ProductModel.DATABean.ItemsBean selectedProduct, int qty, String
            note) {

        if (selectedProduct.getProductCode() == null || selectedProduct.getProductCode().equals("")) {
            Toast.makeText(getActivity(), "There is no product code", Toast.LENGTH_SHORT).show();
            return;
        }

        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("product_id", selectedProduct.getProductId() + "");
        formBuilder.add("product_name", selectedProduct.getProductName() + "");
        formBuilder.add("product_price", selectedProduct.getProductPrice() + "");
        formBuilder.add("product_qty", qty + "");
        formBuilder.add("discount_id", discountID);
        formBuilder.add("discount_name", discountName);
        formBuilder.add("product_note", note);
        formBuilder.add("doctor_id", new SharedPreference(getActivity()).getStaffId());
        formBuilder.add("doctor_name", new SharedPreference(getActivity()).getStaffName());
        formBuilder.add("category_id", selectedProduct.getCategory().getCategoryId() + "");
        formBuilder.add("category_name", selectedProduct.getCategory().getCategoryName() + "");
        formBuilder.add("product_code", selectedProduct.getProductCode() + "");
        RequestBody formBody = formBuilder.build();

        dialog = Utils.showProgressDialog(getActivity(), false, "Loading...");

        presenter.addProductToMedicalCart(medicalCartServiceId, formBody);
    }

    void addProductCart(ProductModel.DATABean.ItemsBean selectedProduct, int qty, String
            note) {

        if (selectedProduct.getProductCode() == null || selectedProduct.getProductCode().equals("")) {
            Toast.makeText(getActivity(), "There is no product code", Toast.LENGTH_SHORT).show();
            return;
        }

        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("cart_product_id", TreatmentHelper.getInstance().getCart().getCartProductId() + "");
        formBuilder.add("product_id", selectedProduct.getProductId() + "");
        formBuilder.add("product_name", selectedProduct.getProductName());
        formBuilder.add("category_id", selectedProduct.getCategory().getCategoryId() + "");
        formBuilder.add("category_name", selectedProduct.getCategory().getCategoryName());
        formBuilder.add("product_qty", qty + "");
        formBuilder.add("product_price", selectedProduct.getProductPrice() + "");
        formBuilder.add("discount_id", discountID);
        formBuilder.add("discount_name", discountName);
        formBuilder.add("doctor_id", doctor_id);
        formBuilder.add("doctor_name", doctor_name);
        formBuilder.add("product_note", note);
        formBuilder.add("category_id", selectedProduct.getCategory().getCategoryId() + "");
        formBuilder.add("category_name", selectedProduct.getCategory().getCategoryName() + "");
        formBuilder.add("product_code", selectedProduct.getProductCode() + "");
        RequestBody formBody = formBuilder.build();

        dialog = Utils.showProgressDialog(getActivity(), false, "Loading...");

        presenter.addProductCart(formBody);
    }


}
