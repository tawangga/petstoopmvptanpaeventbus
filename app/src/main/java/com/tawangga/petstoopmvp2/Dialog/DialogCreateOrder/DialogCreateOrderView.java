package com.tawangga.petstoopmvp2.Dialog.DialogCreateOrder;

import com.tawangga.petstoopmvp2.EventBus.OnSuccessReservTreatment;
import com.tawangga.petstoopmvp2.Modul.BaseView;

public interface DialogCreateOrderView extends BaseView {
    void onSuccessReservTreatment(OnSuccessReservTreatment data);
}
