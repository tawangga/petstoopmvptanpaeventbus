package com.tawangga.petstoopmvp2.Dialog.DialogShop;

import android.content.Context;

import com.tawangga.petstoopmvp2.EventBus.OnApiResultSuccess;
import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import okhttp3.RequestBody;

public class DialogShopPresenter {
    private final Context context;
    private final String tag;
    private DialogShopView view;

    public DialogShopPresenter(Context context, String TAG, DialogShopView view) {

        this.context = context;
        tag = TAG;
        this.view = view;
    }

    public void editProductCart(RequestBody formBody) {
        Connector.newInstance(context).addProductToCart(formBody, new Connector.ApiCallback<String>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(String s, String messages) {
                view.OnSuccessApi(new OnApiResultSuccess(OnApiResultSuccess.API_EDIT_CART));
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.EDIT_PRODUCT, t));
            }
        });

    }

    public void editProductMedicalCart(String cartServiceId, RequestBody formBody) {
        Connector.newInstance(context).addMedicalItemToCart(cartServiceId, formBody, new Connector.ApiCallback<String>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(String s, String messages) {
                view.OnSuccessApi(new OnApiResultSuccess(OnApiResultSuccess.API_EDIT_MEDICAL_CART));

            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.EDIT_PRODUCT, t));
            }
        });
    }

    public void deleteProductCart(RequestBody formBody) {
        Connector.newInstance(context).deleteProductCart(formBody, new Connector.ApiCallback<String>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(String s, String messages) {
                view.OnSuccessApi(new OnApiResultSuccess(OnApiResultSuccess.API_DELETE_CART));
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.DELETE_PRODUCT, t));
            }
        });
    }

    public void deleteProductCartDoctor(RequestBody formBody, String cart_id) {
        Connector.newInstance(context).deleteProductCartDoctor(formBody, cart_id, new Connector.ApiCallback<String>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(String s, String messages) {
                view.OnSuccessApi(new OnApiResultSuccess(OnApiResultSuccess.API_DELETE_CART));
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.DELETE_PRODUCT, t));
            }
        });

    }

    public void addProductToMedicalCart(String medicalCartServiceId, RequestBody formBody) {
        Connector.newInstance(context).addMedicalItemToCart(medicalCartServiceId, formBody, new Connector.ApiCallback<String>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(String s, String messages) {
                view.OnSuccessApi(new OnApiResultSuccess(OnApiResultSuccess.API_ADD_MEDICAL_CART));
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.ADD_PRODUCT, t));

            }
        });

    }

    public void addProductCart(RequestBody formBody) {
        Connector.newInstance(context).addProductToCart(formBody, new Connector.ApiCallback<String>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(String s, String messages) {
                view.OnSuccessApi(new OnApiResultSuccess(OnApiResultSuccess.API_ADD_CART));
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.ADD_PRODUCT, t));
            }
        });

    }
}
