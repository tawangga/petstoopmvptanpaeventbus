package com.tawangga.petstoopmvp2.Dialog.DialogAddCCU;

import com.tawangga.petstoopmvp2.Model.GetExamModel;
import com.tawangga.petstoopmvp2.Modul.BaseView;

public interface DialogNewCCUView extends BaseView {
    void successGetExam(GetExamModel.DATABean data);
}
