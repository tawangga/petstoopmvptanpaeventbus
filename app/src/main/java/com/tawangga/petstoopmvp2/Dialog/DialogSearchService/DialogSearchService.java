package com.tawangga.petstoopmvp2.Dialog.DialogSearchService;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.Helper.GlobalVariable;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.ModelNew.CreateCart;
import com.tawangga.petstoopmvp2.ModelNew.GetGroomingService;
import com.tawangga.petstoopmvp2.ModelNew.GetMedicalService;
import com.tawangga.petstoopmvp2.Modul.AddNewTreatment.SelectedServiceAdapter;
import com.tawangga.petstoopmvp2.R;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogSearchService extends DialogFragment implements DialogSearchServiceView {
    private static final String TAG = DialogSearchService.class.getSimpleName();

    @BindView(R.id.txt_treatment_text)
    TextView txtTreatmentText;
    @BindView(R.id.txt_treatment_selected)
    TextView txtTreatmentSelected;
    @BindView(R.id.tv_search_treatment)
    EditText tvSearchTreatment;
    @BindView(R.id.btn_search)
    ImageView btnSearch;
    @BindView(R.id.btn_done)
    TextView btnDone;
    @BindView(R.id.txt_treatment_found)
    TextView txtTreatmentFound;
    @BindView(R.id.rv_selection_treatment)
    RecyclerView rvSelectionTreatment;
    @BindView(R.id.pb_loading)
    AVLoadingIndicatorView pbLoading;
    private int pageService = 0, lastPageService = 1;
    private SelectedServiceAdapter adapterSelectedService;
    private boolean isMedical;
    private CreateCart.DATABean cartData;
    private ServiceSelectionAdapter serviceAdapter;
    private DialogSearchServicePresenter presenter;

    public static DialogSearchService newInstance(SelectedServiceAdapter adapterSelectedService, boolean isMedical, CreateCart.DATABean cartData) {

        DialogSearchService fragment = new DialogSearchService();
        fragment.adapterSelectedService = adapterSelectedService;
        fragment.isMedical = isMedical;
        fragment.cartData = cartData;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = View.inflate(getActivity(), R.layout.layout_treatment_selection, null);
        ButterKnife.bind(this, v);
        presenter = new DialogSearchServicePresenter(getActivity(), TAG, this);
        initView();
        return v;
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        adapterSelectedService.notifyDataChanged();
    }

    private void initView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvSelectionTreatment.setLayoutManager(layoutManager);
        serviceAdapter = new ServiceSelectionAdapter(getActivity(), txtTreatmentSelected, cartData);
        rvSelectionTreatment.setAdapter(serviceAdapter);

        rvSelectionTreatment.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (pageService != lastPageService) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= serviceAdapter.getItemCount()) {
                        if (isMedical) {
                            getMedicalService();
                        } else {
                            getGroomingService();
                        }
                    }
                }
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        tvSearchTreatment.measure(0, 0);

        btnSearch.getLayoutParams().height = tvSearchTreatment.getMeasuredHeight();
        btnSearch.requestLayout();
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updatePagination(0, 1);
                serviceAdapter.clearAllData();
                if (isMedical) {
                    getMedicalService();

                } else {
                    getGroomingService();
                }
                Utils.hideSoftKeyboardUsingView(getActivity(), tvSearchTreatment);
            }
        });

        tvSearchTreatment.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                updatePagination(0, 1);
                serviceAdapter.clearAllData();
                if (isMedical) {
                    getMedicalService();

                } else {
                    getGroomingService();
                }
                Utils.hideSoftKeyboardUsingView(getActivity(), tvSearchTreatment);
                return false;
            }
        });


        if (isMedical) {
            getMedicalService();
        } else {
            getGroomingService();
        }

    }

    private void updatePagination(int page, int lastPage) {
        this.pageService = page;
        this.lastPageService = lastPage;
    }

    private void setLoadingCenter() {
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.CENTER_IN_PARENT);
        pbLoading.setLayoutParams(lp);

    }

    private void setLoadingBottom() {
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
        pbLoading.setLayoutParams(lp);

    }

    private void getGroomingService() {
        if ((pageService == lastPageService)) {
            pbLoading.setVisibility(View.GONE);
            return;
        }

        if (serviceAdapter.getDataGrooming().size() == 0) {
            setLoadingCenter();
        } else {
            setLoadingBottom();
        }
        pbLoading.setVisibility(View.VISIBLE);
        Map<String, String> params = new HashMap<>();
        params.put("limit", String.valueOf(GlobalVariable.LIMIT_ITEM));
        params.put("page", String.valueOf(pageService + 1));
        params.put("search", tvSearchTreatment.getText().toString().trim());

        presenter.getGroomingService(params);
    }

    @Override
    public void onFailGettingData(OnFailGettingData data) {
        pbLoading.setVisibility(View.GONE);
        data.getThrowable().printStackTrace();

    }

    @Override
    public void onSuccessGetGroomingService(GetGroomingService.DATABean data) {
        pbLoading.setVisibility(View.GONE);
        updatePagination(data.getPage(), data.getLastPage());
        for (GetGroomingService.DATABean.ItemsBean service : data.getItems()) {
            for (CreateCart.DATABean.ServiceBean selectedService : cartData.getServiceGrooming()) {
                if (service.getGroomingId() == selectedService.getServiceId()) {
                    service.setAdded(true);
                }
            }
        }
        serviceAdapter.setDataGrooming(data.getItems());

    }


    private void getMedicalService() {
        if ((pageService == lastPageService)) {
            pbLoading.setVisibility(View.GONE);
            return;
        }

        if (serviceAdapter.getDataMedical().size() == 0) {
            setLoadingCenter();
        } else {
            setLoadingBottom();
        }
        pbLoading.setVisibility(View.VISIBLE);
        Map<String, String> params = new HashMap<>();
        params.put("limit", String.valueOf(GlobalVariable.LIMIT_ITEM));
        params.put("page", String.valueOf(pageService + 1));
        params.put("search", tvSearchTreatment.getText().toString().trim());

        presenter.getMedicalService(params);

    }

    @Override
    public void onSuccessGetMedicalService(GetMedicalService.DATABean data) {
        pbLoading.setVisibility(View.GONE);
        updatePagination(data.getPage(), data.getLastPage());
        for (GetMedicalService.DATABean.ItemsBean service : data.getItems()) {
            for (CreateCart.DATABean.ServiceBean selectedService : cartData.getServiceMedical()) {
                if (service.getMedicalId() == selectedService.getServiceId()) {
                    service.setAdded(true);
                }
            }
        }
        serviceAdapter.setDataMedical(data.getItems());
    }

    @Override
    public void onResume() {
        super.onResume();
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
    }


}
