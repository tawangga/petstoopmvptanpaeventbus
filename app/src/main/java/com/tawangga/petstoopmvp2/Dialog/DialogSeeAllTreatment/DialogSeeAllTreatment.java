package com.tawangga.petstoopmvp2.Dialog.DialogSeeAllTreatment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.Helper.GlobalVariable;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Model.DetailCustomerModel;
import com.tawangga.petstoopmvp2.Model.GetOrderConfirmAnamnesis;
import com.tawangga.petstoopmvp2.ModelNew.GetBoardingTreatmentRecord;
import com.tawangga.petstoopmvp2.ModelNew.GetGroomingTreatmentRecord;
import com.tawangga.petstoopmvp2.ModelNew.GetMedicalTreatmentRecord;
import com.tawangga.petstoopmvp2.R;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class DialogSeeAllTreatment extends DialogFragment implements DialogSeeAllTreatmentView {

    private static final String TAG = DialogSeeAllTreatment.class.getSimpleName();
    @BindView(R.id.iv_pic_pet_record)
    CircleImageView ivPicPetRecord;
    @BindView(R.id.txt_owner_name)
    TextView txtOwnerName;
    @BindView(R.id.txt_pet_name)
    TextView txtPetName;
    @BindView(R.id.txt_record_count)
    TextView txtRecordCount;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.rv_medical_record)
    RecyclerView rvMedicalRecord;
    @BindView(R.id.pbLoadingMedicalRecord)
    AVLoadingIndicatorView pbLoadingMedicalRecord;
    @BindView(R.id.ll_medical_treatment)
    LinearLayout llMedicalTreatment;
    @BindView(R.id.rv_grooming_record)
    RecyclerView rvGroomingRecord;
    @BindView(R.id.pbLoadingGroomingRecord)
    AVLoadingIndicatorView pbLoadingGroomingRecord;
    @BindView(R.id.ll_grooming_treatment)
    LinearLayout llGroomingTreatment;
    @BindView(R.id.rv_boarding_record)
    RecyclerView rvBoardingRecord;
    @BindView(R.id.pbLoadingBoardingRecord)
    AVLoadingIndicatorView pbLoadingBoardingRecord;
    @BindView(R.id.ll_boarding_treatment)
    LinearLayout llBoardingTreatment;
    @BindView(R.id.ll_list)
    LinearLayout llList;
    @BindView(R.id.ed_anamnesis)
    EditText edAnamnesis;
    @BindView(R.id.ll_anamnesis)
    LinearLayout llAnamnesis;
    @BindView(R.id.rv_clinical_checkup)
    RecyclerView rvClinicalCheckup;
    @BindView(R.id.ll_clinical_checkup)
    LinearLayout llClinicalCheckup;
    @BindView(R.id.ed_diagnosis)
    EditText edDiagnosis;
    @BindView(R.id.ll_diagnosis)
    LinearLayout llDiagnosis;
    @BindView(R.id.rv_treatment)
    RecyclerView rvTreatment;
    @BindView(R.id.ll_treatment)
    LinearLayout llTreatment;
    @BindView(R.id.rv_medical_item)
    RecyclerView rvMedicalItem;
    @BindView(R.id.ll_medical_items)
    LinearLayout llMedicalItems;
    @BindView(R.id.ll_detail_treatment)
    LinearLayout llDetailTreatment;

    String[] tabItems = new String[]{"MEDICAL", "GROOMING", "BOARDING"};
    private int pageMedicalRecord = 0, lastPageMedicalRecord = 1;
    private int pageGroomingRecord = 0, lastPageGroomingRecord = 1;
    private int pageBoardingRecord = 0, lastPageBoardingRecord = 1;

    private DetailCustomerModel.DATABean.PetsBean pet;
    private DetailCustomerModel.DATABean customer;
    private DialogSeeAllTreatmentPresenter presenter;
    private BoardingRecordsAdapter boardingRecordsAdapter;
    private GroomingRecordsAdapter groomingRecordsAdapter;
    private MedicalRecordsAdapter medicalRecordsAdapter;
    private ProgressDialog progressDialog;

    public static DialogSeeAllTreatment newInstance(DetailCustomerModel.DATABean customer, DetailCustomerModel.DATABean.PetsBean pet) {


        DialogSeeAllTreatment fragment = new DialogSeeAllTreatment();
        fragment.pet = pet;
        fragment.customer = customer;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = View.inflate(getContext(), R.layout.dialog_medical_records, null);
        ButterKnife.bind(this, v);
        presenter = new DialogSeeAllTreatmentPresenter(getActivity(), TAG, this);
        initView();
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

    }

    private void initView() {
        Picasso.get().load(pet.getPetPhoto())
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .tag(TAG)
                .into(ivPicPetRecord, new Callback() {
                    @Override
                    public void onSuccess() {
                        ivPicPetRecord.setVisibility(View.VISIBLE);

                    }

                    @Override
                    public void onError(Exception e) {
                        ivPicPetRecord.setImageDrawable(getResources().getDrawable(R.drawable.no_image));
                        ivPicPetRecord.setVisibility(View.VISIBLE);
                        e.printStackTrace();
                    }
                });

        txtOwnerName.setText(customer.getCustomerName());
        txtPetName.setText(pet.getPetName());

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    showMedicalRecord();
                } else if (tab.getPosition() == 1) {
                    showGroomingRecord();
                } else {
                    showBoardingRecord();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        for (int i = 0; i < 3; i++) {
            tabLayout.addTab(tabLayout.newTab().setText(tabItems[i]));
        }

    }

    private void showBoardingRecord() {
        llGroomingTreatment.setVisibility(View.GONE);
        llMedicalTreatment.setVisibility(View.GONE);
        llBoardingTreatment.setVisibility(View.VISIBLE);

        pageBoardingRecord = 0;
        lastPageBoardingRecord = 1;

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvBoardingRecord.setLayoutManager(layoutManager);
        boardingRecordsAdapter = new BoardingRecordsAdapter(getActivity());
        rvBoardingRecord.setAdapter(boardingRecordsAdapter);

        rvBoardingRecord.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (pageBoardingRecord != pageBoardingRecord) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= boardingRecordsAdapter.getItemCount()) {
                        getBoardingRecord(pbLoadingBoardingRecord);
                    }
                }
            }
        });
        getBoardingRecord(pbLoadingBoardingRecord);
    }

    private void getBoardingRecord(AVLoadingIndicatorView pb_loading) {
        if ((pageBoardingRecord == lastPageBoardingRecord)) {
            pb_loading.setVisibility(View.GONE);
            return;
        }

        if (boardingRecordsAdapter.getData().size() == 0) {
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            lp.addRule(RelativeLayout.CENTER_IN_PARENT);
            pb_loading.setLayoutParams(lp);
        } else {
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
            pb_loading.setLayoutParams(lp);
        }
        pb_loading.setVisibility(View.VISIBLE);

        Map<String, String> params = new HashMap<>();
        params.put("limit", String.valueOf(GlobalVariable.LIMIT_ITEM));
        params.put("page", String.valueOf(pageBoardingRecord + 1));

        presenter.getBoardingRecord(pet.getPetId() + "", params);
    }

    @Override
    public void onSuccessBoardingRecord(GetBoardingTreatmentRecord.DATABean data) {
        pbLoadingBoardingRecord.setVisibility(View.GONE);
        pageBoardingRecord = data.getPage();
        lastPageBoardingRecord = data.getLastPage();
        if (data.getTotalItem() > 1) {
            txtRecordCount.setText(data.getTotalItem() + " Records");
        } else {
            txtRecordCount.setText(data.getTotalItem() + " Record");
        }
        boardingRecordsAdapter.setData(data.getItems());
    }

    private void showGroomingRecord() {
        llGroomingTreatment.setVisibility(View.VISIBLE);
        llMedicalTreatment.setVisibility(View.GONE);
        llBoardingTreatment.setVisibility(View.GONE);

        pageGroomingRecord = 0;
        lastPageGroomingRecord = 1;

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvGroomingRecord.setLayoutManager(layoutManager);
        groomingRecordsAdapter = new GroomingRecordsAdapter(getActivity());
        rvGroomingRecord.setAdapter(groomingRecordsAdapter);

        rvGroomingRecord.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (pageGroomingRecord != lastPageGroomingRecord) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= groomingRecordsAdapter.getItemCount()) {
                        getGroomingRecord(pbLoadingGroomingRecord);
                    }
                }
            }
        });
        getGroomingRecord(pbLoadingGroomingRecord);
    }

    private void getGroomingRecord(AVLoadingIndicatorView pb_loading) {
        if ((pageGroomingRecord == lastPageGroomingRecord)) {
            pb_loading.setVisibility(View.GONE);
            return;
        }

        if (groomingRecordsAdapter.getData().size() == 0) {
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            lp.addRule(RelativeLayout.CENTER_IN_PARENT);
            pb_loading.setLayoutParams(lp);
        } else {
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
            pb_loading.setLayoutParams(lp);
        }
        pb_loading.setVisibility(View.VISIBLE);
        Map<String, String> params = new HashMap<>();
        params.put("limit", String.valueOf(GlobalVariable.LIMIT_ITEM));
        params.put("page", String.valueOf(pageGroomingRecord + 1));

        presenter.getGroomingRecord(pet.getPetId() + "", params);
    }

    @Override
    public void onSuccessGroomingRecord(GetGroomingTreatmentRecord.DATABean dataBean) {
        pbLoadingGroomingRecord.setVisibility(View.GONE);
        pageGroomingRecord = dataBean.getPage();
        lastPageGroomingRecord = dataBean.getLastPage();
        if (dataBean.getTotalItem() > 1) {
            txtRecordCount.setText(dataBean.getTotalItem() + " Records");
        } else {
            txtRecordCount.setText(dataBean.getTotalItem() + " Record");
        }
        groomingRecordsAdapter.setData(dataBean.getItems());

    }

    private void showMedicalRecord() {
        llBoardingTreatment.setVisibility(View.GONE);
        llGroomingTreatment.setVisibility(View.GONE);
        llMedicalTreatment.setVisibility(View.VISIBLE);

        pageMedicalRecord = 0;
        lastPageMedicalRecord = 1;

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvMedicalRecord.setLayoutManager(layoutManager);
        medicalRecordsAdapter = new MedicalRecordsAdapter(getActivity());
        medicalRecordsAdapter.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showDetailTreatment(medicalRecordsAdapter.getDatas().get(position).getCartServiceId());
                llList.setVisibility(View.GONE);

            }
        });
        rvMedicalRecord.setAdapter(medicalRecordsAdapter);

        rvMedicalRecord.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (pageMedicalRecord != lastPageMedicalRecord) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= medicalRecordsAdapter.getItemCount()) {
                        getMedicalRecord(pbLoadingMedicalRecord, txtRecordCount);
                    }
                }
            }
        });
        getMedicalRecord(pbLoadingMedicalRecord, txtRecordCount);
    }

    private void getMedicalRecord(AVLoadingIndicatorView pb_loading, TextView txt_record_count) {
        if ((pageMedicalRecord == lastPageMedicalRecord)) {
            pb_loading.setVisibility(View.GONE);
            return;
        }

        if (medicalRecordsAdapter.getDatas().size() == 0) {
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            lp.addRule(RelativeLayout.CENTER_IN_PARENT);
            pb_loading.setLayoutParams(lp);
        } else {
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
            pb_loading.setLayoutParams(lp);
        }
        pb_loading.setVisibility(View.VISIBLE);
        Map<String, String> params = new HashMap<>();
        params.put("limit", String.valueOf(GlobalVariable.LIMIT_ITEM));
        params.put("page", String.valueOf(pageMedicalRecord + 1));

        presenter.getMedicalRecord(pet.getPetId() + "", params);

    }

    @Override
    public void onSuccessMedicalRecord(GetMedicalTreatmentRecord.DATABean data) {
        pbLoadingMedicalRecord.setVisibility(View.GONE);
        pageMedicalRecord = data.getPage();
        lastPageMedicalRecord = data.getLastPage();

        if (data.getTotalItem() > 1) {
            txtRecordCount.setText(data.getTotalItem() + " Records");
        } else {
            txtRecordCount.setText(data.getTotalItem() + " Record");
        }
        medicalRecordsAdapter.setDatas(data.getItems());

    }


    private void showDetailTreatment(int cartServiceId) {
        llDetailTreatment.setVisibility(View.VISIBLE);
        progressDialog = Utils.showProgressDialog(getActivity(), false, "Loading...");
        presenter.getDetailAnamnesis(cartServiceId + "");
    }

    @Override
    public void onSuccessDetailAnamnesis(GetOrderConfirmAnamnesis.DATABean dataBean) {
        progressDialog.dismiss();

        rvTreatment.setLayoutManager(new LinearLayoutManager(getActivity()));
        TreatmentKonsultasiDokterAdapter konsultasiDokterAdapter = new TreatmentKonsultasiDokterAdapter(getActivity());
        konsultasiDokterAdapter.setDatas(dataBean.getTreatments());
        rvTreatment.setAdapter(konsultasiDokterAdapter);

        rvMedicalItem.setLayoutManager(new LinearLayoutManager(getActivity()));
        AdapterMedicalItems adapterMedicalItems = new AdapterMedicalItems(getActivity(), true);
        adapterMedicalItems.setDatas(dataBean.getMedicalItems());
        rvMedicalItem.setAdapter(adapterMedicalItems);

        rvClinicalCheckup.setLayoutManager(new LinearLayoutManager(getActivity()));
        AdapterClinicalCheckUp2 adapterClinicalCheckUp = new AdapterClinicalCheckUp2(getActivity());
        adapterClinicalCheckUp.setDatas(dataBean.getClinicalCheckups());
        rvClinicalCheckup.setAdapter(adapterClinicalCheckUp);


        edAnamnesis.setText(dataBean.getDiagnosis());
        edDiagnosis.setText(dataBean.getAnamnesis());

    }

    @Override
    public void onFailGettingData(OnFailGettingData data) {
        if (data.getApi() == OnFailGettingData.DETAIL_ANAMNESIS) {
            progressDialog.dismiss();
        }
        if (data.getApi() == OnFailGettingData.MEDICAL_RECORD) {
            pbLoadingMedicalRecord.setVisibility(View.GONE);
        }
        if (data.getApi() == OnFailGettingData.GROOMING_RECORD) {
            pbLoadingGroomingRecord.setVisibility(View.GONE);
        }
        if (data.getApi() == OnFailGettingData.BOARDING_RECORD) {
            pbLoadingBoardingRecord.setVisibility(View.GONE);
        }
        Toast.makeText(getActivity(), data.getThrowable().getMessage(), Toast.LENGTH_SHORT).show();
        data.getThrowable().printStackTrace();
    }
}
