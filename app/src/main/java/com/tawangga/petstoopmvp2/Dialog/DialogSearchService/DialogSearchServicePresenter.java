package com.tawangga.petstoopmvp2.Dialog.DialogSearchService;

import android.content.Context;

import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.ModelNew.GetGroomingService;
import com.tawangga.petstoopmvp2.ModelNew.GetMedicalService;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import java.util.Map;

public class DialogSearchServicePresenter {
    private final Context context;
    private final String tag;
    private DialogSearchServiceView view;

    public DialogSearchServicePresenter(Context context, String TAG, DialogSearchServiceView view) {

        this.context = context;
        tag = TAG;
        this.view = view;
    }

    public void getGroomingService(Map<String, String> params) {
        Connector.newInstance(context).getGroomingService(params, new Connector.ApiCallback<GetGroomingService.DATABean>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(GetGroomingService.DATABean dataBean, String messages) {
                view.onSuccessGetGroomingService(dataBean);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.SEARCH_SERVICE, t));
            }
        });

    }

    public void getMedicalService(Map<String, String> params) {
        Connector.newInstance(context).getMedicalService(params, new Connector.ApiCallback<GetMedicalService.DATABean>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(GetMedicalService.DATABean dataBean, String messages) {
                view.onSuccessGetMedicalService(dataBean);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.SEARCH_SERVICE, t));

            }
        });

    }
}
