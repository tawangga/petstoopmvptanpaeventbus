package com.tawangga.petstoopmvp2.Dialog.DialogAddCCU;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Model.GetExamModel;
import com.tawangga.petstoopmvp2.Model.GetOrderConfirmAnamnesis;
import com.tawangga.petstoopmvp2.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExamAdapter extends RecyclerView.Adapter<ExamAdapter.ViewHolder> {
    private final Context context;
    private final TextView txt_treatment_found;
    private GetOrderConfirmAnamnesis.DATABean dataAnamnesis;
    private List<GetExamModel.DATABean.ItemsBean> data;

    public ExamAdapter(Context context, TextView txt_treatment_found, GetOrderConfirmAnamnesis.DATABean dataAnamnesis) {

        this.context = context;
        this.txt_treatment_found = txt_treatment_found;
        this.dataAnamnesis = dataAnamnesis;
        data = new ArrayList<>();

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_treatment_selection, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
//        if (position % 2 == 1) {
//            holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.gray_light));
//        } else {
//            holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.white));
//        }

        GetExamModel.DATABean.ItemsBean selectedData = data.get(position);
        holder.txtTreatmentName.setText(selectedData.getExamName());
        holder.txtTreatmentPrice.setText(Utils.toRupiah2(selectedData.getExamPrice() + ""));

        if (selectedData.isAdded()) {
            holder.tvAdd.setText("Added");
            holder.tvAdd.setTextColor(context.getResources().getColor(R.color.brown_light));
            holder.tvAdd.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));

        } else {
            holder.tvAdd.setText("+");
            holder.tvAdd.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvAdd.setBackgroundColor(context.getResources().getColor(R.color.brown_light));

            holder.tvAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedData.setAdded(true);
                    holder.tvAdd.setText("Added");
                    holder.tvAdd.setTextColor(context.getResources().getColor(R.color.brown_light));
                    holder.tvAdd.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));

                    GetOrderConfirmAnamnesis.DATABean.ClinicalCheckupsBean newExam = new GetOrderConfirmAnamnesis.DATABean.ClinicalCheckupsBean();
                    newExam.setExam(true);
                    newExam.setClinicalCheckupId(selectedData.getExamId());
                    newExam.setClinicalCheckupName(selectedData.getExamName());
                    newExam.setClinicalCheckupUnit(selectedData.getExamUnit());
                    newExam.setClinicalCheckupIsPriority(0);
                    newExam.setClinicalCheckupType("1");
                    newExam.setValue("0");
                    dataAnamnesis.getClinicalCheckups().add(newExam);
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<GetExamModel.DATABean.ItemsBean> data) {

        this.data.addAll(data);
        notifyDataSetChanged();
    }

    public List<GetExamModel.DATABean.ItemsBean> getData() {
        return data;
    }

    public void clearAllData() {
        data.clear();
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_treatment_name)
        TextView txtTreatmentName;
        @BindView(R.id.txt_treatment_price)
        TextView txtTreatmentPrice;
        @BindView(R.id.tv_add)
        TextView tvAdd;
        @BindView(R.id.ll_background)
        RelativeLayout llBackground;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

}
