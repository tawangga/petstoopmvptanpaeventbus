package com.tawangga.petstoopmvp2.Dialog.DialogPilihCustomer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.tawangga.petstoopmvp2.Model.ProcessInvoice;
import com.tawangga.petstoopmvp2.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.ViewHolder> {
    private Context context;
    private List<ProcessInvoice.DATABean.CustomersBean> data;
    private AdapterView.OnItemClickListener onItemClickListener;

    public UserListAdapter(Context context) {

        this.context = context;
    }

    @NonNull
    @Override
    public UserListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_user_list, parent, false);

        return new UserListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull UserListAdapter.ViewHolder holder, int position) {
        holder.position = position;
        final long identity = System.currentTimeMillis();
        holder.identity = identity;

        ProcessInvoice.DATABean.CustomersBean model = data.get(position);
        Glide.with(context).load(model.getCustomerPhoto()).into(holder.iv_customer);
        holder.tv_OwnerName.setText(model.getCustomerName());
        if (model.isDeposit()) {
            holder.tv_OwnerDeposit.setVisibility(View.VISIBLE);
        } else holder.tv_OwnerDeposit.setVisibility(View.GONE);


    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setDatas(List<ProcessInvoice.DATABean.CustomersBean> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        int position;
        long identity;
        @BindView(R.id.iv_customer)
        CircleImageView iv_customer;
        @BindView(R.id.txt_owner_name)
        TextView tv_OwnerName;
        @BindView(R.id.txt_owner_deposit)
        TextView tv_OwnerDeposit;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(null, v, position, 0);
            }

        }
    }
}