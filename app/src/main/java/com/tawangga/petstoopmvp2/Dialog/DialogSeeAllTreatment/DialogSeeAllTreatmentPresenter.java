package com.tawangga.petstoopmvp2.Dialog.DialogSeeAllTreatment;

import android.content.Context;

import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.Model.GetOrderConfirmAnamnesis;
import com.tawangga.petstoopmvp2.ModelNew.GetBoardingTreatmentRecord;
import com.tawangga.petstoopmvp2.ModelNew.GetGroomingTreatmentRecord;
import com.tawangga.petstoopmvp2.ModelNew.GetMedicalTreatmentRecord;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import java.util.Map;

import static com.tawangga.petstoopmvp2.EventBus.OnFailGettingData.BOARDING_RECORD;

public class DialogSeeAllTreatmentPresenter {

    private Context context;
    private String TAG;
    private DialogSeeAllTreatmentView view;

    public DialogSeeAllTreatmentPresenter(Context context, String TAG, DialogSeeAllTreatmentView view) {

        this.context = context;
        this.TAG = TAG;
        this.view = view;
    }

    public void getBoardingRecord(String petId, Map<String, String> params) {
        Connector.newInstance(context).getPetBoardingTreatmentRecord(String.valueOf(petId), params, new Connector.ApiCallback<GetBoardingTreatmentRecord.DATABean>() {
            @Override
            public String getKey() {
                return TAG;
            }

            @Override
            public void success(GetBoardingTreatmentRecord.DATABean dataBean, String messages) {
                view.onSuccessBoardingRecord(dataBean);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(BOARDING_RECORD, t));
            }
        });

    }

    public void getGroomingRecord(String petId, Map<String, String> params) {
        Connector.newInstance(context).getPetGroomingTreatmentRecord(petId, params, new Connector.ApiCallback<GetGroomingTreatmentRecord.DATABean>() {
            @Override
            public String getKey() {
                return TAG;
            }

            @Override
            public void success(GetGroomingTreatmentRecord.DATABean dataBean, String messages) {
                view.onSuccessGroomingRecord(dataBean);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.GROOMING_RECORD, t));
            }
        });

    }

    public void getMedicalRecord(String petId, Map<String, String> params) {
        Connector.newInstance(context).getPetMedicalTreatmentRecord(petId, params, new Connector.ApiCallback<GetMedicalTreatmentRecord.DATABean>() {
            @Override
            public String getKey() {
                return TAG;
            }

            @Override
            public void success(GetMedicalTreatmentRecord.DATABean dataBean, String messages) {
                view.onSuccessMedicalRecord(dataBean);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.MEDICAL_RECORD, t));
            }
        });

    }

    public void getDetailAnamnesis(String cartServiceId) {
        Connector.newInstance(context).getOrderConfirmAnamnesis(String.valueOf(cartServiceId), 1, new Connector.ApiCallback<GetOrderConfirmAnamnesis.DATABean>() {
            @Override
            public String getKey() {
                return TAG;
            }

            @Override
            public void success(GetOrderConfirmAnamnesis.DATABean dataBean, String messages) {
                view.onSuccessDetailAnamnesis(dataBean);
            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.DETAIL_ANAMNESIS, t));
            }
        });

    }
}
