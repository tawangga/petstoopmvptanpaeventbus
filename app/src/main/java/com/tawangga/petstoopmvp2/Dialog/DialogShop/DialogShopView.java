package com.tawangga.petstoopmvp2.Dialog.DialogShop;

import com.tawangga.petstoopmvp2.EventBus.OnApiResultSuccess;
import com.tawangga.petstoopmvp2.Modul.BaseView;

public interface DialogShopView extends BaseView {
    void OnSuccessApi(OnApiResultSuccess onApiResultSuccess);
}
