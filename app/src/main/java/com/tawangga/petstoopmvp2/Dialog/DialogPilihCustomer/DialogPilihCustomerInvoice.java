package com.tawangga.petstoopmvp2.Dialog.DialogPilihCustomer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.Model.DataInvoice;
import com.tawangga.petstoopmvp2.Model.ProcessInvoice;
import com.tawangga.petstoopmvp2.Modul.Main.MainActivity;
import com.tawangga.petstoopmvp2.R;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogPilihCustomerInvoice extends DialogFragment {

    @BindView(R.id.tvDetectUser)
    TextView tvDetectUser;
    @BindView(R.id.rv_user_list)
    RecyclerView rvUserList;
    private List<ProcessInvoice.DATABean.CustomersBean> data;
    private boolean dp;
    private DataInvoice dataInvoice;

    public static DialogPilihCustomerInvoice newInstance(List<ProcessInvoice.DATABean.CustomersBean> data, boolean dp, DataInvoice dataInvoice) {


        DialogPilihCustomerInvoice fragment = new DialogPilihCustomerInvoice();
        fragment.data = data;
        fragment.dp = dp;
        fragment.dataInvoice = dataInvoice;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = View.inflate(getActivity(), R.layout.layout_detect_user, null);
        ButterKnife.bind(this, v);
        initView();
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

    }

    private void initView() {
        tvDetectUser.setText("we detected that were " + data.size() + " users in this invoice");
        rvUserList.setLayoutManager(new LinearLayoutManager(getContext()));
        UserListAdapter userListAdapter = new UserListAdapter(getContext());
        userListAdapter.setDatas(data);
        rvUserList.setAdapter(userListAdapter);

        userListAdapter.setOnItemClickListener((adapterView, view, position, l) -> {
            Bundle bundle = new Bundle();
            bundle.putString("customer_id", String.valueOf(data.get(position).getCustomerId()));
            bundle.putParcelable("invoice", dataInvoice);
            if (dp) {
                dismiss();
                ((MainActivity) Objects.requireNonNull(getContext())).toInvoiceDetailPayment(true, bundle);

            } else {
                dismiss();
                ((MainActivity) Objects.requireNonNull(getContext())).toInvoiceDetailPayment(false, bundle);

            }
        });
    }


}
