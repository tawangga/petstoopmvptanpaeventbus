package com.tawangga.petstoopmvp2.Dialog.DialogSeeAllTreatment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.Helper.GlobalVariable;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.ModelNew.GetGroomingTreatmentRecord;
import com.tawangga.petstoopmvp2.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GroomingRecordsAdapter extends RecyclerView.Adapter<GroomingRecordsAdapter.ViewHolder> {
    private List<GetGroomingTreatmentRecord.DATABean.ItemsBean> data;
    private Context context;

    public GroomingRecordsAdapter(Context context) {
        this.context = context;
        this.data = new ArrayList<>();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_grooming_records, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if (position % 2 == 1) {
            holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.brown_light_2));
        } else {
            holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.white));
        }
        GetGroomingTreatmentRecord.DATABean.ItemsBean selectedData = data.get(position);
        holder.txtOrderCode.setText(selectedData.getOrderCode());
        holder.txtOrderDate.setText(Utils.changeDateFormat(selectedData.getOrderDate(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME_FULL));
        if (selectedData.getGroomingLocationSchedule() != null)
            holder.txtDoctorName.setText(selectedData.getGroomingLocationSchedule().getLocationName());

        holder.txtTreatment.setText(selectedData.getServiceText());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public List<GetGroomingTreatmentRecord.DATABean.ItemsBean> getData() {
        return data;
    }

    public void setData(List<GetGroomingTreatmentRecord.DATABean.ItemsBean> data) {
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_order_code)
        TextView txtOrderCode;
        @BindView(R.id.txt_order_date)
        TextView txtOrderDate;
        @BindView(R.id.txt_doctor_name)
        TextView txtDoctorName;
        @BindView(R.id.txt_treatment)
        TextView txtTreatment;
        @BindView(R.id.ll_background)
        LinearLayout llBackground;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
