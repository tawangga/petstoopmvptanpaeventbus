package com.tawangga.petstoopmvp2.Dialog.DialogAddCCU;

import android.content.Context;

import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.Model.GetExamModel;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import java.util.Map;

public class DialogAddNewCCUPresenter {
    private final Context context;
    private final String tag;
    private DialogNewCCUView view;

    public DialogAddNewCCUPresenter(Context context, String TAG, DialogNewCCUView view) {

        this.context = context;
        tag = TAG;
        this.view = view;
    }

    public void getExam(Map<String, String> params) {
        Connector.newInstance(context).getExam(params, new Connector.ApiCallback<GetExamModel.DATABean>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(GetExamModel.DATABean dataBean, String messages) {
                view.successGetExam(dataBean);
            }

            @Override
            public void onFailure(Throwable t) {

                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.GET_EXAM, t));
            }
        });

    }
}
