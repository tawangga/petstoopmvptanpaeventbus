package com.tawangga.petstoopmvp2.Dialog.DialogInvoiceDP;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.Model.DataInvoice;
import com.tawangga.petstoopmvp2.Modul.Main.MainActivity;
import com.tawangga.petstoopmvp2.R;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogInvoiceDP extends DialogFragment {

    @BindView(R.id.invoices)
    TextView invoices;
    @BindView(R.id.rv_open_selected_invoice)
    RecyclerView rvOpenSelectedInvoice;
    private List<DataInvoice> data;
    private RecyclerView rvInvoice;

    public static DialogInvoiceDP newInstance(List<DataInvoice> data, RecyclerView rvInvoice) {
        DialogInvoiceDP fragment = new DialogInvoiceDP();
        fragment.data = data;
        fragment.rvInvoice = rvInvoice;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = View.inflate(getActivity(), R.layout.layout_open_invoice, null);
        ButterKnife.bind(this, v);
        initView();
        return v;
    }

    private void initView() {
        if (data.size() > 1) {
            invoices.setText("(" + data.size() + " Invoices Found)");
        } else {
            invoices.setText("(" + data.size() + " Invoice Found)");
        }

        OpenSelectedInvoiceAdapter adapter = new OpenSelectedInvoiceAdapter(getContext());
        adapter.setDatas(data);
        rvOpenSelectedInvoice.setLayoutManager(new LinearLayoutManager(getContext()));


        rvOpenSelectedInvoice.setAdapter(adapter);
        adapter.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                ((MainActivity) Objects.requireNonNull(getContext())).bundle.putParcelable("dataInvoice", data.get(pos));
                ((MainActivity) Objects.requireNonNull(getContext())).position = 3;
                ((MainActivity) Objects.requireNonNull(getContext())).changePosition();
                rvInvoice.setVisibility(View.GONE);
                dismiss();
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

    }

}
