package com.tawangga.petstoopmvp2.Dialog.DialogSearchServiceDoctor;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.Dialog.DialogSearchService.DialogSearchServicePresenter;
import com.tawangga.petstoopmvp2.Dialog.DialogSearchService.DialogSearchServiceView;
import com.tawangga.petstoopmvp2.Dialog.DialogSearchService.ServiceSelectionAdapter;
import com.tawangga.petstoopmvp2.Dialog.DialogSeeAllTreatment.TreatmentKonsultasiDokterAdapter;
import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.Helper.GlobalVariable;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Model.GetOrderConfirmAnamnesis;
import com.tawangga.petstoopmvp2.ModelNew.GetGroomingService;
import com.tawangga.petstoopmvp2.ModelNew.GetMedicalService;
import com.tawangga.petstoopmvp2.R;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogSearchServiceDoctor extends DialogFragment implements DialogSearchServiceView {
    private static final String TAG = DialogSearchServiceDoctor.class.getSimpleName();
    private DialogSearchServicePresenter presenter;
    @BindView(R.id.txt_treatment_text)
    TextView txtTreatmentText;
    @BindView(R.id.txt_treatment_selected)
    TextView txtTreatmentSelected;
    @BindView(R.id.tv_search_treatment)
    EditText tvSearchTreatment;
    @BindView(R.id.btn_search)
    ImageView btnSearch;
    @BindView(R.id.btn_done)
    TextView btnDone;
    @BindView(R.id.txt_treatment_found)
    TextView txtTreatmentFound;
    @BindView(R.id.rv_selection_treatment)
    RecyclerView rvSelectionTreatment;
    @BindView(R.id.pb_loading)
    AVLoadingIndicatorView pbLoading;
    private int pageService = 0, lastPageService = 1;
    private List<GetOrderConfirmAnamnesis.DATABean.TreatmentsBean> treatments;
    private GetOrderConfirmAnamnesis.DATABean dataAnamnesis = null;
    private TreatmentKonsultasiDokterAdapter konsultasiDokterAdapter;
    ServiceSelectionAdapter serviceAdapter;

    public static DialogSearchServiceDoctor newInstance(List<GetOrderConfirmAnamnesis.DATABean.TreatmentsBean> treatments, GetOrderConfirmAnamnesis.DATABean dataAnamnesis, TreatmentKonsultasiDokterAdapter konsultasiDokterAdapter) {
        DialogSearchServiceDoctor fragment = new DialogSearchServiceDoctor();
        fragment.treatments = treatments;
        fragment.dataAnamnesis = dataAnamnesis;
        fragment.konsultasiDokterAdapter = konsultasiDokterAdapter;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = View.inflate(getActivity(), R.layout.layout_treatment_selection, null);
        ButterKnife.bind(this, v);
        presenter = new DialogSearchServicePresenter(getActivity(), TAG, this);
        initView();
        return v;
    }

    private void initView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvSelectionTreatment.setLayoutManager(layoutManager);
        serviceAdapter = new ServiceSelectionAdapter(getActivity(), txtTreatmentSelected, dataAnamnesis);
        rvSelectionTreatment.setAdapter(serviceAdapter);

        rvSelectionTreatment.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (pageService != lastPageService) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= serviceAdapter.getItemCount()) {
                        getMedicalService();
                    }
                }
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        tvSearchTreatment.measure(0, 0);

        btnSearch.getLayoutParams().height = tvSearchTreatment.getMeasuredHeight();
        btnSearch.requestLayout();
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updatePagination(0, 1);
                serviceAdapter.clearAllData();
                getMedicalService();
                Utils.hideSoftKeyboardUsingView(getActivity(), tvSearchTreatment);
            }
        });

        tvSearchTreatment.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                updatePagination(0, 1);
                serviceAdapter.clearAllData();
                getMedicalService();
                Utils.hideSoftKeyboardUsingView(getActivity(), tvSearchTreatment);
                return false;
            }
        });

        getMedicalService();

    }

    private void getMedicalService() {
        if (pageService == lastPageService) {
            pbLoading.setVisibility(View.GONE);
            return;
        }

        if (serviceAdapter.getDataMedical().size() == 0) {
            setLoadingCenter();
        } else {
            setLoadingBottom();
        }
        pbLoading.setVisibility(View.VISIBLE);
        Map<String, String> params = new HashMap<>();
        params.put("limit", String.valueOf(GlobalVariable.LIMIT_ITEM));
        params.put("page", String.valueOf(pageService + 1));
        params.put("search", tvSearchTreatment.getText().toString().trim());

        presenter.getMedicalService(params);


    }

    @Override
    public void onSuccessGetMedicalService(GetMedicalService.DATABean data) {
        pbLoading.setVisibility(View.GONE);
        updatePagination(data.getPage(), data.getLastPage());
        for (GetMedicalService.DATABean.ItemsBean service : data.getItems()) {
            for (GetOrderConfirmAnamnesis.DATABean.TreatmentsBean selectedService : dataAnamnesis.getTreatments()) {
                if (selectedService.getServiceType().equals("1")) {
                    if (service.getMedicalId() == selectedService.getServiceId()) {
                        service.setAdded(true);
                    }
                }
            }
        }

        serviceAdapter.setDataMedical(data.getItems());
    }

    @Override
    public void onSuccessGetGroomingService(GetGroomingService.DATABean data) {
// TODO: 19/09/20 nothing here
    }

    @Override
    public void onFailGettingData(OnFailGettingData data) {
        pbLoading.setVisibility(View.GONE);
        data.getThrowable().printStackTrace();
    }

    private void setLoadingCenter() {
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.CENTER_IN_PARENT);
        pbLoading.setLayoutParams(lp);

    }

    @Override
    public void onResume() {
        super.onResume();
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
    }

    private void setLoadingBottom() {
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
        pbLoading.setLayoutParams(lp);

    }

    private void updatePagination(int page, int lastPage) {
        this.pageService = page;
        this.lastPageService = lastPage;
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        treatments.clear();
        for (GetOrderConfirmAnamnesis.DATABean.TreatmentsBean allTreatment : dataAnamnesis.getTreatments()) {
            if (allTreatment.getServiceType().equals("1")) {
                treatments.add(allTreatment);
            }
        }
        konsultasiDokterAdapter.setDatas(treatments);

    }
}
