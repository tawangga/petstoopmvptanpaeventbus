package com.tawangga.petstoopmvp2.Dialog.DialogSeeAllTreatment;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Model.GetOrderConfirmAnamnesis;
import com.tawangga.petstoopmvp2.R;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class AdapterMedicalItems extends RecyclerView.Adapter<AdapterMedicalItems.ViewHolder> {

    private List<GetOrderConfirmAnamnesis.DATABean.MedicalItemsBean> data;
    private Context context;
    private boolean isMedicalRecord;
    private AdapterView.OnItemClickListener onItemClickListener;

    public AdapterMedicalItems(Context context, boolean isMedicalRecord) {

        this.context = context;
        this.isMedicalRecord = isMedicalRecord;
        data = new ArrayList<>();
    }


    @NonNull
    @Override
    public AdapterMedicalItems.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_medical_items, parent, false);

        return new AdapterMedicalItems.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterMedicalItems.ViewHolder holder, int position) {
        holder.position = position;
        final long identity = System.currentTimeMillis();
        holder.identity = identity;
        GetOrderConfirmAnamnesis.DATABean.MedicalItemsBean selectedMedicalItem = data.get(position);
        holder.txt_item_name.setText(selectedMedicalItem.getProductName());
        holder.txt_qty.setText(selectedMedicalItem.getProductQty() + "");

        if (isMedicalRecord) {
            holder.btn_cancel.setVisibility(View.GONE);
        }

        holder.btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                deleteMedicalItems(data.get(position));

            }
        });


    }

    private void deleteMedicalItems(GetOrderConfirmAnamnesis.DATABean.MedicalItemsBean datas) {
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("product_id", datas.getProductId() + "");

        RequestBody formBody = formBuilder.build();
        ProgressDialog dialog = Utils.showProgressDialog(context, false, "Loading...");
        Connector.newInstance(context).deleteMedicalItems(formBody, String.valueOf(datas.getCartServiceId()), new Connector.ApiCallback<String>() {
            @Override
            public String getKey() {
                return null;
            }

            @Override
            public void success(String s, String messages) {
                data.remove(datas);
                notifyDataSetChanged();
                dialog.dismiss();

            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                t.printStackTrace();

            }
        });

    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setDatas(List<GetOrderConfirmAnamnesis.DATABean.MedicalItemsBean> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.ll_background)
        LinearLayout llBackground;
        @BindView(R.id.txt_item_name)
        TextView txt_item_name;
        @BindView(R.id.txt_qty)
        TextView txt_qty;
        @BindView(R.id.btn_cancel)
        ImageView btn_cancel;
        int position;
        long identity;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(null, v, position, 0);
            }

        }
    }
}