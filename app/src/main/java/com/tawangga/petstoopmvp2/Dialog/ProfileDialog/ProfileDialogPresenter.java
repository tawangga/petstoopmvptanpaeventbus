package com.tawangga.petstoopmvp2.Dialog.ProfileDialog;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.tawangga.petstoopmvp2.Helper.SharedPreference;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Modul.LoginCustomer.SignInCustomerActivity;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import okhttp3.FormBody;
import okhttp3.RequestBody;

public class ProfileDialogPresenter {

    private Context context;

    public ProfileDialogPresenter(Context context) {

        this.context = context;
    }

    public void cancelInvoice() {
        if (!new SharedPreference(context).isCustomer()) {
            FormBody.Builder formBuilder = new FormBody.Builder();
            formBuilder.add("transaction_id", "");
            RequestBody formBody = formBuilder.build();
            ProgressDialog dialog = Utils.showProgressDialog(context, false, "Loading...");
            Connector.newInstance(context).cancelInvoice(formBody, new Connector.ApiCallback<String>() {
                @Override
                public String getKey() {
                    return null;
                }

                @Override
                public void success(String s, String messages) {
                    Utils.dismissProgressDialog(dialog);
                    new SharedPreference(context).clearSession();
                    Intent intent = new Intent(context, SignInCustomerActivity.class);
                    context.startActivity(intent);
                    ((Activity) context).finish();

                }

                @Override
                public void onFailure(Throwable t) {
                    Utils.dismissProgressDialog(dialog);
                    Log.e("error_cancelInvoice", t.getMessage());
                }
            });
        }

    }

}
