package com.tawangga.petstoopmvp2.Dialog.DialogCreateOrder;

import android.content.Context;

import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.EventBus.OnSuccessReservTreatment;
import com.tawangga.petstoopmvp2.ModelNew.CreateCart;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import okhttp3.RequestBody;

public class DialogCreateOrderPresenter {
    private final Context context;
    private final String tag;
    private DialogCreateOrderView view;

    public DialogCreateOrderPresenter(Context context, String TAG, DialogCreateOrderView view) {

        this.context = context;
        tag = TAG;
        this.view = view;
    }

    public void reservTreatment(RequestBody formBody) {
        Connector.newInstance(context).reservTreatment(formBody, new Connector.ApiCallback<CreateCart.DATABean>() {
            @Override
            public String getKey() {
                return tag;
            }

            @Override
            public void success(CreateCart.DATABean dataBean, String messages) {
                view.onSuccessReservTreatment(new OnSuccessReservTreatment());

            }

            @Override
            public void onFailure(Throwable t) {
                view.onFailGettingData(new OnFailGettingData(OnFailGettingData.RESERV_TREATMENT, t));
            }
        });

    }
}
