package com.tawangga.petstoopmvp2.Dialog.DialogSeeAllTreatment;

import com.tawangga.petstoopmvp2.Model.GetOrderConfirmAnamnesis;
import com.tawangga.petstoopmvp2.ModelNew.GetBoardingTreatmentRecord;
import com.tawangga.petstoopmvp2.ModelNew.GetGroomingTreatmentRecord;
import com.tawangga.petstoopmvp2.ModelNew.GetMedicalTreatmentRecord;
import com.tawangga.petstoopmvp2.Modul.BaseView;

public interface DialogSeeAllTreatmentView extends BaseView {
    void onSuccessBoardingRecord(GetBoardingTreatmentRecord.DATABean data);

    void onSuccessGroomingRecord(GetGroomingTreatmentRecord.DATABean dataBean);

    void onSuccessMedicalRecord(GetMedicalTreatmentRecord.DATABean data);

    void onSuccessDetailAnamnesis(GetOrderConfirmAnamnesis.DATABean dataBean);
}
