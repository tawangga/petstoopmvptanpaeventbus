package com.tawangga.petstoopmvp2.Dialog.DialogSeeAllTreatment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.Helper.GlobalVariable;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.ModelNew.GetBoardingTreatmentRecord;
import com.tawangga.petstoopmvp2.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BoardingRecordsAdapter extends RecyclerView.Adapter<BoardingRecordsAdapter.ViewHolder> {
    private final Context context;

    private List<GetBoardingTreatmentRecord.DATABean.ItemsBean> data;

    public BoardingRecordsAdapter(Context context) {

        this.context = context;
        data = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_boarding_record, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GetBoardingTreatmentRecord.DATABean.ItemsBean selectedData = data.get(position);

        if (position % 2 == 1) {
            holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.brown_light_2));
        } else {
            holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.white));
        }
        holder.txtOrderCode.setText(selectedData.getOrderCode());
        holder.txtOrderDate.setText(Utils.changeDateFormat(selectedData.getOrderDate(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME_FULL));

        if (selectedData.getBoardingSchedule() != null) {
            holder.txtCage.setText(selectedData.getBoardingSchedule().getCageName());
            holder.txtFrom.setText(Utils.changeDateFormat(selectedData.getBoardingSchedule().getScheduleDateFrom(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME_FULL));
            holder.txtUntil.setText(Utils.changeDateFormat(selectedData.getBoardingSchedule().getScheduleDateUntil(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME_FULL));
        } else {
            holder.txtCage.setText("");
            holder.txtFrom.setText("");
            holder.txtUntil.setText("");

        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public List<GetBoardingTreatmentRecord.DATABean.ItemsBean> getData() {
        return data;
    }

    public void setData(List<GetBoardingTreatmentRecord.DATABean.ItemsBean> data) {
        this.data.addAll(data);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_order_code)
        TextView txtOrderCode;
        @BindView(R.id.txt_order_date)
        TextView txtOrderDate;
        @BindView(R.id.txt_doctor_name)
        TextView txtDoctorName;
        @BindView(R.id.txt_cage)
        TextView txtCage;
        @BindView(R.id.txt_from)
        TextView txtFrom;
        @BindView(R.id.txt_until)
        TextView txtUntil;
        @BindView(R.id.ll_background)
        LinearLayout llBackground;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
