package com.tawangga.petstoopmvp2.Dialog.DialogInvoiceDP;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Model.DataInvoice;
import com.tawangga.petstoopmvp2.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OpenSelectedInvoiceAdapter extends RecyclerView.Adapter<OpenSelectedInvoiceAdapter.ViewHolder> {

    private List<DataInvoice> data = new ArrayList<>();
    private Context context;
    private AdapterView.OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public OpenSelectedInvoiceAdapter(Context context) {
        this.context = context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_open_selected_invoice, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.position = position;
        DataInvoice getData = data.get(position);

        //Double remainingPayment = Double.parseDouble(Utils.toRupiah(getData.getTotalTransaction())) - Double.parseDouble(Utils.toRupiah(getData.getTransactionDp()));
        float total = Float.parseFloat(getData.getTotalTransaction());
        float downpayment = Float.parseFloat(getData.getTransactionDp());
        float remainingPayment = total - downpayment;


        holder.tvInvoiceNo.setText(getData.getTransactionInvoiceCode());
        holder.tvOwnerPetName.setText(String.format("%s", getData.getCustomerName()));
        holder.tv_total.setText(Utils.toRupiah(String.valueOf(total))); // Total atau Grand Total
        holder.tv_downpayment.setText(Utils.toRupiah(String.valueOf(downpayment))); // Down Payment
        holder.tv_remainingpayment.setText(Utils.toRupiah(String.valueOf(remainingPayment))); // Remaining Payment
        holder.tvTreatment.setText(String.format("%s Treatment", getData.getCount_treatment()));
        holder.tvProduct.setText(String.format("%s Product", getData.getCount_product()));

        holder.txtStatus2.measure(0, 0);
        holder.txtStatus.getLayoutParams().width = holder.txtStatus2.getMeasuredWidth();
        holder.txtStatus.requestLayout();


    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setDatas(List<DataInvoice> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        int position;
        @BindView(R.id.tv_invoice_no)
        TextView tvInvoiceNo;
        @BindView(R.id.tv_owner_pet_name)
        TextView tvOwnerPetName;
        @BindView(R.id.tv_treatment)
        TextView tvTreatment;
        @BindView(R.id.tv_product)
        TextView tvProduct;
        @BindView(R.id.txt_status)
        TextView txtStatus;
        @BindView(R.id.txt_status_2)
        TextView txtStatus2;
        @BindView(R.id.tv_total)
        TextView tv_total;
        @BindView(R.id.tv_downpayment)
        TextView tv_downpayment;
        @BindView(R.id.tv_remainingpayment)
        TextView tv_remainingpayment;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            txtStatus.setVisibility(View.GONE); // Menghilangkan tombol Down Payment di open invoice list.
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onItemClickListener.onItemClick(null, view, position, 0);
        }
    }
}
