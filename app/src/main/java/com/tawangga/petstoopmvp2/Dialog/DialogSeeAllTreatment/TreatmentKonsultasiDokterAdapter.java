package com.tawangga.petstoopmvp2.Dialog.DialogSeeAllTreatment;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Model.GetOrderConfirmAnamnesis;
import com.tawangga.petstoopmvp2.R;
import com.tawangga.petstoopmvp2.ServerSide.Connector;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class TreatmentKonsultasiDokterAdapter extends RecyclerView.Adapter<TreatmentKonsultasiDokterAdapter.ViewHolder> {

    private List<GetOrderConfirmAnamnesis.DATABean.TreatmentsBean> data;
    private Context context;
    private boolean isMedicalRecord;
    private AdapterView.OnItemClickListener onItemClickListener;
    GetOrderConfirmAnamnesis.DATABean dataAnamnesis;

    public TreatmentKonsultasiDokterAdapter(Context context) {

        this.context = context;
        this.isMedicalRecord = true;
        data = new ArrayList<>();
    }

    public TreatmentKonsultasiDokterAdapter(Context context, GetOrderConfirmAnamnesis.DATABean dataAnamnesis) {

        this.context = context;
        this.isMedicalRecord = false;
        data = new ArrayList<>();
        this.dataAnamnesis = dataAnamnesis;
    }


    @NonNull
    @Override
    public TreatmentKonsultasiDokterAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_treatment_konsultasi_dokter, parent, false);

        return new TreatmentKonsultasiDokterAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TreatmentKonsultasiDokterAdapter.ViewHolder holder, int position) {
        holder.position = position;
        final long identity = System.currentTimeMillis();
        holder.identity = identity;
        holder.txt_item_name.setText(data.get(position).getServiceName());
        if (isMedicalRecord || data.get(position).getServiceId() == 1) {
            holder.btn_cancel.setVisibility(View.GONE);
        }
        holder.btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                deleteService(data.get(position));

            }
        });

    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setDatas(List<GetOrderConfirmAnamnesis.DATABean.TreatmentsBean> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.ll_background)
        LinearLayout llBackground;
        @BindView(R.id.txt_item_name)
        TextView txt_item_name;
        @BindView(R.id.btn_cancel)
        ImageView btn_cancel;
        int position;
        long identity;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(null, v, position, 0);
            }

        }
    }


    private void deleteService(GetOrderConfirmAnamnesis.DATABean.TreatmentsBean datas) {
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("cart_service_id", datas.getCartServiceId() + "");
        formBuilder.add("service_id", datas.getServiceId() + "");
        formBuilder.add("service_type", datas.getServiceType() + "");
        RequestBody formBody = formBuilder.build();
        ProgressDialog dialog = Utils.showProgressDialog(context, false, "Loading...");

        Connector.newInstance(context).deleteServiceTreatment(formBody, new Connector.ApiCallback<String>() {
            @Override
            public String getKey() {
                return null;
            }

            @Override
            public void success(String dataBean, String messages) {
                data.remove(datas);
                dataAnamnesis.getTreatments().remove(datas);
                notifyDataSetChanged();
                dialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                t.printStackTrace();

            }
        });
    }

}