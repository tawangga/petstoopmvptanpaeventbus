package com.tawangga.petstoopmvp2.Dialog.DialogSeeAllTreatment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.Helper.GlobalVariable;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.ModelNew.GetMedicalTreatmentRecord;
import com.tawangga.petstoopmvp2.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MedicalRecordsAdapter extends RecyclerView.Adapter<MedicalRecordsAdapter.ViewHolder> {

    private List<GetMedicalTreatmentRecord.DATABean.ItemsBean> data;
    private Context context;
    private AdapterView.OnItemClickListener onItemClickListener;

    public MedicalRecordsAdapter(Context context) {

        this.context = context;
        data = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_medical_records, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.position = position;
        final long identity = System.currentTimeMillis();
        holder.identity = identity;

        if (position % 2 == 1) {
            holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.brown_light_2));
        } else {
            holder.llBackground.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

        GetMedicalTreatmentRecord.DATABean.ItemsBean selectedItem = data.get(position);
        holder.txtOrderCode.setText(selectedItem.getOrderCode());
        holder.txtOrderDate.setText(Utils.changeDateFormat(selectedItem.getOrderDate(), GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.DATE_FORMAT_MONTH_NAME_FULL));
        holder.txtDoctorName.setText(selectedItem.getDoctorName());
        holder.txtAnamnesis.setText(selectedItem.getAnamnesis());
        holder.txtTreatment.setText(selectedItem.getTreatmentText());
        holder.txtClinicalCheckup.setText(selectedItem.getClinicalCheckupText());
        holder.txtDiagnosis.setText(selectedItem.getDiagnosis());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setDatas(List<GetMedicalTreatmentRecord.DATABean.ItemsBean> data) {
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public List<GetMedicalTreatmentRecord.DATABean.ItemsBean> getDatas() {
        return data;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.txt_order_code)
        TextView txtOrderCode;
        @BindView(R.id.txt_order_date)
        TextView txtOrderDate;
        @BindView(R.id.txt_doctor_name)
        TextView txtDoctorName;
        @BindView(R.id.txt_anamnesis)
        TextView txtAnamnesis;
        @BindView(R.id.txt_clinical_checkup)
        TextView txtClinicalCheckup;
        @BindView(R.id.txt_diagnosis)
        TextView txtDiagnosis;
        @BindView(R.id.txt_treatment)
        TextView txtTreatment;
        @BindView(R.id.ll_background)
        LinearLayout llBackground;
        int position;
        long identity;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(null, v, position, 0);
            }

        }
    }
}