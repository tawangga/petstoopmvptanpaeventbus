package com.tawangga.petstoopmvp2.Dialog.DialogAddCCU;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tawangga.petstoopmvp2.EventBus.OnFailGettingData;
import com.tawangga.petstoopmvp2.Helper.GlobalVariable;
import com.tawangga.petstoopmvp2.Helper.Utils;
import com.tawangga.petstoopmvp2.Model.GetExamModel;
import com.tawangga.petstoopmvp2.Model.GetOrderConfirmAnamnesis;
import com.tawangga.petstoopmvp2.Modul.Anamnesis.AdapterClinicalCheckup;
import com.tawangga.petstoopmvp2.R;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogAddNewCCU extends DialogFragment implements DialogNewCCUView {
    private static final String TAG = DialogAddNewCCU.class.getSimpleName();
    @BindView(R.id.txt_treatment_text)
    TextView txtTreatmentText;
    @BindView(R.id.txt_treatment_selected)
    TextView txtTreatmentSelected;
    @BindView(R.id.tv_search_treatment)
    EditText tvSearchTreatment;
    @BindView(R.id.btn_search)
    ImageView btnSearch;
    @BindView(R.id.btn_done)
    TextView btnDone;
    @BindView(R.id.txt_treatment_found)
    TextView txtTreatmentFound;
    @BindView(R.id.rv_selection_treatment)
    RecyclerView rvSelectionTreatment;
    @BindView(R.id.pb_loading)
    AVLoadingIndicatorView pbLoading;
    private GetOrderConfirmAnamnesis.DATABean dataAnamnesis;
    private AdapterClinicalCheckup adapterClinicalCheckup;
    private ExamAdapter examAdapter;
    private int pageService = 0, lastPageService = 1;
    private DialogAddNewCCUPresenter presenter;

    public static DialogAddNewCCU newInstance(GetOrderConfirmAnamnesis.DATABean dataAnamnesis, AdapterClinicalCheckup adapterClinicalCheckup) {
        DialogAddNewCCU fragment = new DialogAddNewCCU();
        fragment.dataAnamnesis = dataAnamnesis;
        fragment.adapterClinicalCheckup = adapterClinicalCheckup;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = View.inflate(getActivity(), R.layout.layout_treatment_selection, null);
        ButterKnife.bind(this, v);
        presenter = new DialogAddNewCCUPresenter(getActivity(), TAG, this);
        initView();
        return v;
    }

    private void initView() {
        txtTreatmentSelected.setVisibility(View.GONE);
        txtTreatmentText.setText("EXAM SELECTION");
        tvSearchTreatment.setHint("Search Exam");
        txtTreatmentFound.setText("Exam List");

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvSelectionTreatment.setLayoutManager(layoutManager);
        examAdapter = new ExamAdapter(getActivity(), txtTreatmentFound, dataAnamnesis);
        rvSelectionTreatment.setAdapter(examAdapter);

        rvSelectionTreatment.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (pageService != lastPageService) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= examAdapter.getItemCount()) {
                        getExam();
                    }
                }
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        tvSearchTreatment.measure(0, 0);

        btnSearch.getLayoutParams().height = tvSearchTreatment.getMeasuredHeight();
        btnSearch.requestLayout();
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updatePagination(0, 1);
                examAdapter.clearAllData();
                getExam();

                Utils.hideSoftKeyboardUsingView(getActivity(), tvSearchTreatment);
            }
        });

        tvSearchTreatment.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                updatePagination(0, 1);
                examAdapter.clearAllData();
                getExam();
                Utils.hideSoftKeyboardUsingView(getActivity(), tvSearchTreatment);
                return false;
            }
        });

        getExam();
    }


    private void updatePagination(int page, int lastPage) {
        this.pageService = page;
        this.lastPageService = lastPage;
    }


    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        adapterClinicalCheckup.notifyDataSetChanged();

    }

    private void getExam() {
        if (pageService == lastPageService) {
            pbLoading.setVisibility(View.GONE);
            return;
        }

        if (examAdapter.getData().size() == 0) {
            setLoadingCenter();
        } else {
            setLoadingBottom();
        }
        pbLoading.setVisibility(View.VISIBLE);
        Map<String, String> params = new HashMap<>();
        params.put("limit", String.valueOf(GlobalVariable.LIMIT_ITEM));
        params.put("page", String.valueOf(pageService + 1));
        params.put("search", tvSearchTreatment.getText().toString().trim());

        presenter.getExam(params);
    }


    private void setLoadingCenter() {
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.CENTER_IN_PARENT);
        pbLoading.setLayoutParams(lp);

    }

    @Override
    public void onResume() {
        super.onResume();
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
    }

    private void setLoadingBottom() {
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
        pbLoading.setLayoutParams(lp);

    }

    @Override
    public void successGetExam(GetExamModel.DATABean dataBean) {
        pbLoading.setVisibility(View.GONE);
        updatePagination(dataBean.getPage(), dataBean.getLastPage());


        for (GetExamModel.DATABean.ItemsBean service : dataBean.getItems()) {
            for (GetOrderConfirmAnamnesis.DATABean.ClinicalCheckupsBean selectedService : dataAnamnesis.getClinicalCheckups()) {
                if (selectedService.getClinicalCheckupId() == service.getExamId() && selectedService.isExam()) {
                    service.setAdded(true);
                }
            }
        }


        examAdapter.setData(dataBean.getItems());

    }

    @Override
    public void onFailGettingData(OnFailGettingData data) {
        if (data.getApi() == OnFailGettingData.GET_EXAM) {
            pbLoading.setVisibility(View.GONE);
            Toast.makeText(getActivity(), data.getThrowable().getMessage(), Toast.LENGTH_SHORT).show();
            data.getThrowable().printStackTrace();

        }
    }
}
