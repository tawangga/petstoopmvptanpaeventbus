package com.tawangga.petstoopmvp2.ModelNew;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetMedicalTreatmentRecord {

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {

        @SerializedName("page")
        private int page;
        @SerializedName("last_page")
        private int lastPage;
        @SerializedName("total_item")
        private int totalItem;
        @SerializedName("limit")
        private int limit;
        @SerializedName("items")
        private List<ItemsBean> items;

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public int getTotalItem() {
            return totalItem;
        }

        public void setTotalItem(int totalItem) {
            this.totalItem = totalItem;
        }

        public int getLimit() {
            return limit;
        }

        public void setLimit(int limit) {
            this.limit = limit;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public static class ItemsBean {

            @SerializedName("pet_id")
            private int petId;
            @SerializedName("cart_service_id")
            private int cartServiceId;
            @SerializedName("doctor_schedule_id")
            private int doctorScheduleId;
            @SerializedName("order_code")
            private String orderCode;
            @SerializedName("order_date")
            private String orderDate;
            @SerializedName("service_type")
            private String serviceType;
            @SerializedName("service_name")
            private String serviceName;
            @SerializedName("service_id")
            private int serviceId;
            @SerializedName("anamnesis")
            private String anamnesis;
            @SerializedName("diagnosis")
            private String diagnosis;
            @SerializedName("clinical_checkup_text")
            private String clinicalCheckupText;
            @SerializedName("treatment_text")
            private String treatmentText;
            @SerializedName("doctor_name")
            private String doctorName;

            public int getPetId() {
                return petId;
            }

            public void setPetId(int petId) {
                this.petId = petId;
            }

            public int getCartServiceId() {
                return cartServiceId;
            }

            public void setCartServiceId(int cartServiceId) {
                this.cartServiceId = cartServiceId;
            }

            public int getDoctorScheduleId() {
                return doctorScheduleId;
            }

            public void setDoctorScheduleId(int doctorScheduleId) {
                this.doctorScheduleId = doctorScheduleId;
            }

            public String getOrderCode() {
                return orderCode;
            }

            public void setOrderCode(String orderCode) {
                this.orderCode = orderCode;
            }

            public String getOrderDate() {
                return orderDate;
            }

            public void setOrderDate(String orderDate) {
                this.orderDate = orderDate;
            }

            public String getServiceType() {
                return serviceType;
            }

            public void setServiceType(String serviceType) {
                this.serviceType = serviceType;
            }

            public String getServiceName() {
                return serviceName;
            }

            public void setServiceName(String serviceName) {
                this.serviceName = serviceName;
            }

            public int getServiceId() {
                return serviceId;
            }

            public void setServiceId(int serviceId) {
                this.serviceId = serviceId;
            }

            public String getAnamnesis() {
                return anamnesis;
            }

            public void setAnamnesis(String anamnesis) {
                this.anamnesis = anamnesis;
            }

            public String getDiagnosis() {
                return diagnosis;
            }

            public void setDiagnosis(String diagnosis) {
                this.diagnosis = diagnosis;
            }

            public String getClinicalCheckupText() {
                return clinicalCheckupText;
            }

            public void setClinicalCheckupText(String clinicalCheckupText) {
                this.clinicalCheckupText = clinicalCheckupText;
            }

            public String getTreatmentText() {
                return treatmentText;
            }

            public void setTreatmentText(String treatmentText) {
                this.treatmentText = treatmentText;
            }

            public String getDoctorName() {
                return doctorName;
            }

            public void setDoctorName(String doctorName) {
                this.doctorName = doctorName;
            }
        }
    }
}
