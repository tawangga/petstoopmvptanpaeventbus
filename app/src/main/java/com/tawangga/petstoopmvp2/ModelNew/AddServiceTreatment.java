package com.tawangga.petstoopmvp2.ModelNew;

import com.google.gson.annotations.SerializedName;

public class AddServiceTreatment {

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private CreateCart.DATABean.ServiceBean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public CreateCart.DATABean.ServiceBean getDATA() {
        return DATA;
    }

    public void setDATA(CreateCart.DATABean.ServiceBean DATA) {
        this.DATA = DATA;
    }


}
