package com.tawangga.petstoopmvp2.Helper;

import android.view.View;

public class ViewUtils {

    public static void changeVisibility(View v) {
        if (v != null)
            v.setVisibility(v.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
    }
}
