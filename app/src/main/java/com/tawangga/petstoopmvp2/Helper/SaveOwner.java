package com.tawangga.petstoopmvp2.Helper;

import com.tawangga.petstoopmvp2.Model.DetailCustomerModel;

public class SaveOwner {
    private DetailCustomerModel.DATABean dataBean;
    private boolean isEdit;

    public SaveOwner(DetailCustomerModel.DATABean dataBean, boolean isEdit) {

        this.dataBean = dataBean;
        this.isEdit = isEdit;
    }

    public DetailCustomerModel.DATABean getData() {
        return dataBean;
    }

    public void setData(DetailCustomerModel.DATABean dataBean) {
        this.dataBean = dataBean;
    }

    public boolean isEdit() {
        return isEdit;
    }

    public void setEdit(boolean edit) {
        isEdit = edit;
    }
}
