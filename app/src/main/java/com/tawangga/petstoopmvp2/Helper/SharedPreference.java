package com.tawangga.petstoopmvp2.Helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.tawangga.petstoopmvp2.Model.FindSite;
import com.tawangga.petstoopmvp2.ModelNew.GetClients;
import com.tawangga.petstoopmvp2.ModelNew.GetSite;


public class SharedPreference {
    private static final String STAFF_CATEGORY = "STAFF_CATEGORY";
    private static final String STAFF_CODE = "STAFF_CODE";
    private static final String PHOTO_OLD = "PHOTO_OLD";
    private static final String SAVED_CLIENT_SITE = "SAVED_CLIENT_SITE";
    private final SharedPreferences appPreference;
    private static final String STAFF_USERNAME = "STAFF_USERNAME";
    private static final String STAFF_ID = "STAFF_ID";
    private static final String STAFF_NAME = "STAFF_NAME";
    private static final String STAFF_EMAIL = "STAFF_EMAIL";
    private static final String STAFF_PHOTO = "STAFF_PHOTO";
    private static final String STAFF_TOKEN = "STAFF_TOKEN";
    private static final String STAFF_PHONE = "STAFF_PHONE";
    private static final String STAFF_PASSWORD = "STAFF_PASSWORD";
    private static final String SITE = "SITE";
    private static final String SAVED_SITE = "SAVED_SITE";
    private static final String SAVED_CLIENT = "SAVED_CLIENT";

    private static final String CUSTOMER_ID = "CUSTOMER_ID";
    private static final String CUSTOMER_NAME = "CUSTOMER_NAME";
    private static final String CUSTOMER_EMAIL = "CUSTOMER_EMAIL";
    private static final String CUSTOMER_PHOTO = "CUSTOMER_PHOTO";
    private static final String CUSTOMER_PHONE = "CUSTOMER_PHONE";
    private static final String CUSTOMER_ADDRESS = "CUSTOMER_ADDRESS";
    private static final String CUSTOMER_CODE = "CUSTOMER_CODE";

    public SharedPreference(Context context) {
        appPreference = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setSession(String key, String value) {
        SharedPreferences.Editor editor = appPreference.edit();
        editor.putString(key, value);
        editor.apply();

    }

    public void clearSession() {
        SharedPreferences.Editor editor = appPreference.edit();
//        editor.clear();
        editor.remove(STAFF_CATEGORY);
        editor.remove(STAFF_CODE);
        editor.remove(PHOTO_OLD);
        editor.remove(STAFF_USERNAME);
        editor.remove(STAFF_ID);
        editor.remove(STAFF_NAME);
        editor.remove(STAFF_EMAIL);
        editor.remove(STAFF_PHOTO);
        editor.remove(STAFF_TOKEN);
        editor.remove(STAFF_PHOTO);
        editor.remove(STAFF_PASSWORD);
        editor.remove(SITE);
        editor.remove("IS_LOGIN");
        editor.remove("IS_CUSTOMER");
        editor.apply();
//        editor.commit();
    }

    public String getSession(String key) {
        String value = appPreference.getString(key, "");
        if (TextUtils.isEmpty(value)) {
            return "";
        }
        return value;
    }

    public void setIsLogin(boolean stat) {
        SharedPreferences.Editor editor = appPreference.edit();
        editor.putBoolean("IS_LOGIN", stat);
        editor.apply();
    }

    public boolean getIsLogin() {
        boolean isLogin = appPreference.getBoolean("IS_LOGIN", false);
        return isLogin;
    }

    public void setCustomerId(String customerId) {
        setSession(CUSTOMER_ID, customerId);
    }

    public String getCustomerId() {
        return getSession(CUSTOMER_ID);
    }
    public void setCustomerName(String customerName) {
        setSession(CUSTOMER_NAME, customerName);
    }

    public String getCustomerName() {
        return getSession(CUSTOMER_NAME);
    }
    public void setCustomerEmail(String customerEmail) {
        setSession(CUSTOMER_EMAIL, customerEmail);
    }

    public String getCustomerEmail() {
        return getSession(CUSTOMER_EMAIL);
    }
    public void setCustomerPhoto(String customerPhoto) {
        setSession(CUSTOMER_PHOTO, customerPhoto);
    }

    public String getCustomerPhoto() {
        return getSession(CUSTOMER_PHOTO);
    }
    public void setCustomerPhone(String customerPhone) {
        setSession(CUSTOMER_PHONE, customerPhone);
    }

    public String getCustomerPhone() {
        return getSession(CUSTOMER_PHONE);
    }
    public void setCustomerAddres(String customerAddrss) {
        setSession(CUSTOMER_ADDRESS, customerAddrss);
    }

    public String getCustomerAddress() {
        return getSession(CUSTOMER_ADDRESS);
    }
    public void setCustomerCode(String customerCode) {
        setSession(CUSTOMER_CODE, customerCode);
    }

    public String getCustomerCode() {
        return getSession(CUSTOMER_CODE);
    }

    public void setStaffUsername(String staffUsername) {
        setSession(STAFF_USERNAME, staffUsername);
    }

    public String getStaffUsername() {
        return getSession(STAFF_USERNAME);
    }

    public void setStaffId(String staffId) {
        setSession(STAFF_ID, staffId);
    }

    public String getStaffId() {
        return getSession(STAFF_ID);
    }

    public void setStaffName(String staffName) {
        setSession(STAFF_NAME, staffName);
    }

    public String getStaffName() {
        return getSession(STAFF_NAME);
    }

    public void setStaffEmail(String staffEmail) {
        setSession(STAFF_EMAIL, staffEmail);
    }

    public String getStaffEmail() {
        return getSession(STAFF_EMAIL);
    }

    public void setStaffPhoto(String staffPhoto) {
        setSession(STAFF_PHOTO, staffPhoto);
    }

    public String getStaffPhoto() {
        return getSession(STAFF_PHOTO);
    }

    public void setStaffToken(String staffToken) {
        setSession(STAFF_TOKEN, staffToken);
    }

    public String getStaffToken() {
        return getSession(STAFF_TOKEN);
    }

    public void setStaffPhone(String staffPhone) {
        setSession(STAFF_PHONE, staffPhone);
    }

    public String getStaffPhone() {
        return getSession(STAFF_PHONE);
    }

    public void setStaffPassword(String staffPassword) {
        setSession(STAFF_PASSWORD, staffPassword);
    }

    public String getStaffPassword() {
        return getSession(STAFF_PASSWORD);
    }

    public void setStaffCategory(String staff_category) {
        setSession(STAFF_CATEGORY, staff_category);
    }

    public boolean isDoctor() {
        return getSession(STAFF_CATEGORY).equals("Doctor");
    }

    public boolean isCustomer(){
        boolean isLogin = appPreference.getBoolean("IS_CUSTOMER", false);
        return isLogin;
    }


    public void setIsCustomer(boolean stat) {
        SharedPreferences.Editor editor = appPreference.edit();
        editor.putBoolean("IS_CUSTOMER", stat);
        editor.apply();
    }

    public void setStaffCode(String staffCode) {
        setSession(STAFF_CODE, staffCode);

    }

    public String getStaffCode() {
        return getSession(STAFF_CODE);
    }

    public void setPhotoOld(String photoOld) {
        setSession(PHOTO_OLD, photoOld);

    }


    public String getPhotoOld() {
        return getSession(PHOTO_OLD);
    }

    public void setSite(String site) {
        setSession(SITE, site);
    }

    public String getSite() {
        return getSession(SITE);
    }

    public void setSavedSite(String siteId) {
        setSession(SAVED_SITE, siteId);
    }

    public GetSite.DATABean.ItemsBean getSavedSite() {
        String json = getSession(SAVED_SITE);
        if (json.equals("")) {
            return null;
        }
        return new Gson().fromJson(json, GetSite.DATABean.ItemsBean.class);
    }


    public void setSavedClient(String siteId) {
        setSession(SAVED_CLIENT, siteId);
    }

    public void setSavedClientSite(String siteId) {
        setSession(SAVED_CLIENT_SITE, siteId);
    }

    public FindSite.DATABean getSavedClientSite() {
        String json = getSession(SAVED_CLIENT_SITE);
        if (json.equals("")) {
            return null;
        }
        return new Gson().fromJson(json, FindSite.DATABean.class);

    }

    public GetClients.DATABean.ItemsBean getSavedClient() {
        String json = getSession(SAVED_CLIENT);
        if (json.equals("")) {
            return null;
        }
        return new Gson().fromJson(json, GetClients.DATABean.ItemsBean.class);
    }

}
