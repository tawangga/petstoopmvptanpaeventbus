package com.tawangga.petstoopmvp2.EventBus;

import com.tawangga.petstoopmvp2.Model.CartModel;

public class OnCreateCart {
    private CartModel.DATABean data;
    private String TAG;

    public OnCreateCart(CartModel.DATABean data, String tag) {
        this.data = data;
        TAG = tag;
    }

    public String getTAG() {
        return TAG;
    }

    public void setTAG(String TAG) {
        this.TAG = TAG;
    }

    public CartModel.DATABean getData() {
        return data;
    }

    public void setData(CartModel.DATABean data) {
        this.data = data;
    }
}
