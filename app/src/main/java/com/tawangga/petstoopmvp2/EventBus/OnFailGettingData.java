package com.tawangga.petstoopmvp2.EventBus;

public class OnFailGettingData {
    public static int BOARDING_RECORD = 1;
    public static int GROOMING_RECORD = 2;
    public static int MEDICAL_RECORD = 3;
    public static int DETAIL_ANAMNESIS = 4;
    public static int GET_APP_VERSION = 5;
    public static int GET_TREATMENT_RECORD = 6;
    public static int GET_CUSTOMER_BY_PET = 7;
    public static int GET_PET_LIST = 8;
    public static int GET_CUSTOMER = 9;
    public static int GET_DETAIL_CUSTOMER = 10;
    public static int SAVE_OWNER = 11;
    public static int ROLLBACK_CART = 12;
    public static int GET_PRODUCT = 13;
    public static int CREATE_CART = 14;
    public static int CREATE_CART_MEDICAL = 15;
    public static int GET_PRODUCT_CATEGORY = 16;
    public static int CLEAN_CART = 17;
    public static int GET_PROMO = 18;
    public static int GET_ALL_DOCTOR = 19;
    public static int GET_RESERVATION = 20;
    public static int GET_ORDER = 21;
    public static int EDIT_PRODUCT = 22;
    public static int DELETE_PRODUCT = 23;
    public static int ADD_PRODUCT = 24;
    public static int GET_INVOICE = 25;
    public static int CANCEL_INVOICE = 26;
    public static int ADD_ORDER_TO_INVOICE = 27;
    public static int PROCESS_INVOICE = 28;
    public static int INVOICE_DP = 29;
    public static int CREATE_INVOICE = 30;
    public static int PROCESS_INVOICE2 = 31;
    public static int GET_DOCTOR = 32;
    public static int GET_GROOMING_LOCATION = 33;
    public static int GET_BOARDING = 34;
    public static int CREATE_CART_TREATMENT = 35;
    public static int TREATMENT_CANCEL = 36;
    public static int SEARCH_SERVICE = 37;
    public static int RESERV_TREATMENT = 38;
    public static int GET_CONFIRM_ORDER_ANAMNESIS = 39;
    public static int DOCTOR_SCHEDULE = 40;
    public static int GET_EXAM = 41;
    private int api;
    private Throwable throwable;

    public OnFailGettingData(int api, Throwable t) {
        this.api = api;
        this.throwable = t;
    }


    public int getApi() {
        return api;
    }

    public Throwable getThrowable() {
        return throwable;
    }

}
