package com.tawangga.petstoopmvp2.EventBus;

public class NetworkStateCheck {
    private boolean isConnected;

    public NetworkStateCheck(boolean isConnected) {

        this.isConnected = isConnected;
    }

    public boolean isConnected() {
        return isConnected;
    }
}
