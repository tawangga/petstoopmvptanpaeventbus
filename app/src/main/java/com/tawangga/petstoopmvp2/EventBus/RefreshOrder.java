package com.tawangga.petstoopmvp2.EventBus;

import com.tawangga.petstoopmvp2.Model.GetOrders;

import java.util.List;

public class RefreshOrder {
    private List<GetOrders.DATABean> data;

    public RefreshOrder(List<GetOrders.DATABean> data) {
        this.data = data;
    }

    public List<GetOrders.DATABean> getData() {
        return data;
    }
}
