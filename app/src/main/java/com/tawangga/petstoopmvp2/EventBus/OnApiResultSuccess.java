package com.tawangga.petstoopmvp2.EventBus;

public class OnApiResultSuccess {
    public static int API_EDIT_CART = 1;
    public static int API_EDIT_MEDICAL_CART = 2;
    public static int API_DELETE_CART = 666;
    public static int API_DELETE_MEDICAL_CART = 4;
    public static int API_ADD_CART = 5;
    public static int API_ADD_MEDICAL_CART = 6;
    public static int API_CANCEL_INVOICE = 7;
    public static int PROCESS_INVOICE = 8;
    public static int SAVE_ANAMNESIS = 9;
    private int api;

    public OnApiResultSuccess(int api) {
        this.api = api;
    }

    public int getApi() {
        return api;
    }

}
