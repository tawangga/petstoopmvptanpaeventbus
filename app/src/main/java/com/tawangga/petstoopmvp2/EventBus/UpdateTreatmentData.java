package com.tawangga.petstoopmvp2.EventBus;

import com.tawangga.petstoopmvp2.ModelNew.GetTreatment;

public class UpdateTreatmentData {
    private GetTreatment.DATABean data;
    private String TAG;

    public UpdateTreatmentData(GetTreatment.DATABean data, String tag) {

        this.data = data;
        this.TAG = tag;
    }

    public String getTAG() {
        return TAG;
    }

    public GetTreatment.DATABean getData() {
        return data;
    }
}
