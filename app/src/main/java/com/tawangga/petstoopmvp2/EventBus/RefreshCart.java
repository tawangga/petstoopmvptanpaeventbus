package com.tawangga.petstoopmvp2.EventBus;

public class RefreshCart {
    public static final int CART = 1;
    public static final int MEDICAL_CART = 2;
    private int cart;

    public RefreshCart(int cart) {
        this.cart = cart;
    }

    public int getCart() {
        return cart;
    }

    public void setCart(int cart) {
        this.cart = cart;
    }
}
