package com.tawangga.petstoopmvp2.EventBus;

public class NavigateToShop {

    private String cartServiceId;

    public NavigateToShop(String cartServiceId) {
        this.cartServiceId = cartServiceId;
    }

    public String getCartServiceId() {
        return cartServiceId;
    }
}
