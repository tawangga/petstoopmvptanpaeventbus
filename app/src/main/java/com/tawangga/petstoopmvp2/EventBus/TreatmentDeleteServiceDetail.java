package com.tawangga.petstoopmvp2.EventBus;

public class TreatmentDeleteServiceDetail {
    private String serviceId;
    private String serviceType;

    public TreatmentDeleteServiceDetail(int serviceId, String serviceType) {
        this.serviceId = serviceId + "";
        this.serviceType = serviceType;
    }

    public String getServiceId() {
        return serviceId;
    }

    public String getServiceType() {
        return serviceType;
    }
}
