package com.tawangga.petstoopmvp2.EventBus;

import com.tawangga.petstoopmvp2.Model.TreatmentAddCustomer;

public class OnTreatmentChangeDate {
    private TreatmentAddCustomer.DATABean data;

    public OnTreatmentChangeDate(TreatmentAddCustomer.DATABean data) {
        this.data = data;
    }

    public TreatmentAddCustomer.DATABean getData() {
        return data;
    }
}
