package com.tawangga.petstoopmvp2.RecyclerSwipe;

import android.view.MotionEvent;

public interface OnActivityTouchListener {
    void getTouchCoordinates(MotionEvent ev);
}
